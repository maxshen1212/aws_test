import 'dart:ui';
import 'package:flutter/material.dart';

class AppColors {
  /// white background
  static const Color primaryBackground = Color.fromARGB(255, 255, 255, 255);

  /// grey background
  static const Color primarySecondaryBackground =
      Color.fromARGB(255, 247, 247, 249);

  /// main widget color blue
  static const Color primaryElement = Color.fromARGB(255, 61, 61, 216);

  /// main text color black
  static const Color primaryText = Color.fromARGB(255, 0, 0, 0);
  // video background color
  static const Color primary_bg = Color.fromARGB(210, 32, 47, 62);

  /// main widget text color white
  static const Color primaryElementText = Color.fromARGB(255, 255, 255, 255);
  // main widget text color grey
  static const Color primarySecondaryElementText =
      Color.fromARGB(255, 102, 102, 102);
  // main widget third color grey
  static const Color primaryThirdElementText =
      Color.fromARGB(255, 170, 170, 170);

  static const Color primaryFourthElementText =
      Color.fromARGB(255, 204, 204, 204);
  //state color
  static const Color primaryElementStatus = Color.fromARGB(255, 88, 174, 127);

  static const Color primaryElementBg = Color.fromARGB(255, 238, 121, 99);

  static const Color primaryLogo = Color.fromARGB(255, 227, 14, 128);

  // chart color
  static const Color primary = contentColorCyan;
  static const Color menuBackground = Color(0xFF090912);
  static const Color itemsBackground = Color(0xFF1B2339);
  static const Color pageBackground = Color(0xFF282E45);
  static const Color mainTextColor1 = Colors.white;
  static const Color mainTextColor2 = Colors.white70;
  static const Color mainTextColor3 = Colors.white38;
  static const Color mainGridLineColor = Colors.white10;
  static const Color borderColor = Colors.white54;
  static const Color gridLinesColor = Color(0x11FFFFFF);
  static const Color contentColorBlack = Colors.black;
  static const Color contentColorWhite = Colors.white;
  static const Color contentColorBlue = Color(0xFF2196F3);
  static const Color contentColorYellow = Color(0xFFFFC300);
  static const Color contentColorOrange = Color(0xFFFF683B);
  static const Color contentColorGreen = Color(0xFF3BFF49);
  static const Color contentColorPurple = Color(0xFF6E1BFF);
  static const Color contentColorPink = Color(0xFFFF3AF2);
  static const Color contentColorRed = Color(0xFFE80054);
  static const Color contentColorCyan = Color(0xFF50E4FF);
}
