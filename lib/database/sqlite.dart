import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/widgets.dart';

class Sqlite {
  final sqlFileName = 'furkid.db';
  Database? database;
  //連接資料庫
  Future<Database> connectDB() async {
    if (database != null) {
      print("DB locate: $database");
      return database!;
    } else {
      print(await getDatabasesPath());
      return await initDatabase();
    }
  }

  //初始化資料庫
  Future<Database> initDatabase() async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'furkid.db'),
      onCreate: (db, version) {
        // 創建資料表（測試用）
        db.execute(
          "CREATE TABLE test(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)",
        );
        db.execute(
          '''CREATE TABLE `acupoint` (
              `id` INTEGER  NOT NULL ,
              `meridian_id` INTEGER DEFAULT NULL,
              `name` TEXT DEFAULT NULL,
              `nick_name` TEXT DEFAULT NULL,
              `other_name` TEXT DEFAULT NULL,
              `deepth` TEXT DEFAULT NULL,
              `sn` INTEGER DEFAULT NULL,
              `parts` TEXT DEFAULT NULL,
              `relative_acupoint` TEXT DEFAULT NULL,
              `category` TEXT DEFAULT NULL ,
              `heavenly_stems` TEXT DEFAULT NULL,
              `five_elements` TEXT DEFAULT NULL ,
              `five_shu_points` TEXT DEFAULT NULL,
              `function` TEXT DEFAULT NULL ,
              `sickness` TEXT DEFAULT NULL,
              `pathogenesis` TEXT DEFAULT NULL ,
              `diagnosis` TEXT DEFAULT '',
              `treatment` TEXT DEFAULT NULL,
              `characteristic` TEXT DEFAULT '',
              `position_description` TEXT DEFAULT NULL ,
              `memo` TEXT DEFAULT NULL ,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `sort` INTEGER DEFAULT NULL, `tag` text ,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `acupoint_img`(
              `id` INTEGER  NOT NULL ,
              `acupoint_id` INTEGER DEFAULT NULL,
              `img` TEXT DEFAULT NULL,
              `type` TEXT DEFAULT NULL ,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `banner`(
              `id` INTEGER  NOT NULL ,
              `img` TEXT DEFAULT NULL,
              `url` TEXT DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `calories_recommend`(
              `id` INTEGER  NOT NULL ,
              `type` TEXT DEFAULT NULL,
              `weight` float(5,1) DEFAULT NULL,
              `calories_need` INTEGER DEFAULT NULL,
              `calories_per_kg` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `food`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `category_id` INTEGER DEFAULT NULL,
              `weight` float(10,4) DEFAULT NULL,
              `kcal` float(10,4) DEFAULT NULL,
              `water` float(10,4) DEFAULT NULL,
              `protein` float(10,4) DEFAULT NULL,
              `fat` float(10,4) DEFAULT NULL,
              `carbohydrate` float(10,4) DEFAULT NULL,
              `fiber` float(10,4) DEFAULT NULL,
              `ash` float(10,4) DEFAULT NULL,
              `ca` float(10,4) DEFAULT NULL ,
              `ph` float(10,4) DEFAULT NULL ,
              `kalium` float(10,4) DEFAULT NULL ,
              `na` float(10,4) DEFAULT NULL,
              `fe` float(10,4) DEFAULT NULL,
              `zn` float(10,4) DEFAULT NULL,
              `cu` float(10,4) DEFAULT NULL,
              `mn` float(10,4) DEFAULT NULL,
              `mg` float(10,4) DEFAULT NULL,
              `iodine` float(10,4) DEFAULT NULL ,
              `v_a` float(10,4) DEFAULT NULL,
              `v_b1` float(10,4) DEFAULT NULL,
              `v_b2` float(10,4) DEFAULT NULL,
              `v_b5` float(10,4) DEFAULT NULL,
              `v_b6` float(10,4) DEFAULT NULL,
              `v_b12` float(10,4) DEFAULT NULL,
              `v_c` float(10,4) DEFAULT NULL,
              `v_e` float(10,4) DEFAULT NULL,
              `folic` float(10,4) DEFAULT NULL ,
              `status` tinyINTEGER DEFAULT '1',
              `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `sort` INTEGER DEFAULT NULL,
              `content` TEXT DEFAULT NULL,
              `old id` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `food_category` (
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `sort` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `matching_acupoint`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `sickness` TEXT DEFAULT NULL ,
              `symptoms` TEXT DEFAULT NULL ,
              `diagnosis` TEXT DEFAULT NULL ,
              `content` TEXT DEFAULT NULL ,
              `pathogenesis` TEXT DEFAULT '' ,
              `memo` TEXT DEFAULT NULL, `times` INTEGER DEFAULT NULL ,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `update_time` datetime DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1' ,
              `type` tinyINTEGER DEFAULT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `sort` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `matching_acupoint_collect`(
              `id` INTEGER  NOT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `matching_acupoint_id` INTEGER DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `matching_acupoint_list`(
              `id` INTEGER  NOT NULL ,
              `matching_acupoint_id` INTEGER DEFAULT NULL,
              `acupoint_id` INTEGER DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1' ,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        // db.execute(
        //   '''CREATE TABLE `member`(
        //       `id` INTEGER  NOT NULL ,
        //       `gmid` INTEGER DEFAULT NULL,
        //       `fks mid` INTEGER DEFAULT NULL,
        //       `name` TEXT DEFAULT NULL,
        //       `nick_name` TEXT DEFAULT NULL,
        //       `phone` TEXT DEFAULT NULL,
        //       `email` TEXT DEFAULT NULL,
        //       `gender` TEXT DEFAULT NULL,
        //       `birth` date DEFAULT NULL,
        //       `password` TEXT DEFAULT NULL,
        //       `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        //       `status` tinyINTEGER DEFAULT '1',
        //       `last_logged_time` datetime DEFAULT NULL,
        //       `can_upload` tinyINTEGER DEFAULT '1' ,
        //       `can_view_public_recipe` tinyINTEGER DEFAULT '1' ,
        //       `can_view_public_matching_acupoint` tinyINTEGER NOT NULL DEFAULT '1' ,
        //       `can_use_acupoint_function` tinyINTEGER DEFAULT '0' ,
        //       `logged_in_agent` TEXT DEFAULT NULL ,
        //       `last_logged_ip` TEXT DEFAULT NULL ,
        //       `pet_num_limit` INTEGER DEFAULT NULL,
        //       `last_change_pwd_time` datetime DEFAULT NULL,
        //       `password_type` TEXT DEFAULT 'md5' ,
        //       `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        //       PRIMARY KEY("id" AUTOINCREMENT)
        //     )''',
        // );
        db.execute(
          '''CREATE TABLE `member`(
              `base_id` INTEGER  NOT NULL ,
              `birthday` datetime DEFAULT NULL,
              `country_code` INTEGER DEFAULT NULL,
              `email` TEXT DEFAULT NULL,
              `gender` INTEGER DEFAULT NULL,
              `gmid` INTEGER DEFAULT NULL,
              `member_group` TEXT DEFAULT NULL,
              `mobile` INTEGER DEFAULT NULL,
              `name` TEXT DEFAULT NULL,
              `user_code` TEXT DEFAULT NULL,
              PRIMARY KEY("base_id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `meridian`(
              `id` INTEGER  NOT NULL ,
              `sn` INTEGER DEFAULT NULL ,
              `meridian_sn` INTEGER DEFAULT NULL ,
              `name` TEXT DEFAULT NULL,
              `name_1` TEXT DEFAULT NULL ,
              `name_2` TEXT DEFAULT NULL ,
              `name_3` TEXT DEFAULT NULL ,
              `code` TEXT DEFAULT NULL,
              `description` TEXT DEFAULT NULL ,
              `position_description` TEXT DEFAULT NULL ,
              `five_elements` TEXT DEFAULT '' ,
              `season` TEXT DEFAULT NULL,
              `times` TEXT DEFAULT NULL ,
              `solar_terms` TEXT DEFAULT NULL ,
              `acupoint` TEXT DEFAULT NULL ,
              `back_shu_points` TEXT DEFAULT NULL ,
              `memo` TEXT DEFAULT NULL ,
              `sort` INTEGER DEFAULT NULL,
              `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `tag` text ,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `meridian_img`(
              `id` INTEGER  NOT NULL ,
              `meridian_id` INTEGER DEFAULT NULL,
              `img` TEXT DEFAULT NULL,
              `type` TEXT DEFAULT NULL ,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `message`(
              `id` INTEGER  NOT NULL ,
              `title` TEXT DEFAULT NULL,
              `content` text,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `only_to_member` INTEGER DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              `from_mem_id` INTEGER DEFAULT NULL,
              `gmid` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `message_comment`(
              `id` INTEGER  NOT NULL ,
              `message_id` INTEGER DEFAULT NULL,
              `type` tinyINTEGER DEFAULT '1' ,
              `mem_id` INTEGER DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `content` text,
              `gmid` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `message_read`(
              `id` INTEGER  NOT NULL ,
              `message_id` INTEGER DEFAULT NULL,
              `mem_id` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `options`(
              `id` INTEGER  NOT NULL ,
              `field_name` TEXT DEFAULT NULL,
              `option` TEXT DEFAULT NULL,
             PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `pet`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `gender` TEXT DEFAULT NULL,
              `birth` date DEFAULT NULL,
              `img` TEXT DEFAULT NULL,
              `weight` INTEGER DEFAULT NULL,
              `category` TEXT DEFAULT NULL ,
              `type` TEXT DEFAULT NULL ,
              `color` TEXT DEFAULT NULL,
              `hair` TEXT DEFAULT NULL,
              `neuter` tinyINTEGER DEFAULT NULL,
              `neuter_age` INTEGER DEFAULT NULL,
              `mem_id` INTEGER DEFAULT NULL,
              `gmid` INTEGER DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `note` TEXT DEFAULT NULL,
              `feed_time` tinyINTEGER DEFAULT NULL,
              `update_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `pet_acupoint_record`(
              `id` INTEGER  NOT NULL ,
              `pet_id` INTEGER DEFAULT NULL,
              `matching_acupoint_id` INTEGER DEFAULT NULL,
              `time` INTEGER DEFAULT NULL ,
              `date` date DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              `note` TEXT DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `pet_drink_record`(
              `id` INTEGER  NOT NULL ,
              `pet_id` INTEGER DEFAULT NULL,
              `date` date DEFAULT NULL,
              `time` time DEFAULT NULL,
              `quantity` INTEGER DEFAULT NULL,
              `note` TEXT DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `pet_eat_record`(
              `id` INTEGER  NOT NULL ,
              `pet_id` INTEGER DEFAULT NULL,
              `recipe_id` INTEGER DEFAULT NULL,
              `date` date DEFAULT NULL,
              `quantity` INTEGER DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              `note` TEXT NOT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `pet_note_record`(
              `id` INTEGER  NOT NULL ,
              `pet_id` INTEGER DEFAULT NULL,
              `date` date DEFAULT NULL,
              `note` text,
              `status` tinyINTEGER DEFAULT '1',
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `public_message`(
              `id` INTEGER  NOT NULL ,
              `title` TEXT DEFAULT NULL,
              `content` text,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `recipe`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `category_id` INTEGER DEFAULT NULL,
              `food_weight` INTEGER DEFAULT NULL ,
              `finish_weight` INTEGER DEFAULT NULL ,
              `calories_need` INTEGER DEFAULT NULL ,
              `daily_quantity` INTEGER DEFAULT NULL ,
              `ill_tags` text,
              `type` tinyINTEGER DEFAULT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `update_time` datetime DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1' ,
              `calories` float(10,4) DEFAULT NULL,
              `water` float(10,4) DEFAULT NULL,
              `protein` float(10,4) DEFAULT NULL,
              `fat` float(10,4) DEFAULT NULL,
              `carbohydrate` float(10,4) DEFAULT NULL,
              `fiber` float(10,4) DEFAULT NULL,
              `ash` float(10,4) DEFAULT NULL,
              `other` float(10,4) DEFAULT NULL,
              `ca` float(10,4) DEFAULT NULL,
              `ph` float(10,4) DEFAULT NULL,
              `kalium` float(10,4) DEFAULT NULL,
              `na` float(10,4) DEFAULT NULL,
              `fe` float(10,4) DEFAULT NULL,
              `zn` float(10,4) DEFAULT NULL,
              `cu` float(10,4) DEFAULT NULL,
              `mn` float(10,4) DEFAULT NULL,
              `mg` float(10,4) DEFAULT NULL,
              `iodine` float(10,4) DEFAULT NULL,
              `v_a` float(10,4) DEFAULT NULL,
              `v_b1` float(10,4) DEFAULT NULL,
              `v_b2` float(10,4) DEFAULT NULL,
              `v_b5` float(10,4) DEFAULT NULL,
              `v_b6` float(10,4) DEFAULT NULL,
              `v_b12` float(10,4) DEFAULT NULL,
              `v_c` float(10,4) DEFAULT NULL,
              `v_e` float(10,4) DEFAULT NULL,
              `dry_rate_protein` float(5,1) DEFAULT NULL,
              `dry_rate_fat` float(5,1) DEFAULT NULL,
              `dry_rate_carbo` float(5,1) DEFAULT NULL,
              `dry_rate_fiber` float(5,1) DEFAULT NULL,
              `dry_rate_ash` float(5,1) DEFAULT NULL,
              `dry_rate_other` float(5,1) DEFAULT NULL,
              `calory_rate_protein` float(5,1) DEFAULT NULL,
              `calory_rate_fat` float(5,1) DEFAULT NULL,
              `calory_rate_carbo` float(5,1) DEFAULT NULL,
              `calories_per_100g` float(5,1) DEFAULT NULL,
              `water_per_100g` float(5,1) DEFAULT NULL,
              `protein_per_100g` float(5,1) DEFAULT NULL,
              `fat_per_100g` float(5,1) DEFAULT NULL,
              `carbohydrate_per_100g` float(5,1) DEFAULT NULL,
              `fiber_per_100g` float(5,1) DEFAULT NULL,
              `ash_per_100g` float(5,1) DEFAULT NULL,
              `other_per_100g` float(5,1) DEFAULT NULL,
              `btn_1_num` tinyINTEGER DEFAULT NULL ,
              `btn_2_num` tinyINTEGER DEFAULT NULL,
              `btn_3_num` tinyINTEGER DEFAULT NULL,
              `btn_1_txt` TEXT DEFAULT NULL ,
              `btn_2_txt` TEXT DEFAULT NULL,
              `btn_3_txt` TEXT DEFAULT NULL,
              `btn_1_content` TEXT DEFAULT NULL ,
              `btn_2_content` TEXT DEFAULT NULL,
              `btn_3_content` TEXT DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `recipe_btn_click_record`(
              `id` INTEGER  NOT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `recipe_id` INTEGER DEFAULT NULL,
              `btn_id` tinyINTEGER DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `recipe_category`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `recipe_collect`(
              `id` INTEGER  NOT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `recipe_id` INTEGER DEFAULT NULL,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `recipe_list`(
              `id` INTEGER  NOT NULL ,
              `food_id` INTEGER DEFAULT NULL,
              `quantity` INTEGER DEFAULT NULL,
              `recipe_id` INTEGER DEFAULT NULL,
              `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `setting`(
              `id` INTEGER  NOT NULL ,
              `store_content` text,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `test_fields`(
              `id` INTEGER  NOT NULL ,
              `test_type` INTEGER DEFAULT NULL,
              `name` TEXT DEFAULT NULL,
              `name_tw` TEXT DEFAULT NULL,
              `unit` TEXT DEFAULT NULL,
              `factor` float(10,4) DEFAULT NULL,
              `conversion_unit` TEXT DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `description` TEXT DEFAULT NULL,
              `field_type` tinyINTEGER DEFAULT NULL ,
              `sort` INTEGER DEFAULT NULL,
              `auto_calculate` tinyINTEGER DEFAULT '0',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `test_fields_new`(
              `id` INTEGER  NOT NULL ,
              `test_type` INTEGER DEFAULT NULL,
              `name` TEXT DEFAULT NULL,
              `name_tw` TEXT DEFAULT NULL,
              `unit` TEXT DEFAULT NULL,
              `factor` float(10,4) DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `field_type` tinyINTEGER DEFAULT NULL ,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `test_img`(
              `id` INTEGER  NOT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `pet_id` INTEGER DEFAULT NULL,
              `img` TEXT DEFAULT NULL,
              `date` date DEFAULT NULL,
              `img_type` TEXT DEFAULT NULL,
              `tags` TEXT DEFAULT NULL,
              `note` TEXT DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `test_record`(
              `id` INTEGER  NOT NULL ,
              `mem_id` INTEGER DEFAULT NULL,
              `pet_id` INTEGER DEFAULT NULL,
              `date` date DEFAULT NULL,
              `field_id` INTEGER DEFAULT NULL,
              `test_type` INTEGER DEFAULT NULL ,
              `value` float(10,4) DEFAULT NULL,
              `unit` TEXT DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `status` tinyINTEGER DEFAULT '1',
              `note` TEXT DEFAULT NULL,
              `value_string` TEXT DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        db.execute(
          '''CREATE TABLE `test_type`(
              `id` INTEGER  NOT NULL ,
              `name` TEXT DEFAULT NULL,
              `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `sort` INTEGER DEFAULT NULL,
              PRIMARY KEY("id" AUTOINCREMENT)
            )''',
        );
        // db.execute(
        //   '''CREATE UNIQUE INDEX `matching_acupoint_collect_mem_id` ON `matching_acupoint_collect` (`mem_id`)''',
        // );
        // db.execute(
        //   '''CREATE UNIQUE INDEX `message_read_message_id` ON `message_read` (`message_id`)''',
        // );
        // db.execute(
        //   '''CREATE UNIQUE INDEX `recipe_collect_mem_id` ON `recipe_collect` (`mem_id`)''',
        // );
      },
      version: 1,
      singleInstance: true,
    );
    return database!;
  }

  //查詢資料
  Future<List<Map<String, dynamic>>> query(tableName) async {
    final Database db = await connectDB();
    // 查詢資料庫語法
    List<Map<String, dynamic>> maps = await db.query(tableName);
    return maps;
  }

  //新增資料
  insert(tableName, Map<String, dynamic> data) async {
    final Database db = await connectDB();
    try {
      await db.insert(
        tableName,
        data,
        // 當資料發生衝突，定義將會採用 replace 覆蓋之前的資料
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    } catch (error) {
      return error;
    }
  }

  //修改資料
  update(tableName, Map<String, dynamic> m) async {
    final Database db = await connectDB();
    await db.update(
      tableName,
      m,
      // 確定id是否匹配
      where: "id = ?",
      // 通過 where 傳遞 id 可以防止 SQL 注入
      // 注意 不要使用 where: "id = ${dog.id}"
      whereArgs: [m['id']],
    );
  }

  // 刪除資料
  delete(tableName, int id) async {
    final Database db = await connectDB();
    // 刪除資料庫語法
    await db.delete(
      tableName,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}

Future readJsonToInsertSqlite(String fileName, String tableName) async {
  WidgetsFlutterBinding.ensureInitialized();
  final sqlite = Sqlite();
  String jsonString = await rootBundle.loadString("assets/json/$fileName.json");
  List l = jsonDecode(jsonString);
  l.forEach((e) async => sqlite.insert(tableName, e));
}
