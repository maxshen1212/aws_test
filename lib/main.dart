// packages

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// PAGES imports
// -passbook
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/analyze_and_search.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/1_single_column_query/single_column.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/2_multi_column_query/multi_column.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/3_chart_analyze/chart_analyze.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/3_chart_analyze/show_chart.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/4_drink_query/drink_query.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/5_food_query/food_query.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/6_acupoint_query/acupoint_query.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/7_summary_query/summary_query.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/8_image_query/image_query.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/1_furkid_heslth_app_image_upload/image_upload.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/2_furkid_heslth_app_CBC/CBC.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/3_furkid_heslth_app_biochemistry_test/biochemistry_test.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/4_furkid_heslth_app_urine_test/urine_test.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/5_furkid_heslth_app_else_test/else_test.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/6_furkid_heslth_app_drink/drink.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/7_furkid_heslth_app_food/food.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/8_furkid_heslth_app_acupoint_record/acupoint_record.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/9_furkid_heslth_app_summary/summary.dart';
// -recipe
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/fukid_heslth_suggest/fukid_heslth_suggest.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/furkid_heslth_ingredients/fukkid_heslth_ingredients.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/myrecipe.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/plus_recipe/plus_recipe.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/recipe_search.dart';
// -meridian
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/add_point_matching/add_point_matching.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/meridian_point_query/furkid_heslth_meridian_point_query.dart';
// -personal-all
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_email_verify/personal_email_verify.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_blocs.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_bloc.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_update_password/personal_update_password.dart';
import 'package:pet_save_app/pages/personal-all/personal_home/personal_home.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/personal_info.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/personal_petinfo.dart';
import 'package:pet_save_app/pages/personal-all/personal_member_discount/bonus_points/bonus_points.dart';
import 'package:pet_save_app/pages/personal-all/personal_member_discount/my_wallet/my_wallet.dart';
// -furkid_health
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_home/furkid_heslth_home.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/point_matching_query/point_matching_query.dart';
// -others
import 'package:pet_save_app/pages/home/home.dart';
import 'package:pet_save_app/pages/login/update_password/update_password.dart';
import 'package:pet_save_app/pages/login/petsafe_login/petsafe_login.dart';
import 'package:pet_save_app/pages/online_customer_service/online_customer_service.dart';
import 'package:pet_save_app/pages/register/register.dart';
import 'package:pet_save_app/pages/register/register_mail_and_phone.dart';

// BLOC imports
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_bloc.dart';
import 'package:pet_save_app/pages/register/bloc/register_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/meridian_point_query/bloc/furkid_heslth_merdian_point_query_bloc.dart';
import 'package:pet_save_app/pages/login/forget_password/bloc/forget_password_bloc.dart';
import 'package:pet_save_app/pages/register/userTerms.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/point_matching_query/bloc/point_matching_query_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/my_point_matching/bloc/my_point_matching_bloc.dart';

import 'database/sqlite.dart';

void main() async {
  // 新API測試用程式
  // dynamic responseData;
  // try {
  //   print("start");
  //   Response response = await Dio().post(
  //       "https://test.yous.tw/fmi/data/vLatest/databases/dClient/layouts/dClient_api/_find",
  //       data: {
  //         "query": [
  //           {"name": "測試"}
  //         ]
  //       },
  //       options: Options(headers: {
  //         "key": "Content-Type",
  //         "name": "Content-Type",
  //         "type": "text",
  //         "value": "application/json",
  //         "Authorization":
  //             "Bearer e1f5fbdcc7df067cf3f51e5e9a8659d7c809168a2d3d5113638c"
  //       }));
  //   responseData = jsonDecode(response.toString());
  //   print(responseData);
  //   // print(response.statusCode);
  //   print("end");
  // } catch (e) {
  //   print(e);
  // }

  // await readJsonToInsertSqlite("acupoint_img", "acupoint_img");
  // await readJsonToInsertSqlite("acupoint", "acupoint");
  // await readJsonToInsertSqlite("calories_rcommend", "calories_rcommend");
  // await readJsonToInsertSqlite("food_category", "food_category");
  // await readJsonToInsertSqlite("food", "food");
  // await readJsonToInsertSqlite(
  //     "matching_acupoint_collect", "matching_acupoint_collect");
  // await readJsonToInsertSqlite(
  //     "matching_acupoint_list", "matching_acupoint_list");
  // await readJsonToInsertSqlite("matching_acupoint", "matching_acupoint");

  // await readJsonToInsertSqlite("meridian_img", "meridian_img");
  // await readJsonToInsertSqlite("meridian", "meridian");
  // await readJsonToInsertSqlite("option", "option");
  // await readJsonToInsertSqlite("pet_acupoint_record", "pet_acupoint_record");
  // await readJsonToInsertSqlite("pet_drink_record", "pet_drink_record");
  // await readJsonToInsertSqlite("pet_eat_record", "pet_eat_record");
  
  // await readJsonToInsertSqlite("pet_note_record", "pet_note_record");

  // await readJsonToInsertSqlite(
  //     "recipe_btn_click_record", "recipe_btn_click_record");
  // await readJsonToInsertSqlite("recipe_category", "recipe_category");
  // await readJsonToInsertSqlite("recipe_collect", "recipe_collect");
  // await readJsonToInsertSqlite("recipe_list", "recipe_list");
  // await readJsonToInsertSqlite("recipe", "recipe");

  // await readJsonToInsertSqlite("test_fields_new", "test_fields_new");
  // await readJsonToInsertSqlite("test_fields", "test_fields");
  // await readJsonToInsertSqlite("test_img", "test_img");
  // await readJsonToInsertSqlite("test_record", "test_record");
  // await readJsonToInsertSqlite("test_type", "test_type");

  // await readJsonToInsertSqlite("member", "member");
  // await readJsonToInsertSqlite("pet", "pet");
  // await readJsonToInsertSqlite("member_test", "member");
  // print("finish loading");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        BlocProvider(
          create: (context) => PetSafeLoginBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyPointMatchinMyFavoriteBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthPointMatchingQueryBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyPointMatchingBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyPointMatchinNotFavoriteBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyPointMatchinFavoriteBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthPointBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMeridianBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMeridainPointBlocs(),
        ),
        BlocProvider(
          create: (context) => PetSafePasswordBlocs(),
        ),
        BlocProvider(
          create: (context) => ForgetPasswordBlocs(),
        ),
        BlocProvider(
          create: (context) => RegisterBloc(),
        ),
        BlocProvider(
          create: (context) => PersonalPetInfoBloc(),
        ),
        BlocProvider(
          create: (context) => PetinfoSelectBloc(),
        ),
        BlocProvider(
          create: (context) => PersonalinfoBloc(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyRecipePageBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthMyRecipeRecipeIdBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthReviseRecipeBlocs(),
        ),
        BlocProvider(
          create: (context) => FurKidHeslthSearchRecipePageBlocs(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(390, 844),
        builder: (context, child) => MaterialApp(
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate],
          supportedLocales: const [Locale('en'), Locale('ch')],
          debugShowCheckedModeBanner: false,
          home: const FurKidHeslthHome(),
          theme: ThemeData(
            appBarTheme:
                const AppBarTheme(elevation: 0, backgroundColor: Colors.white),
          ),
          routes: {
            //登入
            "petsafe_login": (context) => PetSafeLogin(),
            "update_password": (context) => const UpdatePassword(),

            //註冊
            "register": (context) => const Register(),
            "register_mail_and_phone": (context) =>
                const RegisterMailAndPhone(),
            "user_terms": (context) => const UserTerms(),

            //登入後首頁
            "home": (context) => const Home(),

            //個人檔案
            "personal": (context) => const Personal(),
            "personalinfo": (context) => const PersonalInfo(),
            "personalpetinfo": (context) => const PersonalPetInfo(),
            "personal_email_verify": (context) => const PersonalEmailVerify(),
            "personal_update_password": (context) =>
                const PersonalUpdatePassword(),
            "personal_bonus_points": (context) => const PersonalBonusPoint(),
            "personal_my_wallet": (context) => const PersonalMyWallet(),

            //線上客服
            "online_customer_service": (context) =>
                const OnlineCustomerService(),

            //寵安快譯通首頁
            "furkid_heslth_home": (context) => const FurKidHeslthHome(),

            //毛孩飲食與食譜
            "furkid_heslth_ingredient": (context) =>
                const FurKidHeslthIngredients(),
            "furkid_heslth_suggest": (context) => const FurKidHeslthSuggest(),
            "furkid_heslth_recipesearch": (context) => const RecipeSearch(),
            "furkid_heslth_myrecipe": (context) => const MyRecipe(),
            "furkid_heslth_plusrecipe": (context) => PlusRecipe(),

            //經絡穴位
            "furkid_heslth_meridian_point": (context) =>
                const FurKidHeslthMeridainPoint(),
            "furkid_heslth_point_matching_query": (context) =>
                const FurkidPointMatchingQuery(),
            "furkid_heslth_add_point_matching": (context) =>
                const FurkidAddPointMatching(),
            // "furkid_heslth_my_point_matching": (context) =>
            //     const FurkidMyPointMatching(),

            //健康存摺
            "furkid_heslth_upload_image": (context) => const ImageUpload(),
            "furkid_heslth_cbc": (context) => const CBC(),
            "furkid_heslth_biochemistry_test": (context) =>
                const BiochemistryTest(),
            "furkid_heslth_urine_test": (context) => const UrineTest(),
            "furkid_heslth_else_test": (context) => const ElseTest(),
            "furkid_heslth_drink": (context) => const Drink(),
            "furkid_heslth_food": (context) => const Food(),
            "furkid_heslth_acupoint_record": (context) =>
                const AcupointRecord(),
            "furkid_heslth_summary": (context) => const Summary(),
            "furkid_heslth_analyze_and_search": (context) =>
                const AnalyzeAndSearch(),
            "single_column": (context) => const SingleColumn(),
            "multi_column": (context) => const MultiColumn(),
            "chart_analyze": (context) => const ChartAnalyze(),
            "drink_query": (context) => const DrinkQuery(),
            "food_query": (context) => const FoodQuery(),
            "acupoint_query": (context) => const AcupointQuery(),
            "summary_query": (context) => const SummaryQuery(),
            "image_query": (context) => const ImageQuery(),
          },
        ),
      ),
    );
  }
}
