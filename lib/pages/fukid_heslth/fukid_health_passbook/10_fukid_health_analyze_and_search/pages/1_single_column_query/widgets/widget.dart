import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/widgets/info_page.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';

// 表單
class SingleColumnForm extends StatefulWidget {
  const SingleColumnForm({super.key});

  @override
  SingleColumnFormState createState() {
    return SingleColumnFormState();
  }
}

class SingleColumnFormState extends State<SingleColumnForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> petList = <String>['下拉式選單', 'Two', 'Free', 'Four'];
  String pet = '下拉式選單';
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final TextEditingController note = TextEditingController();
  String testTypeValue = allTestTypesList[0];
  late String testItemValue = cbcTestItems[0];
  late List<String> testItemList = cbcTestItems;

  @override
  void initState() {
    petDropdownList = getPetInfo();
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day} - ${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期範圍(最長6個月)"),
            DateRangePicker(controller: date),
            buildText("檢查類別"),
            NomalDropdownButton(
              dropdownList: allTestTypesList,
              selectedValue: testTypeValue,
              onChanged: (newValue) {
                setState(() {
                  testTypeValue = newValue;
                  if (testTypeValue == "CBC檢測") {
                    testItemValue = cbcTestItems[0];
                    testItemList = cbcTestItems;
                  } else if (testTypeValue == "血液生化檢測") {
                    testItemValue = biochemistryTestItems[0];
                    testItemList = biochemistryTestItems;
                  } else if (testTypeValue == "尿液檢測") {
                    testItemValue = urineTestItems[0];
                    testItemList = urineTestItems;
                  } else {
                    testItemValue = "檢測結果";
                    testItemList = ["檢測結果"];
                  }
                });
              },
            ),
            buildText("項目"),
            NomalDropdownButton(
              dropdownList: testItemList,
              selectedValue: testItemValue,
              onChanged: (newValue) {
                setState(() {
                  testItemValue = newValue;
                });
              },
            ),
            buildText("備註"),
            buildTextField("請輸入欲查詢的備註關鍵字", note),
            SizedBox(
              height: 30.h,
            ),
            Center(
              child: queryRecordButton(
                context,
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return InfoPage(
                          // 路由参数
                          title: "單一欄位查詢",
                          petName: petDropdownValue,
                          testDate: date.text,
                          testType: testTypeValue,
                          testItem: testItemValue,
                        );
                      },
                    ),
                  );
                  print("1");
                },
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
