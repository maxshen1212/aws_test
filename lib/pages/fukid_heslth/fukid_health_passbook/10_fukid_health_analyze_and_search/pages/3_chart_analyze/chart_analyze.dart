import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/setup/setup.dart';

import 'widgets/widget.dart';

class ChartAnalyze extends StatelessWidget {
  const ChartAnalyze({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 20.w, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 100.w,
        height: 300.h,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "圖表分析"),
      body: const ChartAnalyzeForm(),
    );
  }
}
