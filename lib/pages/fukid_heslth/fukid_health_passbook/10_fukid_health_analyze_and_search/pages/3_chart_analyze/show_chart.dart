import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:multi_dropdown/models/value_item.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/3_chart_analyze/widgets/widget.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:sqflite/sqflite.dart';

class ShowChart extends StatefulWidget {
  const ShowChart(
      {super.key,
      this.selectOption1,
      this.selectOption2,
      this.selectOption3,
      required this.testDate,
      required this.petName,
      required this.selectList});
  final String petName;
  final String testDate;
  final List<ValueItem<dynamic>> selectList;
  final String? selectOption1;
  final String? selectOption2;
  final String? selectOption3;
  @override
  State<ShowChart> createState() => _ShowChartState();
}

class _ShowChartState extends State<ShowChart> {
  @override
  void initState() {
    super.initState();
  }

  // 搜尋test_record紀錄
  Future<dynamic> searchRecord(
      petName, String testDate, List<ValueItem<dynamic>> selectList) async {
    final sqlite = Sqlite();
    Database? db = await sqlite.connectDB();

    // testDateList[0]為開始日期;testDateList[1]為結束日期
    List testDateList = testDate.split(RegExp(" - "));
    // 多選檢測項目(所以此為二維陣列 ex:[[檢測項目1的紀錄],[檢測項目2的紀錄],[檢測項目3的紀錄]])
    List<List> recordAll = [];

    // 單位轉換函式
    // 將檢測項目的單位轉換為標準單位（檢測項目的單位可能不同）
    // 傳入參數：檢測項目的原始紀錄、檢測項目的id
    // 回傳值：轉換後的檢測項目紀錄
    Future<List<Map<String, dynamic>>> convertUnit(
        List<Map<String, dynamic>> initRecord, int originFieldId) async {
      // 為了避免變更原始紀錄，所以先從原始紀錄複製出新的紀錄
      List<Map<String, dynamic>> convertRecord = [];
      // 遍歷原始紀錄
      for (var singleInitRecord in initRecord) {
        // 從原始紀錄複製出新的紀錄
        Map<String, dynamic> copyRecord = Map.from(singleInitRecord);
        // 如果檢測項目id不是原始檢測項目id，則進行單位轉換
        if (singleInitRecord["field_id"] != originFieldId) {
          // 從test_fields_new資料表中取得檢測項目的轉換因子
          List<Map<String, dynamic>> factor = await db.query("test_fields_new",
              columns: ["factor"], // 取得factor欄位的值
              where: "id = ?", // 條件：id = ?
              whereArgs: [copyRecord["field_id"]] // 條件的參數
              );
          // 將檢測項目的值除以轉換因子，將轉換後的值放到新紀錄中
          copyRecord["value"] = copyRecord["value"] / factor[0]["factor"];
        }
        // 將轉換後的新紀錄加入至轉換後的紀錄清單中
        convertRecord.add(copyRecord);
      }
      // 回傳轉換後的紀錄清單
      return convertRecord;
    }

    for (var element in selectList) {
      // 單個檢測項目的紀錄 ex:[{第1筆紀錄},{第2筆紀錄},{第3筆紀錄}]
      List record = [];
      // testList[0]為檢測類別,testList[1]為檢測項目
      List testList = element.value.split(RegExp(" : "));
      // 檢測類別
      String testType = testList[0];
      // 檢測項目
      String testName = testList[1];
      // 寵物ID
      dynamic petID = await getPetID(petName);
      try {
        print("test name: $testName");
        // 抓取此檢測項目的所有field_id，因為某些檢測項目有多個單位
        List<Map<String, dynamic>> fieldId = await db.query(
          "test_fields_new",
          columns: ["id", "unit"],
          where: "name = ?",
          whereArgs: [testName], // 檢測項目
        );
        // print(fieldId);

        // 檢測項目有多個單位
        if (fieldId.isNotEmpty) {
          // 查詢符合條件的檢測紀錄
          List<Map<String, dynamic>> initRecord = await db.rawQuery(
            // 使用rawQuery，可以讓我們使用WHERE子句中的BETWEEN運算子
            // 讓我們可以一次查詢多個檢測項目的紀錄
            '''
            SELECT date, value, field_id FROM test_record WHERE
            (field_id BETWEEN ? AND ?)
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?
            AND TRIM(value) <> "";
            ''',
            [
              fieldId.first["id"], // 1. 檢測項目的最小field_id
              fieldId.last["id"], // 2. 檢測項目的最大field_id
              testDateList[0], // 3. 檢測的開始日期
              testDateList[1], // 4. 檢測的結束日期
              petID, // 5. 寵物的ID
            ],
          );
          // 印出初始檢測紀錄
          // print("init record: $initRecord");

          // 將檢測項目的單位轉換為原始單位的值
          record = await convertUnit(initRecord, fieldId.first["id"]);
          // 並將原始單位放入每筆紀錄的欄位中
          for (var singleRecord in record) {
            singleRecord["unit"] = fieldId.first["unit"];
          }
          print("test record: $record");
        }
        // 檢測只有一個單位
        else {
          // 取得此檢測項目在test_type資料表中的id
          List<Map<String, dynamic>> testTypeID = await db.query(
            "test_type",
            columns: ["id"],
            where: "name = ?",
            whereArgs: [testType], // 檢測類別
          );

          // 從test_record資料表中取得符合條件的資料
          List<Map<String, dynamic>> initRecord = await db.rawQuery(
            '''
            SELECT date, value, unit FROM test_record WHERE
            test_type = ?
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?;
            ''',
            [
              testTypeID[0]["id"], // 條件的參數：test_type = ?
              testDateList[0], // 條件的參數：date BETWEEN ? AND ?
              testDateList[1],
              petID, // 條件的參數：pet_id = ?
            ],
          );

          // 將initRecord資料丟進record
          for (var singleInitRecord in initRecord) {
            record.add(singleInitRecord); // 將從test_record資料表中取得的資料丟進record中
          }
        }
      } catch (e) {
        print("資料庫錯誤：$e");
        return null;
      }
      // 將此檢測項目的紀錄透過陣列保存(每個檢測項目的迴圈都要保存)
      if (record.isNotEmpty) {
        recordAll.add(record);
      }
    }
    // 回傳多選檢測項目紀錄
    // print("all");
    // print(recordAll);
    if (recordAll.isNotEmpty) {
      return recordAll;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 10, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "圖表分析"),
      // body: const LineChartSample(),
      body: Container(
        // padding: EdgeInsets.only(left: 5.w, right: 20.w, bottom: 30.h),
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: FutureBuilder(
                future: searchRecord(
                    widget.petName, widget.testDate, widget.selectList),
                builder: (context, snapshot) {
                  // print("snapshot結果 ${snapshot.connectionState}");
                  // 顯示資料結果
                  print("snapshot結果 ${snapshot.data}");
                  // print(snapshot.hasData);
                  // print(snapshot.data.runtimeType);
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text("錯誤:${snapshot.hasError}");
                  } else if (!snapshot.hasData) {
                    return const Center(child: Text("沒有資料"));
                  } else {
                    return ListView.builder(
                      itemExtent: 380.h,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index1) {
                        print("index:$index1");
                        List<String> date = List.generate(
                            snapshot.data[index1].length,
                            (index2) => snapshot.data[index1][index2]['date']);
                        print(date);
                        List<FlSpot> data = List<FlSpot>.generate(
                            snapshot.data[index1].length,
                            (index2) => FlSpot(index2.toDouble(),
                                snapshot.data[index1][index2]['value']));
                        print(data);
                        return Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 18.h),
                                child: Text(
                                  widget.selectList[index1].label,
                                  style: const TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 0.6,
                                      decoration: TextDecoration.underline),
                                ),
                              ),

                              // ListView builder一定要用expanded包起來
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 30.w),
                                  child: LineChartFormal(
                                    data: data,
                                    date: date,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// 顯示歷史數據
class ShowRecord extends StatefulWidget {
  const ShowRecord(
      {super.key,
      this.selectOption1,
      this.selectOption2,
      this.selectOption3,
      required this.testDate,
      required this.petName,
      required this.selectList});
  final String petName;
  final String testDate;
  final List<ValueItem<dynamic>> selectList;
  final String? selectOption1;
  final String? selectOption2;
  final String? selectOption3;
  @override
  State<ShowRecord> createState() => _ShowRecordState();
}

class _ShowRecordState extends State<ShowRecord> {
  @override
  void initState() {
    super.initState();
  }

  // 搜尋test_record紀錄
  Future<dynamic> searchRecord(
      petName, String testDate, List<ValueItem<dynamic>> selectList) async {
    final sqlite = Sqlite();
    Database? db = await sqlite.connectDB();

    // testDateList[0]為開始日期;testDateList[1]為結束日期
    List testDateList = testDate.split(RegExp(" - "));
    // 多選檢測項目(所以此為二維陣列 ex:[[檢測項目1的紀錄],[檢測項目2的紀錄],[檢測項目3的紀錄]])
    List<List> recordAll = [];

    // 單位轉換函式
    // 將檢測項目的單位轉換為標準單位（檢測項目的單位可能不同）
    // 傳入參數：檢測項目的原始紀錄、檢測項目的id
    // 回傳值：轉換後的檢測項目紀錄
    Future<List<Map<String, dynamic>>> convertUnit(
        List<Map<String, dynamic>> initRecord, int originFieldId) async {
      // 為了避免變更原始紀錄，所以先從原始紀錄複製出新的紀錄
      List<Map<String, dynamic>> convertRecord = [];
      // 遍歷原始紀錄
      for (var singleInitRecord in initRecord) {
        // 從原始紀錄複製出新的紀錄
        Map<String, dynamic> copyRecord = Map.from(singleInitRecord);
        // 如果檢測項目id不是原始檢測項目id，則進行單位轉換
        if (singleInitRecord["field_id"] != originFieldId) {
          // 從test_fields_new資料表中取得檢測項目的轉換因子
          List<Map<String, dynamic>> factor = await db.query("test_fields_new",
              columns: ["factor"], // 取得factor欄位的值
              where: "id = ?", // 條件：id = ?
              whereArgs: [copyRecord["field_id"]] // 條件的參數
              );
          // 將檢測項目的值除以轉換因子，將轉換後的值放到新紀錄中
          copyRecord["value"] = copyRecord["value"] / factor[0]["factor"];
        }
        // 將轉換後的新紀錄加入至轉換後的紀錄清單中
        convertRecord.add(copyRecord);
      }
      // 回傳轉換後的紀錄清單
      return convertRecord;
    }

    for (var element in selectList) {
      // 單個檢測項目的紀錄 ex:[{第1筆紀錄},{第2筆紀錄},{第3筆紀錄}]
      List record = [];
      // testList[0]為檢測類別,testList[1]為檢測項目
      List testList = element.value.split(RegExp(" : "));
      // 檢測類別
      String testType = testList[0];
      // 檢測項目
      String testName = testList[1];
      // 寵物ID
      dynamic petID = await getPetID(petName);
      try {
        print("test name: $testName");
        // 抓取此檢測項目的所有field_id，因為某些檢測項目有多個單位
        List<Map<String, dynamic>> fieldId = await db.query(
          "test_fields_new",
          columns: ["id", "unit"],
          where: "name = ?",
          whereArgs: [testName], // 檢測項目
        );
        print(fieldId);

        // 檢測項目有多個單位
        if (fieldId.isNotEmpty) {
          // 查詢符合條件的檢測紀錄
          List<Map<String, dynamic>> initRecord = await db.rawQuery(
            // 使用rawQuery，可以讓我們使用WHERE子句中的BETWEEN運算子
            // 讓我們可以一次查詢多個檢測項目的紀錄
            '''
            SELECT date, value, field_id FROM test_record WHERE
            (field_id BETWEEN ? AND ?)
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?
            AND TRIM(value) <> "";
            ''',
            [
              fieldId.first["id"], // 1. 檢測項目的最小field_id
              fieldId.last["id"], // 2. 檢測項目的最大field_id
              testDateList[0], // 3. 檢測的開始日期
              testDateList[1], // 4. 檢測的結束日期
              petID, // 5. 寵物的ID
            ],
          );
          // 印出初始檢測紀錄
          print("init record:");
          print(initRecord);

          // 將檢測項目的單位轉換為原始單位的值
          record = await convertUnit(initRecord, fieldId.first["id"]);
          // 並將原始單位放入每筆紀錄的欄位中
          for (var singleRecord in record) {
            singleRecord["unit"] = fieldId.first["unit"];
          }
        }
        // 檢測只有一個單位
        else {
          // 取得此檢測項目在test_type資料表中的id
          List<Map<String, dynamic>> testTypeID = await db.query(
            "test_type",
            columns: ["id"],
            where: "name = ?",
            whereArgs: [testType], // 檢測類別
          );

          // 從test_record資料表中取得符合條件的資料
          List<Map<String, dynamic>> initRecord = await db.rawQuery(
            '''
            SELECT date, value, unit FROM test_record WHERE
            test_type = ?
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?;
            ''',
            [
              testTypeID[0]["id"], // 條件的參數：test_type = ?
              testDateList[0], // 條件的參數：date BETWEEN ? AND ?
              testDateList[1],
              petID, // 條件的參數：pet_id = ?
            ],
          );

          // 將initRecord資料丟進record
          for (var singleInitRecord in initRecord) {
            record.add(singleInitRecord); // 將從test_record資料表中取得的資料丟進record中
          }
        }
      } catch (e) {
        print("資料庫錯誤：$e");
        return null;
      }
      // 將此檢測項目的紀錄透過陣列保存(每個檢測項目的迴圈都要保存)
      recordAll.add(record);
    }
    // 回傳多選檢測項目紀錄
    print("answer");
    print(recordAll);

    if (recordAll.isNotEmpty) {
      return recordAll;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "圖表分析"),
      // body: const LineChartSample(),
      body: Container(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 30.h),
        decoration: const BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: FutureBuilder(
                future: searchRecord(
                    widget.petName, widget.testDate, widget.selectList),
                builder: (context, snapshot) {
                  // print("snapshot結果 ${snapshot.connectionState}");
                  // 顯示資料結果
                  // print("snapshot結果 ${snapshot.data}");
                  // print(snapshot.hasData);
                  // print(snapshot.data.runtimeType);
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text("錯誤:${snapshot.hasError}");
                  } else if (!snapshot.hasData) {
                    return const Center(child: Text("沒有資料"));
                  } else {
                    return Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                  width: 2.0, color: Color(0xFF929292)),
                            ),
                          ),
                          margin: EdgeInsets.symmetric(vertical: 20.h),
                          child: Container(
                            padding: EdgeInsets.only(bottom: 10.h),
                            child: const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '日期',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontFamily: 'Inter',
                                    fontWeight: FontWeight.w700,
                                    height: 0,
                                    letterSpacing: 0.70,
                                  ),
                                ),
                                Text(
                                  '項目',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontFamily: 'Inter',
                                    fontWeight: FontWeight.w700,
                                    height: 0,
                                    letterSpacing: 0.70,
                                  ),
                                ),
                                Text(
                                  '數值',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontFamily: 'Inter',
                                    fontWeight: FontWeight.w700,
                                    height: 0,
                                    letterSpacing: 0.70,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            itemExtent: 200.h,
                            itemCount: snapshot.data.length,
                            itemBuilder: (BuildContext context, int index1) {
                              return Column(
                                children: [
                                  Text(widget.selectList[index1].label),
                                  // ListView builder一定要用expanded包起來
                                  Expanded(
                                    child: ListView.builder(
                                      itemExtent: 30.h,
                                      itemCount: snapshot.data[index1].length,
                                      itemBuilder:
                                          (BuildContext context, int index2) {
                                        return Container(
                                          decoration: const BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  width: 1,
                                                  color: Color(0xFF929292)),
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                "${snapshot.data[index1][index2]['date']}",
                                                style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Inter',
                                                  fontWeight: FontWeight.w300,
                                                  height: 0,
                                                  letterSpacing: 0.70,
                                                ),
                                              ),
                                              Text(
                                                "${snapshot.data[index1][index2]['value']} ${snapshot.data[index1][index2]['unit'] ?? ""}",
                                                textAlign: TextAlign.right,
                                                style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Inter',
                                                  fontWeight: FontWeight.w300,
                                                  height: 0,
                                                  letterSpacing: 0.70,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
