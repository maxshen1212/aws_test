import 'dart:math' as math;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/pages/3_chart_analyze/show_chart.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:multi_dropdown/multiselect_dropdown.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';

// 表單
class ChartAnalyzeForm extends StatefulWidget {
  const ChartAnalyzeForm({super.key});

  @override
  ChartAnalyzeFormState createState() {
    return ChartAnalyzeFormState();
  }
}

class ChartAnalyzeFormState extends State<ChartAnalyzeForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> petList = <String>['下拉式選單', 'Two', 'Free', 'Four'];
  String pet = '下拉式選單';
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final MultiSelectController<dynamic> _controller = MultiSelectController();
  var valueItemList = <ValueItem>[];
  @override
  void initState() {
    petDropdownList = getPetInfo();
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day} - ${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";

    for (var i in allTestTypesList) {
      if (i == "CBC檢測") {
        for (var j in cbcTestItems) {
          valueItemList.add(ValueItem(label: "$i : $j", value: '$i : $j'));
        }
      } else if (i == "血液生化檢測") {
        for (var j in biochemistryTestItems) {
          valueItemList.add(ValueItem(label: "$i : $j", value: '$i : $j'));
        }
      } else if (i == "尿液檢測") {
        for (var j in urineTestItems) {
          valueItemList.add(ValueItem(label: "$i : $j", value: '$i : $j'));
        }
      } else {
        valueItemList.add(ValueItem(label: "$i : 檢測結果", value: '$i : 檢測結果'));
      }
    }
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期範圍(最長6個月)"),
            DateRangePicker(
              controller: date,
            ),
            buildText("檢測值最多三樣"),
            Container(
              margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
              child: MultiSelectDropDown(
                showClearIcon: true,
                controller: _controller,
                // 選擇時專用函式
                onOptionSelected: (options) {
                  // debugPrint(options.toString());
                },
                options: valueItemList,
                maxItems: 3,
                selectionType: SelectionType.multi,
                chipConfig: const ChipConfig(
                  autoScroll: true,
                  // 垂直排列間距
                  runSpacing: -10,
                  wrapType: WrapType.wrap,
                  backgroundColor: Color.fromARGB(255, 246, 48, 153),
                  labelStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                ),
                optionTextStyle: const TextStyle(fontSize: 16),
                selectedOptionIcon: const Icon(Icons.check),
                selectedOptionTextColor:
                    const Color.fromARGB(255, 246, 48, 153),
                // 禁用選項
                // disabledOptions: const [
                //   ValueItem(label: 'Option 1', value: '1')
                // ],
                searchEnabled: true,
                dropdownMargin: 2,
                backgroundColor: const Color(0xffececec),
                optionsBackgroundColor:
                    const Color.fromARGB(255, 202, 202, 202),
                optionSeparator: Divider(
                  height: 0.3.h,
                  color: const Color.fromARGB(255, 72, 69, 69),
                ),
                selectedOptionBackgroundColor:
                    const Color.fromARGB(255, 202, 202, 202),
                dropdownHeight: 300,
                borderColor: Colors.transparent,
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
            Center(
              child: queryRecordButton(
                context,
                () {
                  // for (var i in _controller.selectedOptions) {
                  //   print(i.value);
                  // }

                  if (_controller.selectedOptions.length > 0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          if (_controller.selectedOptions.length == 1) {
                            return ShowChart(
                              // 路由参数
                              selectOption1:
                                  _controller.selectedOptions[0].value,
                              testDate: date.text,
                              petName: petDropdownValue,
                              selectList: _controller.selectedOptions,
                            );
                          } else if (_controller.selectedOptions.length == 2) {
                            return ShowChart(
                              // 路由参数
                              selectOption1:
                                  _controller.selectedOptions[0].value,
                              selectOption2:
                                  _controller.selectedOptions[1].value,
                              testDate: date.text,
                              petName: petDropdownValue,
                              selectList: _controller.selectedOptions,
                            );
                          } else {
                            return ShowChart(
                              // 路由参数
                              selectOption1:
                                  _controller.selectedOptions[0].value,
                              selectOption2:
                                  _controller.selectedOptions[1].value,
                              selectOption3:
                                  _controller.selectedOptions[2].value,
                              testDate: date.text,
                              petName: petDropdownValue,
                              selectList: _controller.selectedOptions,
                            );
                          }
                        },
                      ),
                    );
                  } else {
                    Fluttertoast.showToast(
                        msg: "請選擇分析項目",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 18.0);
                  }
                },
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}

// 正式折線圖
class LineChartFormal extends StatefulWidget {
  const LineChartFormal({super.key, required this.data, required this.date});
  // 用於存儲分數歷史數據的 FlSpot 列表，FlSpot 是用於 fl_chart 庫的類別，表示圖表中的一個數據點
  final List<FlSpot> data;
  final List<String> date; // 存儲每個數據點相對應的日期的列表

  @override
  State<StatefulWidget> createState() => LineChartFormalState();
}

class LineChartFormalState extends State<LineChartFormal> {
  // 宣告兩個狀態變數 _data 和 _date
  // 因為 widget.data 是 final 且不能被重新賦值，因此我們需要使用一個狀態變量來存儲和修改數據
  late List<String> _date;
  late List<FlSpot> _data;
  late double interval;
  late double maxValue;
  late double minValue;
  @override
  void initState() {
    super.initState();
    // 初始化 _data
    _date = widget.date; // widget.date.reversed.toList()
    _data = widget.data;
    // 找出 _data.y 裡的最大值
    maxValue = _data.map<double>((e) => e.y).reduce(math.max);
    minValue = _data.map<double>((e) => e.y).reduce(math.min);

    // Calculate the data range
    double dataRange = maxValue - minValue;
    // Calculate the interval for the labels
    interval = (dataRange / 50).round() *
        10; // For example, divide the data range by 5
  }

  // Y座標標題
  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    if (value == maxValue || value == 0) {
      return Text(value.toInt().toString(),
          style: style, textAlign: TextAlign.center);
    } else {
      return const Text("", style: style, textAlign: TextAlign.center);
    }
  }

  // X座標標題
  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text;
    print(_date[value.toInt()]);
    var datesplitlist = _date[value.toInt()].toString().split('/');
    text = Text("${datesplitlist[1]}月", style: style);

    // Display only month and day (MM-DD)
    // .toInt()：這是將浮點數轉換為整數的方法。由於索引應該是整數，因此您可能會將浮點索引值轉換為整數，以確保正確存取串列
    // _date[value.toInt()]：這部分的結果是從 _date 串列中選擇特定索引位置的日期時間字串
    // if (value.toInt() % 2 == 0 && value.toInt() < _data.length) {
    //   text = Text(_date[value.toInt()].substring(5, 10), style: style);
    // }

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 10,
      child: text,
    );
  }

  // 隨機顏色
  final Color randomColor = Color.fromARGB(
    255,
    Random().nextInt(150) + 100,
    Random().nextInt(30),
    Random().nextInt(100) + 50,
  ).withOpacity(0.5);

  // 圖表資料
  LineChartBarData get lineChartBarData1 => LineChartBarData(
        isCurved: false,
        curveSmoothness: 0,
        color: randomColor,
        barWidth: 5,
        isStrokeCapRound: true,
        dotData: const FlDotData(show: true),
        belowBarData: BarAreaData(show: true, color: randomColor),
        spots: _data,
      );

  // 這是一個 LineChartData 的參數回傳函示，它用來配置折線圖的各種屬性，包括標題、軸範圍、線條、數據等
  LineChartData get sampleData => LineChartData(
        // 觸控互動訊息
        lineTouchData: const LineTouchData(
            enabled: true,
            touchTooltipData: LineTouchTooltipData(
              tooltipBgColor: Color.fromARGB(255, 157, 157, 157),
            )),
        // 顯示網格
        gridData: const FlGridData(show: true),
        // titlesData:：這個屬性用來定義標題的相關設定，包括底部標籤和左側標籤
        titlesData: FlTitlesData(
          bottomTitles: AxisTitles(
            sideTitles: SideTitles(
              getTitlesWidget: bottomTitleWidgets,
              showTitles: true,
              reservedSize: 60,
              interval: 1,
            ),
          ),
          rightTitles: AxisTitles(
            sideTitles: SideTitles(
              getTitlesWidget: leftTitleWidgets,
              showTitles: false,
              reservedSize: 60,
              interval: interval,
            ),
          ),
          topTitles: const AxisTitles(
            sideTitles: SideTitles(showTitles: false),
          ),
          leftTitles: AxisTitles(
            sideTitles: SideTitles(
              getTitlesWidget: leftTitleWidgets,
              showTitles: true,
              reservedSize: 60,
              interval: interval,
            ),
          ),
        ),
        // borderData:：這個屬性用來定義圖表的邊界線相關設定，例如邊界線的顏色和寬度
        borderData: FlBorderData(
          show: true,
          border: Border(
            bottom: BorderSide(
              color: const Color.fromARGB(255, 94, 94, 94).withOpacity(0.2),
              width: 3,
            ),
            left: BorderSide(
              color: const Color.fromARGB(255, 94, 94, 94).withOpacity(0.2),
              width: 3,
            ),
            right: const BorderSide(color: Colors.transparent),
            top: const BorderSide(color: Colors.transparent),
          ),
        ),
        // lineBarsData 屬性定義了要繪製的折線數據，每組數據代表一條線
        lineBarsData: [
          lineChartBarData1,
          // lineChartBarData2,
          // lineChartBarData3,
        ],
        backgroundColor: const Color.fromARGB(255, 244, 244, 244),
        // X軸最小值，null的話會從lineBarsData中讀取數據
        // minX: 1,
        // X軸最大值，null的話會從lineBarsData中讀取數據
        // maxX: 12,
        // Y軸最小值，null的話會從lineBarsData中讀取數據
        // maxY: 6,
        // Y軸最小值，null的話會從lineBarsData中讀取數據
        minY: 0,
      );

  @override
  Widget build(BuildContext context) {
    // 寬高比
    return AspectRatio(
      aspectRatio: 16 / 12,
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: LineChart(
                  sampleData,
                  // duration: const Duration(milliseconds: 250),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
