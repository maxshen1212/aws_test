import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/10_fukid_health_analyze_and_search/widgets/Info_page.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';

// 表單
class SummaryQueryForm extends StatefulWidget {
  const SummaryQueryForm({super.key});

  @override
  SummaryQueryFormState createState() {
    return SummaryQueryFormState();
  }
}

class SummaryQueryFormState extends State<SummaryQueryForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> petList = <String>['下拉式選單', 'Two', 'Free', 'Four'];
  String pet = '下拉式選單';
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final TextEditingController textField1 = TextEditingController();

  @override
  void initState() {
    petDropdownList = getPetInfo();
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day} - ${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期範圍(最長6個月)"),
            DateRangePicker(
              controller: date,
            ),
            buildText("內容"),
            buildTextField("文字輸入", textField1),
            SizedBox(
              height: 30.h,
            ),
            Center(
              child: queryRecordButton(
                context,
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return InfoPage(
                          // 路由参数
                          title: "摘要紀錄查詢",
                          petName: petDropdownValue,
                          testDate: date.text,
                          testType: "note",
                          testItem: null,
                        );
                      },
                    ),
                  );
                  print("7");
                },
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
