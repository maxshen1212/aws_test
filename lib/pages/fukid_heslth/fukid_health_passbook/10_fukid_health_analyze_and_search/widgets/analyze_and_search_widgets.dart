import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// 表單
class AnalyzeAndSearchCatalog extends StatefulWidget {
  const AnalyzeAndSearchCatalog({super.key});

  @override
  AnalyzeAndSearchCatalogState createState() {
    return AnalyzeAndSearchCatalogState();
  }
}

class AnalyzeAndSearchCatalogState extends State<AnalyzeAndSearchCatalog> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 30.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...[
              const CatalogButton(
                name: "單一欄位查詢",
                routeName: "single_column",
              ),
              const CatalogButton(
                name: "多欄位查詢",
                routeName: "multi_column",
              ),
              const CatalogButton(
                name: "圖表分析",
                routeName: "chart_analyze",
              ),
              const CatalogButton(
                name: "喝水量查詢",
                routeName: "drink_query",
              ),
              const CatalogButton(
                name: "飲食紀錄查詢",
                routeName: "food_query",
              ),
              const CatalogButton(
                name: "配穴紀錄查詢",
                routeName: "acupoint_query",
              ),
              const CatalogButton(
                name: "摘要紀錄查詢",
                routeName: "summary_query",
              ),
              const CatalogButton(
                name: "圖片查詢",
                routeName: "image_query",
              ),
            ].expand(
              (widget) => [
                widget,
                const SizedBox(
                  height: 20,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CatalogButton extends StatelessWidget {
  const CatalogButton({super.key, required this.name, required this.routeName});
  final String name;
  final String routeName;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45.h,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5,
          backgroundColor: const Color(0xFFECECEC),
        ),
        onPressed: () {
          Navigator.pushNamed(context, routeName);
        },
        child: Row(
          children: [
            Expanded(
              child: Text(
                name,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w200,
                  height: 0,
                  letterSpacing: 0.70,
                ),
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios,
              color: Color.fromARGB(255, 53, 53, 53),
              size: 25.0,
            ),
          ],
        ),
      ),
    );
  }
}
