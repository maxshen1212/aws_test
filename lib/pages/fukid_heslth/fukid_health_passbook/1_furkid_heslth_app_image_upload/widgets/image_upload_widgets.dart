import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';
import 'package:sqflite/sqflite.dart';

// 驗證表單
class ImageUploadForm extends StatefulWidget {
  const ImageUploadForm({super.key});

  @override
  State<ImageUploadForm> createState() => _ImageUploadFormState();
}

class _ImageUploadFormState extends State<ImageUploadForm> {
  final _formKey = GlobalKey<FormState>();
  late Future<List<String>> petDropdownList;

  // 資料
  String imgPath = "";
  String petDropdownValue = "";
  final TextEditingController date = TextEditingController();
  String allTestTypeValue = allTestTypesList[0];
  final TextEditingController label = TextEditingController();
  final TextEditingController note = TextEditingController();

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  Future<dynamic> getTestTypeID(name) async {
    final sqlite = Sqlite();
    Database? db;
    db = await sqlite.connectDB();
    List<Map<String, Object?>> id = await db.query(
      "test_type",
      columns: ["id"],
      // 通過 where 傳遞 id 可以防止 SQL 注入
      // 注意 不要使用 where: "id = ${dog.id}"
      where: "name = ?",
      whereArgs: ["$name"],
    );
    return id[0]["id"];
  }

  Future insertRecordImg(reqData) async {
    final sqlite = Sqlite();
    await sqlite.insert("test_img", reqData);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 20.h),
              width: double.maxFinite,
              height: 200.h,
              child: UploadImageWidget(
                onImagePicked: (path) {
                  setState(() {
                    imgPath = path;
                  });
                },
                child: imgPath == ""
                    ? Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(13),
                          color: const Color(0xFFCCCCCC),
                        ),
                        child: const Icon(Icons.collections_outlined),
                      )
                    : SizedBox(
                        height: 100,
                        width: 100,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(13),
                          child: Image.file(
                            File(imgPath),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
              ),
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期"),
            DatePicker(
              controller: date,
            ),
            buildText("圖片性質"),
            NomalDropdownButton(
              dropdownList: allTestTypesList,
              selectedValue: allTestTypeValue,
              onChanged: (newValue) {
                setState(() {
                  allTestTypeValue = newValue;
                });
              },
            ),
            buildText("標籤(多項請以,隔開)"),
            buildTextField("文字輸入", label),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(
                  context,
                  () async {
                    Map<String, dynamic> reqData = {
                      "mem_id": 0,
                      "pet_id": await getPetID(petDropdownValue),
                      "img": imgPath,
                      "date": date.text,
                      "img_type": await getTestTypeID(allTestTypeValue),
                      "tags": label.text,
                      "note": note.text,
                      "create_time": formatNowTime(),
                    };
                    // print(reqData);
                    await insertRecordImg(reqData);
                    if (!context.mounted) return;
                    Navigator.of(context).pop();
                  },
                ),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}

// 上傳相片
class UploadImageWidget extends StatelessWidget {
  final void Function(String) onImagePicked;
  final Widget child;

  const UploadImageWidget(
      {Key? key, required this.child, required this.onImagePicked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _showOptions(context);
      },
      child: child,
    );
  }

  void _showOptions(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      var path = await _showCameraLibrary();
                      onImagePicked(path);
                    },
                    leading: const Icon(Icons.photo_camera),
                    title: const Text("拍攝照片")),
                ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      var path = await _showPhotoLibrary();
                      onImagePicked(path);
                    },
                    leading: const Icon(Icons.photo_library),
                    title: const Text("選擇照片"))
              ]));
        });
  }

  Future<String> _showCameraLibrary() async {
    ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      return image.path;
    } else {
      return "";
    }
  }

  Future<String> _showPhotoLibrary() async {
    ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      return image.path;
    } else {
      return "";
    }
  }
}
