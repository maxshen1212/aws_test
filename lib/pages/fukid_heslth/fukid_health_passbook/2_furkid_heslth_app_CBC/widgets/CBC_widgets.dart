import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';

// 驗證表單
class CBCForm extends StatefulWidget {
  const CBCForm({super.key});

  @override
  CBCFormState createState() {
    return CBCFormState();
  }
}

class CBCFormState extends State<CBCForm> {
  final _formKey = GlobalKey<FormState>();
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  List<String> testType = [
    "RBC",
    "HCT",
    "HGB",
    "MCV",
    "MCH",
    "MCHC",
    "RDW",
    "RETIC",
    "%RETIC",
    "WBC",
    "NEU",
    "LYM",
    "MONO",
    "EOS",
    "BASO",
    "PLT",
    "MPV",
    "PDW",
    "PCT",
    "%NEU",
    "%LYM",
    "%MONO",
    "%EOS",
    "%BASO",
  ];
  List<String> dropdownValue = [
    'M/µL',
    "%",
    "g/dL",
    "fL",
    "pg",
    "g/dL",
    "%",
    'K/µL',
    "%",
    'K/µL',
    'K/µL',
    'K/µL',
    'K/µL',
    'K/µL',
    'K/µL',
    'K/µL',
    "fL",
    "fL",
    "%",
    "%",
    "%",
    "%",
    "%",
    "%",
  ];

  final TextEditingController date = TextEditingController();
  final TextEditingController note = TextEditingController();
  List<TextEditingController> valueControllers =
      List.generate(24, (index) => TextEditingController());

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期："),
            DatePicker(
              controller: date,
            ),
            buildText("RBC(紅血球)"),
            UnitDropdownField(
              controller: valueControllers[0],
              dropdownList: cbcUnit1,
              selectedValue: dropdownValue[0],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[0] = newValue;
                });
              },
            ),
            buildText("HCT(血容比)"),
            UnitDropdownField(
              controller: valueControllers[1],
              dropdownList: cbcUnit2,
              selectedValue: dropdownValue[1],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[1] = newValue;
                });
              },
            ),
            buildText("HGB(血紅素)"),
            UnitDropdownField(
              controller: valueControllers[2],
              dropdownList: cbcUnit3,
              selectedValue: dropdownValue[2],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[2] = newValue;
                });
              },
            ),
            buildText("MCV(平均血球容積)"),
            unitField(valueControllers[3], "fL"),
            buildText("MCH(平均血色素)"),
            unitField(valueControllers[4], "pg"),
            buildText("MCHC(平均血色素)"),
            unitField(valueControllers[5], "g/dL"),
            buildText("RDW(紅血球分布寬度)"),
            unitField(valueControllers[6], "%"),
            buildText("RETIC"),
            UnitDropdownField(
              controller: valueControllers[7],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[7],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[7] = newValue;
                });
              },
            ),
            buildText("%RETIC"),
            unitField(valueControllers[8], "%"),
            buildText("WBC(白血球)"),
            UnitDropdownField(
              controller: valueControllers[9],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[9],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[9] = newValue;
                });
              },
            ),
            buildText("NEU(中性球)"),
            UnitDropdownField(
              controller: valueControllers[10],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[10],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[10] = newValue;
                });
              },
            ),
            buildText("LYM(淋巴球)"),
            UnitDropdownField(
              controller: valueControllers[11],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[11],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[11] = newValue;
                });
              },
            ),
            buildText("MONO(單核球)"),
            UnitDropdownField(
              controller: valueControllers[12],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[12],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[12] = newValue;
                });
              },
            ),
            buildText("EOS(嗜酸性球)"),
            UnitDropdownField(
              controller: valueControllers[13],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[13],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[13] = newValue;
                });
              },
            ),
            buildText("BASO(嗜鹼性球)"),
            UnitDropdownField(
              controller: valueControllers[14],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[14],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[14] = newValue;
                });
              },
            ),
            buildText("PLT(血小板)"),
            UnitDropdownField(
              controller: valueControllers[15],
              dropdownList: cbcUnit4,
              selectedValue: dropdownValue[15],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[15] = newValue;
                });
              },
            ),
            buildText("MPV(平均血小板容積)"),
            unitField(valueControllers[16], "fL"),
            buildText("PDW"),
            unitField(valueControllers[17], "fL"),
            buildText("PCT"),
            unitField(valueControllers[18], "%"),
            buildText("%NEU(中性球)"),
            unitField(valueControllers[19], "%"),
            buildText("%LYM(淋巴球)"),
            unitField(valueControllers[20], "%"),
            buildText("%MONO(單核球)"),
            unitField(valueControllers[21], "%"),
            buildText("%EOS(嗜酸性球)"),
            unitField(valueControllers[22], "%"),
            buildText("%BASO(嗜鹼性球)"),
            unitField(valueControllers[23], "%"),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(context, () async {
                  for (var i = 0; i < valueControllers.length; i++) {
                    if (valueControllers[i].text != "") {
                      print("第${i + 1}項目：");
                      Map<String, dynamic> reqData = {
                        "mem_id": 0,
                        "pet_id": await getPetID(petDropdownValue),
                        "date": date.text,
                        "field_id":
                            await getTestField(testType[i], dropdownValue[i]),
                        "test_type": 1,
                        "value": valueControllers[i].text,
                        "unit": dropdownValue[i],
                        "note": note.text,
                      };
                      // print(reqData);
                      await insertTestRecord(reqData);
                    }
                  }

                  if (!context.mounted) return;
                  Navigator.of(context).pop();
                }),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
