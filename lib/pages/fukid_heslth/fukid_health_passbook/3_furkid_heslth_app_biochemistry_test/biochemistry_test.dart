import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import '../widgets/passbook_widgets.dart';
import 'widgets/biochemistry_test_widgets.dart';

class BiochemistryTest extends StatelessWidget {
  const BiochemistryTest({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "血液生化檢查"),
      body: BiochemistryTestForm(),
    );
  }
}

final Future<String> _calculation = Future<String>.delayed(
  const Duration(seconds: 2),
  () => 'Data Loaded',
); // 在兩秒後顯示 'Data Loaded'

Widget testFutureBuilder() {
  return FutureBuilder<String>(
    future: _calculation, // 執行_calculation 內定義的非同步任務
    builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
      List<Widget> children;
      if (snapshot.hasData) {
        // 若是成功執行_calculation並獲取回傳的內容，則顯示children UI如下
        children = <Widget>[
          const Icon(
            Icons.check_circle_outline,
            color: Colors.green,
            size: 60,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Result: ${snapshot.data}'),
          ),
        ];
      } else if (snapshot.hasError) {
        // 若是在_calculation執行過程中出現錯誤，則顯示children UI如下
        children = <Widget>[
          const Icon(
            Icons.error_outline,
            color: Colors.red,
            size: 60,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Error: ${snapshot.error}'),
          ),
        ];
      } else {
        // 預設顯示的children UI
        children = const <Widget>[
          SizedBox(
            width: 60,
            height: 60,
            child: CircularProgressIndicator(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: Text('Awaiting result...'),
          ),
        ];
      }
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        ),
      );
    },
  );
}

class TextFieldDialog extends StatefulWidget {
  const TextFieldDialog({super.key});

  @override
  State<TextFieldDialog> createState() => _TextFieldDialogState();
}

class _TextFieldDialogState extends State<TextFieldDialog> {
  final TextEditingController _controller =
      TextEditingController(); // 建立controller

  @override
  void dispose() {
    _controller.dispose(); // 在離開頁面、Widget被銷毀時，要記得將controller註銷
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text('What number comes next in the sequence?'),
          const Text('1, 1, 2, 3, 5, 8...?'),
          TextField(
            controller: _controller, // 指派Controller給TextField
            onChanged: (String value) async {
              // 隨時監聽輸入文字的變化
              if (value != '13') {
                return;
              }
              await showDialog<void>(
                // 若當下的輸入值是13，會跳出視窗提示
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('That is correct!'),
                    content: const Text('13 is the right answer.'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('OK'),
                      ),
                    ],
                  );
                },
              );
            },
            onSubmitted: (String value) async {
              // 觸發送出行為，會把文字長度印出來
              await showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Thanks!'),
                    content: Text(
                        'You typed "$value", which has length ${value.characters.length}.'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('OK'),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
