import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';

String petDropdownValue = "";
late Future<List<String>> petDropdownList;
List<String> testType = [
  "CRE",
  "BUN/Blood Urea Nitrogen",
  "SDMA",
  "ALT(SGPT)",
  "AST(SGOT)",
  "ALKP/Alk Phosphatase",
  "GGT/Gamma GlutamyI Transpeptidase",
  "TBIL/Total Bilirubin",
  "NH3(Ammonia)",
  "AMYL/Amylase",
  "LIPA/Lipase",
  "TRIG/Triglycerides",
  "CK/CPK",
  "TP/Total Protein",
  "ALB/Albumin",
  "GLB/Globulin",
  "CHOL/Cholesterol",
  "GLU/Glucose",
  "PHOS/Phosphorus",
  "CA/Calcium",
  "K/Potassium",
  "Na/Sodium",
  "Cl/Chloride",
  "T4",
  "free T4",
  "Cortisol",
  "T3",
  "Zinc",
  "Mg/Magnesium",
  "Iron"
];
List<String> dropdownValue = [
  "mg/dL",
  "mg/dL",
  "µg/dL",
  "U/L",
  "U/L",
  "U/L",
  "U/L",
  "mg/dL",
  "mg/dL",
  "U/L",
  "U/L",
  "mg/dL",
  "U/L",
  "g/dL",
  "g/dL",
  "g/dL",
  "mg/dL",
  "mg/dL",
  "mg/dL",
  "mg/dL",
  "mEq/L",
  "mEq/L",
  "mEq/L",
  "µg/dL",
  "ng/dL",
  "µg/dL",
  "ng/dL",
  "mg/dL",
  "mg/dL",
  "mg/dL",
];

final TextEditingController date = TextEditingController();
final TextEditingController note = TextEditingController();
List<TextEditingController> valueControllers =
    List.generate(30, (index) => TextEditingController());
var group1 = "+";
var group2 = "+";
var group3 = "+";

// 表單
class BiochemistryTestForm extends StatefulWidget {
  const BiochemistryTestForm({super.key});

  @override
  BiochemistryTestFormState createState() {
    return BiochemistryTestFormState();
  }
}

class BiochemistryTestFormState extends State<BiochemistryTestForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  // print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期："),
            DatePicker(
              controller: date,
            ),
            buildText("CRE(肌酸酐)"),
            UnitDropdownField(
              controller: valueControllers[0],
              dropdownList: bloodUnit4,
              selectedValue: dropdownValue[0],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[0] = newValue;
                });
                ;
              },
            ),
            buildText("BUN/Blood Urea Nitrogen(血中尿素氮)"),
            UnitDropdownField(
              controller: valueControllers[1],
              dropdownList: bloodUnit4,
              selectedValue: dropdownValue[1],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[1] = newValue;
                });
                ;
              },
            ),
            buildText("SDMA(對稱性二甲基精氨酸)"),
            unitField(valueControllers[2], dropdownValue[2]),
            buildText("ALT(SGPT) (丙胺酸轉胺酶)"),
            unitField(valueControllers[3], dropdownValue[3]),
            buildText("AST(SGOT) (天門冬酸轉氨)"),
            unitField(valueControllers[4], dropdownValue[4]),
            buildText("ALKP/Alk Phosphatase (鹼磷酶)"),
            unitField(valueControllers[5], dropdownValue[5]),
            buildText("GGT/Gamma GlutamyI Transpeptidase (加瑪麩氨基轉換酶)"),
            unitField(valueControllers[6], dropdownValue[6]),
            buildText("TBIL/Total Bilirubin (總膽紅素)"),
            UnitDropdownField(
              controller: valueControllers[7],
              dropdownList: bloodUnit9,
              selectedValue: dropdownValue[7],
              onChanged: (newValue) async {
                setState(() {
                  dropdownValue[7] = newValue;
                });
                ;
              },
            ),
            buildText("NH3(Ammonia) (血氨)"),
            UnitDropdownField(
                controller: valueControllers[8],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[8],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[8] = newValue;
                  });
                  ;
                }),
            buildText("AMYL/Amylase (澱粉酶)"),
            unitField(valueControllers[9], dropdownValue[9]),
            buildText("LIPA/Lipase (脂肪酶)"),
            unitField(valueControllers[10], dropdownValue[10]),
            buildText("TRIG/Triglycerides (三酸甘油脂)"),
            UnitDropdownField(
                controller: valueControllers[11],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[11],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[11] = newValue;
                  });
                  ;
                }),
            buildText("CK/CPK (肌酸磷酸活酶)"),
            unitField(valueControllers[12], dropdownValue[12]),
            buildText("TP/Total Protein (總蛋白質)"),
            UnitDropdownField(
                controller: valueControllers[13],
                dropdownList: bloodUnit8,
                selectedValue: dropdownValue[13],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[13] = newValue;
                  });
                  ;
                }),
            buildText("ALB/Albumin (白蛋白)"),
            UnitDropdownField(
                controller: valueControllers[14],
                dropdownList: bloodUnit8,
                selectedValue: dropdownValue[14],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[14] = newValue;
                  });
                  ;
                }),
            buildText("GLB/Globulin (球蛋白)"),
            UnitDropdownField(
                controller: valueControllers[15],
                dropdownList: bloodUnit8,
                selectedValue: dropdownValue[15],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[15] = newValue;
                  });
                  ;
                }),
            buildText("CHOL/Cholesterol (總膽固醇)"),
            UnitDropdownField(
                controller: valueControllers[16],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[16],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[16] = newValue;
                  });
                  ;
                }),
            buildText("GLU/Glucose (血糖)"),
            UnitDropdownField(
                controller: valueControllers[17],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[17],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[17] = newValue;
                  });
                  ;
                }),
            buildText("PHOS/Phosphorus (磷)"),
            UnitDropdownField(
                controller: valueControllers[18],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[18],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[18] = newValue;
                  });
                  ;
                }),
            buildText("CA/Calcium (鈣)"),
            UnitDropdownField(
                controller: valueControllers[19],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[19],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[19] = newValue;
                  });
                  ;
                }),
            buildText("K/Potassium (鉀)"),
            UnitDropdownField(
                controller: valueControllers[20],
                dropdownList: bloodUnit2,
                selectedValue: dropdownValue[20],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[20] = newValue;
                  });
                  ;
                }),
            buildText("Na/Sodium (鈉)"),
            UnitDropdownField(
                controller: valueControllers[21],
                dropdownList: bloodUnit2,
                selectedValue: dropdownValue[21],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[21] = newValue;
                  });
                  ;
                }),
            buildText("Cl/Chloride (氯)"),
            UnitDropdownField(
                controller: valueControllers[22],
                dropdownList: bloodUnit2,
                selectedValue: dropdownValue[22],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[22] = newValue;
                  });
                  ;
                }),
            buildText("T4"),
            UnitDropdownField(
                controller: valueControllers[23],
                dropdownList: bloodUnit1,
                selectedValue: dropdownValue[23],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[23] = newValue;
                  });
                  ;
                }),
            buildText("free T4"),
            UnitDropdownField(
                controller: valueControllers[24],
                dropdownList: bloodUnit3,
                selectedValue: dropdownValue[24],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[24] = newValue;
                  });
                  ;
                }),
            buildText("Cortisol"),
            UnitDropdownField(
                controller: valueControllers[25],
                dropdownList: bloodUnit10,
                selectedValue: dropdownValue[25],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[25] = newValue;
                  });
                  ;
                }),
            buildText("T3"),
            UnitDropdownField(
                controller: valueControllers[26],
                dropdownList: bloodUnit7,
                selectedValue: dropdownValue[26],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[26] = newValue;
                  });
                  ;
                }),
            buildText("Zinc"),
            UnitDropdownField(
                controller: valueControllers[27],
                dropdownList: bloodUnit4,
                selectedValue: dropdownValue[27],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[27] = newValue;
                  });
                  ;
                }),
            buildText("Mg/Magnesium"),
            UnitDropdownField(
                controller: valueControllers[28],
                dropdownList: bloodUnit5,
                selectedValue: dropdownValue[28],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[28] = newValue;
                  });
                  ;
                }),
            buildText("Iron"),
            UnitDropdownField(
                controller: valueControllers[29],
                dropdownList: bloodUnit6,
                selectedValue: dropdownValue[29],
                onChanged: (newValue) async {
                  setState(() {
                    dropdownValue[29] = newValue;
                  });
                  ;
                }),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(context, () async {
                  for (var i = 0; i < valueControllers.length; i++) {
                    if (valueControllers[i].text != "") {
                      print("第${i + 1}比資料：");
                      Map<String, dynamic> reqData = {
                        "mem_id": 0,
                        "pet_id": await getPetID(petDropdownValue),
                        "date": date.text,
                        "field_id":
                            await getTestField(testType[i], dropdownValue[i]),
                        "test_type": 2,
                        "value": valueControllers[i].text,
                        "unit": dropdownValue[i],
                        "note": note.text,
                      };
                      // print(reqData);
                      await insertTestRecord(reqData);
                    }
                  }

                  if (!context.mounted) return;
                  Navigator.of(context).pop();
                }),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
