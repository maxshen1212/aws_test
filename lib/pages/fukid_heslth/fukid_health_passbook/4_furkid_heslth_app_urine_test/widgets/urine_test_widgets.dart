import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/utils.dart';

// 表單
class UrineTestForm extends StatefulWidget {
  const UrineTestForm({super.key});

  @override
  UrineTestFormState createState() {
    return UrineTestFormState();
  }
}

class UrineTestFormState extends State<UrineTestForm> {
  final _formKey = GlobalKey<FormState>();
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final TextEditingController sg = TextEditingController();
  final TextEditingController ph = TextEditingController();
  final TextEditingController note = TextEditingController();
  List<String> testType = [
    "S.G.(Specific Gravity)",
    "PH",
    "PRO(Protein)",
    "GLU(Glucose)",
    "KET(Ketone)",
    "BIL(Bilirubin)",
    "URO(Urobilinogen)",
    "BLD(Occult Blood)",
    "LEU(Leukocyte esterase)",
    "NIT(Nitrite)",
  ];
  List radioValue = [
    null,
    null,
    "+",
    "+",
    "+",
    "+",
    "+",
    "+",
    "+",
    "+",
  ];

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期"),
            DatePicker(
              controller: date,
            ),
            buildText("S.G.(Specific Gravity) (比重)"),
            Container(
              margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
              child: Container(
                height: 38.h,
                decoration: BoxDecoration(
                  color: const Color(0xffececec),
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 12.w),
                  child: TextField(
                    controller: sg,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    decoration: const InputDecoration(
                      hintText: "文字輸入",
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      color: Colors.black,
                    ),
                    autocorrect: false,
                    obscureText: false,
                  ),
                ),
              ),
            ),
            // buildTextField("文字輸入", sg),
            buildText("PH (尿酸鹼值)"),
            Container(
              margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
              child: Container(
                height: 38.h,
                decoration: BoxDecoration(
                  color: const Color(0xffececec),
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 12.w),
                  child: TextField(
                    controller: ph,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    decoration: const InputDecoration(
                      hintText: "文字輸入",
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      color: Colors.black,
                    ),
                    autocorrect: false,
                    obscureText: false,
                  ),
                ),
              ),
            ),
            // buildTextField("文字輸入", ph),
            buildText("PRO(Protein) (尿蛋白)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[2],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[2] = newValue!;
                  });
                }),
            buildText("GLU(Glucose) (尿糖)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[3],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[3] = newValue!;
                  });
                }),
            buildText("KET(Ketone) (尿酮)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[4],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[4] = newValue!;
                  });
                }),
            buildText("BIL(Bilirubin) (尿膽紅素)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[5],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[5] = newValue!;
                  });
                }),
            buildText("URO(Urobilinogen) (尿膽紅素原)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[6],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[6] = newValue!;
                  });
                }),
            buildText("BLD(Occult Blood) (潛血)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[7],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[7] = newValue!;
                  });
                }),
            buildText("LEU(Leukocyte esterase) (白血球脂酶)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[8],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[8] = newValue!;
                  });
                }),
            buildText("NIT(Nitrite) (亞硝酸鹽)"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue[9],
                onChanged: (newValue) {
                  setState(() {
                    radioValue[9] = newValue!;
                  });
                }),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(context, () async {
                  dynamic selectRadioValue = 0;
                  for (var i = 0; i < radioValue.length; i++) {
                    if (i == 0) {
                      if (sg.text == "") {
                        continue;
                      } else {
                        radioValue[0] = sg.text;
                        selectRadioValue = sg.text;
                      }
                    } else if (i == 1) {
                      if (ph.text == "") {
                        continue;
                      } else {
                        radioValue[1] = ph.text;
                        selectRadioValue = ph.text;
                      }
                    }
                    switch (radioValue[i]) {
                      case '+':
                        selectRadioValue = 0;
                        break;
                      case '-':
                        selectRadioValue = 1;
                        break;
                      case '++':
                        selectRadioValue = 2;
                        break;
                    }

                    print("第${i + 1}比資料：");
                    Map<String, dynamic> reqData = {
                      "mem_id": 0,
                      "pet_id": await getPetID(petDropdownValue),
                      "date": date.text,
                      "field_id": await getTestField(testType[i], null),
                      "test_type": 1,
                      "value": selectRadioValue,
                      "note": note.text,
                    };

                    await insertTestRecord(reqData);
                    print(reqData);
                  }

                  if (!context.mounted) return;
                  Navigator.of(context).pop();
                }),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
