import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import '../widgets/passbook_widgets.dart';
import 'widgets/else_test_widgets.dart';

class ElseTest extends StatelessWidget {
  const ElseTest({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "其他套件檢查"),
      body: ElseTestForm(),
    );
  }
}
