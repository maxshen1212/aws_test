import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/unit.dart';

// 表單
class ElseTestForm extends StatefulWidget {
  const ElseTestForm({super.key});

  @override
  ElseTestFormState createState() {
    return ElseTestFormState();
  }
}

class ElseTestFormState extends State<ElseTestForm> {
  final _formKey = GlobalKey<FormState>();

  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  String selectTestTypeValue = othersTestTypesList[0];
  final TextEditingController date = TextEditingController();
  final TextEditingController note = TextEditingController();
  var radioValue = "+";

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇檢查"),
            NomalDropdownButton(
              dropdownList: othersTestTypesList,
              selectedValue: selectTestTypeValue,
              onChanged: (newValue) {
                setState(() {
                  selectTestTypeValue = newValue;
                });
              },
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期"),
            DatePicker(
              controller: date,
            ),
            buildText("檢測結果"),
            ThreeOptionsRadioButton(
                selectedValue: radioValue,
                onChanged: (newValue) {
                  setState(() {
                    radioValue = newValue!;
                  });
                }),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(context, () async {
                  int? selectRadioValue = 0;
                  switch (radioValue) {
                    case '+':
                      selectRadioValue = 0;
                      break;
                    case '-':
                      selectRadioValue = 1;
                      break;
                    case '++':
                      selectRadioValue = 2;
                      break;
                  }
                  Map<String, dynamic> reqData = {
                    "mem_id": 0,
                    "pet_id": await getPetID(petDropdownValue),
                    "date": date.text,
                    "field_id":
                        87 + othersTestTypesList.indexOf(selectTestTypeValue),
                    "test_type":
                        5 + othersTestTypesList.indexOf(selectTestTypeValue),
                    "value": selectRadioValue,
                    "note": note.text,
                  };
                  // print(reqData);
                  await insertTestRecord(reqData);

                  if (!context.mounted) return;
                  Navigator.of(context).pop();
                }),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
