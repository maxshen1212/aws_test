import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';

// 表單
class FoodForm extends StatefulWidget {
  const FoodForm({super.key});

  @override
  FoodFormState createState() {
    return FoodFormState();
  }
}

class FoodFormState extends State<FoodForm> {
  final _formKey = GlobalKey<FormState>();
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final TextEditingController quantity = TextEditingController();
  final TextEditingController note = TextEditingController();
  final radioValue = "+";
  static const List<String> recipeList = ["食譜1", "食譜2", "食譜3"];
  String recipeValue = recipeList[0];

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  getRecipeID() {
    if (recipeValue == "食譜1") {
      return 1;
    } else if (recipeValue == "食譜2") {
      return 2;
    } else if (recipeValue == "食譜3") {
      return 3;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇食譜"),
            NomalDropdownButton(
              dropdownList: recipeList,
              selectedValue: recipeValue,
              onChanged: (newValue) {
                setState(() {
                  recipeValue = newValue;
                });
              },
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期"),
            DatePicker(
              controller: date,
            ),
            buildText("份量(g)"),
            buildTextField("文字輸入", quantity),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(
                  context,
                  () async {
                    Map<String, dynamic> reqData = {
                      "pet_id": await getPetID(petDropdownValue),
                      "recipe_id": getRecipeID(),
                      "date": date.text,
                      "quantity": quantity.text,
                      "note": note.text,
                    };
                    print(reqData);
                    final sqlite = Sqlite();
                    await sqlite.insert("pet_eat_record", reqData);

                    if (!context.mounted) return;
                    Navigator.of(context).pop();
                  },
                ),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
