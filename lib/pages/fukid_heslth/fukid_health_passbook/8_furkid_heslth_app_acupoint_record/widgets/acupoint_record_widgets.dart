import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';
import 'package:pet_save_app/utils.dart';

// 表單
class AcupointRecordForm extends StatefulWidget {
  const AcupointRecordForm({super.key});

  @override
  AcupointRecordFormState createState() {
    return AcupointRecordFormState();
  }
}

class AcupointRecordFormState extends State<AcupointRecordForm> {
  final _formKey = GlobalKey<FormState>();
  String petDropdownValue = "";
  late Future<List<String>> petDropdownList;
  final TextEditingController date = TextEditingController();
  final TextEditingController time = TextEditingController();
  final TextEditingController note = TextEditingController();
  final radioValue = "+";
  static const List<String> acupointList = ["穴位1", "穴位2", "穴位3"];
  String acupointValue = acupointList[0];

  @override
  void initState() {
    date.text =
        "${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}";
    petDropdownList = getPetInfo();
    super.initState();
  }

  Future<List<String>> getPetInfo() async {
    final sqlite = Sqlite();
    var petInfo = await sqlite.query("pet");
    List<String> petDropdownList = [];
    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  getAcupointID() {
    if (acupointValue == "穴位1") {
      return 1;
    } else if (acupointValue == "穴位2") {
      return 2;
    } else if (acupointValue == "穴位3") {
      return 3;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.h,
            ),
            buildText("選擇配穴"),
            NomalDropdownButton(
              dropdownList: acupointList,
              selectedValue: acupointValue,
              onChanged: (newValue) {
                setState(() {
                  acupointValue = newValue;
                });
              },
            ),
            buildText("選擇毛孩"),
            FutureBuilder<List<String>>(
              future: petDropdownList,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  List<String> petDropdownList = snapshot.data!;
                  petDropdownValue = petDropdownValue == ""
                      ? petDropdownList[0]
                      : petDropdownValue;
                  print("下拉選項:$petDropdownValue");
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                }
              },
            ),
            buildText("日期"),
            DatePicker(
              controller: date,
            ),
            buildText("操作時間"),
            // buildTextField("文字輸入", time),
            Container(
              margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
              child: Container(
                height: 38.h,
                decoration: BoxDecoration(
                  color: const Color(0xffececec),
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 12.w),
                  child: TextField(
                    controller: time,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      hintText: "時間輸入",
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      color: Colors.black,
                    ),
                    autocorrect: false,
                    obscureText: false,
                  ),
                ),
              ),
            ),
            buildText("備註"),
            buildTextField("文字輸入", note),
            SizedBox(
              height: 30.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                addRecordButton(
                  context,
                  () async {
                    Map<String, dynamic> reqData = {
                      "pet_id": await getPetID(petDropdownValue),
                      "matching_acupoint_id": getAcupointID(),
                      "date": date.text,
                      "time": int.parse(time.text),
                      "note": note.text,
                    };
                    // print(reqData);
                    final sqlite = Sqlite();
                    await sqlite.insert("pet_acupoint_record", reqData);

                    if (!context.mounted) return;
                    Navigator.of(context).pop();
                  },
                ),
                cancelButton(context)
              ],
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
