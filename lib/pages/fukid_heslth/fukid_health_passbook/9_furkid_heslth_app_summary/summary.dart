import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import '../widgets/passbook_widgets.dart';
import 'widgets/summary_widgets.dart';

class Summary extends StatelessWidget {
  const Summary({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPassbookAppBar(context, "摘要紀錄"),
      body: SummaryForm(),
    );
  }
}
