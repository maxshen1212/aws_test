import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';

// ignore: non_constant_identifier_names
Future<dynamic> API(context, reqdata, address) async {
  // print(reqdata);
  var data = reqdata;
  dynamic responseData;
  try {
    final progress = ProgressHUD.of(context);
    progress?.showWithText('');
    String url = 'http://test.yous.tw:9487/normal/user/$address';
    Response response = await Dio().post(
      url,
      data: data,
      options: Options(
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        },
      ),
    );
    if (response.statusCode == 201) {
      progress?.dismiss();
      responseData = jsonDecode(response.toString());
    } else {
      print("API狀態碼錯誤");
    }
  } catch (e) {
    print(e);
  }
  return Future<dynamic>.value(responseData);
}
