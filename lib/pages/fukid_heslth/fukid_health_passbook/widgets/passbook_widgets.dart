import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:sqflite/sqflite.dart';
import '../../../../../common/values/colors.dart';
import '../../../../utils.dart';

// 導覽列
AppBar furkidHeslthPassbookAppBar(BuildContext context, String names) {
  return AppBar(
    automaticallyImplyLeading: false,
    centerTitle: true,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
        decoration: const BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            AppColors.primaryLogo,
            Colors.purple,
          ]),
    )),
    title: Text(
      names,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: SizedBox(
        width: 20.w,
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 20.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

// 文字輸入匡
Widget buildTextField(String hintText, TextEditingController controller) {
  return Container(
    margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
    child: Container(
      height: 38.h,
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: Container(
        margin: EdgeInsets.only(left: 12.w),
        child: TextField(
          controller: controller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: hintText,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
          style: SafeGoogleFont(
            'Inter',
            fontSize: 14.sp,
            fontWeight: FontWeight.w200,
            color: Colors.black,
            letterSpacing: 0.2,
          ),
          autocorrect: false,
          obscureText: false,
        ),
      ),
    ),
  );
}

// 文字
Widget buildText(text) {
  return Container(
    margin: EdgeInsets.only(top: 20.h, left: 40.w),
    child: Text(
      text,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}

// 具單位及下拉選單之輸入匡
class UnitDropdownField extends StatefulWidget {
  final TextEditingController controller;
  final List<String> dropdownList;
  final String selectedValue;
  final dynamic onChanged;

  const UnitDropdownField({
    super.key,
    required this.controller,
    required this.dropdownList,
    required this.selectedValue,
    required this.onChanged,
  });

  @override
  State<UnitDropdownField> createState() => _UnitDropdownFieldState();
}

class _UnitDropdownFieldState extends State<UnitDropdownField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 12.w),
              child: TextField(
                controller: widget.controller,
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                decoration: const InputDecoration(
                  hintText: "請輸入數值",
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                // autocorrect: false,
                // obscureText: false,
              ),
            ),
          ),
          DropdownButton<String>(
            padding: EdgeInsets.only(left: 15.w),
            value: widget.selectedValue,
            dropdownColor: const Color.fromARGB(255, 202, 202, 202), // 更改背景顏色s
            onChanged: widget.onChanged,
            underline: Container(
              height: 0.h,
            ),
            iconSize: 40.w,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: Colors.black,
            ),
            items: widget.dropdownList
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                alignment: Alignment.centerLeft,
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        ],
      ),
    );
  }
}

// 具單位之輸入匡
Widget unitField(controller, unit) {
  return StatefulBuilder(builder: (context, setState) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 12.w),
              child: TextField(
                controller: controller,
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                decoration: const InputDecoration(
                  hintText: "文字輸入",
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w200,
                  color: Colors.black,
                ),
                autocorrect: false,
                obscureText: false,
              ),
            ),
          ),
          Container(
            width: 40.w,
            margin: EdgeInsets.only(right: 25.w),
            child: Text("$unit"),
          )
        ],
      ),
    );
  });
}

// 三個選項 RadioButton
class ThreeOptionsRadioButton extends StatefulWidget {
  final String selectedValue;
  final dynamic onChanged;
  const ThreeOptionsRadioButton({
    super.key,
    required this.selectedValue,
    required this.onChanged,
  });

  @override
  State<ThreeOptionsRadioButton> createState() =>
      _ThreeOptionsRadioButtonState();
}

class _ThreeOptionsRadioButtonState extends State<ThreeOptionsRadioButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(left: 30.w, top: 15.h),
      child: Row(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Radio(
                value: "+",
                groupValue: widget.selectedValue,
                onChanged: widget.onChanged,
              ),
              Text(
                "+",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w700,
                  color: const Color(0xff000000),
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Radio(
                value: "-",
                groupValue: widget.selectedValue,
                onChanged: widget.onChanged,
              ),
              Text(
                "-",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w700,
                  color: const Color(0xff000000),
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Radio(
                value: "++",
                groupValue: widget.selectedValue,
                onChanged: widget.onChanged,
              ),
              Text(
                "++",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w700,
                  color: const Color(0xff000000),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

// 日期範圍選擇器
class DateRangePicker extends StatelessWidget {
  const DateRangePicker({super.key, required this.controller});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      child: Container(
        height: 38.h,
        decoration: BoxDecoration(
          color: const Color(0xffececec),
          borderRadius: BorderRadius.all(Radius.circular(10.w)),
        ),
        child: Container(
          margin: EdgeInsets.only(left: 12.w),
          child: TextField(
            controller: controller,
            onTap: () async {
              var dateRange = await showRangeDatePickerForTheme(context);
              if (dateRange?.start.year != null &&
                  dateRange?.start.month != null &&
                  dateRange?.start.day != null &&
                  dateRange?.end.day != null &&
                  dateRange?.end.day != null &&
                  dateRange?.end.day != null) {
                controller.text =
                    "${dateRange?.start.year}/${dateRange?.start.month}/${dateRange?.start.day} - ${dateRange?.end.year}/${dateRange?.end.month}/${dateRange?.end.day}";
                print(controller.text);
              }
            },
            keyboardType: TextInputType.multiline,
            decoration: const InputDecoration(
              hintText: "請選擇時間",
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: Colors.black,
            ),
            autocorrect: false,
            obscureText: false,
          ),
        ),
      ),
    );
  }
}

Future<DateTimeRange?> showRangeDatePickerForTheme(BuildContext context) {
  return showDateRangePicker(
    context: context,
    locale: const Locale("zh", "TW"),
    firstDate: DateTime.now().subtract(const Duration(days: 180)), // 开始日期
    lastDate: DateTime.now().add(const Duration(days: 180)), // 结束日期
    currentDate: DateTime.now(), // 当前日期
    initialDateRange: DateTimeRange(
      start: DateTime.now(),
      end: DateTime.now(),
    ), // 初始时间范围
    initialEntryMode: DatePickerEntryMode
        .input, // DatePickerEntryMode 日历弹框样式 calendar: 默认显示日历，可切换成输入模式，input:默认显示输入模式，可切换到日历，calendarOnly:只显示日历，inputOnly:只显示输入模式
    helpText: "請選擇日期範圍", // 左上角提示语
    cancelText: "取消", // 取消按钮 文案
    confirmText: "確定", // 确认按钮 文案
    saveText: "完成", // 保存按钮 文案
    errorFormatText: "輸入格式有誤", // 格式错误时下方提示
    errorInvalidRangeText: "開始日期不可以在結束日期之後", // 输入日期范围不合法 开始日期在结束日期之后
    errorInvalidText: "輸入不合範圍", // 输入了不在 first 与 last 之间的日期提示语
    fieldStartHintText: "請輸入開始日期", // 开始日期 输入框默认提示语
    fieldEndHintText: "請輸入结束日期", // 结束日期 输入框默认提示语
    fieldStartLabelText: "開始日期", // 开始日期 输入框上方提示语
    fieldEndLabelText: "結束日期", // 结束日期
    builder: (context, child) {
      return Theme(
        data: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        child: child!,
      );
    },
  );
}

// 日期選擇器
class DatePicker extends StatelessWidget {
  const DatePicker({super.key, required this.controller});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      child: Container(
        height: 38.h,
        decoration: BoxDecoration(
          color: const Color(0xffececec),
          borderRadius: BorderRadius.all(Radius.circular(10.w)),
        ),
        child: Container(
          margin: EdgeInsets.only(left: 12.w),
          child: TextField(
            controller: controller,
            onTap: () async {
              var date = await showDatePickerForTheme(context);
              if (date?.year != null &&
                  date?.month != null &&
                  date?.day != null) {
                controller.text = "${date?.year}/${date?.month}/${date?.day}";
              }
              // print(date);
            },
            keyboardType: TextInputType.multiline,
            decoration: const InputDecoration(
              hintText: "請選擇時間",
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: Colors.black,
            ),
            autocorrect: false,
            obscureText: false,
          ),
        ),
      ),
    );
  }
}

Future<DateTime?> showDatePickerForTheme(BuildContext context) {
  return showDatePicker(
    context: context,
    locale: const Locale("zh", "TW"),
    initialDate: DateTime.now(), // 初始化选中日期
    firstDate: DateTime.now().subtract(const Duration(days: 180)), // 开始日期
    lastDate: DateTime.now().add(const Duration(days: 180)), // 结束日期
    currentDate: DateTime.now(), // 当前日期
    initialEntryMode: DatePickerEntryMode
        .input, // 日历弹框样式 calendar: 默认显示日历，可切换成输入模式，input:默认显示输入模式，可切换到日历，calendarOnly:只显示日历，inputOnly:只显示输入模式
    selectableDayPredicate: (dayTime) {
      // 自定义哪些日期可选
      // if (dayTime == DateTime(2022, 5, 6) || dayTime == DateTime(2022, 6, 8)) {
      //   return false;
      // }
      return true;
    },
    builder: (context, child) {
      return Theme(
        data: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        child: child!,
      );
    },
    helpText: "請選擇日期", // 左上角提示文字
    cancelText: "取消", // 取消按钮 文案
    confirmText: "確認", // 确认按钮 文案
    initialDatePickerMode: DatePickerMode.day, // 日期选择模式 默认为天
    useRootNavigator: false, // 是否使用根导航器
    textDirection: TextDirection.ltr, // 水平方向 显示方向 默认 ltr
  );
}

// 時間點選擇器
class TimePicker extends StatelessWidget {
  const TimePicker({super.key, required this.controller});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      child: Container(
        height: 38.h,
        decoration: BoxDecoration(
          color: const Color(0xffececec),
          borderRadius: BorderRadius.all(Radius.circular(10.w)),
        ),
        child: Container(
          margin: EdgeInsets.only(left: 12.w),
          child: TextField(
            controller: controller,
            onTap: () async {
              var time = await showTimePickerForTheme(context);
              if (time != null) {
                if (!context.mounted) return;
                String formattedTime = _formatTime(time);
                controller.text = formattedTime;
              }
              // print(time);
            },
            keyboardType: TextInputType.multiline,
            decoration: const InputDecoration(
              hintText: "請選擇時間",
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: Colors.black,
            ),
            autocorrect: false,
            obscureText: false,
          ),
        ),
      ),
    );
  }

  // 格式化選擇的時間為24小時制的字串
  String _formatTime(TimeOfDay time) {
    int hour = time.hour;
    int minute = time.minute;
    // 使用String的padLeft函數來確保小時和分鐘始終有兩位數
    return '${hour.toString().padLeft(2, '0')}:${minute.toString().padLeft(2, '0')}';
  }
}

Future<TimeOfDay?> showTimePickerForTheme(BuildContext context) {
  return showTimePicker(
    context: context,
    initialTime: TimeOfDay.now(), // 初始化时间
    initialEntryMode: TimePickerEntryMode.input, // 时间选择模式 dial input
    useRootNavigator: true, // 是否使用根导航 默认true
    cancelText: "取消", // 取消按钮 文案
    confirmText: "確認", // 确认按钮 文案
    helpText: "請選擇時間", // 左上角提示语
    errorInvalidText: "輸入時間不合法", // 属于时间不合法 提示语
    hourLabelText: "小時", // 小时 提示语
    minuteLabelText: "分鐘", // 分钟 提示语
    onEntryModeChanged: (mode) {
      print("Mode Changed $mode");
    },
    builder: (context, child) {
      return Theme(
        data: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        child: child!,
      );
    },
  );
}

// 非同步下拉選單
class AsyncDropdown extends StatefulWidget {
  final List<String> dropdownList;
  final String selectedValue;
  final dynamic onChanged;

  const AsyncDropdown({
    required this.dropdownList,
    required this.selectedValue,
    required this.onChanged,
    super.key,
  });

  @override
  State<AsyncDropdown> createState() => _AsyncDropdownState();
}

class _AsyncDropdownState extends State<AsyncDropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: DropdownButton<String>(
        padding: EdgeInsets.only(left: 15.w),
        isExpanded: true,
        value: widget.selectedValue,
        dropdownColor: const Color.fromARGB(255, 202, 202, 202),
        onChanged: widget.onChanged,
        underline: Container(
          height: 0.h,
        ),
        iconSize: 40.w,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        items:
            widget.dropdownList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            alignment: Alignment.centerLeft,
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }
}

Future<List<String>> getPetInfo() async {
  final sqlite = Sqlite();
  var petInfo = await sqlite.query("pet");
  List<String> petDropdownList = [];
  for (var pet in petInfo) {
    petDropdownList.add(pet["name"]);
  }
  return petDropdownList;
}

// 一般下拉式選單
class NomalDropdownButton extends StatefulWidget {
  final List<String> dropdownList;
  final String selectedValue;
  final dynamic onChanged;

  const NomalDropdownButton({
    required this.dropdownList,
    required this.selectedValue,
    required this.onChanged,
    super.key,
  });

  @override
  State<NomalDropdownButton> createState() => _NomalDropdownButtonState();
}

class _NomalDropdownButtonState extends State<NomalDropdownButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(left: 35.w, right: 35.w, top: 5.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: DropdownButton<String>(
        padding: EdgeInsets.only(left: 15.w),
        isExpanded: true,
        value: widget.selectedValue,
        dropdownColor: const Color.fromARGB(255, 202, 202, 202), // 更改背景顏色
        onChanged: widget.onChanged,
        underline: Container(
          height: 0.h,
        ),
        iconSize: 40.w,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        items:
            widget.dropdownList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            alignment: Alignment.centerLeft,
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }
}

// 確認按鈕(要將對應的API傳入)
Widget addRecordButton(BuildContext context, dynamic onPressed) {
  return SizedBox(
    width: 95.w,
    height: 40.h,
    child: TextButton(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: const Color(0xffe4007f),
          borderRadius: BorderRadius.circular(8.r),
        ),
        child: Center(
          child: Text(
            "新增",
            textAlign: TextAlign.center,
            style: SafeGoogleFont('Inter',
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
                letterSpacing: 0.8.r,
                color: const Color(0xffffffff),
                decoration: TextDecoration.none),
          ),
        ),
      ),
    ),
  );
}

// 取消按鈕
Widget cancelButton(BuildContext context) {
  return Container(
    width: 95.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 30.w),
    child: TextButton(
        onPressed: () {
          Navigator.pop(context);
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: const Color(0xffe4007f),
            borderRadius: BorderRadius.circular(8.r),
          ),
          child: Center(
            child: Text(
              "取消",
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: const Color(0xffffffff),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

// 查詢按鈕(要將對應的API傳入)
Widget queryRecordButton(BuildContext context, Function getFunc) {
  return Container(
    width: 95.w,
    height: 40.h,
    child: TextButton(
      onPressed: () async {
        // dynamic responseData = await API(context, reqdata, address);
        // print(responseData);
        await getFunc();
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        // margin: EdgeInsets.only(left: 14.w),
        decoration: BoxDecoration(
          color: const Color(0xffe4007f),
          borderRadius: BorderRadius.circular(8.r),
        ),
        child: Center(
          child: Text(
            "查詢",
            textAlign: TextAlign.center,
            style: SafeGoogleFont('Inter',
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
                letterSpacing: 0.8.r,
                color: const Color(0xffffffff),
                decoration: TextDecoration.none),
          ),
        ),
      ),
    ),
  );
}

// format現在時間為資料庫格式
String formatNowTime() {
  DateTime now = DateTime.now();
  String time =
      "${now.year}/${now.month}/${now.day} ${_twoDigits(now.hour)}:${_twoDigits(now.minute)}:${_twoDigits(now.second)}";
  return time;
}

String _twoDigits(int n) {
  if (n >= 10) {
    return "$n";
  } else {
    return "0$n";
  }
}

// 取得寵物ID
Future<dynamic> getPetID(name) async {
  final sqlite = Sqlite();
  Database? db;
  db = await sqlite.connectDB();
  // 先搜尋mem_id
  List<Map<String, Object?>> memResp = await db.query(
    "member",
    columns: ["base_id"],
  );
  var memID = memResp[0]["base_id"];
  // 再搜尋pet_id
  List<Map<String, Object?>> resp = await db.query(
    "pet",
    columns: ["id"],
    // 通過 where 傳遞 變數 可以防止 SQL 注入
    // 注意 要使用注入符號
    where: "name = ? AND mem_id = ?",
    whereArgs: ["$name", memID],
  );
  // 空值檢查，不然陣列是空的會報錯
  try {
    // print("petid:${resp[0]["id"]}");
    return resp[0]["id"];
  } catch (e) {
    print(e);
    return null;
  }
}

// 取得檢測單位ID
Future<dynamic> getTestField(name, unit) async {
  final sqlite = Sqlite();
  Database? db;
  db = await sqlite.connectDB();
  if (unit != null) {
    List<Map<String, Object?>> resp = await db.query(
      "test_fields_new",
      columns: ["id"],
      where: "name = ? AND unit = ?",
      whereArgs: ["$name", "$unit"],
    );

    return resp[0]["id"];
  } else {
    List<Map<String, Object?>> resp = await db.query(
      "test_fields_new",
      columns: ["id"],
      where: "name = ? ",
      whereArgs: ["$name"],
    );
    return resp[0]["id"];
  }
}

// 插入紀錄
Future insertTestRecord(reqData) async {
  final sqlite = Sqlite();
  await sqlite.insert("test_record", reqData);
}

// 搜尋紀錄(單一欄位、多欄位、喝水量、摘要)
Future<dynamic> searchRecord(
    petName, String testDate, testItem, testType) async {
  Database? db;
  final sqlite = Sqlite();
  db = await sqlite.connectDB();
  List testDateList = testDate.split(RegExp(" - "));
  dynamic petID = await getPetID(petName);
  try {
    // 單一欄位查詢
    if (testItem != null && testType != null) {
      // 抓取此項目所有單位的ID，因為有些項目有多個單位
      List<Map<String, dynamic>> itemID = await db.query(
        "test_fields_new",
        columns: ["id"],
        where: "name = ?",
        whereArgs: ["$testItem"],
      );

      // 如果檢查項目有多個單位
      if (itemID.isNotEmpty) {
        List<Map<String, dynamic>> resp = await db.rawQuery(
          '''
            SELECT date, value, unit FROM test_record WHERE
            (field_id BETWEEN ? AND ?)
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?
            AND TRIM(value) <> "";
          ''',
          [
            itemID.first["id"],
            itemID.last["id"],
            testDateList[0],
            testDateList[1],
            petID,
          ],
        );

        // for (var a in resp) {
        //   print("日期:${a["date"]} 值:${a["value"]}${a["unit"]}");
        // }
        if (resp.isNotEmpty) {
          return resp;
        }
        return null;
      }
      // 其餘檢查項目只有一個單位
      else {
        List<Map<String, dynamic>> typeID = await db.query(
          "test_type",
          columns: ["id"],
          where: "name = ?",
          whereArgs: ["$testType"],
        );
        List<Map<String, dynamic>> resp = await db.rawQuery(
          '''
            SELECT date, value, unit FROM test_record WHERE
            field_id = ?
            AND (date BETWEEN ? AND ?)
            AND pet_id = ?
            AND TRIM(value) <> "";
          ''',
          [
            typeID[0]["id"],
            testDateList[0],
            testDateList[1],
            petID,
          ],
        );

        // for (var a in resp) {
        //   print("日期:${a["date"]} 值:${a["value"]}${a["unit"]}");
        // }
        if (resp.isNotEmpty) {
          return resp;
        }
        return null;
      }
    }
    // 多欄位查詢
    else if (testItem == null && testType == null) {
      List<Map<String, dynamic>> data = await db.rawQuery('''
        SELECT date, value, unit, field_id  FROM test_record WHERE
        (date BETWEEN '${testDateList[0]}' AND '${testDateList[1]}')
        AND pet_id = $petID
        AND TRIM(value) <> "";
        ''');

      // print("資料庫查詢結果 $data");
      if (data.isNotEmpty) {
        List<Map> resp = data.map((map) => Map.from(map)).toList();
        for (var a in resp) {
          // 先查詢檢查項目
          var item = await db.query(
            "test_fields_new",
            columns: ["name"],
            where: "id = ?",
            whereArgs: ["${a["field_id"]}"],
          );
          // 插入map
          a["item"] = item[0]["name"];
          // 再查詢檢查類別id
          var typeId = await db.query(
            "test_fields_new",
            columns: ["test_type"],
            where: "id = ?",
            whereArgs: ["${a["field_id"]}"],
          );
          // 再用檢查類別id，查詢檢查類別name
          var type = await db.query(
            "test_type",
            columns: ["name"],
            where: "id = ?",
            whereArgs: ["${typeId[0]["test_type"]}"],
          );
          // 插入map
          a["type"] = type[0]["name"];
          a.remove("field_id");
          // print(
          //     "日期:${a["date"]} 檢測：${a["type"]} 欄位：${a["item"]} 值:${a["value"]}${a["unit"]}");
        }

        return resp;
      }
      return null;
    }
    // 喝水量查詢
    else if (testItem == null && testType == "drink") {
      List<Map<String, dynamic>> data = await db.rawQuery(
        '''
          SELECT date, quantity FROM pet_drink_record WHERE
          (date BETWEEN ? AND ?)
          AND pet_id = ?
          AND TRIM(quantity) <> "";
        ''',
        [
          testDateList[0],
          testDateList[1],
          petID,
        ],
      );
      List<Map> resp = data.map((map) => Map.from(map)).toList();
      for (var a in resp) {
        a["unit"] = "c.c";
        a["value"] = a["quantity"];
        a.remove("quantity");
      }
      if (resp.isNotEmpty) {
        return resp;
      }
      return null;
    }
    // 摘要查詢
    else if (testItem == null && testType == "note") {
      List<Map<String, dynamic>> data = await db.rawQuery(
        '''
          SELECT date, note FROM pet_note_record WHERE
          (date BETWEEN ? AND ?)
          AND pet_id = ?
          AND TRIM(note) <> "";
        ''',
        [
          testDateList[0],
          testDateList[1],
          petID,
        ],
      );
      List<Map> resp = data.map((map) => Map.from(map)).toList();
      for (var a in resp) {
        a["unit"] = "";
        a["value"] = a["note"];
        a.remove("note");
      }
      if (resp.isNotEmpty) {
        return resp;
      }
      return null;
    }
    // 飲食紀錄查詢
    else if (testItem == null && testType == "eat") {
      List<Map<String, dynamic>> resp = await db.rawQuery(
        '''
          SELECT date, quantity, recipe_id FROM pet_eat_record WHERE
          (date BETWEEN ? AND ?)
          AND pet_id = ?
          AND TRIM(quantity) <> "";
        ''',
        [
          testDateList[0],
          testDateList[1],
          petID,
        ],
      );

      if (resp.isNotEmpty) {
        return resp;
      }
      return null;
    }
    // 配穴紀錄查詢
    else if (testItem == null && testType == "acupoint") {
      List<Map<String, dynamic>> resp = await db.rawQuery(
        '''
          SELECT date, time, matching_acupoint_id FROM pet_acupoint_record WHERE
          (date BETWEEN ? AND ?)
          AND pet_id = ?
          AND TRIM(time) <> "";
        ''',
        [
          testDateList[0],
          testDateList[1],
          petID,
        ],
      );
      if (resp.isNotEmpty) {
        return resp;
      }
      return null;
    }
  } catch (e) {
    print("資料庫錯誤：$e");
  }
}
