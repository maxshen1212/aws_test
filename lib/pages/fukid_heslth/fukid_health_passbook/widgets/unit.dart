import 'package:http/http.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/widgets/passbook_widgets.dart';

// CBC檢測單位
final List<String> cbcUnit1 = <String>['M/µL', '10^12/L', '10^6/mL', '10^6/µL'];
final List<String> cbcUnit2 = <String>['%', 'L/L'];
final List<String> cbcUnit3 = <String>['g/dL', 'g/L'];
final List<String> cbcUnit4 = <String>['K/µL', '10^9/L', '10^3/mL', '10^3/µL'];
// 血液檢測單位
final List<String> bloodUnit1 = <String>['µg/dL', 'nmol/L', 'mg/dL'];
final List<String> bloodUnit2 = <String>['mEq/L', 'mmol/L'];
final List<String> bloodUnit3 = <String>['ng/dL', 'pmol/L'];
final List<String> bloodUnit4 = <String>['mg/dL', 'mmol/L'];
final List<String> bloodUnit5 = <String>['mg/dL', 'mEq/L', 'mmol/L'];
final List<String> bloodUnit6 = <String>['mg/dL', 'mg/kg'];
final List<String> bloodUnit7 = <String>['ng/dL', 'mmol/L', 'ng/mL'];
final List<String> bloodUnit8 = <String>['g/dL', 'g/L'];
final List<String> bloodUnit9 = <String>['mg/dL', 'µmol/L'];
final List<String> bloodUnit10 = <String>['µg/dL', 'nmol/L'];
// 所有檢查類別
final List<String> allTestTypesList = [
  "CBC檢測",
  "血液生化檢測",
  "尿液檢測",
  "犬心絲蟲-成蟲",
  "艾利希體-犬型",
  "萊姆病",
  "嗜吞噬球無形體 / 片狀邊蟲",
  "犬小病毒",
  "犬冠狀病毒",
  "犬瘟熱",
  "梨形鞭毛蟲",
  "犬腺病毒",
  "犬流感病毒",
  "犬小焦蟲",
  "艾利希體-血小板型",
  "犬血巴東體",
  "鉤端螺旋體",
  "胰臟炎",
  "貓愛滋病毒",
  "貓白血病",
  "貓心絲蟲",
  "貓瘟病毒",
  "貓冠狀病毒",
  "弓蟲",
  "貓血巴東體",
  "貓披衣菌",
  "貓皰疹病毒",
  "貓卡利西病毒",
];
// 其他檢查類別
final List<String> othersTestTypesList = [
  "犬心絲蟲-成蟲",
  "艾利希體-犬型",
  "萊姆病",
  "嗜吞噬球無形體 / 片狀邊蟲",
  "犬小病毒",
  "犬冠狀病毒",
  "犬瘟熱",
  "梨形鞭毛蟲",
  "犬腺病毒",
  "犬流感病毒",
  "犬小焦蟲",
  "艾利希體-血小板型",
  "犬血巴東體",
  "鉤端螺旋體",
  "胰臟炎",
  "貓愛滋病毒",
  "貓白血病",
  "貓心絲蟲",
  "貓瘟病毒",
  "貓冠狀病毒",
  "弓蟲",
  "貓血巴東體",
  "貓披衣菌",
  "貓皰疹病毒",
  "貓卡利西病毒",
];
List<String> cbcTestItems = [
  "RBC",
  "HCT",
  "HGB",
  "MCV",
  "MCH",
  "MCHC",
  "RDW",
  "RETIC",
  "%RETIC",
  "WBC",
  "NEU",
  "LYM",
  "MONO",
  "EOS",
  "BASO",
  "PLT",
  "MPV",
  "PDW",
  "PCT",
  "%NEU",
  "%LYM",
  "%MONO",
  "%EOS",
  "%BASO",
];
List<String> biochemistryTestItems = [
  "CRE",
  "BUN/Blood Urea Nitrogen",
  "SDMA",
  "ALT(SGPT)",
  "AST(SGOT)",
  "ALKP/Alk Phosphatase",
  "GGT/Gamma GlutamyI Transpeptidase",
  "TBIL/Total Bilirubin",
  "NH3(Ammonia)",
  "AMYL/Amylase",
  "LIPA/Lipase",
  "TRIG/Triglycerides",
  "CK/CPK",
  "TP/Total Protein",
  "ALB/Albumin",
  "GLB/Globulin",
  "CHOL/Cholesterol",
  "GLU/Glucose",
  "PHOS/Phosphorus",
  "CA/Calcium",
  "K/Potassium",
  "Na/Sodium",
  "Cl/Chloride",
  "T4",
  "free T4",
  "Cortisol",
  "T3",
  "Zinc",
  "Mg/Magnesium",
  "Iron"
];
List<String> urineTestItems = [
  "S.G.(Specific Gravity)",
  "PH",
  "PRO(Protein)",
  "GLU(Glucose)",
  "KET(Ketone)",
  "BIL(Bilirubin)",
  "URO(Urobilinogen)",
  "BLD(Occult Blood)",
  "LEU(Leukocyte esterase)",
  "NIT(Nitrite)",
];
