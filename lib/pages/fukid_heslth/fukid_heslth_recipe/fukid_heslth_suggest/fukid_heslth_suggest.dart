import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/fukid_heslth_suggest/widgets/fukid_heslth_suggest_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';

class FurKidHeslthSuggest extends StatefulWidget {
  const FurKidHeslthSuggest({super.key});

  @override
  State<FurKidHeslthSuggest> createState() => _FurKidHeslthSuggest();
}

class _FurKidHeslthSuggest extends State<FurKidHeslthSuggest> {
  bool catcolor = true;
  bool dogcolor = false;
  void greyorred(String doc) {
    setState(() {
      if (doc == "犬") {
        if (dogcolor == true) {
          dogcolor = !dogcolor;
          catcolor = !catcolor;
        }
      }
      if (doc == "貓") {
        if (catcolor == true) {
          catcolor = !catcolor;
          dogcolor = !dogcolor;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        drawerEdgeDragWidth: 220, //用拉的也能彈出設定
        endDrawer: SizedBox(
          width: 90.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        appBar: furkidHeslthSuggest("熱量建議", context),
        body: SingleChildScrollView(
          child: Column(
            children: [
              dogAndCat(dogcolor, catcolor, greyorred),
              Container(
                  //color: Colors.amber,
                  margin: EdgeInsets.only(top: 10.h),
                  width: 310.w,
                  //color: Colors.brown,
                  child: Column(
                    children: [
                      bigtitle(dogcolor),
                      dogcolor ? cat() : dog(),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
