import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/utils.dart';

PreferredSize furkidHeslthSuggest(String names, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      leading: goBack(),
      leadingWidth: 100.w,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                AppColors.primaryLogo,
                Colors.purple,
              ]),
        ),
        //margin: EdgeInsets.only(left: 25.w, top: 25.h),
      ),
      actions: [newSetUp()],
      centerTitle: true,
      automaticallyImplyLeading: false,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).pushNamed("furkid_heslth_home");
        Navigator.pop(context);
      },
      child: Padding(
        padding: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  });
}

Widget dogAndCat(bool dogcolor, bool catcolor, Function greyorred) {
  return Container(
    margin: EdgeInsets.only(top: 20.h),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        TextButton(
          onPressed: () {
            greyorred("犬");
          },
          style: TextButton.styleFrom(
            padding: EdgeInsets.zero,
          ),
          child: Container(
            width: 130.w,
            height: 40.h,
            decoration: BoxDecoration(
              color: dogcolor ? Color(0xff929292) : AppColors.primaryLogo,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                '犬',
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: Color(0xffffffff),
                ),
              ),
            ),
          ),
        ),
        TextButton(
          onPressed: () {
            greyorred("貓");
          },
          style: TextButton.styleFrom(
            padding: EdgeInsets.zero,
          ),
          child: Container(
            width: 130.w,
            height: 40.h,
            decoration: BoxDecoration(
              color: catcolor ? Color(0xff929292) : AppColors.primaryLogo,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                '貓',
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: Color(0xffffffff),
                ),
              ),
            ),
          ),
        )
      ],
    ),
  );
}

Widget bigtitle(bool dogcolor) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              //color: Colors.amberAccent,
              child: Padding(
                padding: EdgeInsets.only(left: 20.w),
                child: Text(
                  dogcolor ? '貓' : '犬',
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.8,
                    color: Color(0xff000000),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              //color: Colors.blue,
              child: Text(
                "公斤數",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              //color: Colors.deepOrange,
              child: Text(
                "所需熱量",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              //color: Colors.pink,
              child: Column(
                children: [
                  Text(
                    "每公斤",
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                  Text(
                    "所需熱量",
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      Container(
        margin: EdgeInsets.only(top: 10.h),
        width: double.infinity,
        height: 2.5.h,
        color: Color.fromARGB(157, 48, 45, 45),
      ),
    ],
  );
}

Widget smalltitle(String doc, String kg, String kcal, String kgkcal) {
  return Container(
    margin: EdgeInsets.only(top: 15.h),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Container(
                //color: Colors.amberAccent,
                child: Padding(
                  padding: EdgeInsets.only(left: 20.w),
                  child: Text(
                    doc,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                //color: Colors.blue,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.w),
                  child: Text(
                    kg,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                //color: Colors.deepOrange,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.w),
                  child: Text(
                    kcal,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                //color: Color.fromARGB(255, 58, 214, 97),
                child: Padding(
                  padding: EdgeInsets.only(left: 15.w),
                  child: Text(
                    kgkcal,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h),
          height: 1.5.h,
          color: Color.fromARGB(157, 196, 190, 190),
        ),
      ],
    ),
  );
}

Widget dog() {
  return Container(
    child: Column(children: [
      smalltitle('犬', '1.0kg', '132kcal', '132kcal'),
      smalltitle('犬', '2.0kg', '214kcal', '107kcal'),
      smalltitle('犬', '3.0kg', '285kcal', '95kcal'),
      smalltitle('犬', '4.0kg', '348kcal', '87kcal'),
      smalltitle('犬', '5.0kg', '405kcal', '81kcal'),
      smalltitle('犬', '6.0kg', '462kcal', '77kcal'),
      smalltitle('犬', '7.0kg', '518kcal', '74kcal'),
      smalltitle('犬', '8.0kg', '568kcal', '71kcal'),
      smalltitle('犬', '9.0kg', '612kcal', '68kcal'),
      smalltitle('犬', '10.0kg', '660kcal', '66kcal'),
      smalltitle('犬', '11.0kg', '704kcal', '64kcal'),
      smalltitle('犬', '12.0kg', '756kcal', '63kcal'),
      smalltitle('犬', '13.0kg', '793kcal', '61kcal'),
      smalltitle('犬', '14.0kg', '840kcal', '60kcal'),
      smalltitle('犬', '15.0kg', '885kcal', '59kcal'),
      smalltitle('犬', '16.0kg', '912kcal', '57kcal'),
      smalltitle('犬', '17.0kg', '952kcal', '56kcal'),
      smalltitle('犬', '18.0kg', '990kcal', '55kcal'),
      smalltitle('犬', '19.0kg', '1045kcal', '55kcal'),
      smalltitle('犬', '20.0kg', '1080kcal', '54kcal'),
      smalltitle('犬', '21.0kg', '1113kcal', '53kcal'),
      smalltitle('犬', '22.0kg', '1144kcal', '52kcal'),
      smalltitle('犬', '23.0kg', '1196kcal', '52kcal'),
      smalltitle('犬', '24.0kg', '1224kcal', '51kcal'),
      smalltitle('犬', '25.0kg', '1250kcal', '50kcal'),
      smalltitle('犬', '26.0kg', '1300kcal', '50kcal'),
      smalltitle('犬', '27.0kg', '1323kcal', '49kcal'),
      smalltitle('犬', '28.0kg', '1372kcal', '49kcal'),
      smalltitle('犬', '29.0kg', '1392kcal', '48kcal'),
      smalltitle('犬', '30.0kg', '1440kcal', '48kcal'),
      smalltitle('犬', '31.0kg', '1488kcal', '48kcal'),
      smalltitle('犬', '32.0kg', '1536kcal', '48kcal'),
      smalltitle('犬', '33.0kg', '1584kcal', '48kcal'),
      smalltitle('犬', '34.0kg', '1632kcal', '48kcal'),
      smalltitle('犬', '35.0kg', '1575kcal', '45kcal'),
      smalltitle('犬', '36.0kg', '1620kcal', '45kcal'),
      smalltitle('犬', '37.0kg', '1665kcal', '45kcal'),
      smalltitle('犬', '38.0kg', '1710kcal', '45kcal'),
      smalltitle('犬', '39.0kg', '1755kcal', '45kcal'),
      smalltitle('犬', '40.0kg', '1760kcal', '44kcal'),
      smalltitle('犬', '41.0kg', '1804kcal', '44kcal'),
      smalltitle('犬', '42.0kg', '1848kcal', '44kcal'),
      smalltitle('犬', '43.0kg', '1892kcal', '44kcal'),
      smalltitle('犬', '44.0kg', '1936kcal', '44kcal'),
      smalltitle('犬', '45.0kg', '1890kcal', '42kcal'),
      smalltitle('犬', '46.0kg', '1932kcal', '42kcal'),
      smalltitle('犬', '47.0kg', '1974kcal', '42kcal'),
      smalltitle('犬', '48.0kg', '2016kcal', '42kcal'),
      smalltitle('犬', '49.0kg', '2058kcal', '42kcal'),
      smalltitle('犬', '50.0kg', '2050kcal', '41kcal'),
      smalltitle('犬', '51.0kg', '2091kcal', '41kcal'),
      smalltitle('犬', '52.0kg', '2132kcal', '41kcal'),
      smalltitle('犬', '53.0kg', '2173kcal', '41kcal'),
      smalltitle('犬', '54.0kg', '2214kcal', '41kcal'),
      smalltitle('犬', '55.0kg', '2200kcal', '40kcal'),
      smalltitle('犬', '56.0kg', '2240kcal', '40kcal'),
      smalltitle('犬', '57.0kg', '2280kcal', '40kcal'),
      smalltitle('犬', '58.0kg', '2320kcal', '40kcal'),
      smalltitle('犬', '59.0kg', '2360kcal', '40kcal'),
      smalltitle('犬', '60.0kg', '2340kcal', '39kcal'),
    ]),
  );
}

Widget cat() {
  return Container(
    child: Column(children: [
      smalltitle('貓', '1.0kg', '80kcal', '80kcal'),
      smalltitle('貓', '1.5kg', '108kcal', '72kcal'),
      smalltitle('貓', '2.0kg', '134kcal', '67kcal'),
      smalltitle('貓', '2.5kg', '160kcal', '64kcal'),
      smalltitle('貓', '3.0kg', '183kcal', '61kcal'),
      smalltitle('貓', '3.5kg', '203kcal', '58kcal'),
      smalltitle('貓', '4.0kg', '228kcal', '57kcal'),
      smalltitle('貓', '4.5kg', '248kcal', '55kcal'),
      smalltitle('貓', '5.0kg', '265kcal', '53kcal'),
      smalltitle('貓', '5.5kg', '286kcal', '52kcal'),
      smalltitle('貓', '6.0kg', '300kcal', '50kcal'),
      smalltitle('貓', '6.5kg', '325kcal', '50kcal'),
      smalltitle('貓', '7.0kg', '336kcal', '48kcal'),
      smalltitle('貓', '7.5kg', '360kcal', '48kcal'),
      smalltitle('貓', '8.0kg', '376kcal', '47kcal'),
    ]),
  );
}
