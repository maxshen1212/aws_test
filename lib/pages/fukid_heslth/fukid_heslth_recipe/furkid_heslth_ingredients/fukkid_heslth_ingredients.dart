import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/furkid_heslth_ingredients/widgets/fukkid_heslth_ingredients_widgets.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';
import 'package:sqflite/sqflite.dart';
import 'package:pet_save_app/database/sqlite.dart';

class SqliteAddFood {
  Sqlite sqlite = Sqlite();
  getCategoryQuery(categoryId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food
      WHERE category_id==$categoryId
    ''');
    return foodResult;
  }

  getDetailAll(elementname) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food
      WHERE name='$elementname'
    ''');
    return foodResult;
  }

  Future<List<String>> getname(elementname) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food
      WHERE name LIKE '%$elementname%'
    ''');
    List<String> foodSuggestions =
        foodResult.map((item) => item['name'].toString()).toList();
    return foodSuggestions;
  }
}

class FurKidHeslthIngredients extends StatefulWidget {
  const FurKidHeslthIngredients({super.key});

  @override
  State<FurKidHeslthIngredients> createState() =>
      _FurKidHeslthIngredientsState();
}

class _FurKidHeslthIngredientsState extends State<FurKidHeslthIngredients> {
  // @override
  // void initState() {
  //   super.initState();

  //   // Start listening to changes.
  //   findcontroller.addListener(_printLatestValue);
  // }

  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is removed from the widget tree.
  //   // This also removes the _printLatestValue listener.
  //   findcontroller.dispose();
  //   super.dispose();
  // }

  // void _printLatestValue() async {
  //   final text = findcontroller.text;
  //   if (text.isEmpty) {
  //     var foodfsadorm = await SqliteAddFood().getname('ergwefgbhb');
  //     foodformlist = foodfsadorm as List;

  //   } else {
  //     var foodform = await SqliteAddFood().getname(text);
  //     foodformlist = foodform as List;

  //   }
  //   // var foodform = await SqliteAddFood().getname(text);
  //   // foodformlist = foodform as List;
  //   // print(foodformlist.length);
  //   setState(() {});
  // }

  Map<String, bool> buttonStates = {
    "btn_禽肉": true,
    "btn_畜肉": true,
    "btn_魚類": true,
    "btn_水產": true,
    "btn_蛋奶": true,
    "btn_澱粉": true,
    "btn_蔬菜_葉菜": true,
    "btn_蔬菜_根莖": true,
    "btn_蔬菜_芽菜": true,
    "btn_蔬菜_瓜實": true,
    "btn_穀類": true,
    "btn_補充品": true,
    "btn_塊莖類": true,
    "btn_蔬菜_其他": true,
    "btn_堅果": true,
    "btn_菇類": true,
    "btn_油脂": true,
    "btn_水果": true,
    "btn_豆類": true,
    "btn_其他": true,
  };
  bool gotobody2 = false;
  String name = "";
  //小按鈕切換到營養成分頁面
  void _togglebody2(String elementname) {
    setState(() {
      name = elementname;

      gotobody2 = !gotobody2;
    });
  }

//營養成分按返回body1
  void _togglebody1() async {
    setState(() {
      gotobody2 = !gotobody2;
    });
  }

//大按鈕變色
  void _toggleImage(String iconName) {
    setState(() {
      for (var key in buttonStates.keys) {
        if (key != iconName) buttonStates[key] = true;
      }
      buttonStates[iconName] = !buttonStates[iconName]!;
    });
  }

  @override
  Widget build(
    BuildContext context,
  ) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        drawerEdgeDragWidth: 220, //用拉的也能彈出設定
        endDrawer: SizedBox(
          width: 90.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        appBar:
            furkidHeslthIngredients1("食材查詢", gotobody2, _togglebody1, context),

        body: AnimatedSwitcher(
          duration: const Duration(milliseconds: 300), // 设置动画持续时间
          child: gotobody2
              ? body2(name)
              : body1(
                  buttonStates,
                  _toggleImage,
                  _togglebody2,
                  context,
                ),
        ),
      ),
    );
  }
}
