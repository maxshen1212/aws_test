import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/furkid_heslth_ingredients/fukkid_heslth_ingredients.dart';
import 'package:pet_save_app/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

//----------------------appbar-------------------------
PreferredSize furkidHeslthIngredients1(
    String names, bool gotobody2, Function togglebody1, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      leading: goBack(gotobody2, togglebody1),
      leadingWidth: 100.w,
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      automaticallyImplyLeading: false,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                AppColors.primaryLogo,
                Colors.purple,
              ]),
        ),
        //margin: EdgeInsets.only(left: 25.w, top: 25.h),
      ),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack(bool gotobody2, Function togglebody1) {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (gotobody2 == false) {
          Navigator.of(context).pop();
        } else {
          togglebody1();
        }
      },
      child: Padding(
        padding: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  });
}
//--------------------appbar-------------------------

//----------------------body1-------------------------

Widget body1(
  Map<String, bool> buttonStates,
  Function(String) toggleImage,
  Function(String) togglebody2,
  BuildContext context,
) {
  return SingleChildScrollView(
    child: Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 30.w, top: 20.h),
              child: Text(
                '食材查詢',
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w200,
                  height: 1.2125,
                  letterSpacing: 0.8,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              child: foodTextField(
                togglebody2: togglebody2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(left: 15.w, top: 10.h, right: 15.w),
              decoration: BoxDecoration(
                //color: Colors.amber,
                borderRadius: BorderRadius.circular(10),
                // 添加背景颜色
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      textImageButton("btn_禽肉", context, () {
                        toggleImage("btn_禽肉");
                      }, buttonStates["btn_禽肉"]),
                      textImageButton("btn_畜肉", context, () {
                        toggleImage("btn_畜肉");
                      }, buttonStates["btn_畜肉"]),
                      textImageButton("btn_魚類", context, () {
                        toggleImage("btn_魚類");
                      }, buttonStates["btn_魚類"]),
                      textImageButton("btn_水產", context, () {
                        toggleImage("btn_水產");
                      }, buttonStates["btn_水產"]),
                    ],
                  ),
                  (buttonStates["btn_禽肉"] == false)
                      ? chickenPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_畜肉"] == false)
                      ? porkPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_魚類"] == false)
                      ? fishPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_水產"] == false)
                      ? seaPress(togglebody2)
                      : SizedBox(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      textImageButton("btn_蛋奶", context, () {
                        toggleImage("btn_蛋奶");
                      }, buttonStates["btn_蛋奶"]),
                      textImageButton("btn_澱粉", context, () {
                        toggleImage("btn_澱粉");
                      }, buttonStates["btn_澱粉"]),
                      textImageButton("btn_蔬菜_葉菜", context, () {
                        toggleImage("btn_蔬菜_葉菜");
                      }, buttonStates["btn_蔬菜_葉菜"]),
                      textImageButton("btn_蔬菜_根莖", context, () {
                        toggleImage("btn_蔬菜_根莖");
                      }, buttonStates["btn_蔬菜_根莖"]),
                    ],
                  ),
                  (buttonStates["btn_蛋奶"] == false)
                      ? eggPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_澱粉"] == false)
                      ? starchPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_蔬菜_葉菜"] == false)
                      ? leafPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_蔬菜_根莖"] == false)
                      ? rootPress(togglebody2)
                      : SizedBox(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      textImageButton("btn_蔬菜_芽菜", context, () {
                        toggleImage("btn_蔬菜_芽菜");
                      }, buttonStates["btn_蔬菜_芽菜"]),
                      textImageButton("btn_蔬菜_瓜實", context, () {
                        toggleImage("btn_蔬菜_瓜實");
                      }, buttonStates["btn_蔬菜_瓜實"]),
                      textImageButton("btn_穀類", context, () {
                        toggleImage("btn_穀類");
                      }, buttonStates["btn_穀類"]),
                      textImageButton("btn_補充品", context, () {
                        toggleImage("btn_補充品");
                      }, buttonStates["btn_補充品"]),
                    ],
                  ),
                  (buttonStates["btn_蔬菜_芽菜"] == false)
                      ? budPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_蔬菜_瓜實"] == false)
                      ? seedPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_穀類"] == false)
                      ? cerealsPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_補充品"] == false)
                      ? supPress(togglebody2)
                      : SizedBox(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      textImageButton("btn_塊莖類", context, () {
                        toggleImage("btn_塊莖類");
                      }, buttonStates["btn_塊莖類"]),
                      textImageButton("btn_蔬菜_其他", context, () {
                        toggleImage("btn_蔬菜_其他");
                      }, buttonStates["btn_蔬菜_其他"]),
                      textImageButton("btn_堅果", context, () {
                        toggleImage("btn_堅果");
                      }, buttonStates["btn_堅果"]),
                      textImageButton("btn_菇類", context, () {
                        toggleImage("btn_菇類");
                      }, buttonStates["btn_菇類"]),
                    ],
                  ),
                  (buttonStates["btn_塊莖類"] == false)
                      ? tuberPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_蔬菜_其他"] == false)
                      ? vgotherPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_堅果"] == false)
                      ? nutPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_菇類"] == false)
                      ? mushroomPress(togglebody2)
                      : SizedBox(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      textImageButton("btn_油脂", context, () {
                        toggleImage("btn_油脂");
                      }, buttonStates["btn_油脂"]),
                      textImageButton("btn_水果", context, () {
                        toggleImage("btn_水果");
                      }, buttonStates["btn_水果"]),
                      textImageButton("btn_豆類", context, () {
                        toggleImage("btn_豆類");
                      }, buttonStates["btn_豆類"]),
                      textImageButton("btn_其他", context, () {
                        toggleImage("btn_其他");
                      }, buttonStates["btn_其他"]),
                    ],
                  ),
                  (buttonStates["btn_油脂"] == false)
                      ? oilPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_水果"] == false)
                      ? fruitPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_豆類"] == false)
                      ? beanPress(togglebody2)
                      : SizedBox(),
                  (buttonStates["btn_其他"] == false)
                      ? otherPress(togglebody2)
                      : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

class foodTextField extends StatefulWidget {
  final GlobalKey<AutoCompleteTextFieldState<String>> foodkey = GlobalKey();
  final Function togglebody2;
  foodTextField({super.key, required this.togglebody2});

  @override
  _FoodTextFieldState createState() => _FoodTextFieldState();
}

class _FoodTextFieldState extends State<foodTextField> {
  List<String> foodlist = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.h, left: 25.w, right: 25.w, bottom: 2.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: Autocomplete(
        optionsBuilder: (TextEditingValue textEditingValue) async {
          if (textEditingValue.text.isNotEmpty) {
            foodlist = await SqliteAddFood().getname(textEditingValue.text);
          } else {
            foodlist = [];
          }
          print(foodlist);
          return foodlist;
        },
        onSelected: (String selection) {
          FocusScope.of(context).unfocus();
          widget.togglebody2(selection);
        },
        fieldViewBuilder: (
          BuildContext context,
          TextEditingController controller,
          FocusNode focusNode,
          void Function() onFieldSubmitted,
        ) {
          return TextField(
            controller: controller,
            focusNode: focusNode,
            onSubmitted: (value) {
              onFieldSubmitted();
            },
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: ' 文字輸入',
              contentPadding: EdgeInsets.only(left: 10.w, top: 11.h),
              suffixIcon: IconButton(
                onPressed: () {
                  print('object');
                },
                //highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                icon: Image.asset('assets/appdesign/images/btnicinquire.png'),
              ),
            ),
          );
        },
        optionsViewBuilder: (BuildContext context,
            void Function(String) onSelected, Iterable<String> options) {
          return Material(
            child: ListView.builder(
              shrinkWrap: true,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
              itemCount: options.length,
              itemExtent: 70.h,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    onSelected(options.elementAt(index));
                  },
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(options.elementAt(index)),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 5.w, right: 60.w),
                        height: 1.h,
                        color: const Color.fromARGB(255, 159, 157, 151),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}

Widget buildTextField(TextEditingController findcontroller) {
  return Container(
    // width: 330.w,
    // height: 45.h,
    margin: EdgeInsets.only(top: 10.h, left: 30.w, right: 30.w, bottom: 2.h),
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 325.w,
              child: TextField(
                controller: findcontroller,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  hintText: '文字輸入',
                  isDense: true,
                  suffixIcon: Container(
                    child: IconButton(
                      onPressed: () {},
                      //highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      icon: Image.asset(
                          'assets/appdesign/images/btnicinquire.png'),
                    ),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromARGB(0, 249, 208, 208),
                    ),
                  ),
                  disabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
                ),
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w200,
                  color: const Color(0xff595757),
                ),
                autocorrect: false,
                obscureText: false,
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

Widget foodlist(String name, Function togglebody2) {
  return GestureDetector(
    onTap: () {
      togglebody2(name);
    },
    child: Container(
      margin: EdgeInsets.only(bottom: 0.5.h),
      padding: EdgeInsets.only(left: 10.w, bottom: 3.h),
      width: 325.w,
      height: 40.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8.w)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1)
        ],
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(name,
            style: const TextStyle(
              color: Color(0xFF595757),
              fontSize: 16,
              fontFamily: 'Inter',
              fontWeight: FontWeight.w300,
              letterSpacing: 1,
            )),
      ),
    ),
  );
}

Widget textImageButton(
  String iconName,
  BuildContext context,
  Null Function() param2,
  bool? buttonStat,
) {
  return SizedBox(
      width: 81.w,
      height: 95.h,
      child: IconButton(
        onPressed: () {
          param2();
        },
        iconSize: 80,
        icon: buttonStat ?? false
            ? SvgPicture.asset("assets/ingredients/gray/$iconName.svg")
            : SvgPicture.asset("assets/ingredients/red/$iconName.svg"),
        //icon: Image.asset("assets/ingredients/red/$iconName.png"),
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
      ));
}

//大按鈕攤開後的小按鈕
Widget noName(String name, Function togglebody2) {
  return GestureDetector(
    onTap: () {
      togglebody2(name);
    },
    child: Container(
      margin: EdgeInsets.only(bottom: 6.h),
      padding: EdgeInsets.only(left: 10.w, bottom: 3.h),
      width: 310.w,
      height: 40.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8.w)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1)
        ],
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(name,
            style: const TextStyle(
              color: Color(0xFF595757),
              fontSize: 16,
              fontFamily: 'Inter',
              fontWeight: FontWeight.w300,
              letterSpacing: 1,
            )),
      ),
    ),
  );
}

//禽
Widget chickenPress(Function togglebody2) {
  const categoryId = 12;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//畜
Widget porkPress(Function togglebody2) {
  const categoryId = 6;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//魚
Widget fishPress(Function togglebody2) {
  const categoryId = 9;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//水產
Widget seaPress(Function togglebody2) {
  const categoryId = 2;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//蛋奶
Widget eggPress(Function togglebody2) {
  const categoryId = 10;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//澱粉
Widget starchPress(Function togglebody2) {
  const categoryId = 20;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//葉菜
Widget leafPress(Function togglebody2) {
  const categoryId = 19;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//根莖
Widget rootPress(Function togglebody2) {
  const categoryId = 18;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//芽菜
Widget budPress(Function togglebody2) {
  const categoryId = 16;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//瓜實
Widget seedPress(Function togglebody2) {
  const categoryId = 15;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//穀類
Widget cerealsPress(Function togglebody2) {
  const categoryId = 14;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//補充品
Widget supPress(Function togglebody2) {
  const categoryId = 13;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//塊莖類
Widget tuberPress(Function togglebody2) {
  const categoryId = 11;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//蔬菜其他
Widget vgotherPress(Function togglebody2) {
  const categoryId = 17;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//堅果
Widget nutPress(Function togglebody2) {
  const categoryId = 8;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//菇類
Widget mushroomPress(Function togglebody2) {
  const categoryId = 7;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//油脂
Widget oilPress(Function togglebody2) {
  const categoryId = 5;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//水果
Widget fruitPress(Function togglebody2) {
  const categoryId = 1;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//豆類
Widget beanPress(Function togglebody2) {
  const categoryId = 3;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}

//其他
Widget otherPress(Function togglebody2) {
  const categoryId = 4;
  return FutureBuilder(
    future: SqliteAddFood().getCategoryQuery(categoryId),
    builder: (context, snap) {
      if (snap.hasData) {
        List foodListData = snap.data as List;
        return Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];

                  return Column(
                    children: [
                      noName(foodName, togglebody2),
                    ],
                  );
                }),
          ],
        );
      }
      return Container();
    },
  );
}
//----------------------body1-------------------------

//----------------------body2-------------------------
Widget body2(String elementname) {
  return FutureBuilder(
      future: SqliteAddFood().getDetailAll(elementname),
      builder: (context, snap) {
        if (snap.hasData) {
          List foodListData = snap.data as List;
          var food = foodListData[0];
          var kcal = food['kcal'] ?? '0.0';
          var water = food['water'] ?? '0.0';
          var protein = food['protein'] ?? '0.0';
          var fat = food['fat'] ?? '0.0';
          var carbohydrate = food['carbohydrate'] ?? '0.0';
          var ash = food['ash'] ?? '0.0';
          var ca = food['ca'] ?? '0.0';
          var ph = food['ph'] ?? '0.0';
          var kalium = food['kalium'] ?? '0.0';
          var na = food['na'] ?? '0.0';
          var fe = food['fe'] ?? '0.0';
          var zn = food['zn'] ?? '0.0';
          var cu = food['cu'] ?? '0.0';
          var mn = food['mn'] ?? '0.0';
          var mg = food['mg'] ?? '0.0';
          var iodine = food['iodine'] ?? '0.0';
          var v_a = food['v_a'] ?? '0.0';
          var v_b1 = food['v_b1'] ?? '0.0';
          var v_b2 = food['v_b2'] ?? '0.0';
          var v_b5 = food['v_b5'] ?? '0.0';
          var v_b6 = food['v_b6'] ?? '0.0';
          var v_b12 = food['v_b12'] ?? '0.0';
          var v_c = food['v_c'] ?? '0.0';
          var v_e = food['v_e'] ?? '0.0';

          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                // Hc5 (392:6068)
                child: Container(
                  margin: EdgeInsets.only(top: 20.h),
                  child: Text(
                    elementname,
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 20,
                      fontWeight: FontWeight.w200,
                      height: 1.2125,
                      letterSpacing: 0.8,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
              Center(
                // n33 (392:6069)
                child: Container(
                  margin: EdgeInsets.only(top: 5.h),
                  child: Text(
                    '食材屬性',
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 12,
                      fontWeight: FontWeight.w200,
                      height: 1.2125,
                      letterSpacing: 0.6,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
              Container(
                // autogroupjfqugu7 (S1jP5XaNQgGtKk3GZTjfQu)
                margin: EdgeInsets.only(
                    left: 120.w, right: 120.w, top: 10.h, bottom: 20.h),
                width: double.infinity,
                height: 25,
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Center(
                  child: Text(
                    '每百克 營養成分',
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      height: 1.2125,
                      letterSpacing: 0.7,
                      color: Color(0xffffffff),
                    ),
                  ),
                ),
              ),
              element("熱量", '${kcal}kcal', "水分", "${water}g"),
              element("粗蛋白質", "${protein}g", "粗脂肪", "${fat}g"),
              element("碳水化合物", "${carbohydrate}g", "灰份", "${ash}g"),
              SizedBox(
                height: 30.h,
              ),
              element("鈣片", "${ca}mg", "碘", "${iodine}mg"),
              element("磷", "${ph}mg", "維生素A", "${v_a}mg"),
              element("鉀", "${kalium}mg", "維生素B1", "${v_b1}mg"),
              element("鈉", "${na}mg", "維生素B2", "${v_b2}mg"),
              element("鐵", "${fe}mg", "維生素B5", "${v_b5}mg"),
              element("鋅", "${zn}mg", "維生素B6", "${v_b6}mg"),
              element("銅", "${cu}mg", "維生素B12", "${v_b12}mg"),
              element("錳", "${mn}mg", "維生素C", "${v_c}mg"),
              element("鎂", "${mg}mg", "維生素E", "${v_e}mg"),
            ],
          );
        }
        return Container();
      });
}

Widget element(
  String name1,
  String unit1,
  String name2,
  String unit2,
) {
  return Column(
    children: [
      Container(
        //color: Colors.amber,
        margin: EdgeInsets.only(left: 25.w, right: 25.w, top: 5.h),
        child: Flex(
          direction: Axis.horizontal,
          children: [
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  Text(
                    name1,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5.5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            unit1,
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.7,
                              color: Color(0xff000000),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  Text(
                    name2,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5.5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            unit2,
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.7,
                              color: Color(0xff000000),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      Container(
        margin:
            EdgeInsets.only(left: 30.w, right: 30.w, top: 7.h, bottom: 15.h),
        height: 1.h,
        color: Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}
//----------------------body2-------------------------
