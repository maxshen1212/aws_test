import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_event.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_state.dart';

class FurKidHeslthMyRecipePageBlocs extends Bloc<FurKidHeslthMyRecipePageEvents,
    FurKidHeslthMyRecipePageStates> {
  FurKidHeslthMyRecipePageBlocs() : super(InitStates()) {
    on<Page0>((event, emit) {
      emit(FurKidHeslthMyRecipePageStates(pageState: event.page));
    });
  }
}

class FurKidHeslthMyRecipeRecipeIdBlocs extends Bloc<
    FurKidHeslthMyRecipeRecipeIdEvents, FurKidHeslthMyRecipeRecipeIdStates> {
  FurKidHeslthMyRecipeRecipeIdBlocs() : super(RecipeIdInitStates()) {
    on<GetId>((event, emit) {
      emit(FurKidHeslthMyRecipeRecipeIdStates(recipeId: event.recipeId));
    });
  }
}

class FurKidHeslthReviseRecipeBlocs extends Bloc<FurKidHeslthReviseRecipeEvents,
    FurKidHeslthReviseRecipeStates> {
  FurKidHeslthReviseRecipeBlocs() : super(ReviseRecipeInitStates()) {
    on<Tofalsetotal>((event, emit) {
      emit(FurKidHeslthReviseRecipeStates(
          totalsee: false, namesee: state.namesee));
    });
    on<Toturetotal>((event, emit) {
      emit(FurKidHeslthReviseRecipeStates(
          totalsee: true, namesee: state.namesee));
    });
    on<Totruename>((event, emit) {
      emit(FurKidHeslthReviseRecipeStates(
          totalsee: state.totalsee, namesee: true));
    });
    on<Tofalsename>((event, emit) {
      emit(FurKidHeslthReviseRecipeStates(
          totalsee: state.totalsee, namesee: false));
    });
  }
}
