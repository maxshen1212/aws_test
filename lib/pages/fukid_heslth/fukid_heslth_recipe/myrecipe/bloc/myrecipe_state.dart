class FurKidHeslthMyRecipePageStates {
  int pageState;
  FurKidHeslthMyRecipePageStates({required this.pageState});
}

class InitStates extends FurKidHeslthMyRecipePageStates {
  InitStates() : super(pageState: 0);
}
//------------------------------------------------------------

class FurKidHeslthMyRecipeRecipeIdStates {
  int recipeId = 1;
  FurKidHeslthMyRecipeRecipeIdStates({required this.recipeId});
}

class RecipeIdInitStates extends FurKidHeslthMyRecipeRecipeIdStates {
  RecipeIdInitStates() : super(recipeId: 1);
}

//------------------------------------------------------------------
class FurKidHeslthReviseRecipeStates {
  bool totalsee = false;
  bool namesee = true;
  FurKidHeslthReviseRecipeStates(
      {required this.totalsee, required this.namesee});
}

class ReviseRecipeInitStates extends FurKidHeslthReviseRecipeStates {
  ReviseRecipeInitStates() : super(totalsee: false, namesee: true);
}
