import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_state.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/widgets/myrecipe_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';
import 'package:sqflite/sqflite.dart';

class SqliteMyRecipe {
  Sqlite sqlite = Sqlite();
  searchStatus1Recipe() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT recipe_id FROM recipe_collect
      WHERE status=?
    ''', ['1']);
    List<Map<String, dynamic>> namelist = [];

    for (int i = 0; i < foodResult.length; i++) {
      int recipe_id = foodResult[i]['recipe_id'];
      List<Map<String, dynamic>> name = await searchName(recipe_id);

      if (name.isNotEmpty) {
        namelist.add(name[0]);
      }
    }

    return namelist;
  }

  searchStatus0Recipe() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT recipe_id FROM recipe_collect
      WHERE status=?
      
    ''', ['0']);
    List<Map<String, dynamic>> namelist = [];
    for (int i = 0; i < foodResult.length; i++) {
      int recipe_id = foodResult[i]['recipe_id'];
      List<Map<String, dynamic>> name = await searchName(recipe_id);

      if (name.isNotEmpty) {
        namelist.add(name[0]);
      }
    }
    return namelist;
  }

  searchName(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,id FROM recipe
      WHERE id=?
    ''', ['$id']);

    return foodResult;
  }

  status1to0(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate(
        'UPDATE recipe_collect SET status = ? WHERE recipe_id=?', ['0', '$id']);
  }

  status0to1(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate(
        'UPDATE recipe_collect SET status = ? WHERE recipe_id=?', ['1', '$id']);
  }

  deleteRecipe(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT category_id FROM recipe
      WHERE id=?
    ''', ['$id']);
    await db.rawUpdate('DELETE FROM recipe_category WHERE id=?',
        ['${foodResult[0]['category_id']}']);

    await db.rawUpdate('DELETE FROM recipe_collect WHERE recipe_id=?', ['$id']);
    await db.rawUpdate('DELETE FROM recipe WHERE id=?', ['$id']);
    await db.rawUpdate('DELETE FROM recipe_list WHERE recipe_id=?', ['$id']);
  }

  searchRecipe1(String keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    List<Map<String, dynamic>> foodfinalResult = [];
    final List<Map<String, dynamic>> foodsssResult = await db.rawQuery('''
      SELECT status,recipe_id FROM recipe_collect
      WHERE status =0
    ''');
    for (int i = 0; i < foodsssResult.length; i++) {
      final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,id FROM recipe
      WHERE id = ? AND name LIKE '%$keyword%'
    ''', ['${foodsssResult[i]['recipe_id']}']);

      if (foodResult.isNotEmpty) {
        for (int j = 0; j < foodResult.length; j++) {
          foodfinalResult.add({
            'name': foodResult[j]['name'],
            'id': foodResult[j]['id'],
          });
        }
      }
    }
    return foodfinalResult;
  }

  searchRecipe2(String keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    List<Map<String, dynamic>> foodfinalResult = [];
    final List<Map<String, dynamic>> foodsssResult = await db.rawQuery('''
      SELECT status,recipe_id FROM recipe_collect
      WHERE status = 1 
    ''');
    for (int i = 0; i < foodsssResult.length; i++) {
      final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,id FROM recipe
      WHERE id = ? AND name LIKE '%$keyword%'
    ''', ['${foodsssResult[i]['recipe_id']}']);

      if (foodResult.isNotEmpty) {
        for (int j = 0; j < foodResult.length; j++) {
          foodfinalResult.add({
            'name': foodResult[j]['name'],
            'id': foodResult[j]['id'],
          });
        }
      }
    }
    return foodfinalResult;
  }

  //copy---------------------------
  copyRecipe(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT category_id,name FROM recipe
      WHERE id=?

    ''', ['$id']);
    print(foodResult);

    await db.execute('''
  INSERT INTO recipe_category (name,status)
  SELECT name,status
  FROM recipe_category
  WHERE id = ?;
''', ['${foodResult[0]['category_id']}']);
    int? categoryid =
        Sqflite.firstIntValue(await db.rawQuery('SELECT last_insert_rowid();'));
    await db.execute('''
  INSERT INTO recipe (food_weight,finish_weight,ill_tags,mem_id,
      calories,water,protein,fat,carbohydrate,fiber,ash,
      dry_rate_protein,dry_rate_fat,dry_rate_carbo,dry_rate_fiber,dry_rate_ash,
      calory_rate_protein,calory_rate_fat,calory_rate_carbo,
      calories_per_100g,water_per_100g,protein_per_100g,fat_per_100g,carbohydrate_per_100g,fiber_per_100g,ash_per_100g)
  SELECT food_weight,finish_weight,ill_tags,mem_id,
      calories,water,protein,fat,carbohydrate,fiber,ash,
      dry_rate_protein,dry_rate_fat,dry_rate_carbo,dry_rate_fiber,dry_rate_ash,
      calory_rate_protein,calory_rate_fat,calory_rate_carbo,
      calories_per_100g,water_per_100g,protein_per_100g,fat_per_100g,carbohydrate_per_100g,fiber_per_100g,ash_per_100g
  FROM recipe
  WHERE id = ?;
''', ['$id']);
    int? newrecipeid =
        Sqflite.firstIntValue(await db.rawQuery('SELECT last_insert_rowid();'));
    await db.rawUpdate('UPDATE recipe SET name = ?,category_id=? WHERE id=?',
        ['${foodResult[0]['name']}的複製', '$categoryid', '$newrecipeid']);

    await db.execute('''
  INSERT INTO recipe_collect (recipe_id,mem_id,status)
  SELECT ?,mem_id,status
  FROM recipe_collect
  WHERE recipe_id = ?;
''', ['$newrecipeid', '$id']);

    final List<Map<String, dynamic>> listlength = await db.rawQuery('''
      SELECT id,recipe_id FROM recipe_list
      WHERE recipe_id=?

    ''', ['$id']);
    for (int i = 0; i < listlength.length; i++) {
      await db.execute('''
 INSERT INTO recipe_list (food_id,quantity,recipe_id)
  SELECT food_id,quantity,?
  FROM recipe_list
  WHERE id = ?;
''', ['$newrecipeid', '${listlength[i]['id']}']);
    }
  }

  //copy---------------------------

  //detail---------------------------------------------------------
  searchNameAndCategory(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,category_id,ill_tags FROM recipe
      WHERE id=?
    ''', ['$id']);
    int categoryId = foodResult[0]['category_id'];
    final List<Map<String, dynamic>> categoryResult = await db.rawQuery('''
      SELECT name FROM recipe_category
      WHERE id=?
    ''', ['$categoryId']);

    String recipename = foodResult[0]['name'];
    String categoryname = categoryResult[0]['name'];
    String illtags = foodResult[0]['ill_tags'];
    List<Map<String, dynamic>> Result = [];
    Result.add({
      'recipename': recipename,
      'categoryname': categoryname,
      'illtags': illtags
    });
    return Result;
  }

  searchRecipeList(int recipeId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT food_id,quantity FROM recipe_list
      WHERE recipe_id=?
    ''', ['$recipeId']);
    List<Map<String, dynamic>> namelist = [];
    for (int i = 0; i < foodResult.length; i++) {
      int id = foodResult[i]['food_id'];
      List<Map<String, dynamic>> name = await searchFoodName(id);
      namelist.add(name[0]);
    }

    List<Map<String, dynamic>> mergedList = [];
    for (int i = 0; i < foodResult.length; i++) {
      int quantity = foodResult[i]['quantity'];
      String name = namelist[i]['name'];

      mergedList.add({'name': name, 'quantity': quantity});
    }

    return mergedList;
  }

  searchFoodName(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name FROM food
      WHERE id=?
    ''', ['$id']);
    return foodResult;
  }

  searchDetail(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM recipe
      WHERE id=?
    ''', ['$id']);
    return foodResult;
  }
  //detail---------------------------------------------------------

  //Revise---------------------------------------------------------

  searchControllerName(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,category_id,food_weight,finish_weight,ill_tags FROM recipe
      WHERE id=?
    ''', ['$id']);
    int cateId = foodResult[0]['category_id'];
    final List<Map<String, dynamic>> cateResult = await db.rawQuery('''
      SELECT name FROM recipe_category
      WHERE id=?
    ''', ['$cateId']);

    List<Map<String, dynamic>> controllerResult = [];
    controllerResult.add({
      'nameCodeController': foodResult[0]['name'],
      'kindController': cateResult[0]['name'],
      'food_weight': foodResult[0]['food_weight'],
      'finish_weight': foodResult[0]['finish_weight'],
      'ill_tags': foodResult[0]['ill_tags'],
    });
    //print(controllerResult);
    return controllerResult;
  }

  searchFoodIdAndWeight(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT food_id,quantity FROM recipe_list
      WHERE recipe_id=?
    ''', ['$id']);
    //print(foodResult);
    for (int i = 0; i < foodResult.length; i++) {
      await foodTablestatus(
          foodResult[i]['food_id'], foodResult[i]['quantity']);
    }
    final List<Map<String, dynamic>> foodNameResult = await db.rawQuery('''
      SELECT name,id,weight FROM food
      WHERE status=?
    ''', ['0']);
  }

  searchFood() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT weight,name,id FROM food
      WHERE status=0
    ''');
    //print('$foodResult清單');
    return foodResult;
  }

  foodTablestatus(int id, int quantity) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status = ?, weight=? WHERE id=?',
        ['0', '$quantity', '$id']);
  }

  foodTablestatusInit() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status = ?,weight=?', ['1', null]);
  }

  foodGramRevise(int id, int gram) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db
        .rawUpdate('UPDATE food SET weight=? WHERE id = ?', ['$gram', '$id']);
  }

  foodGramtotal() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT weight FROM food
      WHERE status=0
    ''');
    int total = 0;
    double a = 0;
    for (int i = 0; i < foodResult.length; i++) {
      a += foodResult[i]['weight'] ?? 0;
    }
    total = a.toInt();
    print(total);
    return total;
  }

  deletefood(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate(
        'UPDATE food SET status = 1,weight = ? WHERE id=?', [null, '$id']);
  }

  searchfoodweight(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT weight FROM food
      WHERE id = ?
    ''', ['$id']);
    //print('$foodResult清單');
    return foodResult;
  }

  checkfoodlistnotnull() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT status FROM food
      WHERE status = 0
    ''');
    if (foodResult.isEmpty) {
      print('$foodResult清單null');
      return false;
    } else {
      return true;
    }
  }

  checkfoodlistnotzero() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
       SELECT name FROM food
      WHERE  status == 0 AND weight IS NULL
    ''');
    if (foodResult.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  savereviserecipe(
    int recipeId,
    String name,
    String categoryname,
    int foodweight,
    String finishweights,
    String illtags,
  ) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,category_id,food_weight,finish_weight,ill_tags FROM recipe
      WHERE id = ?
    ''', ['$recipeId']);
    await db.rawUpdate(
        'UPDATE recipe SET name = ?, food_weight = ?, finish_weight = ?, ill_tags = ? WHERE id = ?',
        [name, foodweight, finishweights, illtags, recipeId]);

    await db.rawUpdate('UPDATE recipe_category SET name = ? WHERE id = ?',
        [categoryname, '${foodResult[0]['category_id']}']);
    final List<Map<String, dynamic>> food = await db.rawQuery('''
      SELECT name,weight,id FROM food
      WHERE status=0
      
    ''');
    print(food);

    await db
        .execute('DELETE FROM recipe_list WHERE recipe_id = ?;', ['$recipeId']);

    for (int i = 0; i < food.length; i++) {
      double wei = food[i]['weight'];
      int weight = wei.toInt();
      await db.execute('''
            INSERT INTO recipe_list (food_id,quantity,recipe_id)VALUES('${food[i]['id']}','$weight','$recipeId');
''');
    }

    List foodListData = await SqliteMyRecipe().analyzeFood();
    double finishweight = double.parse(finishweights);

    double allkcal = 0;
    double allwater = 0;
    double allprotein = 0;
    double allfat = 0;
    double allcarbohydrate = 0;
    double allfiber = 0;
    double allash = 0;

    for (int i = 0; i < foodListData.length; i++) {
      allkcal += foodListData[i]['weight'] * foodListData[i]['kcal'] / 100;
      // allwater += foodListData[i]['weight'] *
      //     foodListData[i]['water'] /
      //     100;
      allprotein +=
          foodListData[i]['weight'] * foodListData[i]['protein'] / 100;
      allfat += foodListData[i]['weight'] * foodListData[i]['fat'] / 100;
      allcarbohydrate +=
          foodListData[i]['weight'] * foodListData[i]['carbohydrate'] / 100;
      allfiber += foodListData[i]['weight'] * foodListData[i]['fiber'] / 100;
      allash += foodListData[i]['weight'] * foodListData[i]['ash'] / 100;
    }
    //營養成分含量
    allkcal = double.parse(allkcal.toStringAsFixed(2));
    allwater = finishweight.toDouble() -
        allprotein -
        allfat -
        allcarbohydrate -
        allfiber -
        allash;
    allwater = double.parse(allwater.toStringAsFixed(2));
    allprotein = double.parse(allprotein.toStringAsFixed(2));
    allfat = double.parse(allfat.toStringAsFixed(2));
    allcarbohydrate = double.parse(allcarbohydrate.toStringAsFixed(2));
    allfiber = double.parse(allfiber.toStringAsFixed(2));
    allash = double.parse(allash.toStringAsFixed(2));
    //100g營養成分
    double hundredgram(double all, double foodweight) {
      return double.parse((all / foodweight * 100).toStringAsFixed(2));
    }

    double kcal100g = hundredgram(allkcal, finishweight);
    double water100g = hundredgram(allwater, finishweight);
    double protein100g = hundredgram(allprotein, finishweight);
    double fat100g = hundredgram(allfat, finishweight);
    double carbohydrate100g = hundredgram(allcarbohydrate, finishweight);
    double fiber100g = hundredgram(allfiber, finishweight);
    double ash100g = hundredgram(allash, finishweight);
    //乾物比
    double alldry = double.parse(
        (100 - (allwater / finishweight * 100)).toStringAsFixed(2));
    double percent(double element) {
      double result = (element / finishweight * 100);
      result = result / alldry * 100;
      return double.parse(result.toStringAsFixed(1));
    }

    double proteinpercent = percent(allprotein);
    double fatpercent = percent(allfat);
    double carbohydratepercent = percent(allcarbohydrate);
    double fiberpercent = percent(allfiber);
    double ashpercent = percent(allash);
    //熱量分布
    double calory_rate_protein = double.parse(((allprotein * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));
    double calory_rate_fat = double.parse(((allfat * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));
    double calory_rate_carbo = double.parse(((allcarbohydrate * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));
    await db.rawUpdate('''
  UPDATE recipe SET
    calories = '$allkcal',
    water = '$allwater',
    protein = '$allprotein',
    fat = '$allfat',
    carbohydrate = '$allcarbohydrate',
    fiber = '$allfiber',
    ash = '$allash',
    dry_rate_protein = '$proteinpercent',
    dry_rate_fat = '$fatpercent',
    dry_rate_carbo = '$carbohydratepercent',
    dry_rate_fiber = '$fiberpercent',
    dry_rate_ash = '$ashpercent',
    calory_rate_protein = '$calory_rate_protein',
    calory_rate_fat = '$calory_rate_fat',
    calory_rate_carbo = '$calory_rate_carbo',
    calories_per_100g = '$kcal100g',
    water_per_100g = '$water100g',
    protein_per_100g = '$protein100g',
    fat_per_100g = '$fat100g',
    carbohydrate_per_100g = '$carbohydrate100g',
    fiber_per_100g = '$fiber100g',
    ash_per_100g = '$ash100g'
  WHERE id = ?;
''', ['$recipeId']);
  }

  analyzeFood() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT  COALESCE(weight, 0) as weight,
      COALESCE(kcal, 0) as kcal,
      COALESCE(water, 0) as water,
      COALESCE(protein, 0) as protein,
      COALESCE(fat, 0) as fat,
      COALESCE(carbohydrate, 0) as carbohydrate,
      COALESCE(fiber, 0) as fiber,
      COALESCE(ash, 0) as ash  FROM food
      WHERE status = '0'
      
    ''');
    return foodResult;
  }

  //Revise---------------------------------------------------------
  //plus-----------------------------------------------------------
  getCategoryQuery() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food_category
      
    ''');
    return foodResult;
  }

  getCategoryFoodQuery(categoryId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food
      WHERE category_id==$categoryId
      
    ''');
    return foodResult;
  }

  checkStatus(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT id,status,name FROM food
      WHERE id = ?
    ''', [id]);
    return foodResult;
  }

  foodplus(int id, int status) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db
        .rawUpdate('UPDATE food SET status = ? WHERE id=?', ['$status', '$id']);
  }

  foodplusbutton() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status =0  WHERE status=2');
  }

  foodpluscancel() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status =1  WHERE status=2');
  }

  searchauto(keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,status FROM food
      WHERE name LIKE '%$keyword%' AND status !=0
    ''');
    List<String> slist = foodResult.map((e) => e['name'].toString()).toList();
    print(foodResult);
    return foodResult;
  }

  foodplusauto1(String name) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status =1  WHERE name=?', [name]);
  }

  foodplusauto2(String name) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status =2  WHERE name=?', [name]);
  }
  //plus-----------------------------------------------------------
}

class MyRecipe extends StatefulWidget {
  const MyRecipe({super.key});

  @override
  State<MyRecipe> createState() => _MyRecipeState();
}

class _MyRecipeState extends State<MyRecipe> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      resizeToAvoidBottomInset: false,
      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthMyRecipe("我的食譜", context),
      body: BlocBuilder<FurKidHeslthMyRecipePageBlocs,
          FurKidHeslthMyRecipePageStates>(builder: (context, state) {
        return IndexedStack(
          index: state.pageState,
          children: [
            pagezero(),
            SingleChildScrollView(child: Detail()),
            Revise(),
            const plusfood(),
          ],
        );
      }),
    );
  }
}
