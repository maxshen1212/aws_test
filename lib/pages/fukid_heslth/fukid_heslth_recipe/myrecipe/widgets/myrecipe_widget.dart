import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_event.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/bloc/myrecipe_state.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/myrecipe/myrecipe.dart';
import 'package:pet_save_app/utils.dart';

PreferredSize furkidHeslthMyRecipe(String names, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      leading: goBack(),
      leadingWidth: 100.w,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                AppColors.primaryLogo,
                Colors.purple,
              ]),
        ),
        //margin: EdgeInsets.only(left: 25.w, top: 25.h),
      ),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack() {
  return BlocBuilder<FurKidHeslthMyRecipePageBlocs,
      FurKidHeslthMyRecipePageStates>(builder: (context, state) {
    return GestureDetector(
      onTap: () async {
        if (state.pageState == 0) {
          Navigator.of(context).pop();
        } else if (state.pageState == 1) {
          BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
              .add(Page0(page: 0));
          BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
              .add(GetId(recipeId: 0));
          //await SqliteMyRecipe().foodTablestatusInit();
        } else if (state.pageState == 2) {
          BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
              .add(Page0(page: 0));
          BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
              .add(GetId(recipeId: 0));
          BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context)
              .add(Tofalsetotal());
          BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context)
              .add(Totruename());
          await SqliteMyRecipe().foodTablestatusInit();
          //  _ReviseFoodTableStateManager.addFoodTableState.clear();
          //print(_ReviseFoodTableStateManager.addFoodTableState);
        } else if (state.pageState == 3) {
          BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
              .add(Page0(page: 2));
          FoodCategoryNamesManager.closeAll();

          //      _ReviseFoodTableStateManager.addFoodTableState.clear();

          await SqliteMyRecipe().foodpluscancel();
        }
      },
      child: Padding(
        padding: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  });
}

//-----------------------------------------------------------------------------
class pagezero extends StatefulWidget {
  @override
  State<pagezero> createState() => _pagezeroState();
}

class _pagezeroState extends State<pagezero> {
  TextEditingController searchController = TextEditingController();

  bool favorite = false;
  bool searchrecipe = true;
  void change() {
    setState(() {
      favorite = !favorite;
    });
  }

  void changesearchfalse() {
    setState(() {
      searchrecipe = false;
    });
  }

  void changesearchttrue() {
    setState(() {
      searchrecipe = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amber,
      margin: EdgeInsets.only(
        left: 30.w,
        right: 30.w,
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 15.h, bottom: 10.h),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "我的食譜",
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ),
          Textfield(
            changesearchfalse: changesearchfalse,
            changesearchtrue: changesearchttrue,
            searchController: searchController,
          ),
          RecipeButton(
            favorite: favorite,
            change: change,
          ),
          searchrecipe
              ? favorite
                  ? Favoriteunrecipe()
                  : Unrecipe()
              : Search(
                  favorite: favorite,
                  searchController: searchController,
                )
        ],
      ),
    );
  }
}

class Textfield extends StatelessWidget {
  final void Function() changesearchfalse;
  final void Function() changesearchtrue;
  final TextEditingController searchController;
  const Textfield(
      {super.key,
      required this.changesearchfalse,
      required this.changesearchtrue,
      required this.searchController});

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: 330.w,
      //height: 40.h,
      margin: EdgeInsets.only(top: 5.h, bottom: 2.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: TextField(
        controller: searchController,
        keyboardType: TextInputType.multiline,
        onChanged: (value) {
          if (value.isEmpty) {
            changesearchtrue();
          } else {
            changesearchfalse();
          }
        },
        decoration: const InputDecoration(
          hintText: '文字輸入',
          isDense: true,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromARGB(0, 249, 208, 208),
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
        ),
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14.sp,
          fontWeight: FontWeight.w200,
          color: const Color(0xff595757),
        ),
        autocorrect: false,
        obscureText: false,
      ),
    );
  }
}

class Search extends StatefulWidget {
  final bool favorite;

  TextEditingController searchController;

  Search({super.key, required this.favorite, required this.searchController});
  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amberAccent,
      height: 525.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: FutureBuilder(
        future: widget.favorite
            ? SqliteMyRecipe().searchRecipe1(widget.searchController.text)
            : SqliteMyRecipe().searchRecipe2(widget.searchController.text),
        builder: (context, snap) {
          if (snap.hasData) {
            List<Map<String, dynamic>> foodListData =
                snap.data as List<Map<String, dynamic>>;

            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodListData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      String foodName = foodListData[index]['name'];
                      int recipe_id = foodListData[index]['id'];
                      return widget.favorite
                          ? FavoriteUnrecipeUI(
                              name: foodName,
                              recipe_id: recipe_id,
                              reload: () {
                                setState(() {});
                              },
                            )
                          : UnrecipeUI(
                              name: foodName,
                              recipe_id: recipe_id,
                              reload: () {
                                setState(() {});
                              },
                            );
                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class RecipeButton extends StatefulWidget {
  final bool favorite;
  final void Function() change;
  const RecipeButton({super.key, required this.favorite, required this.change});
  @override
  State<RecipeButton> createState() => _RecipebuttonState();
}

class _RecipebuttonState extends State<RecipeButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.h),
      //color: Colors.amber,
      child: Row(
        //crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () async {
              if (widget.favorite == true) {
                widget.change();
              }
            },
            style: TextButton.styleFrom(
              padding: EdgeInsets.zero,
            ),
            child: Container(
              width: 130.w,
              height: 40.h,
              decoration: BoxDecoration(
                color:
                    widget.favorite ? Color(0xff929292) : AppColors.primaryLogo,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Text(
                  '未收藏食譜',
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w700,
                    height: 1.2125.h,
                    letterSpacing: 0.8,
                    color: Color(0xffffffff),
                  ),
                ),
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              if (widget.favorite == false) {
                widget.change();
              }
            },
            style: TextButton.styleFrom(
              padding: EdgeInsets.zero,
            ),
            child: Container(
              width: 130.w,
              height: 40.h,
              decoration: BoxDecoration(
                color:
                    widget.favorite ? AppColors.primaryLogo : Color(0xff929292),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Text(
                  '已收藏食譜',
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w700,
                    height: 1.2125.h,
                    letterSpacing: 0.8,
                    color: Color(0xffffffff),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

//未收藏-----------------------------------------------------------
class Unrecipe extends StatefulWidget {
  const Unrecipe({super.key});

  @override
  State<Unrecipe> createState() => _UnrecipeState();
}

class _UnrecipeState extends State<Unrecipe> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amberAccent,
      height: 525.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: FutureBuilder(
        future: SqliteMyRecipe().searchStatus1Recipe(),
        builder: (context, snap) {
          if (snap.hasData) {
            List<Map<String, dynamic>> foodListData =
                snap.data as List<Map<String, dynamic>>;

            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodListData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      String foodName = foodListData[index]['name'];
                      int recipe_id = foodListData[index]['id'];
                      return UnrecipeUI(
                        name: foodName,
                        recipe_id: recipe_id,
                        reload: () {
                          setState(() {});
                        },
                      );
                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class UnrecipeUI extends StatelessWidget {
  final String name;
  final int recipe_id;
  final VoidCallback reload;

  UnrecipeUI(
      {super.key,
      required this.name,
      required this.recipe_id,
      required this.reload});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
            .add(Page0(page: 1));
        BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
            .add(GetId(recipeId: recipe_id));
      },
      child: Container(
        constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
        margin: EdgeInsets.only(top: 10.h),
        width: 330.w,
        decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                color: Color.fromARGB(216, 191, 197, 197),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(-1, 1),
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.w)),
            border: Border.all(color: Colors.grey)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 200.w,
              //height: 150.h,

              margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(name),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 10.w),
              child: Row(children: [
                GestureDetector(
                  onTap: () {
                    _dialogFavoriteBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.favorite_border),
                ),
                GestureDetector(
                  onTap: () async {
                    BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
                        .add(Page0(page: 2));
                    BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
                        .add(GetId(recipeId: recipe_id));

                    await SqliteMyRecipe().searchFoodIdAndWeight(recipe_id);
                  },
                  child: const Icon(Icons.edit),
                ),
                GestureDetector(
                  onTap: () async {
                    _dialogCopyBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.copy),
                ),
                GestureDetector(
                  onTap: () {
                    _dialogDeleteBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.delete),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}

Future<void> _dialogFavoriteBuilder(BuildContext context, recipeId, reload) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '收藏您的食譜後，將無法再次編輯，是否確定要收藏?',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                SqliteMyRecipe().status1to0(recipeId);
                reload();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

//未收藏-----------------------------------------------------------

//收藏-----------------------------------------------------------

class Favoriteunrecipe extends StatefulWidget {
  const Favoriteunrecipe({super.key});

  @override
  State<Favoriteunrecipe> createState() => _Favoriteunrecipe();
}

class _Favoriteunrecipe extends State<Favoriteunrecipe> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amberAccent,
      height: 525.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: FutureBuilder(
        future: SqliteMyRecipe().searchStatus0Recipe(),
        builder: (context, snap) {
          if (snap.hasData) {
            List<Map<String, dynamic>> foodListData =
                snap.data as List<Map<String, dynamic>>;
            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodListData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      String foodName = foodListData[index]['name'];
                      int recipe_id = foodListData[index]['id'];
                      return FavoriteUnrecipeUI(
                        name: foodName,
                        recipe_id: recipe_id,
                        reload: () {
                          setState(() {});
                        },
                      );
                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class FavoriteUnrecipeUI extends StatelessWidget {
  final String name;
  final int recipe_id;
  final VoidCallback reload;

  FavoriteUnrecipeUI(
      {super.key,
      required this.name,
      required this.recipe_id,
      required this.reload});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
            .add(Page0(page: 1));
        BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
            .add(GetId(recipeId: recipe_id));
      },
      child: Container(
        constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
        margin: EdgeInsets.only(top: 10.h),
        width: 330.w,
        decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              color: Color.fromARGB(216, 191, 197, 197),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(-1, 1),
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.w)),
          border: Border.all(color: Colors.grey),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 200.w,
              //height: 150.h,

              margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(name),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 10.w),
              child: Row(children: [
                GestureDetector(
                  onTap: () {
                    _dialogNotFavoriteBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.favorite),
                ),
                SizedBox(
                  width: 5.w,
                ),
                GestureDetector(
                  onTap: () async {
                    _dialogCopyBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.copy),
                ),
                GestureDetector(
                  onTap: () {
                    _dialogDeleteBuilder(context, recipe_id, reload);
                  },
                  child: const Icon(Icons.delete),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}

Future<void> _dialogNotFavoriteBuilder(BuildContext context, recipeId, reload) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確定要解除收藏?',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                SqliteMyRecipe().status0to1(recipeId);
                reload();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}
//收藏-----------------------------------------------------------

//共用------------------------------------------------------------
Future<void> _dialogDeleteBuilder(BuildContext context, recipeId, reload) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確認要刪除？',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                await SqliteMyRecipe().deleteRecipe(recipeId);
                reload();
                toast("已刪除！");
                // ignore: use_build_context_synchronously
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Future<void> _dialogCopyBuilder(BuildContext context, recipeId, reload) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確認要複製此食譜？',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                await SqliteMyRecipe().copyRecipe(recipeId);
                reload();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}

Widget text(name) {
  return Container(
    margin: EdgeInsets.only(top: 20.h, left: 5.w, bottom: 10.h),
    child: Text(
      name,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125.h,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}
//共用------------------------------------------------------------

//detail--------------------------------------------------------------
class Detail extends StatefulWidget {
  Detail({super.key});
  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  bool foodorele = true;

  void change() {
    setState(() {
      foodorele = !foodorele;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthMyRecipeRecipeIdBlocs,
        FurKidHeslthMyRecipeRecipeIdStates>(
      builder: (context, state) {
        return Container(
          //color: Colors.amber,
          margin: EdgeInsets.only(right: 30.w, left: 30.w, top: 20.h),
          child: Column(
            children: [
              FutureBuilder(
                future: SqliteMyRecipe().searchNameAndCategory(state.recipeId),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Map<String, dynamic>> foodListData =
                        snapshot.data as List<Map<String, dynamic>>;
                    if (foodListData.isNotEmpty) {
                      return Column(
                        children: [
                          Text(
                            '${foodListData[0]['recipename']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 24.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: Color(0xff000000),
                            ),
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            '${foodListData[0]['categoryname']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: Color(0xff000000),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                onPressed: () {
                                  if (foodorele == false) {
                                    change();
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  // 可以根据需要添加其他样式属性
                                ),
                                child: text('食材'),
                              ),
                              TextButton(
                                onPressed: () {
                                  if (foodorele == true) {
                                    change();
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  // 可以根据需要添加其他样式属性
                                ),
                                child: text('營養成分'),
                              ),
                            ],
                          ),
                          Container(
                            height: 3.h,
                            color: Colors.black12,
                            margin: EdgeInsets.only(bottom: 10.h),
                          ),
                          foodorele ? FoodDetail() : elementDetail(),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            '適用疾病症狀:${foodListData[0]['illtags']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 20.sp,
                              fontWeight: FontWeight.w500,
                              height: 1.5.h,
                              letterSpacing: 2.4,
                              color: Color.fromARGB(255, 29, 29, 29),
                            ),
                          )
                        ],
                      );
                    }
                  }
                  return Container();
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

class FoodDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthMyRecipeRecipeIdBlocs,
        FurKidHeslthMyRecipeRecipeIdStates>(builder: (context, state) {
      return Container(
        height: 520.h,
        margin: EdgeInsets.only(bottom: 10.h),
        child: FutureBuilder(
          future: SqliteMyRecipe().searchRecipeList(state.recipeId),
          builder: (context, snap) {
            if (snap.hasData) {
              List<Map<String, dynamic>> foodListData =
                  snap.data as List<Map<String, dynamic>>;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: foodListData.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        String foodname = foodListData[index]['name'];
                        int quantity = foodListData[index]['quantity'];
                        return FoodDetailUi(
                            foodname: foodname, quantity: quantity);
                      },
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      );
    });
  }
}

class FoodDetailUi extends StatelessWidget {
  final int quantity;
  final String foodname;

  const FoodDetailUi(
      {super.key, required this.foodname, required this.quantity});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5.h),
      width: 200.w,
      height: 70.h,
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            blurRadius: 1,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
        border: Border.all(color: Colors.black12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                foodname,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  height: 1.5.h,
                  letterSpacing: 2.4,
                  color: Color.fromARGB(255, 29, 29, 29),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Container(
                width: 55.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                  border: Border.all(
                      color: const Color.fromARGB(31, 246, 147, 147)),
                ),
                child: Center(
                  child: Text(
                    '${quantity} g',
                    style: SafeGoogleFont(
                      'Inter',
                      fontWeight: FontWeight.w700,
                      height: 1.5.h,
                      letterSpacing: 1,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class elementDetail extends StatelessWidget {
  double hundredgram(double all, double foodweight) {
    return double.parse((all / foodweight * 100).toStringAsFixed(2));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthMyRecipeRecipeIdBlocs,
        FurKidHeslthMyRecipeRecipeIdStates>(builder: (context, state) {
      return Container(
        height: 520.h,
        margin: EdgeInsets.only(top: 10.h),
        child: Center(
          child: FutureBuilder(
            future: SqliteMyRecipe().searchDetail(state.recipeId),
            builder: (context, snap) {
              if (snap.hasData) {
                List foodListData = snap.data as List;
                double foodweight =
                    foodListData[0]['food_weight'].toDouble() ?? 0.0;
                double finishweight =
                    foodListData[0]['finish_weight'].toDouble() ?? 0.0;
                double allkcal = foodListData[0]['calories'].toDouble() ?? 0.0;
                double allwater = foodListData[0]['water'].toDouble() ?? 0.0;
                double allprotein =
                    foodListData[0]['protein'].toDouble() ?? 0.0;

                double allfat = foodListData[0]['fat'].toDouble() ?? 0.0;
                double allcarbohydrate =
                    foodListData[0]['carbohydrate'].toDouble() ?? 0.0;
                double allfiber = foodListData[0]['fiber'].toDouble() ?? 0.0;
                double allash = foodListData[0]['ash'].toDouble() ?? 0.0;

                double kcal100g =
                    foodListData[0]['calories_per_100g'].toDouble() ?? 0.0;
                double water100g =
                    foodListData[0]['water_per_100g'].toDouble() ?? 0.0;
                double protein100g =
                    foodListData[0]['protein_per_100g'].toDouble() ?? 0.0;
                double fat100g =
                    foodListData[0]['fat_per_100g'].toDouble() ?? 0.0;
                double carbohydrate100g =
                    foodListData[0]['carbohydrate_per_100g'].toDouble() ?? 0.0;
                double fiber100g =
                    foodListData[0]['fiber_per_100g'].toDouble() ?? 0.0;
                double ash100g =
                    foodListData[0]['ash_per_100g'].toDouble() ?? 0.0;
                double proteinpercent = foodListData[0]['dry_rate_protein'];

                double fatpercent = foodListData[0]['dry_rate_fat'];

                double carbohydratepercent = foodListData[0]['dry_rate_carbo'];
                double fiberpercent = foodListData[0]['dry_rate_fiber'];
                double ashpercent = foodListData[0]['dry_rate_ash'];
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      title('營養成分分析'),
                      element(
                          '食材重量', '${foodweight}g', '完成重量', '${finishweight}g'),
                      title('食譜營養成分含量'),
                      element('熱量', '${allkcal}kcal', '水分', '${allwater}g'),
                      element('粗蛋白質', '${allprotein}g', '粗脂肪', '${allfat}g'),
                      element('碳水化合物', '${allcarbohydrate}g', '粗纖維',
                          '${allfiber}g'),
                      element('灰份', '${allash}g', '', ''),
                      title('每100g營養成分'),
                      element('熱量', '${kcal100g}kcal', '水分', '${water100g}g'),
                      element('粗蛋白質', '${protein100g}g', '粗脂肪', '${fat100g}g'),
                      element('碳水化合物', '${carbohydrate100g}g', '粗纖維',
                          '${fiber100g}g'),
                      element('灰份', '${ash100g}g', '', ''),
                      title('乾物比'),
                      element(
                          '粗蛋白質', '$proteinpercent%', '粗脂肪', '$fatpercent%'),
                      element('碳水化合物', '$carbohydratepercent%', '粗纖維',
                          '$fiberpercent%'),
                      element('灰份', '$ashpercent%', '', ''),
                      title('主要營養成分'),
                      bigcircle(1,
                          water: allwater,
                          protein: allprotein,
                          fat: allfat,
                          carbo: allcarbohydrate,
                          ash: allash),
                      title('乾物比'),
                      bigcircle(2,
                          protein: proteinpercent,
                          fat: fatpercent,
                          carbo: carbohydratepercent,
                          ash: ashpercent),
                      title('熱量來源分布(熱量比)'),
                      bigcircle(3,
                          protein: allprotein * 3.5,
                          fat: allfat * 8.5,
                          carbo: allcarbohydrate * 3.5),
                    ],
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      );
    });
  }
}

Widget element(
  String name1,
  String unit1,
  String name2,
  String unit2,
) {
  return Column(
    children: [
      Container(
        margin: EdgeInsets.only(left: 10.w, right: 10.w, top: 5.h),
        width: 300.w,
        height: 25.h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Text(
                name1,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.7,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    unit1,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 13,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Expanded(
              child: Text(
                name2,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.7,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    unit2,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 13,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      Container(
        margin:
            EdgeInsets.only(left: 20.w, right: 20.w, top: 7.h, bottom: 15.h),
        height: 2.h,
        color: const Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget title(String text) {
  return IntrinsicWidth(
    child: Container(
      padding: EdgeInsets.only(left: 15.w, right: 15.w),
      margin: EdgeInsets.only(bottom: 15.h),
      height: 25,
      decoration: BoxDecoration(
        color: const Color(0xffe4007f),
        borderRadius: BorderRadius.circular(100),
      ),
      child: Center(
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 14,
            fontWeight: FontWeight.w700,
            height: 1.2125,
            letterSpacing: 0.7,
            color: const Color(0xffffffff),
          ),
        ),
      ),
    ),
  );
}

Widget circle(String text) {
  Color ccco = const Color(0xFF0075FF);
  if (text == '粗蛋白質') {
    ccco = const Color(0xFFE66676);
  } else if (text == '粗脂肪') {
    ccco = const Color(0xFFFF792D);
  } else if (text == '碳水') {
    ccco = const Color(0xFFAACD06);
  } else if (text == '灰份') {
    ccco = const Color(0xFF565555);
  }
  return Container(
    margin: EdgeInsets.only(left: 8.w, right: 3.w),
    width: 20, // 宽度
    height: 20, // 高度
    decoration: BoxDecoration(
      color: ccco, // 背景颜色
      shape: BoxShape.circle, // 指定形状为圆形
    ),
  );
}

Widget bigcircle(
  int number, {
  double? water,
  double? protein,
  double? fat,
  double? carbo,
  double? ash,
}) {
  var choice = <Widget>[
    circle('水分'),
    circletext('水分'),
    circle('粗蛋白質'),
    circletext('粗蛋白質'),
    circle('粗脂肪'),
    circletext('粗脂肪'),
    circle('碳水'),
    circletext('碳水'),
    circle('灰份'),
    circletext('灰份'),
  ];
  if (number == 2) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
      circle('灰份'),
      circletext('灰份'),
    ];
  }
  if (number == 3) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
    ];
  }
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    width: 290.w,
    height: 350.h,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      shadows: const [
        BoxShadow(
          color: Color(0x3F000000),
          blurRadius: 4,
          offset: Offset(0, 0),
          spreadRadius: 0,
        )
      ],
    ),
    child: Column(
      children: [
        PieChartAnalyze(
          number: number,
          water: water,
          protein: protein,
          fat: fat,
          carbo: carbo,
          ash: ash,
        ),
        Container(
          margin: EdgeInsets.only(top: 25.h, right: 5.w),
          child: IntrinsicWidth(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: choice),
          ),
        )
      ],
    ),
  );
}

Widget circletext(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.black,
      fontSize: 10,
      fontFamily: 'Inter',
      fontWeight: FontWeight.w300,
      height: 0,
      letterSpacing: 0.50,
    ),
  );
}

class PieChartAnalyze extends StatelessWidget {
  final double? water;
  final double? protein;
  final double? fat;
  final double? carbo;
  final double? ash;
  final int number;
  PieChartAnalyze(
      {super.key,
      required this.number,
      this.water = 0.0,
      this.protein = 0.0,
      this.fat = 0.0,
      this.carbo = 0.0,
      this.ash = 0.0});

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: PieChart(
        PieChartData(
          centerSpaceRadius: 0,
          sectionsSpace: 1,
          pieTouchData: PieTouchData(
            touchCallback:
                (FlTouchEvent touchEvent, PieTouchResponse? touchResponse) {
              if (touchEvent is FlTapUpEvent) {}
            },
          ),
          sections: [
            if (number == 1)
              PieChartSectionData(
                  color: const Color(0xFF0075FF),
                  value: water,
                  title:
                      '${(water! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 0.7),

            PieChartSectionData(
                color: const Color(0xFFE66676),
                value: protein,
                title: number == 1
                    ? '${(protein! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(protein! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(protein! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            PieChartSectionData(
                color: const Color(0xFFFF792D),
                value: fat,
                title: number == 1
                    ? '${(fat! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(fat! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(fat! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),

            PieChartSectionData(
                color: const Color(0xFFAACD06),
                value: carbo,
                title: number == 1
                    ? '${(carbo! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(carbo! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(carbo! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            if (number == 2 || number == 1)
              PieChartSectionData(
                  color: const Color(0xFF565555),
                  value: ash,
                  title: number == 1
                      ? '${(ash! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                      : '${(ash! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 1),
            // Add more PieChartSectionData for other sections
          ],
          // Add more chart configurations here
        ),
      ),
    );
  }
}

//detail--------------------------------------------------------------
//Revise---------------------------------------------------------------------

class Revise extends StatefulWidget {
  Revise({super.key});

  @override
  State<Revise> createState() => _ReviseState();
}

class _ReviseState extends State<Revise> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameCodeController = TextEditingController();
  TextEditingController kindController = TextEditingController();
  TextEditingController finishweightController = TextEditingController();
  TextEditingController sickController = TextEditingController();
  int totalgram = 0;

  void totalgramplus() async {
    int aaa = await SqliteMyRecipe().foodGramtotal();

    setState(() {
      BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context)
          .add(Toturetotal());
      totalgram = aaa;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthMyRecipeRecipeIdBlocs,
        FurKidHeslthMyRecipeRecipeIdStates>(
      builder: (context, state) {
        return BlocBuilder<FurKidHeslthReviseRecipeBlocs,
            FurKidHeslthReviseRecipeStates>(builder: (context, state2) {
          return FutureBuilder(
            future: SqliteMyRecipe().searchControllerName(state.recipeId),
            builder: (context, snap) {
              if (snap.hasData) {
                List<Map<String, dynamic>> foodListData =
                    snap.data as List<Map<String, dynamic>>;
                int foodTotalGram = foodListData[0]['food_weight'];
                if (state2.totalsee == false) {
                  totalgram = foodTotalGram;
                }
                if (state2.namesee) {
                  nameCodeController.text =
                      '${foodListData[0]['nameCodeController']}';
                  kindController.text = '${foodListData[0]['kindController']}';
                  finishweightController.text =
                      '${foodListData[0]['finish_weight']}';
                  sickController.text = '${foodListData[0]['ill_tags']}';
                }

                return SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 35.w),
                    width: double.infinity,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          text('食譜名稱'),
                          BuildTextField1(
                            codeController: nameCodeController,
                          ),
                          text('類別'),
                          BuildTextField1(
                            codeController: kindController,
                          ),
                          text('食材'),
                          ReviseFood(totalgramplus: totalgramplus),
                          text('食材合計重量(g)'),
                          //食材合計重量
                          Container(
                            padding: EdgeInsets.only(left: 10.w),
                            margin: EdgeInsets.only(bottom: 10.h),
                            height: 60.h,
                            width: 320.w,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xffececec),
                              border: Border.all(
                                  color: Colors.transparent), // 去掉默认边框
                            ),
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: BlocBuilder<
                                        FurKidHeslthReviseRecipeBlocs,
                                        FurKidHeslthReviseRecipeStates>(
                                    builder: (context, state) {
                                  return Text(
                                    state.totalsee
                                        ? '$totalgram'
                                        : '$foodTotalGram',
                                    //'${state.totalgram}',
                                    style: TextStyle(
                                        fontFamily: "Avenir",
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14.sp),
                                  );
                                })),
                          ),
                          text('完成重量(g)'),
                          BuildTextField1(
                            codeController: finishweightController,
                          ),
                          // ButtonAnalyze(
                          //   name: '營養成分分析',
                          //   formKey: _formKey,
                          //   finishweightController: finishweightController,
                          //   foodTotalGram: foodTotalGram,
                          //   tobody3: widget.tobody3,
                          //   onFinishWeightChanged: widget.onFinishWeightChanged,
                          // ),
                          text('適用疾病症狀(多項請以,隔開)'),
                          //適用疾病症狀
                          TextFormField(
                            controller: sickController,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            // inputFormatters: [
                            //   FilteringTextInputFormatter.allow(
                            //       RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
                            // ],
                            keyboardType: TextInputType.multiline,
                            decoration: const InputDecoration(
                              hintText: '文字輸入',
                              isDense: true,
                              filled: true,
                              fillColor: Color(0xffececec),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                    color: Colors.transparent), // 去掉默认底线
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                    color: Color.fromARGB(0, 11, 128, 32)),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                    color: Colors.transparent), // 去掉错误边框
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                    color: Colors.transparent), // 去掉错误边框
                              ),
                            ),
                            style: TextStyle(
                                fontFamily: "Avenir",
                                fontWeight: FontWeight.normal,
                                fontSize: 14.sp),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10.h),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ButtonNeworCancel(
                                  buttonname: '修改',
                                  nameCodeController: nameCodeController,
                                  kindController: kindController,
                                  finishweightController:
                                      finishweightController,
                                  sickController: sickController,
                                  foodTotalGram: totalgram,
                                  formkey: _formKey,
                                ),
                                ButtonNeworCancel(
                                  buttonname: '取消',
                                  nameCodeController: nameCodeController,
                                  kindController: kindController,
                                  finishweightController:
                                      finishweightController,
                                  sickController: sickController,
                                  foodTotalGram: totalgram,
                                  formkey: _formKey,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
              return Container();
            },
          );
        });
      },
    );
  }
}

class BuildTextField1 extends StatelessWidget {
  final TextEditingController codeController;

  BuildTextField1({
    Key? key,
    required this.codeController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.h),
      child: TextFormField(
        controller: codeController,
        //keyboardType: TextInputType.multiline,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return '請勿為空';
          }

          return null;
        },
        onChanged: (value) {
          BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context)
              .add(Tofalsename());
        },
        decoration: const InputDecoration(
          hintText: '文字輸入',
          isDense: true,
          filled: true,
          fillColor: Color(0xffececec),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉默认底线
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Color.fromARGB(0, 11, 128, 32)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
          ),
        ),
        style: TextStyle(
            fontFamily: "Avenir",
            fontWeight: FontWeight.normal,
            fontSize: 14.sp),
      ),
    );
  }
}

class ReviseFood extends StatefulWidget {
  final void Function() totalgramplus;
  ReviseFood({
    super.key,
    required this.totalgramplus,
  });

  @override
  State<ReviseFood> createState() => _ReviseFoodState();
}

class _ReviseFoodState extends State<ReviseFood> {
  List<Map<String, dynamic>> foodFinalResult = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.h),
      height: 200.h,
      width: double.infinity,
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(8.w)),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                height: 25.h,
                width: 25.w,
                child: FloatingActionButton(
                  onPressed: () async {
                    BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
                        .add(Page0(page: 3));
                  },
                  foregroundColor: Colors.black,
                  backgroundColor: Colors.white,
                  child: const Icon(
                    Icons.add,
                    size: 20,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                width: 100.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.w)),
                ),
                child: Center(
                  child: Text(
                    '食材',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 15.w,
              ),
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                width: 100.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.w)),
                ),
                child: Center(
                  child: Text(
                    '重量(g)',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.h,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: FutureBuilder(
                future: SqliteMyRecipe().searchFood(),
                builder: (context, snap) {
                  if (snap.hasData) {
                    List<Map<String, dynamic>> foodListData =
                        snap.data as List<Map<String, dynamic>>;
                    // print('as');
                    // print(foodListData);
                    // if (foodFinalResult.isEmpty) {
                    //   foodFinalResult.addAll(foodListData);
                    // } else if (foodFinalResult.length < foodListData.length) {
                    //   for (int i = 0; i < foodListData.length; i++) {
                    //     int sta = 0;
                    //     for (int j = 0; j < foodFinalResult.length; j++) {
                    //       if (foodFinalResult[j]['name'] ==
                    //           foodListData[i]['name']) {
                    //         sta = 0;
                    //         break;
                    //       } else {
                    //         sta = 1;
                    //       }
                    //     }
                    //     if (sta == 1) {
                    //       double weight = foodListData[i]['weight'] ?? 0;
                    //       String name = foodListData[i]['name'];
                    //       int id = foodListData[i]['id'];
                    //       foodFinalResult
                    //           .add({'weight': weight, 'name': name, 'id': id});
                    //     }
                    //   }
                    // } else if (foodListData.isEmpty) {
                    //   foodFinalResult = [];
                    // } else if (foodFinalResult.length > foodListData.length) {
                    //   for (int i = 0; i < foodFinalResult.length; i++) {
                    //     int sta = 0;
                    //     for (int j = 0; j < foodListData.length; j++) {
                    //       if (foodFinalResult[i]['name'] ==
                    //           foodListData[j]['name']) {
                    //         sta = 0;
                    //         break;
                    //       } else {
                    //         sta = 1;
                    //       }
                    //     }
                    //     if (sta == 1) {
                    //       foodFinalResult.removeWhere((element) =>
                    //           element['name'] == foodFinalResult[i]['name']);
                    //     }
                    //   }
                    // }
                    // print('$foodListData資料庫');
                    // print('$foodFinalResult最終');
                    return Column(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: foodListData.length,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            String foodname = foodListData[index]['name'];
                            double dwe = foodListData[index]['weight'] ?? 0;
                            int quantity = dwe.toInt();
                            int foodid = foodListData[index]['id'];
                            return ReviseFoodTable(
                                foodname: foodname,
                                quantity: quantity,
                                foodid: foodid,
                                reload: () {
                                  setState(() {});
                                },
                                totalgramplus: widget.totalgramplus);
                          },
                        ),
                      ],
                    );
                  }
                  return Container();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ReviseFoodTable extends StatefulWidget {
  final String foodname;
  final int quantity;
  final int foodid;
  final VoidCallback reload;
  final void Function() totalgramplus;
  ReviseFoodTable(
      {super.key,
      required this.foodname,
      required this.quantity,
      required this.foodid,
      required this.reload,
      required this.totalgramplus});

  @override
  State<ReviseFoodTable> createState() => _ReviseFoodTableState();
}

class _ReviseFoodTableState extends State<ReviseFoodTable> {
  TextEditingController gramController = TextEditingController();
  int gram = 0;

  @override
  void initState() {
    super.initState();

    gram = widget.quantity;
    // _ReviseFoodTableStateManager.register(this);
    // BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context)
    //     .add(Gettotal(plus: gram));
  }

  // void removeTableState() {
  //   _ReviseFoodTableStateManager.remove(this);
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 60,
          //width: 320.w,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(left: 5.w, top: 2.h, right: 30.w),
                          height: 25.h,
                          child: FloatingActionButton(
                            mini: true,
                            onPressed: () async {
                              print('---');
                              //      removeTableState();
                              SqliteMyRecipe().deletefood(widget.foodid);
                              widget.reload();
                              widget.totalgramplus();
                              // BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(
                              //         context)
                              //     .add(Gettotal(
                              //         totalgram: _ReviseFoodTableStateManager
                              //             .getTotalGrams()));
                            },
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            child: const Icon(
                              Icons.remove,
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            widget.foodname,
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 25.w, right: 35.w),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.w),
                          )),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: FutureBuilder(
                                  future: SqliteMyRecipe()
                                      .searchfoodweight(widget.foodid),
                                  builder: (context, snap) {
                                    if (snap.hasData) {
                                      List<Map<String, dynamic>> foodListData =
                                          snap.data
                                              as List<Map<String, dynamic>>;
                                      // print(foodListData);
                                      double dwdas =
                                          foodListData[0]['weight'] ?? 0;
                                      gram = dwdas.toInt();
                                      gramController.text = gram.toString();
                                      // BlocProvider.of<
                                      //             FurKidHeslthReviseRecipeBlocs>(
                                      //         context)
                                      //     .add(Gettotal(
                                      //         totalgram:
                                      //             _ReviseFoodTableStateManager
                                      //                 .getTotalGrams()));
                                      // BlocProvider.of<
                                      //             FurKidHeslthReviseRecipeBlocs>(
                                      //         context)
                                      //     .add(Gettotal(plus: gram));
                                      return TextField(
                                        controller: gramController,
                                        keyboardType: TextInputType.multiline,
                                        decoration: const InputDecoration(
                                          isDense: true,
                                          //hintText: 'sda',
                                          enabledBorder: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                        ),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.allow(
                                              RegExp(r'[0-9]')),
                                        ],
                                        style: SafeGoogleFont(
                                          'Inter',
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w200,
                                          color: Colors.black,
                                        ),
                                        onChanged: (value) async {
                                          if (value.isNotEmpty) {
                                            setState(
                                              () {
                                                gram = int.parse(value);
                                                // BlocProvider.of<
                                                //             FurKidHeslthReviseRecipeBlocs>(
                                                //         context)
                                                //     .add(Gettotal(
                                                //         totalgram:
                                                //             _ReviseFoodTableStateManager
                                                //                 .getTotalGrams()));
                                                SqliteMyRecipe().foodGramRevise(
                                                    widget.foodid, gram);
                                                widget.totalgramplus();
                                              },
                                            );
                                          }
                                        },
                                        autocorrect: false,
                                        obscureText: false,
                                      );
                                    }
                                    return Container();
                                  }),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Container(
                              //padding: EdgeInsets.only(bottom: 10.h),
                              width: 20.w,
                              height: 37.h,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8.w),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Expanded(
                                    // width: double.infinity,
                                    // height: 18.5.h,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          gram = int.parse(gramController.text);

                                          gram++;
                                          gramController.text = gram.toString();
                                          SqliteMyRecipe().foodGramRevise(
                                              widget.foodid, gram);

                                          widget.totalgramplus();
                                        });
                                      },
                                      child: RotationTransition(
                                        turns: AlwaysStoppedAnimation(0.5),
                                        child: Image.asset(
                                          'assets/appdesign/images/btncountrycodesresearch-w53.png',
                                          width: 10.w,
                                          height: 10.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          gram = int.parse(gramController.text);
                                          if (gram > 1) {
                                            gram--;
                                          }
                                          SqliteMyRecipe().foodGramRevise(
                                              widget.foodid, gram);
                                          gramController.text = gram.toString();
                                          widget.totalgramplus();
                                        });
                                      },
                                      child: SizedBox(
                                        width: 10.w,
                                        height: 10.h,
                                        child: Image.asset(
                                          'assets/appdesign/images/btncountrycodesresearch-w53.png',
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          color: Colors.black,
          height: 1.h,
        )
      ],
    );
  }
}

class ButtonNeworCancel extends StatefulWidget {
  final String buttonname;
  //final void Function() goBackNeedFunction;
  final GlobalKey<FormState> formkey;
  final TextEditingController nameCodeController;
  final TextEditingController kindController;
  final TextEditingController finishweightController;
  final TextEditingController sickController;
  final foodTotalGram;

  const ButtonNeworCancel({
    super.key,
    required this.buttonname,
    required this.formkey,
    required this.nameCodeController,
    required this.kindController,
    required this.finishweightController,
    required this.sickController,
    //required this.goBackNeedFunction,
    required this.foodTotalGram,
  });

  @override
  State<ButtonNeworCancel> createState() => _ButtonNeworCancelState();
}

class _ButtonNeworCancelState extends State<ButtonNeworCancel> {
  void goback() {
    BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context).add(Page0(page: 0));
    BlocProvider.of<FurKidHeslthMyRecipeRecipeIdBlocs>(context)
        .add(GetId(recipeId: 0));
    BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context).add(Tofalsetotal());
    BlocProvider.of<FurKidHeslthReviseRecipeBlocs>(context).add(Totruename());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15.h),
      child: Builder(builder: (context) {
        return BlocBuilder<FurKidHeslthMyRecipeRecipeIdBlocs,
            FurKidHeslthMyRecipeRecipeIdStates>(builder: (context, state) {
          return TextButton(
            onPressed: () async {
              if (widget.buttonname == '修改') {
                if (widget.formkey.currentState!.validate()) {
                  Map data = {
                    "name": widget.nameCodeController.text,
                    "kind": widget.kindController.text,
                    //"weight": allweightController.text,
                    "finish": widget.finishweightController.text,
                    "sick": widget.sickController.text,
                    'foodtotalgram': widget.foodTotalGram,
                  };
                  // print(data);
                  bool notnuLL = await SqliteMyRecipe().checkfoodlistnotnull();
                  if (notnuLL) {
                    bool notZero =
                        await SqliteMyRecipe().checkfoodlistnotzero();
                    if (notZero) {
                      await SqliteMyRecipe().savereviserecipe(
                          state.recipeId,
                          widget.nameCodeController.text,
                          widget.kindController.text,
                          widget.foodTotalGram,
                          widget.finishweightController.text,
                          widget.sickController.text);
                      Fluttertoast.showToast(
                        msg: '您已成功修改食譜',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 24.0,
                      );
                      goback();
                      await SqliteMyRecipe().foodTablestatusInit();
                    } else {
                      Fluttertoast.showToast(
                        msg: '食材重量不得為 0',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 24.0,
                      );
                    }
                  } else {
                    Fluttertoast.showToast(
                      msg: '請加入食材',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 24.0,
                    );
                  }
                }
              }
              if (widget.buttonname == '取消') {
                goback();
                await SqliteMyRecipe().foodTablestatusInit();
              }
            },
            style: ButtonStyle(
              overlayColor: MaterialStateProperty.all(Colors.transparent),
              backgroundColor:
                  MaterialStateProperty.all<Color>(AppColors.primaryLogo),
              shape: MaterialStateProperty.all<OutlinedBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
            // 自定義內邊距
            child: Padding(
              padding: EdgeInsets.only(left: 10.w, right: 10.w),
              child: Text(
                widget.buttonname,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  height: 1.2125.h,
                  letterSpacing: 0.8,
                  color: const Color(0xffffffff),
                ),
              ),
            ),
          );
        });
      }),
    );
  }
}

//Revise---------------------------------------------------------------------

//plusfood---------------------------------------------------------------------
class plusfood extends StatelessWidget {
  const plusfood({super.key});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35.w, vertical: 5.w),
      //color: Colors.black38,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              '輸入食材',
              style: SafeGoogleFont(
                'Inter',
                fontSize: 16,
                // fontWeight: FontWeight.bold,
                // height: 1.2125,
                // letterSpacing: 0.8,
                color: Color(0xff000000),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10.h)),
          BuildTextField(),
          Padding(padding: EdgeInsets.only(bottom: 15.h)),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: foodcategory(),
                  ),
                ),
                Container(
                  width: 80.w,
                  height: 45.h,
                  margin: EdgeInsets.only(top: 15.h),
                  child: TextButton(
                    onPressed: () async {
                      // _ReviseFoodTableStateManager.addFoodTableState.clear();
                      // print(_ReviseFoodTableStateManager.addFoodTableState);

                      FoodCategoryNamesManager.closeAll();

                      BlocProvider.of<FurKidHeslthMyRecipePageBlocs>(context)
                          .add(Page0(page: 2));

                      await SqliteMyRecipe().foodplusbutton();
                      //_ReviseFoodTableStateManager.getTotalGrams();
                    },
                    style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all(Colors.transparent),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          AppColors.primaryLogo),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    // 自定義內邊距
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.w, right: 10.w),
                      child: Text(
                        '加入',
                        style: SafeGoogleFont(
                          'Inter',
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w700,
                          height: 1.2125.h,
                          letterSpacing: 0.8,
                          color: const Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10.h)),
        ],
      ),
    );
  }
}

class foodcategory extends StatefulWidget {
  const foodcategory({
    super.key,
  });
  @override
  State<foodcategory> createState() => _foodcategoryState();
}

class _foodcategoryState extends State<foodcategory> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: SqliteMyRecipe().getCategoryQuery(),
      builder: (context, snap) {
        if (snap.hasData) {
          List<Map<String, dynamic>> foodListData =
              snap.data as List<Map<String, dynamic>>;
          return Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];
                  int id = foodListData[index]['id'];
                  return Column(
                    children: [
                      FoodCategoryName(
                        name: foodName,
                        categortId: id,
                      )
                    ],
                  );
                },
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}

class FoodCategoryName extends StatefulWidget {
  final String name;
  final int categortId;

  FoodCategoryName({required this.name, required this.categortId});

  @override
  _FoodCategoryNameState createState() => _FoodCategoryNameState();
}

class _FoodCategoryNameState extends State<FoodCategoryName> {
  bool isOpen = false;

  @override
  void initState() {
    super.initState();
    FoodCategoryNamesManager.register(this);
  }

  void closeOtherFoodCategoryNames() {
    FoodCategoryNamesManager.closeOthers(this);
  }

  void toggleCategoryStateToFalse() {
    setState(() {
      isOpen = false;
    });
  }

  void toggleCategoryStateToTrue() {
    setState(() {
      isOpen = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () async {
            closeOtherFoodCategoryNames();
          },
          child: Container(
            padding: EdgeInsets.only(left: 15),
            margin: EdgeInsets.only(top: 5, bottom: 5.h),
            width: 310,
            height: 60,
            decoration: const BoxDecoration(
              color: Color(0xffececec),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.name,
                style: const TextStyle(
                  color: Color(0xFF595757),
                  fontSize: 18,
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1,
                ),
              ),
            ),
          ),
        ),
        isOpen == true
            ? foodDetail(
                categortId: widget.categortId,
              )
            : Container(),
      ],
    );
  }
}

class FoodCategoryNamesManager {
  static final List<_FoodCategoryNameState> _foodCategoryNames = [];

  static void register(_FoodCategoryNameState foodCategoryName) {
    _foodCategoryNames.add(foodCategoryName);
  }

  static void closeAll() {
    for (var category in _foodCategoryNames) {
      if (category.mounted) {
        category.toggleCategoryStateToFalse();
      }
    }
  }

  static void closeOthers(_FoodCategoryNameState openedCategory) {
    for (var category in _foodCategoryNames) {
      if (category.mounted) {
        if (category == openedCategory) {
          if (category.isOpen) {
            category.toggleCategoryStateToFalse();
          } else {
            category.toggleCategoryStateToTrue();
          }
        } else {
          category.toggleCategoryStateToFalse();
        }
      }
    }
  }
}

class foodDetail extends StatefulWidget {
  final int categortId;

  foodDetail({
    required this.categortId,
  });
  @override
  _foodDetailState createState() => _foodDetailState();
}

class _foodDetailState extends State<foodDetail> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: SqliteMyRecipe().getCategoryFoodQuery(widget.categortId),
      builder: (context, snap) {
        if (snap.hasData) {
          List foodListData = snap.data as List;
          return Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];
                  int id = foodListData[index]['id'];
                  int status = foodListData[index]['status'];
                  if (status == 0) {
                    return Container();
                  } else {
                    return Column(
                      children: [
                        FoodDetailName(
                          detailName: foodName,
                          id: id,
                          status: status,
                        )
                      ],
                    );
                  }
                },
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}

class FoodDetailName extends StatefulWidget {
  final String detailName;
  final int id;
  int status;

  FoodDetailName(
      {required this.detailName, required this.id, required this.status});

  @override
  _FoodDetailNameState createState() => _FoodDetailNameState();
}

class _FoodDetailNameState extends State<FoodDetailName> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (widget.status == 1) {
          await SqliteMyRecipe().foodplus(widget.id, 2);
        } else {
          await SqliteMyRecipe().foodplus(widget.id, 1);
        }

        setState(
          () {
            if (widget.status == 1) {
              widget.status = 0;
            } else {
              widget.status = 1;
            }
          },
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 5, top: 5),
        padding: EdgeInsets.only(left: 10, bottom: 3),
        width: 310,
        height: 40,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(top: 3.h),
              width: 15, // 设定圆圈的宽度
              height: 15, // 设定圆圈的高度
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: widget.status == 1
                    ? Colors.white
                    : Color.fromARGB(255, 66, 114, 209),
                border: Border.all(
                  color: const Color.fromARGB(255, 36, 35, 35), // 如果需要，可以添加边框颜色
                  width: 1, // 如果需要，可以指定边框宽度
                ),
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Text(
                    widget.detailName,
                    style: const TextStyle(
                      color: Color(0xFF595757),
                      fontSize: 16,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w300,
                      letterSpacing: 1,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BuildTextField extends StatefulWidget {
  const BuildTextField({super.key});

  @override
  State<BuildTextField> createState() => _BuildTextFieldState();
}

class _BuildTextFieldState extends State<BuildTextField> {
  List<String> searchFoodList = [];
  List<Map<String, dynamic>> foodResult = [];
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: Autocomplete(
        optionsBuilder: (TextEditingValue textEditingValue) async {
          if (textEditingValue.text.isNotEmpty) {
            foodResult =
                await SqliteMyRecipe().searchauto(textEditingValue.text);
            searchFoodList =
                foodResult.map((map) => map['name'].toString()).toList();
          } else {
            searchFoodList = [];
          }

          return searchFoodList;
        },
        onSelected: (option) {},
        optionsViewBuilder: (BuildContext context,
            void Function(String) onSelect, Iterable<String> options) {
          return Material(
            child: ListView.builder(
              shrinkWrap: true,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
              itemCount: options.length,
              itemExtent: 70.h,
              itemBuilder: (BuildContext context, int index) {
                String fdname = options.elementAt(index).toString();
                int status = foodResult[index]['status'];
                return AutocompleteTextFeild(
                  foodname: fdname,
                  status: status,
                );
              },
            ),
          );
        },
        fieldViewBuilder: (
          BuildContext context,
          TextEditingController controller,
          FocusNode focusNode,
          void Function() onFieldSubmitted,
        ) {
          return Container(
            padding: EdgeInsets.only(left: 10.w),
            child: TextField(
              controller: controller,
              focusNode: focusNode,
              onSubmitted: (value) {
                onFieldSubmitted();
              },
              onChanged: (value) {
                FoodCategoryNamesManager.closeAll();
              },
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintText: '文字輸入',
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    'assets/appdesign/images/btnicinquire.png',
                    height: 20.h,
                    width: 20.w,
                  ),
                  splashColor: Colors.transparent,
                ),
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                contentPadding: EdgeInsets.symmetric(vertical: 15.h), // 調整垂直填充
              ),
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14.sp,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
              autocorrect: false,
              obscureText: false,
            ),
          );
        },
      ),
    );
  }
}

class AutocompleteTextFeild extends StatefulWidget {
  final String foodname;
  late int status;
  AutocompleteTextFeild(
      {super.key, required this.foodname, required this.status});

  @override
  State<AutocompleteTextFeild> createState() => _AutocompleteTextFeildState();
}

class _AutocompleteTextFeildState extends State<AutocompleteTextFeild> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        print(widget.foodname);
        if (widget.status == 2) {
          await SqliteMyRecipe().foodplusauto1(widget.foodname);
        } else {
          await SqliteMyRecipe().foodplusauto2(widget.foodname);
        }
        setState(() {
          if (widget.status == 2) {
            widget.status = 1;
          } else {
            widget.status = 2;
          }
        });
      },
      child: Container(
        color: Colors.white10,
        margin: EdgeInsets.only(right: 70.w),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 3.h, left: 10.w),
                    width: 15, // 设定圆圈的宽度
                    height: 15, // 设定圆圈的高度
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: widget.status == 2
                          ? Color.fromARGB(255, 66, 114, 209)
                          : Colors.white,
                      border: Border.all(
                        color: const Color.fromARGB(
                            255, 36, 35, 35), // 如果需要，可以添加边框颜色
                        width: 1, // 如果需要，可以指定边框宽度
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Text(
                          widget.foodname,
                          style: const TextStyle(
                            color: Color(0xFF595757),
                            fontSize: 16,
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w300,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 1.h,
              color: const Color.fromARGB(255, 159, 157, 151),
            ),
          ],
        ),
      ),
    );
  }
}

//plusfood---------------------------------------------------------------------
