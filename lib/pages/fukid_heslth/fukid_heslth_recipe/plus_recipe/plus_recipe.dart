import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/plus_recipe/widgets/plus_recipe_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:sqflite/sqflite.dart';

class SqliteAddFoodCategory {
  Sqlite sqlite = Sqlite();
  getCategoryQuery() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM food_category
      
    ''');
    return foodResult;
  }

  getCategoryFoodQuery(categoryId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT id, name, status FROM food
      WHERE category_id==$categoryId AND status !=0
      
    ''');

    return foodResult;
  }

  getstatusZeroQuery() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT id,status,name,weight FROM food
      WHERE status ='0' 
      
    ''');
    return foodResult;
  }

  checkStatus(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT id,status,name FROM food
      WHERE id = ?
    ''', [id]);
    return foodResult;
  }

  addFoodbutton() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status=? Where status=? ', ['0', '2']);
  }

  getFoodQuery() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT id,status,name FROM food
     
      
    ''');
    return foodResult;
  }

  deleteAddFood(foodid) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate(
        'UPDATE food SET weight=? ,status=? Where id=? ', [null, '1', foodid]);
  }

  updateWeight(foodid, weight) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db
        .rawUpdate('UPDATE food SET weight=? Where id=? ', [weight, foodid]);
  }

  //復原資料
  updateAll() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('UPDATE food SET status = ?,weight=?', [1, null]);
  }
  //

  autocompleteUpdate(status, name) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db
        .rawUpdate('UPDATE food SET status = ? Where name=?', [status, name]);
  }

  autocompleteStatus(String name) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT status,name FROM food
      WHERE name = ?
    ''', [name]);
    return foodResult;
  }

  searchFood(keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,status FROM food
      WHERE name LIKE '%$keyword%' AND status !=0
    ''');

    return foodResult;
  }

  checkFoodWeight() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name FROM food
      WHERE  status == 0 AND weight IS NULL
    ''');
    if (foodResult.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  //營養分析
  analyzeFood() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT  COALESCE(weight, 0) as weight,
      COALESCE(kcal, 0) as kcal,
      COALESCE(water, 0) as water,
      COALESCE(protein, 0) as protein,
      COALESCE(fat, 0) as fat,
      COALESCE(carbohydrate, 0) as carbohydrate,
      COALESCE(fiber, 0) as fiber,
      COALESCE(ash, 0) as ash  FROM food
      WHERE status = '0'
      
    ''');
    return foodResult;
  }

  //存入食譜
//DELETE FROM recipe_list

  saveFood(name, food_weight, finish_weight, ill_tags, kind) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    //recipe_category
    await db.execute('''
            INSERT INTO recipe_category (name)VALUES('$kind');
''');
    int? Id =
        Sqflite.firstIntValue(await db.rawQuery('SELECT last_insert_rowid();'));
    //recipe
    List foodListData = await SqliteAddFoodCategory().analyzeFood();
    double finishweight = double.parse(finish_weight);

    double allkcal = 0;
    double allwater = 0;
    double allprotein = 0;
    double allfat = 0;
    double allcarbohydrate = 0;
    double allfiber = 0;
    double allash = 0;

    for (int i = 0; i < foodListData.length; i++) {
      allkcal += foodListData[i]['weight'] * foodListData[i]['kcal'] / 100;
      // allwater += foodListData[i]['weight'] *
      //     foodListData[i]['water'] /
      //     100;
      allprotein +=
          foodListData[i]['weight'] * foodListData[i]['protein'] / 100;
      allfat += foodListData[i]['weight'] * foodListData[i]['fat'] / 100;
      allcarbohydrate +=
          foodListData[i]['weight'] * foodListData[i]['carbohydrate'] / 100;
      allfiber += foodListData[i]['weight'] * foodListData[i]['fiber'] / 100;
      allash += foodListData[i]['weight'] * foodListData[i]['ash'] / 100;
    }
    //營養成分含量
    allkcal = double.parse(allkcal.toStringAsFixed(2));
    allwater = finishweight.toDouble() -
        allprotein -
        allfat -
        allcarbohydrate -
        allfiber -
        allash;
    allwater = double.parse(allwater.toStringAsFixed(2));
    allprotein = double.parse(allprotein.toStringAsFixed(2));
    allfat = double.parse(allfat.toStringAsFixed(2));
    allcarbohydrate = double.parse(allcarbohydrate.toStringAsFixed(2));
    allfiber = double.parse(allfiber.toStringAsFixed(2));
    allash = double.parse(allash.toStringAsFixed(2));
    //100g營養成分
    double hundredgram(double all, double foodweight) {
      return double.parse((all / foodweight * 100).toStringAsFixed(2));
    }

    double kcal100g = hundredgram(allkcal, finishweight);
    double water100g = hundredgram(allwater, finishweight);
    double protein100g = hundredgram(allprotein, finishweight);
    double fat100g = hundredgram(allfat, finishweight);
    double carbohydrate100g = hundredgram(allcarbohydrate, finishweight);
    double fiber100g = hundredgram(allfiber, finishweight);
    double ash100g = hundredgram(allash, finishweight);
    //乾物比
    double alldry = double.parse(
        (100 - (allwater / finishweight * 100)).toStringAsFixed(2));
    double percent(double element) {
      double result = (element / finishweight * 100);
      result = result / alldry * 100;
      return double.parse(result.toStringAsFixed(1));
    }

    double proteinpercent = percent(allprotein);
    double fatpercent = percent(allfat);
    double carbohydratepercent = percent(allcarbohydrate);
    double fiberpercent = percent(allfiber);
    double ashpercent = percent(allash);
    //熱量分布
    double calory_rate_protein = double.parse(((allprotein * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));
    double calory_rate_fat = double.parse(((allfat * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));
    double calory_rate_carbo = double.parse(((allcarbohydrate * 3.5) /
            ((allprotein * 3.5) + (allfat * 8.5) + (allcarbohydrate * 3.5)) *
            100)
        .toStringAsFixed(1));

    await db.execute('''
      INSERT INTO recipe (name,category_id,food_weight,finish_weight,ill_tags,mem_id,
      calories,water,protein,fat,carbohydrate,fiber,ash,
      dry_rate_protein,dry_rate_fat,dry_rate_carbo,dry_rate_fiber,dry_rate_ash,
      calory_rate_protein,calory_rate_fat,calory_rate_carbo,
      calories_per_100g,water_per_100g,protein_per_100g,fat_per_100g,carbohydrate_per_100g,fiber_per_100g,ash_per_100g)

        VALUES('$name','$Id','$food_weight','$finish_weight','$ill_tags','0',
        '$allkcal','$allwater','$allprotein','$allfat','$allcarbohydrate','$allfiber','$allash',
        '$proteinpercent','$fatpercent','$carbohydratepercent','$fiberpercent','$ashpercent',
        '$calory_rate_protein','$calory_rate_fat','$calory_rate_carbo',
        '$kcal100g','$water100g','$protein100g','$fat100g','$carbohydrate100g','$fiber100g','$ash100g');
''');

    int? newId =
        Sqflite.firstIntValue(await db.rawQuery('SELECT last_insert_rowid();'));
    //recipe_collect
    await db.execute('''
            INSERT INTO recipe_collect (mem_id,recipe_id)VALUES('0','$newId');
''');

    //recipe_list
    List recipeList = await SqliteAddFoodCategory().getstatusZeroQuery();
    for (int i = 0; i < recipeList.length; i++) {
      await db.execute('''
            INSERT INTO recipe_list (food_id,quantity,recipe_id)VALUES('${recipeList[i]['id']}','${recipeList[i]['weight']}','$newId');
''');
      print(recipeList);
    }
  }
}

class PlusRecipe extends StatefulWidget {
  const PlusRecipe({super.key});

  @override
  State<PlusRecipe> createState() => _PlusRecipeState();
}

class _PlusRecipeState extends State<PlusRecipe> {
  late List myStatus = [];

  bool goBackNeed = true; // goBackNeed暫時解決返回時會出現
  //Another exception was thrown: There are multiple heroes that share the same tag within a subtree.
  //這個問題

  int currentBody = 0;
  int? foodweight;
  int? finishweight;
  void tobody1() {
    setState(() {
      currentBody = 0;
    });
  }

  void tobody2() {
    setState(() {
      currentBody = 1;
    });
  }

  void tobody3() {
    setState(() {
      currentBody = 2;
    });
  }

  void myStatusVoid() async {
    List connectList = await SqliteAddFoodCategory().getstatusZeroQuery();
    setState(() {
      myStatus = connectList;
    });
  }

  void goBackNeedFunction() {
    setState(() {
      goBackNeed = false;
    });
  } // goBackNeedFunction暫時解決返回時會出現
  //Another exception was thrown: There are multiple heroes that share the same tag within a subtree.
  //這個問題

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      resizeToAvoidBottomInset: false,

      endDrawer: SizedBox(
        width: 90.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthPlusRecipe("新增自有食譜", context, tobody1, currentBody,
          myStatus, goBackNeedFunction),
      body: IndexedStack(
        index: currentBody,
        children: [
          Visibility(
            visible: currentBody == 0,
            maintainState: true,
            child: Body1(
              tobody2: tobody2,
              myStatuViod: myStatusVoid,
              goBackNeed: goBackNeed,
              goBackNeedFunction: goBackNeedFunction,
              tobody3: tobody3,
              onFoodWeightChanged: (value) {
                setState(() {
                  foodweight = value; // 更新父部件的 foodweight
                });
              },
              onFinishWeightChanged: (value) {
                setState(() {
                  finishweight = value;
                });
              },
            ),
          ),
          Visibility(
            visible: currentBody == 1,
            child: Body2(
              tobody1: tobody1,
            ),
          ),
          Visibility(
            visible: currentBody == 2,
            child: Body3(
              foodweight: foodweight,
              finishweight: finishweight,
            ),
          ),
        ],
      ),
    );
  }
}
