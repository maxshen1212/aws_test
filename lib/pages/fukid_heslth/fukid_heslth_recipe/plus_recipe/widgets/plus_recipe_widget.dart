import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/3_furkid_heslth_app_biochemistry_test/widgets/biochemistry_test_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_health_passbook/5_furkid_heslth_app_else_test/else_test.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/furkid_heslth_ingredients/widgets/fukkid_heslth_ingredients_widgets.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/plus_recipe/plus_recipe.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/meridian_point_query/bloc/furkid_heslth_merdian_point_query_state.dart';

import 'package:pet_save_app/utils.dart';
//-------------------------appbar-------------------------------------

PreferredSize furkidHeslthPlusRecipe(
    String names,
    BuildContext context,
    void Function() tobody1,
    currentBody,
    myStatus,
    void Function() goBackNeedFunction) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      leading: goBack(tobody1, currentBody, myStatus, goBackNeedFunction),
      leadingWidth: 100.w,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                AppColors.primaryLogo,
                Colors.purple,
              ]),
        ),
        //margin: EdgeInsets.only(left: 25.w, top: 25.h),
      ),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack(void Function() tobody1, currentBody, myStatus,
    void Function() goBackNeedFunction) {
  return Builder(builder: (context) {
    return GestureDetector(
      onTap: () async {
        if (currentBody == 1) {
          await SqliteAddFoodCategory().updateAll();
          print(myStatus);
          for (final status in myStatus) {
            final int id = status['id'];
            await Sqlite().update('food', {'status': 0, 'id': id});
          }
          tobody1();
        } else if (currentBody == 0) {
          goBackNeedFunction();
          await SqliteAddFoodCategory().updateAll();
          _addFoodTableStateManager.addFoodTableState.clear();
          Navigator.of(context).pop();
        } else {
          tobody1();
        }
      },
      // onTapDown: (details) {
      //   if (currentBody == 0) {
      //     Navigator.of(context).pop();
      //   }
      // },
      child: Padding(
        padding: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  });
}
//-------------------------appbar-------------------------------------

//-------------------------body1-------------------------------------

// ignore: must_be_immutable
class Body1 extends StatefulWidget {
  final void Function() tobody2;
  final bool goBackNeed;
  final void Function() myStatuViod;
  final void Function() goBackNeedFunction;
  final void Function() tobody3;

  final void Function(int?) onFoodWeightChanged;
  final void Function(int?) onFinishWeightChanged;

  Body1({
    super.key,
    required this.tobody2,
    //required this.myStatus,
    required this.myStatuViod,
    required this.goBackNeed,
    required this.goBackNeedFunction,
    required this.tobody3,
    required this.onFoodWeightChanged,
    required this.onFinishWeightChanged,
  });

  @override
  State<Body1> createState() => _Body1State();
}

class _Body1State extends State<Body1> {
  final TextEditingController nameCodeController = TextEditingController();

  final TextEditingController kindController = TextEditingController();

  //final TextEditingController allweightController = TextEditingController();

  final TextEditingController finishweightController = TextEditingController();

  final TextEditingController sickController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  int foodTotalGram = 0;

  void foodtotalgram() {
    setState(() {
      foodTotalGram = _addFoodTableStateManager.getTotalGrams();
      widget.onFoodWeightChanged(foodTotalGram);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 35.w),
        width: double.infinity,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              text('食譜名稱'),
              BuildTextField1(codeController: nameCodeController, text: '文字輸入'),
              text('類別'),
              BuildTextField1(codeController: kindController, text: '文字輸入'),
              text('食材'),
              foodadd(
                tobody2: widget.tobody2,
                myStatusVoid: widget.myStatuViod,
                goBackNeed: widget.goBackNeed,
                foodtotalgram: foodtotalgram,
              ),
              text('食材合計重量(g)'),
              //食材合計重量
              Container(
                padding: EdgeInsets.only(left: 10.w),
                margin: EdgeInsets.only(bottom: 10.h),
                height: 60.h,
                width: 320.w,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xffececec),
                  border: Border.all(color: Colors.transparent), // 去掉默认边框
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    foodTotalGram.toString() == '0'
                        ? ''
                        : foodTotalGram.toString(),
                    style: TextStyle(
                        fontFamily: "Avenir",
                        fontWeight: FontWeight.normal,
                        fontSize: 14.sp),
                  ),
                ),
              ),
              text('完成重量(g)'),
              BuildTextField1(
                codeController: finishweightController,
                text: '只輸入數字',
              ),
              ButtonAnalyze(
                name: '營養成分分析',
                formKey: _formKey,
                finishweightController: finishweightController,
                foodTotalGram: foodTotalGram,
                tobody3: widget.tobody3,
                onFinishWeightChanged: widget.onFinishWeightChanged,
              ),
              text('適用疾病症狀(多項請以,隔開)'),
              //適用疾病症狀
              TextFormField(
                controller: sickController,
                //autovalidateMode: AutovalidateMode.onUserInteraction,
                // inputFormatters: [
                //   FilteringTextInputFormatter.allow(
                //       RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
                // ],
                keyboardType: TextInputType.multiline,
                decoration: const InputDecoration(
                  hintText: '文字輸入',
                  isDense: true,
                  filled: true,
                  fillColor: Color(0xffececec),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(color: Colors.transparent), // 去掉默认底线
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide:
                        BorderSide(color: Color.fromARGB(0, 11, 128, 32)),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
                  ),
                ),
                style: TextStyle(
                    fontFamily: "Avenir",
                    fontWeight: FontWeight.normal,
                    fontSize: 14.sp),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ButtonNeworCancel(
                      buttonname: '新增',
                      formkey: _formKey,
                      nameCodeController: nameCodeController,
                      kindController: kindController,
                      foodTotalGram: foodTotalGram,
                      finishweightController: finishweightController,
                      sickController: sickController,
                      goBackNeedFunction: widget.goBackNeedFunction,
                    ),
                    ButtonNeworCancel(
                      buttonname: '取消',
                      formkey: _formKey,
                      nameCodeController: nameCodeController,
                      kindController: kindController,
                      foodTotalGram: foodTotalGram,
                      finishweightController: finishweightController,
                      goBackNeedFunction: widget.goBackNeedFunction,
                      sickController: sickController,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class foodadd extends StatefulWidget {
  final void Function() tobody2;

  final void Function() myStatusVoid;
  final bool goBackNeed;
  final void Function() foodtotalgram;
  foodadd(
      {required this.tobody2,
      required this.myStatusVoid,
      required this.goBackNeed,
      required this.foodtotalgram});
  @override
  State<foodadd> createState() => _foodaddState();
}

class _foodaddState extends State<foodadd> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.h),
      height: 200.h,
      width: double.infinity,
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(8.w)),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                height: 25.h,
                width: 25.w,
                child: FloatingActionButton(
                  onPressed: () async {
                    widget.myStatusVoid();
                    widget.tobody2();
                  },
                  foregroundColor: Colors.black,
                  backgroundColor: Colors.white,
                  child: const Icon(
                    Icons.add,
                    size: 20,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                width: 100.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.w)),
                ),
                child: Center(
                  child: Text(
                    '食材',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 15.w,
              ),
              Container(
                margin: EdgeInsets.only(left: 15.w, top: 10.h),
                width: 100.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.w)),
                ),
                child: Center(
                  child: Text(
                    '重量(g)',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.h,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: widget.goBackNeed
                  ? FutureBuilder(
                      future: SqliteAddFoodCategory().getFoodQuery(),
                      builder: (context, snap) {
                        if (snap.hasData) {
                          List<Map<String, dynamic>> foodListData =
                              snap.data as List<Map<String, dynamic>>;
                          return Column(
                            children: [
                              ListView.builder(
                                shrinkWrap: true,
                                itemCount: foodListData.length,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  int foodStatus =
                                      foodListData[index]['status'];
                                  if (foodStatus == 0) {
                                    String foodName =
                                        foodListData[index]["name"];
                                    int id = foodListData[index]['id'];
                                    return addFoodTable(
                                      name: foodName,
                                      foodtotalgram: widget.foodtotalgram,
                                      id: id,
                                      reload: () {
                                        setState(() {});
                                      },
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                              ),
                            ],
                          );
                        }
                        return Container();
                      },
                    )
                  : const SizedBox(),
            ),
          )
        ],
      ),
    );
  }
}

class addFoodTable extends StatefulWidget {
  final String name;
  final int id;
  final void Function() foodtotalgram;
  final void Function() reload;
  const addFoodTable(
      {super.key,
      required this.name,
      required this.foodtotalgram,
      required this.id,
      required this.reload});
  @override
  State<addFoodTable> createState() => _addFoodTableState();
}

class _addFoodTableState extends State<addFoodTable> {
  TextEditingController gramController = TextEditingController();
  int gram = 0;

  @override
  void initState() {
    super.initState();
    _addFoodTableStateManager.register(this);
  }

  void removeTableState() {
    _addFoodTableStateManager.remove(this);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 60,
          //width: 320.w,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(left: 5.w, top: 2.h, right: 30.w),
                          height: 25.h,
                          child: FloatingActionButton(
                            mini: true,
                            onPressed: () async {
                              await SqliteAddFoodCategory()
                                  .deleteAddFood(widget.id);
                              widget.reload();
                              removeTableState();
                              widget.foodtotalgram();
                              print('刪除食材');
                            },
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            child: const Icon(
                              Icons.remove,
                              size: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            widget.name,
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: const Color(0xff000000),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 25.w, right: 35.w),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.w),
                          )),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: TextField(
                                controller: gramController,
                                keyboardType: TextInputType.multiline,
                                decoration: const InputDecoration(
                                  isDense: true,
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'[0-9]')),
                                ],
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w200,
                                  color: Colors.black,
                                ),
                                onChanged: (value) {
                                  if (value.isNotEmpty) {
                                    gram = int.parse(value);
                                    widget.foodtotalgram();
                                    SqliteAddFoodCategory()
                                        .updateWeight(widget.id, gram);
                                  }
                                },
                                autocorrect: false,
                                obscureText: false,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Container(
                              //padding: EdgeInsets.only(bottom: 10.h),
                              width: 20.w,
                              height: 37.h,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8.w),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Expanded(
                                    // width: double.infinity,
                                    // height: 18.5.h,
                                    child: GestureDetector(
                                      onTap: () {
                                        if ((gramController.text).isNotEmpty) {
                                          int currentValue =
                                              int.parse(gramController.text);
                                          currentValue++;
                                          setState(() {
                                            gramController.text =
                                                currentValue.toString();
                                            gram = currentValue;
                                            widget.foodtotalgram();
                                            SqliteAddFoodCategory()
                                                .updateWeight(widget.id, gram);
                                          });
                                        } else {
                                          setState(() {
                                            gramController.text = '1';
                                            gram = 1;
                                            widget.foodtotalgram();
                                            SqliteAddFoodCategory()
                                                .updateWeight(widget.id, gram);
                                          });
                                        }
                                      },
                                      child: RotationTransition(
                                        turns: AlwaysStoppedAnimation(0.5),
                                        child: Image.asset(
                                          'assets/appdesign/images/btncountrycodesresearch-w53.png',
                                          width: 10.w,
                                          height: 10.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    // width: double.infinity,
                                    // height: 18.5.h,
                                    child: GestureDetector(
                                      onTap: () {
                                        if ((gramController.text).isNotEmpty) {
                                          int currentValue =
                                              int.parse(gramController.text);
                                          gram = currentValue;

                                          if (currentValue == 1) {
                                          } else {
                                            currentValue--;
                                            gram = currentValue;

                                            setState(() {
                                              gramController.text =
                                                  currentValue.toString();
                                              widget.foodtotalgram();
                                              SqliteAddFoodCategory()
                                                  .updateWeight(
                                                      widget.id, gram);
                                            });
                                          }
                                        }
                                      },
                                      child: SizedBox(
                                        width: 10.w,
                                        height: 10.h,
                                        child: Image.asset(
                                          'assets/appdesign/images/btncountrycodesresearch-w53.png',
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          color: Colors.black,
          height: 1.h,
        )
      ],
    );
  }
}

class _addFoodTableStateManager {
  static List<_addFoodTableState> addFoodTableState = [];
  static void register(_addFoodTableState addFoodTableName) {
    addFoodTableState.add(addFoodTableName);
  }

  static void remove(_addFoodTableState addFoodTableName) {
    addFoodTableState.remove(addFoodTableName);
  }

  static int getTotalGrams() {
    int totalGrams = 0;

    for (var footable in addFoodTableState) {
      totalGrams += footable.gram;
    }
    print(addFoodTableState);

    return totalGrams;
  }
}

class BuildTextField1 extends StatelessWidget {
  final TextEditingController codeController;
  final String text;
  BuildTextField1({Key? key, required this.codeController, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.h),
      child: TextFormField(
        controller: codeController,
        //keyboardType: TextInputType.multiline,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return '請勿為空';
          }

          return null;
        },

        decoration: InputDecoration(
          hintText: text,
          isDense: true,
          filled: true,
          fillColor: Color(0xffececec),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉默认底线
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Color.fromARGB(0, 11, 128, 32)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
          ),
        ),
        style: TextStyle(
            fontFamily: "Avenir",
            fontWeight: FontWeight.normal,
            fontSize: 14.sp),
      ),
    );
  }
}

Widget text(name) {
  return Container(
    margin: EdgeInsets.only(top: 20.h, left: 5.w, bottom: 10.h),
    child: Text(
      name,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125.h,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}

class ButtonAnalyze extends StatelessWidget {
  final String name;
  final GlobalKey<FormState> formKey;
  final int foodTotalGram;

  final TextEditingController finishweightController;
  final void Function() tobody3;
  final void Function(int?) onFinishWeightChanged;

  ButtonAnalyze({
    super.key,
    required this.name,
    required this.formKey,
    required this.finishweightController,
    required this.foodTotalGram,
    required this.tobody3,
    required this.onFinishWeightChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15.h),
      child: TextButton(
        onPressed: () async {
          if (finishweightController.text.isEmpty) {
            Fluttertoast.showToast(
              msg: '請輸入完成重量',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 24.0,
            );
          } else {
            onFinishWeightChanged(int.parse(finishweightController.text));
            List<Map<String, dynamic>> foodResult =
                await SqliteAddFoodCategory().analyzeFood();

            tobody3();
          }
        },
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.all(Colors.transparent),
          backgroundColor:
              MaterialStateProperty.all<Color>(const Color(0xff929292)),
          shape: MaterialStateProperty.all<OutlinedBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
        // 自定義內邊距
        child: Padding(
          padding: EdgeInsets.only(left: 10.w, right: 10.w),
          child: Text(
            name,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 16.sp,
              fontWeight: FontWeight.w700,
              height: 1.2125.h,
              letterSpacing: 0.8,
              color: const Color(0xffffffff),
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonNeworCancel extends StatefulWidget {
  final String buttonname;
  final void Function() goBackNeedFunction;
  final GlobalKey<FormState> formkey;
  final TextEditingController nameCodeController;
  final TextEditingController kindController;
  final TextEditingController finishweightController;
  final TextEditingController sickController;
  final foodTotalGram;

  const ButtonNeworCancel({
    super.key,
    required this.buttonname,
    required this.formkey,
    required this.nameCodeController,
    required this.kindController,
    required this.finishweightController,
    required this.sickController,
    required this.goBackNeedFunction,
    required this.foodTotalGram,
  });

  @override
  State<ButtonNeworCancel> createState() => _ButtonNeworCancelState();
}

class _ButtonNeworCancelState extends State<ButtonNeworCancel> {
  void back() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15.h),
      child: Builder(builder: (context) {
        return TextButton(
          onPressed: () async {
            if (widget.buttonname == '新增') {
              if (widget.formkey.currentState!.validate()) {
                Map data = {
                  "name": widget.nameCodeController.text,
                  "kind": widget.kindController.text,
                  //"weight": allweightController.text,
                  "finish": widget.finishweightController.text,
                  "sick": widget.sickController.text,
                };
                if (_addFoodTableStateManager.addFoodTableState.isEmpty) {
                  Fluttertoast.showToast(
                    msg: '請加入食材',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 24.0,
                  );
                } else {
                  bool check = await SqliteAddFoodCategory().checkFoodWeight();
                  if (check == false) {
                    Fluttertoast.showToast(
                      msg: '食材重量請勿為空',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 24.0,
                    );
                  } else {
                    await SqliteAddFoodCategory().saveFood(
                        widget.nameCodeController.text,
                        widget.foodTotalGram,
                        widget.finishweightController.text,
                        widget.sickController.text,
                        widget.kindController.text);
                    widget.goBackNeedFunction();
                    await SqliteAddFoodCategory().updateAll();
                    _addFoodTableStateManager.addFoodTableState.clear();
                    back();
                  }
                }
              }
            }
            if (widget.buttonname == '取消') {
              widget.goBackNeedFunction();
              await SqliteAddFoodCategory().updateAll();
              _addFoodTableStateManager.addFoodTableState.clear();
              back();
            }
          },
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(Colors.transparent),
            backgroundColor:
                MaterialStateProperty.all<Color>(AppColors.primaryLogo),
            shape: MaterialStateProperty.all<OutlinedBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ),
          // 自定義內邊距
          child: Padding(
            padding: EdgeInsets.only(left: 10.w, right: 10.w),
            child: Text(
              widget.buttonname,
              style: SafeGoogleFont(
                'Inter',
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
                height: 1.2125.h,
                letterSpacing: 0.8,
                color: const Color(0xffffffff),
              ),
            ),
          ),
        );
      }),
    );
  }
}

// //-------------------------body1-------------------------------------

// //-------------------------body2-------------------------------------

class Body2 extends StatefulWidget {
  final void Function() tobody1;
  Body2({
    super.key,
    required this.tobody1,
  });
  @override
  @override
  _Body2State createState() => _Body2State();
}

class _Body2State extends State<Body2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35.w, vertical: 5.w),
      //color: Colors.black38,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              '輸入食材',
              style: SafeGoogleFont(
                'Inter',
                fontSize: 16,
                // fontWeight: FontWeight.bold,
                // height: 1.2125,
                // letterSpacing: 0.8,
                color: Color(0xff000000),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10.h)),
          BuildTextField(),
          Padding(padding: EdgeInsets.only(bottom: 15.h)),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: foodcategory(),
                  ),
                ),
                Container(
                  width: 80.w,
                  height: 45.h,
                  margin: EdgeInsets.only(top: 15.h),
                  child: TextButton(
                    onPressed: () async {
                      await SqliteAddFoodCategory().addFoodbutton();
                      print('加入');
                      widget.tobody1();
                    },
                    style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all(Colors.transparent),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          AppColors.primaryLogo),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                    // 自定義內邊距
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.w, right: 10.w),
                      child: Text(
                        '加入',
                        style: SafeGoogleFont(
                          'Inter',
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w700,
                          height: 1.2125.h,
                          letterSpacing: 0.8,
                          color: const Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10.h)),
        ],
      ),
    );
  }
}

class BuildTextField extends StatefulWidget {
  const BuildTextField({super.key});

  @override
  State<BuildTextField> createState() => _BuildTextFieldState();
}

class _BuildTextFieldState extends State<BuildTextField> {
  List<String> searchFoodList = [];
  List<Map<String, dynamic>> foodResult = [];
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: Autocomplete(
        optionsBuilder: (TextEditingValue textEditingValue) async {
          if (textEditingValue.text.isNotEmpty) {
            foodResult =
                await SqliteAddFoodCategory().searchFood(textEditingValue.text);

            searchFoodList =
                foodResult.map((e) => e['name'].toString()).toList();
          } else {
            searchFoodList = [];
          }

          return searchFoodList;
        },
        onSelected: (option) {},
        optionsViewBuilder: (BuildContext context,
            void Function(String) onSelect, Iterable<String> options) {
          return Material(
            child: ListView.builder(
              shrinkWrap: true,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
              itemCount: options.length,
              itemExtent: 70.h,
              itemBuilder: (BuildContext context, int index) {
                String fdname = options.elementAt(index).toString();
                int status = foodResult[index]['status'];
                return AutocompleteTextFeild(
                  foodname: fdname,
                  status: status,
                );
              },
            ),
          );
        },
        fieldViewBuilder: (
          BuildContext context,
          TextEditingController controller,
          FocusNode focusNode,
          void Function() onFieldSubmitted,
        ) {
          return Container(
            padding: EdgeInsets.only(left: 10.w),
            child: TextField(
              controller: controller,
              focusNode: focusNode,
              onSubmitted: (value) {
                onFieldSubmitted();
              },
              onTap: () {
                print('asfsadssa');
              },
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintText: '文字輸入',
                suffixIcon: IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    'assets/appdesign/images/btnicinquire.png',
                    height: 20.h,
                    width: 20.w,
                  ),
                  splashColor: Colors.transparent,
                ),
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                contentPadding: EdgeInsets.symmetric(vertical: 15.h), // 調整垂直填充
              ),
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14.sp,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
              autocorrect: false,
              obscureText: false,
            ),
          );
        },
      ),
    );
  }
}

class AutocompleteTextFeild extends StatefulWidget {
  final String foodname;
  int status;
  AutocompleteTextFeild(
      {super.key, required this.foodname, required this.status});

  @override
  State<AutocompleteTextFeild> createState() => _AutocompleteTextFeildState();
}

class _AutocompleteTextFeildState extends State<AutocompleteTextFeild> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        FoodCategoryNamesManager.closeAll();

        if (widget.status == 2) {
          setState(() {
            widget.status = 1;
          });
          await SqliteAddFoodCategory().autocompleteUpdate(1, widget.foodname);
        } else {
          setState(() {
            widget.status = 2;
          });
          await SqliteAddFoodCategory().autocompleteUpdate(2, widget.foodname);
        }
      },
      child: Container(
        color: Colors.white10,
        margin: EdgeInsets.only(right: 70.w),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 3.h, left: 10.w),
                    width: 15, // 设定圆圈的宽度
                    height: 15, // 设定圆圈的高度
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: widget.status == 1
                          ? Colors.white
                          : Color.fromARGB(255, 66, 114, 209),
                      border: Border.all(
                        color: const Color.fromARGB(
                            255, 36, 35, 35), // 如果需要，可以添加边框颜色
                        width: 1, // 如果需要，可以指定边框宽度
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Text(
                          widget.foodname,
                          style: const TextStyle(
                            color: Color(0xFF595757),
                            fontSize: 16,
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w300,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 1.h,
              color: const Color.fromARGB(255, 159, 157, 151),
            ),
          ],
        ),
      ),
    );
  }
}

class foodcategory extends StatefulWidget {
  const foodcategory({
    super.key,
  });
  @override
  State<foodcategory> createState() => _foodcategoryState();
}

class _foodcategoryState extends State<foodcategory> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: SqliteAddFoodCategory().getCategoryQuery(),
      builder: (context, snap) {
        if (snap.hasData) {
          List<Map<String, dynamic>> foodListData =
              snap.data as List<Map<String, dynamic>>;
          return Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];
                  int id = foodListData[index]['id'];
                  return Column(
                    children: [
                      FoodCategoryName(
                        name: foodName,
                        categortId: id,
                      )
                    ],
                  );
                },
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}

class FoodCategoryName extends StatefulWidget {
  final String name;
  final int categortId;

  FoodCategoryName({required this.name, required this.categortId});

  @override
  _FoodCategoryNameState createState() => _FoodCategoryNameState();
}

class _FoodCategoryNameState extends State<FoodCategoryName> {
  bool isOpen = false;

  @override
  void initState() {
    super.initState();
    FoodCategoryNamesManager.register(this);
  }

  void closeOtherFoodCategoryNames() {
    FoodCategoryNamesManager.closeOthers(this);
  }

  void toggleCategoryStateToFalse() {
    setState(() {
      isOpen = false;
    });
  }

  void toggleCategoryStateToTrue() {
    setState(() {
      isOpen = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () async {
            closeOtherFoodCategoryNames();
          },
          child: Container(
            padding: EdgeInsets.only(left: 15),
            margin: EdgeInsets.only(top: 5, bottom: 5.h),
            width: 310,
            height: 60,
            decoration: const BoxDecoration(
              color: Color(0xffececec),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.name,
                style: const TextStyle(
                  color: Color(0xFF595757),
                  fontSize: 18,
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1,
                ),
              ),
            ),
          ),
        ),
        isOpen == true
            ? foodDetail(
                categortId: widget.categortId,
              )
            : Container(),
      ],
    );
  }
}

class FoodCategoryNamesManager {
  static final List<_FoodCategoryNameState> _foodCategoryNames = [];

  static void register(_FoodCategoryNameState foodCategoryName) {
    _foodCategoryNames.add(foodCategoryName);
  }

  static void closeAll() {
    for (var category in _foodCategoryNames) {
      if (category.mounted) {
        category.toggleCategoryStateToFalse();
      }
    }
  }

  static void closeOthers(_FoodCategoryNameState openedCategory) {
    for (var category in _foodCategoryNames) {
      if (category.mounted) {
        if (category == openedCategory) {
          if (category.isOpen) {
            category.toggleCategoryStateToFalse();
          } else {
            category.toggleCategoryStateToTrue();
          }
        } else {
          category.toggleCategoryStateToFalse();
        }
      }
    }
  }
}

class foodDetail extends StatefulWidget {
  final int categortId;

  foodDetail({
    required this.categortId,
  });
  @override
  _foodDetailState createState() => _foodDetailState();
}

class _foodDetailState extends State<foodDetail> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: SqliteAddFoodCategory().getCategoryFoodQuery(widget.categortId),
      builder: (context, snap) {
        if (snap.hasData) {
          List foodListData = snap.data as List;
          return Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                itemCount: foodListData.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  String foodName = foodListData[index]["name"];
                  int id = foodListData[index]['id'];
                  int status = foodListData[index]['status'];

                  return Column(
                    children: [
                      FoodDetailName(
                        detailName: foodName,
                        id: id,
                        status: status,
                      )
                    ],
                  );
                },
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}

// ignore: must_be_immutable
class FoodDetailName extends StatefulWidget {
  final String detailName;
  final int id;
  int status;

  FoodDetailName({
    required this.detailName,
    required this.id,
    required this.status,
  });

  @override
  _FoodDetailNameState createState() => _FoodDetailNameState();
}

class _FoodDetailNameState extends State<FoodDetailName> {
  bool selected = false;
  @override
  void initState() {
    super.initState();
    if (widget.status == 2) {
      selected = true;
    }
  }

  // void initfood() async {
  //   List<Map<String, dynamic>> aa =
  //       await SqliteAddFoodCategory().checkStatus(widget.id);
  //   if (aa.isNotEmpty && aa[0]['status'] == 0) {
  //     setState(() {
  //       widget.status = 0;
  //       canChange = false;
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (selected == false) {
          await Sqlite().update('food', {'status': 2, 'id': widget.id});
        } else {
          await Sqlite().update('food', {'status': 1, 'id': widget.id});
        }

        setState(() {
          if (selected == false) {
            selected = true;
          } else {
            selected = false;
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 5, top: 5),
        padding: EdgeInsets.only(left: 10, bottom: 3),
        width: 310,
        height: 40,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(top: 3.h),
              width: 15, // 设定圆圈的宽度
              height: 15, // 设定圆圈的高度
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: selected == false
                    ? Colors.white
                    : Color.fromARGB(255, 66, 114, 209),
                border: Border.all(
                  color: const Color.fromARGB(255, 36, 35, 35), // 如果需要，可以添加边框颜色
                  width: 1, // 如果需要，可以指定边框宽度
                ),
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Text(
                    widget.detailName,
                    style: const TextStyle(
                      color: Color(0xFF595757),
                      fontSize: 16,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w300,
                      letterSpacing: 1,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//-------------------------body3-------------------------------------
// ignore: must_be_immutable
class Body3 extends StatefulWidget {
  int? foodweight;
  int? finishweight;
  Body3({super.key, required this.foodweight, required this.finishweight});

  @override
  State<Body3> createState() => _Body3State();
}

class _Body3State extends State<Body3> {
  double hundredgram(double all, double foodweight) {
    return double.parse((all / foodweight * 100).toStringAsFixed(2));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 10.h),
        child: Center(
          child: FutureBuilder(
            future: SqliteAddFoodCategory().analyzeFood(),
            builder: (context, snap) {
              if (snap.hasData) {
                List foodListData = snap.data as List;
                //double foodweight = widget.foodweight?.toDouble() ?? 0.0;
                double finishweight = widget.finishweight?.toDouble() ?? 0.0;

                double allkcal = 0;
                double allwater = 0;
                double allprotein = 0;
                double allfat = 0;
                double allcarbohydrate = 0;
                double allfiber = 0;
                double allash = 0;

                for (int i = 0; i < foodListData.length; i++) {
                  allkcal +=
                      foodListData[i]['weight'] * foodListData[i]['kcal'] / 100;
                  // allwater += foodListData[i]['weight'] *
                  //     foodListData[i]['water'] /
                  //     100;
                  allprotein += foodListData[i]['weight'] *
                      foodListData[i]['protein'] /
                      100;
                  allfat +=
                      foodListData[i]['weight'] * foodListData[i]['fat'] / 100;
                  allcarbohydrate += foodListData[i]['weight'] *
                      foodListData[i]['carbohydrate'] /
                      100;
                  allfiber += foodListData[i]['weight'] *
                      foodListData[i]['fiber'] /
                      100;
                  allash +=
                      foodListData[i]['weight'] * foodListData[i]['ash'] / 100;
                }
                //營養成分含量
                allkcal = double.parse(allkcal.toStringAsFixed(2));
                allwater = finishweight.toDouble() -
                    allprotein -
                    allfat -
                    allcarbohydrate -
                    allfiber -
                    allash;
                allwater = double.parse(allwater.toStringAsFixed(2));
                allprotein = double.parse(allprotein.toStringAsFixed(2));
                allfat = double.parse(allfat.toStringAsFixed(2));
                allcarbohydrate =
                    double.parse(allcarbohydrate.toStringAsFixed(2));
                allfiber = double.parse(allfiber.toStringAsFixed(2));
                allash = double.parse(allash.toStringAsFixed(2));
                //100g營養成分
                double kcal100g = hundredgram(allkcal, finishweight);
                double water100g = hundredgram(allwater, finishweight);
                double protein100g = hundredgram(allprotein, finishweight);
                double fat100g = hundredgram(allfat, finishweight);
                double carbohydrate100g =
                    hundredgram(allcarbohydrate, finishweight);
                double fiber100g = hundredgram(allfiber, finishweight);
                double ash100g = hundredgram(allash, finishweight);
                //乾物比
                double alldry = double.parse(
                    (100 - (allwater / finishweight * 100)).toStringAsFixed(2));
                double percent(double element) {
                  double result = (element / finishweight * 100);
                  result = result / alldry * 100;
                  return double.parse(result.toStringAsFixed(1));
                }

                double proteinpercent = percent(allprotein);
                double fatpercent = percent(allfat);
                double carbohydratepercent = percent(allcarbohydrate);
                double fiberpercent = percent(allfiber);
                double ashpercent = percent(allash);
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    title('營養成分分析'),
                    element('食材重量', '${widget.foodweight}g', '完成重量',
                        '${widget.finishweight}g'),
                    title('食譜營養成分含量'),
                    element('熱量', '${allkcal}kcal', '水分', '${allwater}g'),
                    element('粗蛋白質', '${allprotein}g', '粗脂肪', '${allfat}g'),
                    element(
                        '碳水化合物', '${allcarbohydrate}g', '粗纖維', '${allfiber}g'),
                    element('灰份', '${allash}g', '', ''),
                    title('每100g營養成分'),
                    element('熱量', '${kcal100g}kcal', '水分', '${water100g}g'),
                    element('粗蛋白質', '${protein100g}g', '粗脂肪', '${fat100g}g'),
                    element('碳水化合物', '${carbohydrate100g}g', '粗纖維',
                        '${fiber100g}g'),
                    element('灰份', '${ash100g}g', '', ''),
                    title('乾物比'),
                    element('粗蛋白質', '$proteinpercent%', '粗脂肪', '$fatpercent%'),
                    element('碳水化合物', '$carbohydratepercent%', '粗纖維',
                        '$fiberpercent%'),
                    element('灰份', '$ashpercent%', '', ''),
                    title('主要營養成分'),
                    bigcircle(1,
                        water: allwater,
                        protein: allprotein,
                        fat: allfat,
                        carbo: allcarbohydrate,
                        ash: allash),
                    title('乾物比'),
                    bigcircle(2,
                        protein: proteinpercent,
                        fat: fatpercent,
                        carbo: carbohydratepercent,
                        ash: ashpercent),
                    title('熱量來源分布(熱量比)'),
                    bigcircle(3,
                        protein: allprotein * 3.5,
                        fat: allfat * 8.5,
                        carbo: allcarbohydrate * 3.5),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}

Widget title(String text) {
  return IntrinsicWidth(
    child: Container(
      padding: EdgeInsets.only(left: 15.w, right: 15.w),
      margin: EdgeInsets.only(bottom: 15.h),
      height: 25,
      decoration: BoxDecoration(
        color: const Color(0xffe4007f),
        borderRadius: BorderRadius.circular(100),
      ),
      child: Center(
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 14,
            fontWeight: FontWeight.w700,
            height: 1.2125,
            letterSpacing: 0.7,
            color: const Color(0xffffffff),
          ),
        ),
      ),
    ),
  );
}

Widget element(
  String name1,
  String unit1,
  String name2,
  String unit2,
) {
  return Column(
    children: [
      Container(
        margin: EdgeInsets.only(left: 10.w, right: 10.w, top: 5.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: [
                Container(
                  width: 75.w,
                  child: Text(
                    name1,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 80.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        unit1,
                        style: SafeGoogleFont(
                          'Inter',
                          fontSize: 14,
                          fontWeight: FontWeight.w200,
                          height: 1.2125.h,
                          letterSpacing: 0.7,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 20.w,
                ),
                Container(
                  width: 65.w,
                  child: Text(
                    name2,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 80.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        unit2,
                        style: SafeGoogleFont(
                          'Inter',
                          fontSize: 14,
                          fontWeight: FontWeight.w200,
                          height: 1.2125.h,
                          letterSpacing: 0.7,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
      Container(
        margin:
            EdgeInsets.only(left: 30.w, right: 30.w, top: 7.h, bottom: 15.h),
        height: 1.h,
        color: const Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget circle(String text) {
  Color ccco = const Color(0xFF0075FF);
  if (text == '粗蛋白質') {
    ccco = const Color(0xFFE66676);
  } else if (text == '粗脂肪') {
    ccco = const Color(0xFFFF792D);
  } else if (text == '碳水') {
    ccco = const Color(0xFFAACD06);
  } else if (text == '灰份') {
    ccco = const Color(0xFF565555);
  }
  return Container(
    margin: EdgeInsets.only(left: 8.w, right: 3.w),
    width: 20, // 宽度
    height: 20, // 高度
    decoration: BoxDecoration(
      color: ccco, // 背景颜色
      shape: BoxShape.circle, // 指定形状为圆形
    ),
  );
}

Widget bigcircle(
  int number, {
  double? water,
  double? protein,
  double? fat,
  double? carbo,
  double? ash,
}) {
  var choice = <Widget>[
    circle('水分'),
    circletext('水分'),
    circle('粗蛋白質'),
    circletext('粗蛋白質'),
    circle('粗脂肪'),
    circletext('粗脂肪'),
    circle('碳水'),
    circletext('碳水'),
    circle('灰份'),
    circletext('灰份'),
  ];
  if (number == 2) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
      circle('灰份'),
      circletext('灰份'),
    ];
  }
  if (number == 3) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
    ];
  }
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    width: 290.w,
    height: 350.h,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      shadows: const [
        BoxShadow(
          color: Color(0x3F000000),
          blurRadius: 4,
          offset: Offset(0, 0),
          spreadRadius: 0,
        )
      ],
    ),
    child: Column(
      children: [
        PieChartAnalyze(
          number: number,
          water: water,
          protein: protein,
          fat: fat,
          carbo: carbo,
          ash: ash,
        ),
        Container(
          margin: EdgeInsets.only(top: 25.h, right: 5.w),
          child: IntrinsicWidth(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: choice),
          ),
        )
      ],
    ),
  );
}

Widget circletext(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.black,
      fontSize: 10,
      fontFamily: 'Inter',
      fontWeight: FontWeight.w300,
      height: 0,
      letterSpacing: 0.50,
    ),
  );
}

class PieChartAnalyze extends StatelessWidget {
  final double? water;
  final double? protein;
  final double? fat;
  final double? carbo;
  final double? ash;
  final int number;
  PieChartAnalyze(
      {super.key,
      required this.number,
      this.water = 0.0,
      this.protein = 0.0,
      this.fat = 0.0,
      this.carbo = 0.0,
      this.ash = 0.0});

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: PieChart(
        PieChartData(
          centerSpaceRadius: 0,
          sectionsSpace: 1,
          pieTouchData: PieTouchData(
            touchCallback:
                (FlTouchEvent touchEvent, PieTouchResponse? touchResponse) {
              if (touchEvent is FlTapUpEvent) {
                print('User tapped the pie chart!');
              }
            },
          ),
          sections: [
            if (number == 1)
              PieChartSectionData(
                  color: const Color(0xFF0075FF),
                  value: water,
                  title:
                      '${(water! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 0.7),

            PieChartSectionData(
                color: const Color(0xFFE66676),
                value: protein,
                title: number == 1
                    ? '${(protein! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(protein! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(protein! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            PieChartSectionData(
                color: const Color(0xFFFF792D),
                value: fat,
                title: number == 1
                    ? '${(fat! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(fat! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(fat! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),

            PieChartSectionData(
                color: const Color(0xFFAACD06),
                value: carbo,
                title: number == 1
                    ? '${(carbo! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(carbo! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(carbo! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            if (number == 2 || number == 1)
              PieChartSectionData(
                  color: const Color(0xFF565555),
                  value: ash,
                  title: number == 1
                      ? '${(ash! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                      : '${(ash! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 1),
            // Add more PieChartSectionData for other sections
          ],
          // Add more chart configurations here
        ),
      ),
    );
  }
}
//-------------------------body3-------------------------------------
