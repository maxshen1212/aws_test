import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_event.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_state.dart';

class FurKidHeslthSearchRecipePageBlocs extends Bloc<
    FurKidHeslthSearchRecipePageEvents, FurKidHeslthSearchRecipePageStates> {
  FurKidHeslthSearchRecipePageBlocs() : super(InitStates()) {
    on<Page0>((event, emit) {
      emit(FurKidHeslthSearchRecipePageStates(
          pageState: event.page, recipe_id: event.recipe_id));
    });
  }
}
