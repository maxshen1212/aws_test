class FurKidHeslthSearchRecipePageStates {
  int pageState;
  int recipe_id;
  FurKidHeslthSearchRecipePageStates(
      {required this.pageState, required this.recipe_id});
}

class InitStates extends FurKidHeslthSearchRecipePageStates {
  InitStates() : super(pageState: 0, recipe_id: 0);
}
