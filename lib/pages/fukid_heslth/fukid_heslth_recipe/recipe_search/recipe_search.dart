import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_state.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/widgets/recipe_search_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';
import 'package:sqflite/sqflite.dart';

class SqliteRecipeSearch {
  Sqlite sqlite = Sqlite();
  searchfavoriterecipe() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT recipe_id FROM recipe_collect
      WHERE status=0
    ''');
    List<Map<String, dynamic>> foodnameidResult = [];
    for (int i = 0; i < foodResult.length; i++) {
      final List<Map<String, dynamic>> foodnameid = await db.rawQuery('''
    SELECT name, id FROM recipe
    WHERE id = ?
  ''', ['${foodResult[i]['recipe_id']}']);

      if (foodnameid.isNotEmpty) {
        for (int j = 0; j < foodnameid.length; j++) {
          foodnameidResult.add({
            'name': foodnameid[j]['name'],
            'id': foodnameid[j]['id'],
          });
        }
      }
    }

    return foodnameidResult;
  }

  searchNameAndCategory(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,category_id,ill_tags FROM recipe
      WHERE id=?
    ''', ['$id']);
    int categoryId = foodResult[0]['category_id'];
    final List<Map<String, dynamic>> categoryResult = await db.rawQuery('''
      SELECT name FROM recipe_category
      WHERE id=?
    ''', ['$categoryId']);

    String recipename = foodResult[0]['name'];
    String categoryname = categoryResult[0]['name'];
    String illtags = foodResult[0]['ill_tags'];
    List<Map<String, dynamic>> Result = [];
    Result.add({
      'recipename': recipename,
      'categoryname': categoryname,
      'illtags': illtags
    });
    return Result;
  }

  searchRecipeList(int recipeId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT food_id,quantity FROM recipe_list
      WHERE recipe_id=?
    ''', ['$recipeId']);
    List<Map<String, dynamic>> namelist = [];
    for (int i = 0; i < foodResult.length; i++) {
      int id = foodResult[i]['food_id'];
      List<Map<String, dynamic>> name = await searchFoodName(id);
      namelist.add(name[0]);
    }

    List<Map<String, dynamic>> mergedList = [];
    for (int i = 0; i < foodResult.length; i++) {
      int quantity = foodResult[i]['quantity'];
      String name = namelist[i]['name'];

      mergedList.add({'name': name, 'quantity': quantity});
    }

    return mergedList;
  }

  searchFoodName(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name FROM food
      WHERE id=?
    ''', ['$id']);
    return foodResult;
  }

  searchDetail(int id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT * FROM recipe
      WHERE id=?
    ''', ['$id']);
    return foodResult;
  }

  searchRecipefavorite(String keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    List<Map<String, dynamic>> foodfinalResult = [];
    final List<Map<String, dynamic>> foodsssResult = await db.rawQuery('''
      SELECT status,recipe_id FROM recipe_collect
      WHERE status =0
    ''');
    for (int i = 0; i < foodsssResult.length; i++) {
      final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
      SELECT name,id FROM recipe
      WHERE id = ? AND name LIKE '%$keyword%'
    ''', ['${foodsssResult[i]['recipe_id']}']);

      if (foodResult.isNotEmpty) {
        for (int j = 0; j < foodResult.length; j++) {
          foodfinalResult.add({
            'name': foodResult[j]['name'],
            'id': foodResult[j]['id'],
          });
        }
      }
    }
    return foodfinalResult;
  }

  searchRecipepublic(String keyword) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    List<Map<String, dynamic>> foodfinalResult = [];
    // final List<Map<String, dynamic>> foodsssResult = await db.rawQuery('''
    //   SELECT status,recipe_id FROM recipe_collect
    //   WHERE status =0
    // ''');
    // for (int i = 0; i < foodsssResult.length; i++) {
    //   final List<Map<String, dynamic>> foodResult = await db.rawQuery('''
    //   SELECT name,id FROM recipe
    //   WHERE id = ? AND name LIKE '%$keyword%'
    // ''', ['${foodsssResult[i]['recipe_id']}']);

    //   if (foodResult.isNotEmpty) {
    //     for (int j = 0; j < foodResult.length; j++) {
    //       foodfinalResult.add({
    //         'name': foodResult[j]['name'],
    //         'id': foodResult[j]['id'],
    //       });
    //     }
    //   }
    // }
    return foodfinalResult;
  }
}

class RecipeSearch extends StatefulWidget {
  const RecipeSearch({super.key});

  @override
  State<RecipeSearch> createState() => _RecipeSearchState();
}

class _RecipeSearchState extends State<RecipeSearch> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          drawerEdgeDragWidth: 220, //用拉的也能彈出設定
          endDrawer: SizedBox(
            width: 90.w,
            child: const Drawer(
              child: SetUp(),
            ),
          ),
          appBar: furkidHeslthRecipeSearch("食譜查詢", context),
          body: BlocBuilder<FurKidHeslthSearchRecipePageBlocs,
              FurKidHeslthSearchRecipePageStates>(builder: (context, state) {
            return IndexedStack(
              index: state.pageState,
              children: [
                SingleChildScrollView(child: Body()),
                Detail(),
              ],
            );
          })),
    );
  }
}
