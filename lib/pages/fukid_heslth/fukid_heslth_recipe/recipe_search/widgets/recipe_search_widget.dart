import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_event.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/bloc/recipe_search_state.dart';
import 'package:pet_save_app/pages/fukid_heslth/fukid_heslth_recipe/recipe_search/recipe_search.dart';
import 'package:pet_save_app/utils.dart';
import 'package:sqflite/sqflite.dart';

PreferredSize furkidHeslthRecipeSearch(String names, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      leading: goBack(),
      leadingWidth: 100.w,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                AppColors.primaryLogo,
                Colors.purple,
              ]),
        ),
        //margin: EdgeInsets.only(left: 25.w, top: 25.h),
      ),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return BlocBuilder<FurKidHeslthSearchRecipePageBlocs,
        FurKidHeslthSearchRecipePageStates>(builder: (context, state) {
      return GestureDetector(
        onTap: () {
          if (state.pageState != 0) {
            BlocProvider.of<FurKidHeslthSearchRecipePageBlocs>(context)
                .add(Page0(page: 0, recipe_id: 0));
          } else {
            Navigator.of(context).pop();
          }
        },
        child: Padding(
          padding: EdgeInsets.only(left: 30.w),
          child: Row(
            children: [
              Image.asset(
                'assets/appdesign/images/goback.png',
                width: 18.w,
                height: 16.h,
              ),
              const Text(
                "返回",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.normal),
              )
            ],
          ),
        ),
      );
    });
  });
}

Widget buildTextField(String hintText) {
  return Container(
    // width: 330.w,
    //height: 40.h,
    margin: EdgeInsets.only(top: 5.h, bottom: 2.h),
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: TextField(
      //  controller: searchController,
      keyboardType: TextInputType.multiline,
      onChanged: (value) {
        // if (value.isEmpty) {
        //   changesearchtrue();
        // } else {
        //   changesearchfalse();
        // }
      },
      decoration: const InputDecoration(
        hintText: '文字輸入',
        isDense: true,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color.fromARGB(0, 249, 208, 208),
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w200,
        color: const Color(0xff595757),
      ),
      autocorrect: false,
      obscureText: false,
    ),
  );
}

class Body extends StatefulWidget {
  Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  TextEditingController searchController = TextEditingController();

  bool favoriteorpublic = true;
  bool textfieldsearch = false;
  void change() {
    setState(() {
      favoriteorpublic = !favoriteorpublic;
    });
  }

  void changesearchf() {
    setState(() {
      textfieldsearch = false;
    });
  }

  void changesearcht() {
    setState(() {
      textfieldsearch = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 30.w, right: 30.h),
      child: Column(children: [
        Container(
          margin: EdgeInsets.only(top: 15.h, bottom: 10.h),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "食譜查詢",
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14.sp,
                fontWeight: FontWeight.w200,
                height: 1.2125.h,
                letterSpacing: 0.8,
                color: Color(0xff000000),
              ),
            ),
          ),
        ),
        Textfield(
            changesearchfalse: changesearchf,
            changesearchtrue: changesearcht,
            searchController: searchController),
        recipebutton(favoriteorpublic, change),
        textfieldsearch
            ? Search(
                favorite: favoriteorpublic, searchController: searchController)
            : favoriteorpublic
                ? const Favoriterecipe()
                : Container()
      ]),
    );
  }
}

Widget recipebutton(
  bool favoriteorpublic,
  void Function() change,
) {
  return StatefulBuilder(
    builder: (BuildContext context, setState) {
      return Container(
        // color: Colors.black,
        margin: EdgeInsets.only(top: 15.h),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
              onPressed: () {
                if (favoriteorpublic == false) {
                  change();
                }
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
              ),
              child: Container(
                width: 130.w,
                height: 40.h,
                decoration: BoxDecoration(
                  color: favoriteorpublic
                      ? AppColors.primaryLogo
                      : Color(0xff929292),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Text(
                    '已收藏食譜',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w700,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xffffffff),
                    ),
                  ),
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                if (favoriteorpublic == true) {
                  change();
                }
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
              ),
              child: Container(
                width: 130.w,
                height: 40.h,
                decoration: BoxDecoration(
                  color: favoriteorpublic
                      ? Color(0xff929292)
                      : AppColors.primaryLogo,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Text(
                    '公發食譜',
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w700,
                      height: 1.2125.h,
                      letterSpacing: 0.8,
                      color: Color(0xffffffff),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    },
  );
}

class Textfield extends StatelessWidget {
  final void Function() changesearchfalse;
  final void Function() changesearchtrue;
  final TextEditingController searchController;
  const Textfield(
      {super.key,
      required this.changesearchfalse,
      required this.changesearchtrue,
      required this.searchController});

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: 330.w,
      //height: 40.h,
      margin: EdgeInsets.only(top: 5.h, bottom: 2.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: TextField(
        controller: searchController,
        keyboardType: TextInputType.multiline,
        onChanged: (value) {
          if (value.isEmpty) {
            changesearchfalse();
          } else {
            changesearchtrue();
          }
        },
        decoration: const InputDecoration(
          hintText: '文字輸入',
          isDense: true,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromARGB(0, 249, 208, 208),
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
        ),
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14.sp,
          fontWeight: FontWeight.w200,
          color: const Color(0xff595757),
        ),
        autocorrect: false,
        obscureText: false,
      ),
    );
  }
}

class Search extends StatefulWidget {
  final bool favorite;

  TextEditingController searchController;

  Search({super.key, required this.favorite, required this.searchController});
  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amberAccent,
      height: 525.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: FutureBuilder(
        future: widget.favorite
            ? SqliteRecipeSearch()
                .searchRecipefavorite(widget.searchController.text)
            : SqliteRecipeSearch()
                .searchRecipepublic(widget.searchController.text),
        builder: (context, snap) {
          if (snap.hasData) {
            List<Map<String, dynamic>> foodListData =
                snap.data as List<Map<String, dynamic>>;
            print(foodListData);
            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodListData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      String foodName = foodListData[index]['name'];
                      int recipe_id = foodListData[index]['id'];
                      return widget.favorite
                          ? FavoriterecipeUI(
                              name: foodName,
                              recipe_id: recipe_id,
                            )
                          : Container();
                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class Favoriterecipe extends StatefulWidget {
  const Favoriterecipe({super.key});

  @override
  State<Favoriterecipe> createState() => _FavoriterecipeState();
}

class _FavoriterecipeState extends State<Favoriterecipe> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amberAccent,
      height: 525.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: FutureBuilder(
        future: SqliteRecipeSearch().searchfavoriterecipe(),
        builder: (context, snap) {
          if (snap.hasData) {
            List<Map<String, dynamic>> foodListData =
                snap.data as List<Map<String, dynamic>>;

            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodListData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      String foodName = foodListData[index]['name'];
                      int recipe_id = foodListData[index]['id'];
                      return FavoriterecipeUI(
                        name: foodName,
                        recipe_id: recipe_id,
                      );
                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class FavoriterecipeUI extends StatelessWidget {
  final String name;
  final int recipe_id;

  FavoriterecipeUI({
    super.key,
    required this.name,
    required this.recipe_id,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        BlocProvider.of<FurKidHeslthSearchRecipePageBlocs>(context)
            .add(Page0(page: 1, recipe_id: recipe_id));
      },
      child: Container(
        constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
        margin: EdgeInsets.only(top: 10.h),
        width: 330.w,
        decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                color: Color.fromARGB(216, 191, 197, 197),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(-1, 1),
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.w)),
            border: Border.all(color: Colors.grey)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 200.w,
              margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(name),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Detail extends StatefulWidget {
  Detail({super.key});
  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  bool foodorele = true;

  void change() {
    setState(() {
      foodorele = !foodorele;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthSearchRecipePageBlocs,
        FurKidHeslthSearchRecipePageStates>(
      builder: (context, state) {
        return Container(
          //color: Colors.amber,
          margin: EdgeInsets.only(right: 30.w, left: 30.w, top: 20.h),
          child: Column(
            children: [
              FutureBuilder(
                future:
                    SqliteRecipeSearch().searchNameAndCategory(state.recipe_id),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Map<String, dynamic>> foodListData =
                        snapshot.data as List<Map<String, dynamic>>;
                    if (foodListData.isNotEmpty) {
                      return Column(
                        children: [
                          Text(
                            '${foodListData[0]['recipename']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 24.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: Color(0xff000000),
                            ),
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            '${foodListData[0]['categoryname']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w200,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: Color(0xff000000),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                onPressed: () {
                                  if (foodorele == false) {
                                    change();
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  // 可以根据需要添加其他样式属性
                                ),
                                child: text('食材'),
                              ),
                              TextButton(
                                onPressed: () {
                                  if (foodorele == true) {
                                    change();
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  overlayColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.transparent),
                                  // 可以根据需要添加其他样式属性
                                ),
                                child: text('營養成分'),
                              ),
                            ],
                          ),
                          Container(
                            height: 3.h,
                            color: Colors.black12,
                            margin: EdgeInsets.only(bottom: 10.h),
                          ),
                          foodorele ? FoodDetail() : elementDetail(),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            '適用疾病症狀:${foodListData[0]['illtags']}',
                            style: SafeGoogleFont(
                              'Inter',
                              fontSize: 20.sp,
                              fontWeight: FontWeight.w500,
                              height: 1.5.h,
                              letterSpacing: 2.4,
                              color: Color.fromARGB(255, 29, 29, 29),
                            ),
                          )
                        ],
                      );
                    }
                  }
                  return Container();
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

class FoodDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthSearchRecipePageBlocs,
        FurKidHeslthSearchRecipePageStates>(builder: (context, state) {
      return Container(
        height: 520.h,
        margin: EdgeInsets.only(bottom: 10.h),
        child: FutureBuilder(
          future: SqliteRecipeSearch().searchRecipeList(state.recipe_id),
          builder: (context, snap) {
            if (snap.hasData) {
              List<Map<String, dynamic>> foodListData =
                  snap.data as List<Map<String, dynamic>>;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: foodListData.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        String foodname = foodListData[index]['name'];
                        int quantity = foodListData[index]['quantity'];
                        return FoodDetailUi(
                            foodname: foodname, quantity: quantity);
                      },
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      );
    });
  }
}

class FoodDetailUi extends StatelessWidget {
  final int quantity;
  final String foodname;

  const FoodDetailUi(
      {super.key, required this.foodname, required this.quantity});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5.h),
      width: 200.w,
      height: 70.h,
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            blurRadius: 1,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
        border: Border.all(color: Colors.black12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                foodname,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  height: 1.5.h,
                  letterSpacing: 2.4,
                  color: Color.fromARGB(255, 29, 29, 29),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Container(
                width: 55.w,
                height: 30.h,
                decoration: BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                  border: Border.all(
                      color: const Color.fromARGB(31, 246, 147, 147)),
                ),
                child: Center(
                  child: Text(
                    '${quantity} g',
                    style: SafeGoogleFont(
                      'Inter',
                      fontWeight: FontWeight.w700,
                      height: 1.5.h,
                      letterSpacing: 1,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class elementDetail extends StatelessWidget {
  double hundredgram(double all, double foodweight) {
    return double.parse((all / foodweight * 100).toStringAsFixed(2));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FurKidHeslthSearchRecipePageBlocs,
        FurKidHeslthSearchRecipePageStates>(builder: (context, state) {
      return Container(
        height: 520.h,
        margin: EdgeInsets.only(top: 10.h),
        child: Center(
          child: FutureBuilder(
            future: SqliteRecipeSearch().searchDetail(state.recipe_id),
            builder: (context, snap) {
              if (snap.hasData) {
                List foodListData = snap.data as List;
                double foodweight =
                    foodListData[0]['food_weight'].toDouble() ?? 0.0;
                double finishweight =
                    foodListData[0]['finish_weight'].toDouble() ?? 0.0;
                double allkcal = foodListData[0]['calories'].toDouble() ?? 0.0;
                double allwater = foodListData[0]['water'].toDouble() ?? 0.0;
                double allprotein =
                    foodListData[0]['protein'].toDouble() ?? 0.0;

                double allfat = foodListData[0]['fat'].toDouble() ?? 0.0;
                double allcarbohydrate =
                    foodListData[0]['carbohydrate'].toDouble() ?? 0.0;
                double allfiber = foodListData[0]['fiber'].toDouble() ?? 0.0;
                double allash = foodListData[0]['ash'].toDouble() ?? 0.0;

                double kcal100g =
                    foodListData[0]['calories_per_100g'].toDouble() ?? 0.0;
                double water100g =
                    foodListData[0]['water_per_100g'].toDouble() ?? 0.0;
                double protein100g =
                    foodListData[0]['protein_per_100g'].toDouble() ?? 0.0;
                double fat100g =
                    foodListData[0]['fat_per_100g'].toDouble() ?? 0.0;
                double carbohydrate100g =
                    foodListData[0]['carbohydrate_per_100g'].toDouble() ?? 0.0;
                double fiber100g =
                    foodListData[0]['fiber_per_100g'].toDouble() ?? 0.0;
                double ash100g =
                    foodListData[0]['ash_per_100g'].toDouble() ?? 0.0;
                double proteinpercent = foodListData[0]['dry_rate_protein'];

                double fatpercent = foodListData[0]['dry_rate_fat'];

                double carbohydratepercent = foodListData[0]['dry_rate_carbo'];
                double fiberpercent = foodListData[0]['dry_rate_fiber'];
                double ashpercent = foodListData[0]['dry_rate_ash'];
                return SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      title('營養成分分析'),
                      element(
                          '食材重量', '${foodweight}g', '完成重量', '${finishweight}g'),
                      title('食譜營養成分含量'),
                      element('熱量', '${allkcal}kcal', '水分', '${allwater}g'),
                      element('粗蛋白質', '${allprotein}g', '粗脂肪', '${allfat}g'),
                      element('碳水化合物', '${allcarbohydrate}g', '粗纖維',
                          '${allfiber}g'),
                      element('灰份', '${allash}g', '', ''),
                      title('每100g營養成分'),
                      element('熱量', '${kcal100g}kcal', '水分', '${water100g}g'),
                      element('粗蛋白質', '${protein100g}g', '粗脂肪', '${fat100g}g'),
                      element('碳水化合物', '${carbohydrate100g}g', '粗纖維',
                          '${fiber100g}g'),
                      element('灰份', '${ash100g}g', '', ''),
                      title('乾物比'),
                      element(
                          '粗蛋白質', '$proteinpercent%', '粗脂肪', '$fatpercent%'),
                      element('碳水化合物', '$carbohydratepercent%', '粗纖維',
                          '$fiberpercent%'),
                      element('灰份', '$ashpercent%', '', ''),
                      title('主要營養成分'),
                      bigcircle(1,
                          water: allwater,
                          protein: allprotein,
                          fat: allfat,
                          carbo: allcarbohydrate,
                          ash: allash),
                      title('乾物比'),
                      bigcircle(2,
                          protein: proteinpercent,
                          fat: fatpercent,
                          carbo: carbohydratepercent,
                          ash: ashpercent),
                      title('熱量來源分布(熱量比)'),
                      bigcircle(3,
                          protein: allprotein * 3.5,
                          fat: allfat * 8.5,
                          carbo: allcarbohydrate * 3.5),
                    ],
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      );
    });
  }
}

Widget text(name) {
  return Container(
    margin: EdgeInsets.only(top: 20.h, left: 5.w, bottom: 10.h),
    child: Text(
      name,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125.h,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}

Widget element(
  String name1,
  String unit1,
  String name2,
  String unit2,
) {
  return Column(
    children: [
      Container(
        margin: EdgeInsets.only(left: 10.w, right: 10.w, top: 5.h),
        width: 300.w,
        height: 25.h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Text(
                name1,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.7,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    unit1,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 13,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Expanded(
              child: Text(
                name2,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14,
                  fontWeight: FontWeight.w200,
                  height: 1.2125.h,
                  letterSpacing: 0.7,
                  color: const Color(0xff000000),
                ),
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    unit2,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 13,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: const Color(0xff000000),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      Container(
        margin:
            EdgeInsets.only(left: 20.w, right: 20.w, top: 7.h, bottom: 15.h),
        height: 2.h,
        color: const Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget title(String text) {
  return IntrinsicWidth(
    child: Container(
      padding: EdgeInsets.only(left: 15.w, right: 15.w),
      margin: EdgeInsets.only(bottom: 15.h),
      height: 25,
      decoration: BoxDecoration(
        color: const Color(0xffe4007f),
        borderRadius: BorderRadius.circular(100),
      ),
      child: Center(
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 14,
            fontWeight: FontWeight.w700,
            height: 1.2125,
            letterSpacing: 0.7,
            color: const Color(0xffffffff),
          ),
        ),
      ),
    ),
  );
}

Widget circle(String text) {
  Color ccco = const Color(0xFF0075FF);
  if (text == '粗蛋白質') {
    ccco = const Color(0xFFE66676);
  } else if (text == '粗脂肪') {
    ccco = const Color(0xFFFF792D);
  } else if (text == '碳水') {
    ccco = const Color(0xFFAACD06);
  } else if (text == '灰份') {
    ccco = const Color(0xFF565555);
  }
  return Container(
    margin: EdgeInsets.only(left: 8.w, right: 3.w),
    width: 20, // 宽度
    height: 20, // 高度
    decoration: BoxDecoration(
      color: ccco, // 背景颜色
      shape: BoxShape.circle, // 指定形状为圆形
    ),
  );
}

Widget bigcircle(
  int number, {
  double? water,
  double? protein,
  double? fat,
  double? carbo,
  double? ash,
}) {
  var choice = <Widget>[
    circle('水分'),
    circletext('水分'),
    circle('粗蛋白質'),
    circletext('粗蛋白質'),
    circle('粗脂肪'),
    circletext('粗脂肪'),
    circle('碳水'),
    circletext('碳水'),
    circle('灰份'),
    circletext('灰份'),
  ];
  if (number == 2) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
      circle('灰份'),
      circletext('灰份'),
    ];
  }
  if (number == 3) {
    choice = <Widget>[
      circle('粗蛋白質'),
      circletext('粗蛋白質'),
      circle('粗脂肪'),
      circletext('粗脂肪'),
      circle('碳水'),
      circletext('碳水'),
    ];
  }
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    width: 290.w,
    height: 350.h,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      shadows: const [
        BoxShadow(
          color: Color(0x3F000000),
          blurRadius: 4,
          offset: Offset(0, 0),
          spreadRadius: 0,
        )
      ],
    ),
    child: Column(
      children: [
        PieChartAnalyze(
          number: number,
          water: water,
          protein: protein,
          fat: fat,
          carbo: carbo,
          ash: ash,
        ),
        Container(
          margin: EdgeInsets.only(top: 25.h, right: 5.w),
          child: IntrinsicWidth(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: choice),
          ),
        )
      ],
    ),
  );
}

Widget circletext(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.black,
      fontSize: 10,
      fontFamily: 'Inter',
      fontWeight: FontWeight.w300,
      height: 0,
      letterSpacing: 0.50,
    ),
  );
}

class PieChartAnalyze extends StatelessWidget {
  final double? water;
  final double? protein;
  final double? fat;
  final double? carbo;
  final double? ash;
  final int number;
  PieChartAnalyze(
      {super.key,
      required this.number,
      this.water = 0.0,
      this.protein = 0.0,
      this.fat = 0.0,
      this.carbo = 0.0,
      this.ash = 0.0});

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: PieChart(
        PieChartData(
          centerSpaceRadius: 0,
          sectionsSpace: 1,
          pieTouchData: PieTouchData(
            touchCallback:
                (FlTouchEvent touchEvent, PieTouchResponse? touchResponse) {
              if (touchEvent is FlTapUpEvent) {}
            },
          ),
          sections: [
            if (number == 1)
              PieChartSectionData(
                  color: const Color(0xFF0075FF),
                  value: water,
                  title:
                      '${(water! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 0.7),

            PieChartSectionData(
                color: const Color(0xFFE66676),
                value: protein,
                title: number == 1
                    ? '${(protein! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(protein! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(protein! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            PieChartSectionData(
                color: const Color(0xFFFF792D),
                value: fat,
                title: number == 1
                    ? '${(fat! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(fat! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(fat! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),

            PieChartSectionData(
                color: const Color(0xFFAACD06),
                value: carbo,
                title: number == 1
                    ? '${(carbo! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                    : number == 2
                        ? '${(carbo! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                        : '${(carbo! / (protein! + fat! + carbo!) * 100).toStringAsFixed(1)}%',
                radius: 100,
                titleStyle: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
                titlePositionPercentageOffset: 0.8),
            if (number == 2 || number == 1)
              PieChartSectionData(
                  color: const Color(0xFF565555),
                  value: ash,
                  title: number == 1
                      ? '${(ash! / (water! + protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%'
                      : '${(ash! / (protein! + fat! + carbo! + ash!) * 100).toStringAsFixed(1)}%',
                  radius: 100,
                  titleStyle: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w900),
                  titlePositionPercentageOffset: 1),
            // Add more PieChartSectionData for other sections
          ],
          // Add more chart configurations here
        ),
      ),
    );
  }
}
