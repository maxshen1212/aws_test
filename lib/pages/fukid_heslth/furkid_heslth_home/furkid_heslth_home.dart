import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_home/widgets/furkid_heslth_home_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';

class FurKidHeslthHome extends StatefulWidget {
  const FurKidHeslthHome({super.key});

  @override
  State<FurKidHeslthHome> createState() => _FurKidHeslthHomeState();
}

class _FurKidHeslthHomeState extends State<FurKidHeslthHome> {
  late String name;
  late String mail;
  late int accountLevel;
  @override
  void initState() {
    super.initState();
  }

  Future getMemberInfo() async {
    final sqlite = Sqlite();
    var memberInfo = await sqlite.query("member");
    // print("test1: ${memberInfo[0]}");
    name = memberInfo[0]["name"];
    mail = memberInfo[0]["email"];
    accountLevel = 12;
    return memberInfo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(110.h),
        child: FutureBuilder(
          future: getMemberInfo(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return buildAppbar(context, name, mail, "會員等級", accountLevel);
            }
            if (snapshot.hasError) {
              return const Center(
                  child: Text(
                '使用者載入錯誤',
                style: TextStyle(color: Colors.red, fontSize: 30),
              ));
            }
            return buildLoadingAppbar(context);
          },
        ),
      ),
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              buildHealthPassbookContainer(context, "毛孩健康存摺", 340),
              buildDietAndRecipesContainer(context, "毛孩飲食與食譜", 340),
              meridianpointsContainer(context, "經絡穴位", 340),
              SizedBox(
                height: 30.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
