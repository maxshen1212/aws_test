import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../common/values/colors.dart';
import '../../furkid_heslth_meridian_point/meridian_point_query/widgets/furkid_heslth_meridian_point_query_widgets.dart';
import '../../furkid_heslth_meridian_point/my_point_matching/my_point_matching.dart';
import '../../furkid_heslth_meridian_point/my_point_matching/widgets/my_point_matching_widgets.dart';

AppBar buildAppbar(BuildContext context, String username, String useremail,
    String membershiplevel, int level) {
  return AppBar(
    automaticallyImplyLeading: false,
    actions: [setUp(context)],
    flexibleSpace: Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              AppColors.primaryLogo,
              Colors.purple,
            ]),
      ),
      child: Container(
        margin: EdgeInsets.only(top: 35.h, left: 25.w),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 75.w,
              height: 85.h,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/images/cat.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 18.w),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    username,
                    style: TextStyle(
                        fontSize: 14.sp,
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    useremail,
                    style: TextStyle(
                        fontSize: 14.sp,
                        color: Colors.white,
                        fontWeight: FontWeight.normal),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5.h),
                    padding: EdgeInsets.only(left: 5.w, right: 5.w),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15.w)),
                        border: Border.all(color: Colors.white)),
                    child: Center(
                      child: Text(
                        membershiplevel + " : Lv${level}",
                        style: TextStyle(
                            fontSize: 12.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

AppBar buildLoadingAppbar(BuildContext context) {
  return AppBar(
    automaticallyImplyLeading: false,
    actions: [setUp(context)],
    flexibleSpace: Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              AppColors.primaryLogo,
              Colors.purple,
            ]),
      ),
      child: Container(
        margin: EdgeInsets.only(top: 30.h),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "載入中...",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

Widget reusableLoginIcon(context, String iconName, type, buttonId) {
  return GestureDetector(
    onTap: () async {
      if (type == "毛孩健康存摺") {
        switch (buttonId) {
          //圖片上傳
          case 1:
            Navigator.of(context).pushNamed("furkid_heslth_upload_image");
            break;
          //CBC
          case 2:
            Navigator.of(context).pushNamed("furkid_heslth_cbc");
            break;
          //血液生化檢查
          case 3:
            Navigator.of(context).pushNamed("furkid_heslth_biochemistry_test");
            break;
          //尿液檢查
          case 4:
            Navigator.of(context).pushNamed("furkid_heslth_urine_test");
            break;
          //其他套件檢查
          case 5:
            Navigator.of(context).pushNamed("furkid_heslth_else_test");
            break;
          //喝水量紀錄
          case 6:
            Navigator.of(context).pushNamed("furkid_heslth_drink");
            break;
          //飲食紀錄
          case 7:
            Navigator.of(context).pushNamed("furkid_heslth_food");
            break;
          //配穴紀錄
          case 8:
            Navigator.of(context).pushNamed("furkid_heslth_acupoint_record");
            break;
          //摘要紀錄
          case 9:
            Navigator.of(context).pushNamed("furkid_heslth_summary");
            break;
          //分析與查詢
          case 10:
            Navigator.of(context).pushNamed("furkid_heslth_analyze_and_search");
            break;
        }
      } else if (type == "毛孩飲食與食譜") {
        switch (buttonId) {
          //食譜查詢
          case 1:
            Navigator.of(context).pushNamed("furkid_heslth_recipesearch");

          //食材查詢
          case 2:
            Navigator.of(context).pushNamed("furkid_heslth_ingredient");

          //新增自有食譜
          case 3:
            Navigator.of(context).pushNamed("furkid_heslth_plusrecipe");

          //我的食譜
          case 4:
            Navigator.of(context).pushNamed("furkid_heslth_myrecipe");

          //熱量建議

          case 5:
            Navigator.of(context).pushNamed("furkid_heslth_suggest");
        }
      } else {
        switch (buttonId) {
          //經絡穴位查詢
          case 1:

            // final matchingAcupointNotFavoriteData =
            //       await SqliteMeridianAcuPoint()
            //           .getAllMeridianQuery();
            //   final matchingAcupointFavoriteData =
            //       await SqliteMeridianAcuPoint()
            //           .getAllAcupointQuery();
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //           builder: (context) => FurkidMyPointMatching(
            //                 matchingAcupintNotFavorite:
            //                     matchingAcupointNotFavoriteData,
            //                 matchingAcupintFavorite: matchingAcupointFavoriteData,
            //               )));

            Navigator.of(context).pushNamed("furkid_heslth_meridian_point");
            break;
          //配位查詢
          case 2:
            Navigator.of(context)
                .pushNamed('furkid_heslth_point_matching_query');
            break;
          //新增配位
          case 3:
            Navigator.of(context).pushNamed('furkid_heslth_add_point_matching');
            break;
          //我的配位
          case 4:
            final matchingAcupointNotFavoriteData =
                await SqliteMyMatchingAcupoint()
                    .getAllMatchingAcupointNotFavoriteData();
            final matchingAcupointFavoriteData =
                await SqliteMyMatchingAcupoint()
                    .getAllMatchingAcupointFavoriteData();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FurkidMyPointMatching(
                          matchingAcupintNotFavorite:
                              matchingAcupointNotFavoriteData,
                          matchingAcupintFavorite: matchingAcupointFavoriteData,
                        )));

            //Navigator.of(context).pushNamed('furkid_heslth_my_point_matching');
            break;
        }
      }
    },
    child: SizedBox(
      width: 40.w,
      height: 35.h,
      child: SvgPicture.asset("assets/furkid_health_app/icon/$iconName.svg"),
    ),
  );
}

Widget buildHealthPassbookContainer(
    BuildContext context, String containername, int widths) {
  return Container(
    margin: EdgeInsets.only(top: 30.h),
    width: widths.w, //300
    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h, left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              healthPassbookIcon(
                  context, "圖片上傳", "ic_passbook_image", "毛孩健康存摺", 1),
              healthPassbookIcon(
                  context, "CBC檢查\n項目", "ic_passbook_CBC", "毛孩健康存摺", 2),
              healthPassbookIcon(
                  context, "血液生化\n檢查", "ic_passbook_blood", "毛孩健康存摺", 3),
              healthPassbookIcon(
                  context, "尿液檢查", "ic_passbook_urine", "毛孩健康存摺", 4),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              healthPassbookIcon(
                  context, "其他套件\n檢查", "ic_passbook_report", "毛孩健康存摺", 5),
              healthPassbookIcon(
                  context, "喝水量紀錄", "ic_passbook_water", "毛孩健康存摺", 6),
              healthPassbookIcon(
                  context, "飲食紀錄", "ic_passbook_food", "毛孩健康存摺", 7),
              healthPassbookIcon(
                  context, "配穴紀錄", "ic_passbook_acupoint", "毛孩健康存摺", 8),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 5.w),
          padding: EdgeInsets.only(top: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              healthPassbookIcon(
                  context, "摘要紀錄", "ic_passbook_record", "毛孩健康存摺", 9),
              healthPassbookIcon(
                  context, "分析與查詢", "ic_passbook_found", "毛孩健康存摺", 10),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget buildDietAndRecipesContainer(
  BuildContext context,
  String containername,
  int width,
) {
  return Container(
    margin: EdgeInsets.only(top: 20.h),
    width: width.w, //300
    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp, color: Colors.black),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h, left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              dietAndRecipesIcon(context, "食譜查詢",
                  "ic_recipe_and_ingredient_found", "毛孩飲食與食譜", 1),
              dietAndRecipesIcon(context, "食材查詢",
                  "ic_recipe_and_ingredient_found", "毛孩飲食與食譜", 2),
              dietAndRecipesIcon(
                  context, "新增自有\n食譜", "ic_recipe_add", "毛孩飲食與食譜", 3),
              dietAndRecipesIcon(
                  context, "我的食譜", "ic_recipe_mine", "毛孩飲食與食譜", 4),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              dietAndRecipesIcon(
                  context, "熱量建議", "ic_recipe_kcal", "毛孩飲食與食譜", 5),
              // calorieRecommendations("kcal", "ic_recipe_kcal", context)
            ],
          ),
        ),
      ],
    ),
  );
}

Widget meridianpointsContainer(
  BuildContext context,
  String containername,
  int width,
) {
  return Container(
    margin: EdgeInsets.only(top: 20.h),
    width: width.w, //300

    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp, color: Colors.black),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 8.h, bottom: 7.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              meridianpointsIcon(context, "經絡穴位\n查詢",
                  "ic_acupoint_and_meridian_acupoint_search", "經絡穴位", 1),
              meridianpointsIcon(
                  context, "配穴查詢", "ic_acupoint_search", "經絡穴位", 2),
              meridianpointsIcon(context, "新增配穴", "ic_acupoint_add", "經絡穴位", 3),
              meridianpointsIcon(
                  context, "我的配穴", "ic_acupoint_mine", "經絡穴位", 4),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget healthPassbookIcon(context, String name, iconName, type, buttonId) {
  return Row(
    children: [
      SizedBox(
        width: 80.w,
        height: 85.h,
        child: Column(
          children: [
            reusableLoginIcon(context, iconName, type, buttonId),
            SizedBox(
              height: 3.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget dietAndRecipesIcon(context, String name, iconName, type, buttonId) {
  return Row(
    children: [
      SizedBox(
        width: 80.w,
        height: 85.h,
        child: Column(
          children: [
            reusableLoginIcon(context, iconName, type, buttonId),
            SizedBox(
              height: 5.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget meridianpointsIcon(context, String name, iconName, type, buttonId) {
  return Row(
    children: [
      SizedBox(
        width: 80.w,
        height: 85.h,
        child: Column(
          children: [
            reusableLoginIcon(context, iconName, type, buttonId),
            SizedBox(
              height: 5.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget cbcIcon(String icon_name, String name) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 65.h,
        child: Column(
          children: [
            Container(
              width: 32.w,
              height: 28.h,
              decoration: BoxDecoration(
                border:
                    Border.all(color: const Color.fromARGB(255, 230, 9, 126)),
              ),
              child: Center(
                child: Text(
                  icon_name,
                  style: TextStyle(
                      fontSize: 10.sp,
                      fontWeight: FontWeight.bold,
                      color: const Color.fromARGB(233, 227, 14, 128)),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      )
    ],
  );
}

Widget calorieRecommendations(
    String icon_name, String name, BuildContext context) {
  return GestureDetector(
    onTap: () {
      Navigator.of(context)
          .pushNamedAndRemoveUntil("furkid_heslth_suggest", (route) => false);
    },
    child: Row(
      children: [
        SizedBox(
          width: 72.w,
          height: 65.h,
          child: Column(
            children: [
              Container(
                width: 32.w,
                height: 28.h,
                decoration: BoxDecoration(
                  border:
                      Border.all(color: const Color.fromARGB(255, 87, 11, 101)),
                ),
                child: Center(
                  child: Text(
                    icon_name,
                    style: TextStyle(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold,
                        color: const Color.fromARGB(255, 87, 11, 101)),
                  ),
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
              Text(
                name,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 11.sp),
              ),
            ],
          ),
        )
      ],
    ),
  );
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 30.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}
