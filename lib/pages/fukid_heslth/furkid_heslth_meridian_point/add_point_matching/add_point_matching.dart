import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/add_point_matching/widgets/add_point_matching_widgets.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../database/sqlite.dart';
import '../../../setup/setup.dart';

ScrollController controller = ScrollController();

class FurkidAddPointMatching extends StatefulWidget {
  const FurkidAddPointMatching({
    Key? key,
  });

  @override
  State<FurkidAddPointMatching> createState() => _FurkidAddPointMatchingState();
}

class _FurkidAddPointMatchingState extends State<FurkidAddPointMatching> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController sicknessController = TextEditingController();
  final TextEditingController symptomsController = TextEditingController();
  final TextEditingController diagnosisController = TextEditingController();
  final TextEditingController pathogenesisController = TextEditingController();
  final TextEditingController timesController = TextEditingController();
  final TextEditingController memoController = TextEditingController();
  Map<String, bool> buttonStates = {
    "手太陰肺經": true,
    "手陽明大腸經": true,
    "足厥陰肝經": true,
    "足少陽膽經": true,
    "足少陰腎經": true,
    "足太陽膀胱經": true,
    "手少陰心經": true,
    "手太陽小腸經": true,
    "足太陰脾經": true,
    "足陽明胃經": true,
    "手厥陰心包經": true,
    "手少陽三焦經": true,
    "任脈": true,
    "督脈": true,
    "特殊穴位": true,
  };
  List addPoint = [];
  bool addPointMatchingGotobody2 = false;
  //小按鈕切換到穴位介紹頁面
  void _togglebody2() {
    setState(() {
      addPointMatchingGotobody2 = !addPointMatchingGotobody2;
    });
  }

  //穴位介紹按返回body1
  void _togglebody1(point) {
    setState(() {
      addPointMatchingGotobody2 = !addPointMatchingGotobody2;
    });
    addPoint.addAll(point);
    final josnList = addPoint.map((item) => jsonEncode(item)).toList();
    final uniqueJosnList = josnList.toSet().toList();
    final result = uniqueJosnList.map((item) => jsonDecode(item)).toList();
    addPoint = result;
    print(addPoint);
  }

  void removePoint(index) {
    setState(() {
      addPoint.removeAt(index);
    });
  }

  void _toggleImage(String name) {
    setState(() {
      for (var key in buttonStates.keys) {
        if (key != name) buttonStates[key] = true;
      }
      buttonStates[name] = !buttonStates[name]!;
    });
  }
  void refreshState() {
    setState(() {});
  }
  void initState() {
    super.initState();
    controller.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthAddPointMatching(
          context, "新增配穴", addPointMatchingGotobody2, _togglebody2),
      body: addPointMatchingGotobody2
          ? furkidAddPointMatchingBody2(
              buttonStates, _toggleImage, context, _togglebody1,refreshState)
          : furkidAddPointMatchingBody1(
              context, _togglebody2, addPoint, removePoint,[nameController,sicknessController,symptomsController,diagnosisController,pathogenesisController,timesController,memoController]),
    );
  }
}

ScrollController controllerView() {
  return controller;
}
