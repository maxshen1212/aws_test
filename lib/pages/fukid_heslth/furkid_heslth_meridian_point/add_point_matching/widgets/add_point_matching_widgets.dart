import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:sqflite/sqflite.dart';
import '../../../../../common/values/colors.dart';
import '../../../../../utils.dart';
import '../../my_point_matching/my_point_matching.dart';
import '../../my_point_matching/widgets/my_point_matching_widgets.dart';
import '../add_point_matching.dart';

class SqliteAddPoint {
  insertMeridianAcupoint(table, Map<String, dynamic> data) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianResult = await db.rawQuery('''
      select id from matching_acupoint_collect order by id desc limit 1;
    ''');
    if (meridianResult.length != 0) {
      print(meridianResult);
      data['id'] = meridianResult[0]['id'] + 1;
    } else {
      data['id'] = 1;
    }
    print("ID:${data['id']}");
    await sqlite.insert(table, data);
    return data['id'];
  }

  getSearchMeridian(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> searchAcupoint = await db.rawQuery('''
        SELECT * FROM acupoint
        WHERE name LIKE "%$searchWord%"
    ''');
    List meridian = [];
    for (int i = 0; i < searchAcupoint.length; i++) {
      final List<Map<String, dynamic>> searchMeridian = await db.rawQuery('''
        SELECT * FROM meridian
        WHERE ${searchAcupoint[i]['meridian_id']} == id
      ''');
      meridian.add(searchMeridian);  
    }
    if (meridian.length == 0) {
      return meridian;
    } else {
      return meridian[0];
    }
  }

  getAllMeridianData() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianResult = await db.rawQuery('''
      SELECT id,name FROM meridian
    ''');
    return meridianResult;
  }

  getSearchAcupoint(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> searchAcupoint = await db.rawQuery('''
        SELECT * FROM acupoint
        WHERE name LIKE "%$searchWord%"
    ''');
    return searchAcupoint;
  }

  getAllAcupointQuery(meridianId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupointResult = await db.rawQuery('''
      SELECT id,name,status FROM acupoint
      WHERE meridian_id==$meridianId
    ''');
    return acupointResult;
  }

  getAllSelectAcupointStatus() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupointStatus = await db.rawQuery('''
      SELECT acupoint.id,acupoint.name,acupoint.sn,meridian.name as meridian_name,meridian.code
      FROM acupoint
      LEFT JOIN meridian
      WHERE acupoint.status==1 AND acupoint.meridian_id=meridian.id;
    ''');
    return acupointStatus;
  }

  getAcupointStatus(id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final acupointStatus = await db.rawQuery('''
      SELECT status FROM acupoint
      WHERE id==$id
    ''');
    return acupointStatus[0]["status"];
  }

  updateAcupointStatus(status, id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('''
    UPDATE acupoint
    SET status=?
    WHERE ID = ?
    ''', [status, id]);
  }

  initUpdateAcupointStatus() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawUpdate('''
    UPDATE acupoint
    SET status=?
    ''', [0]);
  }
}

AppBar furkidHeslthAddPointMatching(BuildContext context, String names,
    bool addPointMatchingGotobody2, Function togglebody2) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(addPointMatchingGotobody2, togglebody2),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
        decoration: const BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            AppColors.primaryLogo,
            Colors.purple,
          ]),
    )),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget goBack(addPointMatchingGotobody2, togglebody2) {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!addPointMatchingGotobody2) {
          Navigator.pop(context);
        } else {
          togglebody2();
        }
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

Widget buildText(name) {
  return Container(
    margin: EdgeInsets.only(left: 25.w),
    child: Text(
      name,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}

Widget pointText(name, color) {
  return Text(
    '$name' ?? "",
    textAlign: TextAlign.center,
    style: SafeGoogleFont(
      'Inter',
      fontSize: 14.sp,
      fontWeight: FontWeight.w200,
      height: 1.2125,
      letterSpacing: 0.8,
      color: Color(color),
    ),
  );
}

Widget buildTextField(String hintText, textController, String category) {
  return Center(
    child: Container(
      width: 340.w,
      //padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 5.h),
      child: TextFormField(
          controller: textController,
          keyboardType: TextInputType.multiline,
          validator: (value) {
            if (category == "配穴名稱") {
              if (value == null || value.isEmpty) {
                return '請勿為空';
              }
            }
            return null;
          },
          decoration: const InputDecoration(
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            hintText: '文字輸入',
            isDense: true,
            filled: true,
            fillColor: Color(0xffececec),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.transparent), // 去掉默认底线
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Color.fromARGB(0, 11, 128, 32)),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.transparent), // 去掉错误边框
            ),
          ),
          style: TextStyle(
              fontFamily: "Avenir",
              fontWeight: FontWeight.normal,
              fontSize: 14.sp),
          autocorrect: false,
          obscureText: false),
    ),
  );
}

Widget pointList(acupointListData, Function removePoint) {
  return FutureBuilder(
    builder: (context, snap) {
      if (acupointListData.length != 0) {
        return ListView.builder(
            shrinkWrap: true,
            itemCount: acupointListData.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return pointListView(acupointListData, index, removePoint);
            });
      } else {
        return SizedBox(
          width: 330.w,
          child: const Text(
            "尚未加入任何穴位",
            textAlign: TextAlign.start,
          ),
        );
      }
    },
    future: null,
  );
}

Widget pointListView(acupointListData, index, Function removePoint) {
  return Container(
    decoration: const BoxDecoration(
      color: Color(0xffffffff),
      border: Border(bottom: BorderSide(color: Colors.black)),
    ),
    height: 30.h,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 2.w),
          child: Row(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 5.w),
                  child: GestureDetector(
                    child: const Icon(Icons.remove_circle),
                    onTap: () {
                      removePoint(index);
                    },
                  )),
              Container(
                width: 60.w,
                child: Text(
                  acupointListData[index]['name'] ?? "",
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w200,
                    height: 1.2125,
                    letterSpacing: 0.8,
                    color: const Color(0xff000000),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 92.w,
          child:
              pointText(acupointListData[index]['meridian_name'], 0xff000000),
        ),
        Container(
          margin: EdgeInsets.only(left: 25.w),
          width: 35.w,
          child: pointText(acupointListData[index]['code'], 0xff000000),
        ),
        Container(
          margin: EdgeInsets.only(left: 46.w),
          width: 28.w,
          child: pointText(acupointListData[index]['sn'], 0xff000000),
        ),
      ],
    ),
  );
}

Widget furkidAddPointMatchingBody1(BuildContext context, Function _togglebody2,
    addPoint, Function removePoint, textController) {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  return SingleChildScrollView(
    child: Form(
      key: _formKey,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(
          height: 20.h,
        ),
        buildText('配穴名稱'),
        Container(
          margin: EdgeInsets.only(bottom: 10.h),
          child: buildTextField("文字輸入", textController[0], '配穴名稱'),
        ),
        Container(
          margin: EdgeInsets.only(top: 5.h, bottom: 5.h, left: 25.w),
          child: Text(
            '穴位',
            style: SafeGoogleFont(
              'Inter',
              fontSize: 16.sp,
              fontWeight: FontWeight.w200,
              height: 1.2125,
              letterSpacing: 0.8,
              color: const Color(0xff000000),
            ),
          ),
        ),
        Center(
          child: Container(
              width: 340.w,
              height: 210.h,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(236, 236, 236, 1),
                borderRadius: BorderRadius.all(Radius.circular(10.w)),
              ),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        color: const Color.fromARGB(255, 87, 80, 80),
                        height: 30.h,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              child: Text(
                                "穴位名稱",
                                textAlign: TextAlign.center,
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w200,
                                  height: 1.2125,
                                  letterSpacing: 0.8,
                                  color: const Color(0xffffffff),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 50.w),
                              child: Text(
                                "經絡名",
                                textAlign: TextAlign.center,
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w200,
                                  height: 1.2125,
                                  letterSpacing: 0.8,
                                  color: const Color(0xffffffff),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 35.w),
                              child: Text(
                                "經絡代號",
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w200,
                                  height: 1.2125,
                                  letterSpacing: 0.8,
                                  color: const Color(0xffffffff),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 18.w),
                              child: Text(
                                "穴位序號",
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w200,
                                  height: 1.2125,
                                  letterSpacing: 0.8,
                                  color: const Color(0xffffffff),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: SizedBox(
                            width: 340.w,
                            child: Column(
                              children: [
                                pointList(addPoint, removePoint),
                                SizedBox(
                                  height: 40.h,
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: 155.h,
                    left: 300.w,
                    child: SizedBox(
                      width: 30.w,
                      child: FloatingActionButton(
                        foregroundColor: Colors.black,
                        backgroundColor: Colors.white,
                        onPressed: () async {
                          await SqliteAddPoint().initUpdateAcupointStatus();
                          _togglebody2();
                        },
                        child: const Icon(Icons.add),
                      ),
                    ),
                  ),
                ],
              )),
        ),
        SizedBox(
          height: 10.h,
        ),
        buildText('適用疾病(多項請以,隔開)'),
        buildTextField("文字輸入", textController[1], '疾病'),
        SizedBox(
          height: 10.h,
        ),
        buildText('病機'),
        buildTextField("文字輸入", textController[2], '病機'),
        SizedBox(
          height: 10.h,
        ),
        buildText('症狀(多項請以,隔開)'),
        Container(
          child: buildTextField("文字輸入", textController[3], '症狀'),
        ),
        SizedBox(
          height: 10.h,
        ),
        buildText('診斷(多項請以,隔開)'),
        Container(
          child: buildTextField("文字輸入", textController[4], '診斷'),
        ),
        SizedBox(
          height: 10.h,
        ),
        buildText('建議操作時間(分)'),
        Container(
          child: buildTextField("文字輸入", textController[5], '時間'),
        ),
        SizedBox(
          height: 10.h,
        ),
        buildText('備註'),
        Container(
          child: buildTextField("文字輸入", textController[6], '備註'),
        ),
        SizedBox(
          height: 30.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            furkidConfirmButton(
                context, textController, "新增", addPoint, _formKey),
            furkidCancelButton(context, "捨棄"),
          ],
        ),
        SizedBox(
          height: 30.h,
        ),
      ]),
    ),
  );
}

Widget furkidConfirmButton(
    BuildContext context, textController, String text, addPoint, formKey) {
  return SizedBox(
    width: 100.w,
    height: 40.h,
    child: TextButton(
        onPressed: () async {
          final sqlite = Sqlite();
          if (formKey.currentState!.validate()) {
            if (addPoint.length == 0) {
              toast("請輸入至少一個穴位！");
            } else {
              Map<String, dynamic> addMatchingAcupoint = {
                'id': null,
                'name': textController[0].text,
                'sickness': textController[1].text,
                'symptoms': textController[2].text,
                'diagnosis': textController[3].text,
                'pathogenesis': textController[4].text,
                'times': textController[5].text,
                'memo': textController[6].text,
                'mem_id': 1,
              };
              print(addMatchingAcupoint);
              dynamic matchingAcupointId = await SqliteAddPoint()
                  .insertMeridianAcupoint(
                      "matching_acupoint", addMatchingAcupoint);

              Map<String, dynamic> addMatchingAcupointCollect = {
                'mem_id': addMatchingAcupoint['mem_id'],
                'matching_acupoint_id': matchingAcupointId,
              };
              await sqlite.insert(
                  "matching_acupoint_collect", addMatchingAcupointCollect);
              List<Map<String, dynamic>> matchingAcupointList = [];
              for (int i = 0; i < addPoint.length; i++) {
                Map<String, dynamic> item = {
                  "matching_acupoint_id": matchingAcupointId,
                  "acupoint_id": addPoint[i]['id']
                };
                matchingAcupointList.add(item);
              }
              matchingAcupointList.forEach(
                  (e) async => sqlite.insert("matching_acupoint_list", e));

              toast("您以成功新增配穴！");
              final matchingAcupointNotFavoriteData =
                  await SqliteMyMatchingAcupoint()
                      .getAllMatchingAcupointNotFavoriteData();
              final matchingAcupointFavoriteData =
                  await SqliteMyMatchingAcupoint()
                      .getAllMatchingAcupointNotFavoriteData();
              // ignore: use_build_context_synchronously
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FurkidMyPointMatching(
                            matchingAcupintNotFavorite:
                                matchingAcupointNotFavoriteData,
                            matchingAcupintFavorite:
                                matchingAcupointFavoriteData,
                          )));
            }
          }
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: const Color(0xffe4007f),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: const Color(0xffffffff),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Future<void> _dialogBuilder(BuildContext context) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確認捨棄？',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                Navigator.of(context)
                  ..pop()
                  ..pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Widget furkidCancelButton(BuildContext context, String text) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 30.w),
    child: TextButton(
        onPressed: () {
          _dialogBuilder(context);
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: const Color(0xff929292),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: const Color(0xffffffff),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

//------------------Body2------------------
Widget furkidAddPointMatchingBody2(
    Map<String, bool> buttonStates,
    Function(String) toggleImage,
    BuildContext context,
    Function togglebody1,
    refreshState) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(
        height: 20.h,
      ),
      buildText("穴位搜尋"),
      Container(
        margin: EdgeInsets.only(bottom: 10.h),
        child: buildTextFieldBody2("搜尋穴位", refreshState),
      ),
      FutureBuilder(
          future: searchTextMeridianAcupointcontroller.text == ""
              ? SqliteAddPoint().getAllMeridianData()
              : SqliteAddPoint()
                  .getSearchMeridian(searchTextMeridianAcupointcontroller.text),
          builder: (context, snap) {
            if (snap.hasData) {
              List meridianListData = snap.data as List;
              return Column(
                children: [
                  Container(
                    height: 550.h,
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: meridianListData.length,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              if (searchTextMeridianAcupointcontroller.text ==
                                  "") ...[
                                furkidPointList(
                                    meridianListData[index], context, () {
                                  toggleImage(meridianListData[index]["name"]);
                                },
                                    buttonStates[meridianListData[index]
                                        ["name"]]),
                                (buttonStates[meridianListData[index]
                                            ["name"]] ==
                                        false)
                                    ? pressPointList(meridianListData[index],
                                        togglebody1, index)
                                    : SizedBox()
                              ] else ...[
                                pressPointList(meridianListData[index],
                                    togglebody1, index),
                              ]
                            ],
                          );
                        }),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    child:
                        furkidAddPointButtonBody2(context, "加入穴位", togglebody1),
                  ),
                ],
              );
            }
            return Container();
          }),
    ],
  );
}

Widget pressPointList(meridian, Function togglebody2, allIndex) {
  return Column(
    children: [
      SizedBox(
          width: 320.w,
          child: FutureBuilder(
              future: searchTextMeridianAcupointcontroller.text == ""
                  ? SqliteAddPoint().getAllAcupointQuery(meridian["id"])
                  : SqliteAddPoint().getSearchAcupoint(
                      searchTextMeridianAcupointcontroller.text),
              builder: (context, snap) {
                if (snap.hasData) {
                  List acupointListData = snap.data as List;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: acupointListData.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return pressPointListView(acupointListData, index,
                            acupointListData[index]['status'], togglebody2);
                      });
                }
                return Container();
              })),
      SizedBox(
        height: 5.h,
      )
    ],
  );
}

Widget furkidAddPointButtonBody2(BuildContext context, text, togglebody1) {
  return Center(
    child: Container(
      width: 100.w,
      height: 50.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: TextButton(
          onPressed: () async {
            final acupointInformation =
                await SqliteAddPoint().getAllSelectAcupointStatus();
            togglebody1(
              acupointInformation,
            );
          },
          style: TextButton.styleFrom(
            padding: EdgeInsets.zero,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: const Color(0xffe4007f),
              borderRadius: BorderRadius.circular(6.r),
            ),
            child: Center(
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: SafeGoogleFont('Inter',
                    fontSize: 15.sp,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.8.r,
                    color: const Color(0xffffffff),
                    decoration: TextDecoration.none),
              ),
            ),
          )),
    ),
  );
}

Widget furkidPointList(
    meridian, BuildContext context, Null Function() param2, bool? buttonStat) {
  return SizedBox(
    width: 320.w,
    child: TextButton(
      onPressed: () {
        param2();
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        height: 42.h,
        decoration: BoxDecoration(
          color: const Color(0xffececec),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          SizedBox(
            width: 8.w,
          ),
          SizedBox(
            width: 100.w,
            child: Text(
              meridian['name'],
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14.sp,
                fontWeight: FontWeight.w300,
                letterSpacing: 0.7.r,
                color: const Color(0xff000000),
              ),
            ),
          ),
          Container(
            width: 20.w,
            margin: EdgeInsets.only(left: 170.h),
            child: Text(
              ">",
              style: TextStyle(fontSize: 18.sp, color: Colors.black),
            ),
          )
        ]),
      ),
    ),
  );
}

final TextEditingController searchTextMeridianAcupointcontroller =
    new TextEditingController();
Widget buildTextFieldBody2(String hintText, refreshState) {
  return Center(
    child: Container(
      margin: EdgeInsets.only(top: 10.h),
      height: 40.h,
      width: 320.w,
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.w)),
      ),
      child: Container(
        margin: EdgeInsets.only(left: 10.w),
        child: TextField(
          onChanged: (text) {
            refreshState();
          },
          controller: searchTextMeridianAcupointcontroller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: hintText,
            isDense: true,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
          autocorrect: false,
          obscureText: false,
        ),
      ),
    ),
  );
}

Widget pressPointListView(pointList, index, ischeck, Function togglebody2) {
  return StatefulBuilder(builder: (context, setState) {
    return GestureDetector(
      onTap: () async {
        dynamic getStatus =
            await SqliteAddPoint().getAcupointStatus(pointList[index]['id']);
        setState(() {
          if (getStatus == 1) {
            ischeck = 0;
            SqliteAddPoint()
                .updateAcupointStatus(ischeck, pointList[index]['id']);
          } else {
            ischeck = 1;
            SqliteAddPoint()
                .updateAcupointStatus(ischeck, pointList[index]['id']);
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(top: 6.h),
        width: 310.w,
        height: 34.h,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8.w)),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1)
          ],
        ),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(left: 10.w),
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.shade300,
                  width: 2.0,
                ),
                color: const Color(0xffececec),
                borderRadius: BorderRadius.circular(100),
              ),
              child: ischeck == 1
                  ? Image.asset("assets/appdesign/images/auto-group-p8ym.png")
                  : null,
            ),
            Container(
              margin: EdgeInsets.only(left: 7.5.w),
              width: 52.w,
              child: Text(pointList[index]["name"] ?? "",
                  style: TextStyle(
                    color: const Color(0xFF595757),
                    fontSize: 12.sp,
                    fontFamily: 'Inter',
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1,
                  )),
            ),
          ],
        ),
      ),
    );
  });
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
