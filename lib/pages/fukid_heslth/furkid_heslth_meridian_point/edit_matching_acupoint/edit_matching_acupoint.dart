import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/edit_matching_acupoint/widgets/edit_matching_acupoint_widgets.dart';

import '../../../setup/setup.dart';

class FurkidEditPointMatching extends StatefulWidget {
  FurkidEditPointMatching({Key? key, required this.matchingAcupintEditdata,required this.matchingAcupointText }) : super(key: key);
  final matchingAcupintEditdata;
  final matchingAcupointText;
  
  @override
  State<FurkidEditPointMatching> createState() => _FurkidEditPointMatchingState();
}

class _FurkidEditPointMatchingState extends State<FurkidEditPointMatching> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController sicknessController = TextEditingController();
  final TextEditingController symptomsController = TextEditingController();
  final TextEditingController diagnosisController = TextEditingController();
  final TextEditingController pathogenesisController = TextEditingController();
  final TextEditingController timesController = TextEditingController();
  final TextEditingController memoController = TextEditingController();
   
  
  
  Map<String, bool> buttonStates = {
    "手太陰肺經": true,
    "手陽明大腸經": true,
    "足厥陰肝經": true,
    "足少陽膽經": true,
    "足少陰腎經": true,
    "足太陽膀胱經": true,
    "手少陰心經": true,
    "手太陽小腸經": true,
    "足太陰脾經": true,
    "足陽明胃經": true,
    "手厥陰心包經": true,
    "手少陽三焦經": true,
    "任脈": true,
    "督脈": true,
    "特殊穴位": true,
  };
  
  List addPoint = [];
  bool addPointMatchingGotobody2 = false;
  //小按鈕切換到穴位介紹頁面
  void _togglebody2() {
    setState(() {
      addPointMatchingGotobody2 = !addPointMatchingGotobody2;
    });
  }
  
  //穴位介紹按返回body1
  void _togglebody1(point) {
    setState(() {
      addPointMatchingGotobody2 = !addPointMatchingGotobody2;
    });
    addPoint.addAll(point);
    final josnList = addPoint.map((item) => jsonEncode(item)).toList();
    final uniqueJosnList = josnList.toSet().toList();
    final result = uniqueJosnList.map((item) => jsonDecode(item)).toList();
    addPoint = result;
  }

  void removePoint(index) {
    setState(() {
      
      addPoint.removeAt(index);
      print(addPoint);
    });
  }

  void _toggleImage(String name) {
    setState(() {
      for (var key in buttonStates.keys) {
        if (key != name) buttonStates[key] = true;
      }
      buttonStates[name] = !buttonStates[name]!;
    });
  }
  void refreshState() {
    setState(() {});
  }
  void initState() {
    super.initState();
    nameController.text=widget.matchingAcupointText[0]["name"]??"";
    sicknessController.text=widget.matchingAcupointText[0]["sickness"]??"";
    symptomsController.text=widget.matchingAcupointText[0]["symptoms"]??"";
    diagnosisController.text=widget.matchingAcupointText[0]["diagnosis"]??"";
    pathogenesisController.text=widget.matchingAcupointText[0]["pathogenesis"]??"";
    timesController.text="${widget.matchingAcupointText[0]["times"]}"??"";
    memoController.text=widget.matchingAcupointText[0]["memo"]??"";
    addPoint.addAll(widget.matchingAcupintEditdata);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: furkidHeslthEditPointMatching(
          context, "編輯配穴", addPointMatchingGotobody2, _togglebody2),
      body: addPointMatchingGotobody2
          ? furkidEditPointMatchingBody2(
              buttonStates, _toggleImage, context, _togglebody1,refreshState)
          : furkidEditPointMatchingBody1(
              context, _togglebody2, addPoint,removePoint,[nameController,sicknessController,symptomsController,diagnosisController,pathogenesisController,timesController,memoController],widget.matchingAcupointText[0]["id"]),
    );
  }
}