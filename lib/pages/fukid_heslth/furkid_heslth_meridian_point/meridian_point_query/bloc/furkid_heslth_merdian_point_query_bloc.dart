import 'package:flutter_bloc/flutter_bloc.dart';
import 'furkid_heslth_merdian_point_query_event.dart';
import 'furkid_heslth_merdian_point_query_state.dart';

class FurKidHeslthMeridainPointBlocs extends Bloc<
    FurKidHeslthMeridainPointEvents, FurKidHeslthMeridainPointStates> {
  FurKidHeslthMeridainPointBlocs() : super(InitStates()) {
    on<OnMeridain>((event, emit) {
      emit(FurKidHeslthMeridainPointStates(
        state: true,
        onMeridainbuttonColor: 0xffe4007f,
        onPointbuttonColor: 0xff929292,
      ));
    });
    on<OnPoint>((event, emit) {
      emit(FurKidHeslthMeridainPointStates(
        state: false,
        onMeridainbuttonColor: 0xff929292,
        onPointbuttonColor: 0xffe4007f,
      ));
    });
  }
}

class FurKidHeslthPointBlocs
    extends Bloc<FurKidHeslthPointEvents, FurKidHeslthPointStates> {
  FurKidHeslthPointBlocs() : super(PointInitStates()) {
    on<OnPointBasicInformation>((event, emit) {
      emit(FurKidHeslthPointStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onDogButtonColor: 0xff929292,
        onCatButtonColor: 0xff929292,
      ));
    });
    on<OnPointDog>((event, emit) {
      emit(FurKidHeslthPointStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onDogButtonColor: 0xffe4007f,
        onCatButtonColor: 0xff929292,
      ));
    });
    on<OnPointCat>((event, emit) {
      emit(FurKidHeslthPointStates(
        state: 3,
        onBasicInformationButtonColor: 0xff929292,
        onDogButtonColor: 0xff929292,
        onCatButtonColor: 0xffe4007f,
      ));
    });
  }
}

class FurKidHeslthMeridianBlocs
    extends Bloc<FurKidHeslthMeridianEvents, FurKidHeslthMeridianStates> {
  FurKidHeslthMeridianBlocs() : super(MeridianInitStates()) {
    on<OnMeridianBasicInformation>((event, emit) {
      emit(FurKidHeslthMeridianStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onPointListButtonColor: 0xff929292,
        onDogButtonColor: 0xff929292,
        onCatButtonColor: 0xff929292,
      ));
    });
    on<OnMeridianPointList>((event, emit) {
      emit(FurKidHeslthMeridianStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onPointListButtonColor: 0xffe4007f,
        onDogButtonColor: 0xff929292,
        onCatButtonColor: 0xff929292,
      ));
    });
    on<OnMeridianDog>((event, emit) {
      emit(FurKidHeslthMeridianStates(
        state: 3,
        onBasicInformationButtonColor: 0xff929292,
        onPointListButtonColor: 0xff929292,
        onDogButtonColor: 0xffe4007f,
        onCatButtonColor: 0xff929292,
      ));
    });
    on<OnMeridianCat>((event, emit) {
      emit(FurKidHeslthMeridianStates(
        state: 4,
        onBasicInformationButtonColor: 0xff929292,
        onPointListButtonColor: 0xff929292,
        onDogButtonColor: 0xff929292,
        onCatButtonColor: 0xffe4007f,
      ));
    });
  }
}
