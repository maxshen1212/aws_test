import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/meridian_point_query/widgets/furkid_heslth_meridian_point_query_widgets.dart';

import '../../../../utils.dart';
import '../../../setup/setup.dart';
import 'bloc/furkid_heslth_merdian_point_query_bloc.dart';
import 'bloc/furkid_heslth_merdian_point_query_state.dart';

ScrollController controller = ScrollController();

class FurKidHeslthMeridainPoint extends StatefulWidget {
  const FurKidHeslthMeridainPoint({super.key});
  @override
  State<FurKidHeslthMeridainPoint> createState() =>
      _FurKidHeslthMeridainPointState();
}

class _FurKidHeslthMeridainPointState extends State<FurKidHeslthMeridainPoint> {
  Map<String, bool> buttonStates = {
    "手太陰肺經": true,
    "手陽明大腸經": true,
    "足厥陰肝經": true,
    "足少陽膽經": true,
    "足少陰腎經": true,
    "足太陽膀胱經": true,
    "手少陰心經": true,
    "手太陽小腸經": true,
    "足太陰脾經": true,
    "足陽明胃經": true,
    "手厥陰心包經": true,
    "手少陽三焦經": true,
    "任脈": true,
    "督脈": true,
    "特殊穴位": true,
  };
  bool meridianGotobody2 = false;
  Map meridianMap = {};
  bool pointGotobody2 = false;
  Map acupointMap = {};
  
  void refreshState() {
    setState(() {});
  }

  //小按鈕切換到穴位介紹頁面
  void _togglebody2(Map element) {
    setState(() {
      acupointMap = element;
      pointGotobody2 = !pointGotobody2;
    });
  }

  //穴位介紹按返回body1
  void _togglebody1() {
    setState(() {
      pointGotobody2 = !pointGotobody2;
    });
  }

  void _meridianToggleBody2(Map element) {
    setState(() {
      meridianMap = element;
      
      meridianGotobody2 = !meridianGotobody2;
      print(meridianGotobody2);
    });
  }

  //穴位介紹按返回body1
  void _meridianToggleBody1() {
    setState(() {
      meridianGotobody2 = !meridianGotobody2;
    });
  }

  void _toggleImage(String name) {
    setState(() {
      for (var key in buttonStates.keys) {
        if (key != name) buttonStates[key] = true;
      }
      buttonStates[name] = !buttonStates[name]!;
    });
  }

  void initState() {
    super.initState();
  }
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return ProgressHUD(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            drawerEdgeDragWidth: 220, //用拉的也能彈出設定
            endDrawer: Container(
              width: 75.w,
              child: const Drawer(
                child: SetUp(),
              ),
            ),
            appBar: furkidHeslthMeridianPoint(context, "經絡穴位查詢", pointGotobody2,
                meridianGotobody2, _togglebody1, _meridianToggleBody1),
            body: BlocBuilder<FurKidHeslthMeridainPointBlocs,
                FurKidHeslthMeridainPointStates>(builder: (context, state) {
              return BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
                      .state
                      .state
                  ? furkidMeridaButtonOn(meridianMap, context,
                      _meridianToggleBody2, meridianGotobody2, refreshState)
                  : furkidPointButtonOn(
                      buttonStates,
                      _toggleImage,
                      _togglebody2,
                      pointGotobody2,
                      acupointMap,
                      context,
                      refreshState);
            })));
  }
}

// ScrollController controllerView() {
//   return controller;
// }
