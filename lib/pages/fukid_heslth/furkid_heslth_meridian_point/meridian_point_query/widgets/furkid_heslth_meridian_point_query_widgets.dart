import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:path/path.dart';
import 'package:pet_save_app/common/values/colors.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/meridian_point_query/bloc/furkid_heslth_merdian_point_query_event.dart';
import 'package:sqflite/sqflite.dart';
import '../../../../../database/sqlite.dart';
import '../../../../../utils.dart';
import '../bloc/furkid_heslth_merdian_point_query_bloc.dart';
import '../bloc/furkid_heslth_merdian_point_query_state.dart';
import '../furkid_heslth_meridian_point_query.dart';

class SqliteMeridianAcuPoint {
  getSearchMeridian(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    print(db);
    final List<Map<String, dynamic>> searchMeridian = await db.rawQuery('''
        SELECT * FROM meridian 
        WHERE name LIKE "%$searchWord%"
    ''');
    return searchMeridian;
  }

  getSearchAcupointMeridian(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> searchAcupoint = await db.rawQuery('''
        SELECT * FROM acupoint
        WHERE name LIKE "%$searchWord%"
    ''');
    List meridian = [];
    for (int i = 0; i < searchAcupoint.length; i++) {
      final List<Map<String, dynamic>> searchMeridian = await db.rawQuery('''
        SELECT * FROM meridian
        WHERE ${searchAcupoint[i]['meridian_id']} == id
      ''');
      meridian.add(searchMeridian);
    }
    if (meridian.length == 0) {
      return meridian;
    } else {
      return meridian[0];
    }
  }

  getSearchAcupoint(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> searchAcupoint = await db.rawQuery('''
        SELECT * FROM acupoint
        WHERE name LIKE "%$searchWord%"
    ''');
    return searchAcupoint;
  }

  getAllMeridianImgDog(meridianId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianImgDog = await db.rawQuery('''
        SELECT meridian_img.img FROM meridian_img
        WHERE meridian_img.meridian_id == $meridianId AND meridian_img.type=="dog"
    ''');
    return meridianImgDog;
  }

  getAllMeridianImgCat(meridianId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianImgCat = await db.rawQuery('''
        SELECT meridian_img.img FROM meridian_img
        WHERE meridian_img.meridian_id == $meridianId AND meridian_img.type=="cat"
    ''');
    return meridianImgCat;
  }

  getAllAcupointImgDog(acupointId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupointImgDog = await db.rawQuery('''
        SELECT acupoint_img.img FROM acupoint_img
        WHERE acupoint_img.acupoint_id == $acupointId AND acupoint_img.type=="dog"
    ''');
    return acupointImgDog;
  }

  getAllAcupointImgCat(acupointId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupointImgCat = await db.rawQuery('''
        SELECT acupoint_img.img FROM acupoint_img
        WHERE acupoint_img.acupoint_id == $acupointId AND acupoint_img.type=="cat"
    ''');
    return acupointImgCat;
  }

  getAllMeridianQuery() async {
    final sqlite = Sqlite();
    final meridianListData = await sqlite.query('meridian');
    return meridianListData;
  }

  getAllAcupointQuery(meridianId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupointResult = await db.rawQuery('''
        SELECT * FROM acupoint
        WHERE meridian_id==$meridianId
      ''');
    return acupointResult;
  }
}

AppBar furkidHeslthMeridianPoint(
    BuildContext context,
    String names,
    bool furkidPointGotoBody2,
    bool furkidMeridianGotoBody2,
    Function furkidPointToggleBody1,
    Function furkidMeridianToggleBody1) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(furkidPointGotoBody2, furkidMeridianGotoBody2,
          furkidPointToggleBody1, furkidMeridianToggleBody1),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
        decoration: const BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            AppColors.primaryLogo,
            Colors.purple,
          ]),
    )),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

final TextEditingController searchTextMeridianAcupointcontroller = new TextEditingController();
Widget buildTextField(refreshState) {
  return Container(
    height: 40.h,
    width: 330.w,
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Container(
      margin: EdgeInsets.only(left: 10.w),
      child: BlocBuilder<FurKidHeslthMeridainPointBlocs,
          FurKidHeslthMeridainPointStates>(builder: (context, state) {
        return TextField(
          //autofocus: true,
          onChanged: (text) {
              refreshState();
          },
          controller: searchTextMeridianAcupointcontroller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: "文字輸入",
            isDense: true,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
          autocorrect: false,
          obscureText: false,
        );
      }),
    ),
  );
}

Widget goBack(bool furkidPointGotoBody2, bool furkidMeridianGotoBody2,
    Function furkidPointToggleBody1, Function furkidMeridianToggleBody1) {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
            .state
            .state) {
          if (furkidMeridianGotoBody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            furkidMeridianToggleBody1();
          }
        } else {
          if (furkidPointGotoBody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            furkidPointToggleBody1();
          }
        }
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

Widget furkidText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? '-',
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color(0xff000000),
      ),
    ),
  );
}

Widget furkidMeridianPointListText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? "",
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color.fromARGB(255, 42, 39, 39),
      ),
    ),
  );
}

Widget furkidMeridianButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
              .add(OnMeridain());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(10.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget furkidPointButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
              .add(OnPoint());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(10.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

//------------------經絡之頁面---------------------
Widget furkidMeridaButtonOn(name, BuildContext context,
    Function(Map) togglebody2, bool gotobody2, refreshState) {
  return gotobody2
      ? furkidMeridainBody2(name, context)
      : furkidMeridainBody1(togglebody2, refreshState);
}

//----------------------body1-------------------------
Widget furkidMeridainBody1(Function(Map) togglebody2, refreshState) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Container(
      margin: EdgeInsets.only(top: 20.h, right: 230.w),
      child: Text(
        '經絡穴位查詢',
        style: SafeGoogleFont(
          'Inter',
          fontSize: 16.sp,
          fontWeight: FontWeight.w200,
          height: 1.2125,
          letterSpacing: 0.8,
          color: Color(0xff000000),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField(refreshState),
    ),
    BlocBuilder<FurKidHeslthMeridainPointBlocs,
        FurKidHeslthMeridainPointStates>(builder: (context, state) {
      return SizedBox(
        width: 330.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                child: furkidMeridianButton(
                    context,
                    '經絡',
                    BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
                        .state
                        .onMeridainbuttonColor,
                    0xffffffff)),
            Container(
                child: furkidPointButton(
                    context,
                    '穴位',
                    BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
                        .state
                        .onPointbuttonColor,
                    0xffffffff)),
          ],
        ),
      );
    }),
    Column(children: [
      Center(
        child: Container(
          width: 330.w,
          margin: EdgeInsets.only(top: 20.h),
          child: Column(children: [
            Container(
              margin: EdgeInsets.only(bottom: 3.h),
              decoration: const BoxDecoration(
                border: Border(bottom: BorderSide(color: Color(0xff929292))),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: 5.w,
                  ),
                  furkidText("名稱"),
                  SizedBox(
                    width: 71.w,
                  ),
                  furkidText("編號"),
                  SizedBox(
                    width: 60.w,
                  ),
                  furkidText("四象"),
                  SizedBox(
                    width: 16.w,
                  ),
                  furkidText("五行"),
                  SizedBox(
                    width: 14.w,
                  ),
                  furkidText("季節"),
                ],
              ),
            ),
            Container(
              height: 520.h,
              child: FutureBuilder(
                  future: searchTextMeridianAcupointcontroller.text == ""
                      ? SqliteMeridianAcuPoint().getAllMeridianQuery()
                      : SqliteMeridianAcuPoint().getSearchMeridian(
                          searchTextMeridianAcupointcontroller.text),
                  builder: (context, snap) {
                    if (snap.hasData) {
                      List meridianListData = snap.data as List;
                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: meridianListData.length,
                          physics: AlwaysScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return furkidMeridianList(
                                meridianListData[index], togglebody2);
                          });
                    }
                    return Container();
                  }),
            )
          ]),
        ),
      ),
    ]),
  ]);
}

Widget furkidMeridianList(meridian, Function togglebody2) {
  return SizedBox(
    width: 330.w,
    child: TextButton(
      onPressed: () async {
        togglebody2({
          "id": meridian["id"],
          "meridian_sn": meridian["meridian_sn"],
          "sn": meridian["sn"],
          "name": meridian["name"],
          "five_elements": meridian["five_elements"],
          "solar_terms": meridian["solar_terms"],
          "season": meridian["season"],
          "times": meridian["times"],
          "back_shu_points": meridian["back_shu_points"],
          "acupoint": meridian["acupoint"],
          "description": meridian["description"],
          "position_description": meridian["position_description"],
          "memo": meridian["memo"],
        });
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        height: 42.h,
        decoration: BoxDecoration(
          color: Color(0xffececec),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 8.w,
            ),
            SizedBox(
              width: 100.w,
              child: Text(
                meridian["name"],
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w300,
                  letterSpacing: 0.7.r,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 18.w),
              width: 25.w,
              child: furkidText(meridian["code"]),
            ),
            Container(
              width: 25.w,
              margin: EdgeInsets.only(right: 18.w),
              child: furkidText(meridian["name_1"]),
            ),
            Container(
              margin: EdgeInsets.only(right: 10.w),
              width: 35.w,
              child: furkidText(meridian["name_2"]),
            ),
            Container(
              margin: EdgeInsets.only(right: 9.w),
              width: 35.w,
              child: furkidText(meridian["five_elements"]),
            ),
            Container(
              width: 35.w,
              child: furkidText(meridian["season"]),
            ),
          ],
        ),
      ),
    ),
  );
}
//----------------------body1-------------------------

//----------------------body2-------------------------
Widget furkidMeridainBody2(Map element, BuildContext context) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            padding: EdgeInsets.only(
                left: 10.w, right: 10.w, top: 30.h, bottom: 15.h),
            child: BlocBuilder<FurKidHeslthMeridianBlocs,
                FurKidHeslthMeridianStates>(builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  furkidMeridianBody2BasicInformationButton(
                      '基本資料',
                      context,
                      BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                          .state
                          .onBasicInformationButtonColor),
                  furkidMeridianBody2PointListButton(
                      '穴位列表',
                      context,
                      BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                          .state
                          .onPointListButtonColor),
                  furkidMeridianBody2DogButton(
                      '圖示(狗)',
                      context,
                      BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                          .state
                          .onDogButtonColor),
                  furkidMeridianBody2CatButton(
                      '圖示(貓)',
                      context,
                      BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                          .state
                          .onCatButtonColor),
                ],
              );
            })),
        BlocBuilder<FurKidHeslthMeridianBlocs, FurKidHeslthMeridianStates>(
            builder: (context, state) {
          if (BlocProvider.of<FurKidHeslthMeridianBlocs>(context).state.state ==
              1) {
            return furkidMeridianBasicInformationView(element);
          } else if (BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                  .state
                  .state ==
              2) {
            return furkidMeridianPointListView(element);
          } else if (BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
                  .state
                  .state ==
              3) {
            return Container(child: furkidMeridianDogList(element));
          } else {
            return furkidMeridianCatList(element);
          }
        })
      ],
    ),
  );
}

Widget furkidMeridianBasicInformationView(Map element) {
  return Container(
    child: Column(
      children: [
        furkidMeridianElement(
            element["meridian_sn"], element["sn"], element["name"]),
        furkidMeridianElement2(element["five_elements"], element["solar_terms"],
            element["season"], element["times"]),
        furkidMeridianElement3(element["back_shu_points"], element["acupoint"]),
        furkidMeridianElement4("內容", element["description"]),
        furkidMeridianElement4("巡行位置", element["position_description"]),
        furkidMeridianElement4("備註", element["memo"]),
        SizedBox(
          height: 20.h,
        ),
      ],
    ),
  );
}

Widget furkidMeridianPointList(acupointListData, index) {
  return Column(children: [
    Container(
      width: 320.w,
      margin: EdgeInsets.only(bottom: 5.h, top: 5.h),
      decoration: const BoxDecoration(
        border: Border(
            bottom: BorderSide(color: Color.fromARGB(255, 220, 210, 210))),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 10.w),
            width: 30.w,
            child: Text("$index"),
          ),
          Container(
            margin: EdgeInsets.only(left: 3.w),
            width: 60.w,
            child: furkidMeridianPointListText(acupointListData["name"]),
          ),
          Container(
            margin: EdgeInsets.only(left: 8.w),
            width: 40.w,
            child:
                furkidMeridianPointListText(acupointListData["five_elements"]),
          ),
          Container(
            margin: EdgeInsets.only(left: 20.w),
            width: 40.w,
            child: furkidMeridianPointListText(
                acupointListData["five_shu_points"]),
          ),
          Container(
            margin: EdgeInsets.only(left: 12.w),
            width: 50.w,
            child: furkidMeridianPointListText(acupointListData["category"]),
          ),
          Container(
            width: 45.w,
            child: furkidMeridianPointListText(acupointListData["nick_name"]),
          )
        ],
      ),
    ),
  ]);
}

Widget furkidMeridianPointListView(element) {
  return Column(
    children: [
      Container(
        width: 320.w,
        margin: EdgeInsets.only(bottom: 3.h),
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xff929292))),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: furkidText("序號"),
            ),
            Container(
              margin: EdgeInsets.only(left: 28.w),
              child: furkidText("名稱"),
            ),
            Container(
              margin: EdgeInsets.only(left: 24.w),
              child: furkidText("五行"),
            ),
            Container(
              margin: EdgeInsets.only(left: 30.w),
              child: furkidText("五俞"),
            ),
            Container(
              margin: EdgeInsets.only(left: 30.w),
              child: furkidText("穴別"),
            ),
            Container(
              margin: EdgeInsets.only(left: 20.w),
              child: furkidText("別名"),
            ),
          ],
        ),
      ),
      FutureBuilder(
          future: SqliteMeridianAcuPoint().getAllAcupointQuery(element["id"]),
          builder: (context, snap) {
            if (snap.hasData) {
              List acupointListData = snap.data as List;
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: acupointListData.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return furkidMeridianPointList(
                        acupointListData[index], index + 1);
                  });
            }
            return Container();
          })
    ],
  );
}

Widget furkidMeridianDogList(element) {
  return FutureBuilder(
      future: SqliteMeridianAcuPoint().getAllMeridianImgDog(element["id"]),
      builder: (context, snap) {
        if (snap.hasData) {
          List imageDogListData = snap.data as List;
          return ListView.builder(
              shrinkWrap: true,
              itemCount: imageDogListData.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return furkidMeridianBody2DogImageListView(
                    imageDogListData, index);
              });
        }
        return Container();
      });
}

Widget furkidMeridianCatList(element) {
  return FutureBuilder(
      future: SqliteMeridianAcuPoint().getAllMeridianImgCat(element["id"]),
      builder: (context, snap) {
        if (snap.hasData) {
          List imageCatListData = snap.data as List;
          return ListView.builder(
              shrinkWrap: true,
              itemCount: imageCatListData.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return furkidMeridianBody2CatImageListView(
                    imageCatListData, index);
              });
        }
        return Container();
      });
}

Widget furkidMeridianBody2BasicInformationButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
            .add(OnMeridianBasicInformation());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMeridianBody2PointListButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
            .add(OnMeridianPointList());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMeridianBody2DogButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
            .add(OnMeridianDog());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMeridianBody2CatButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMeridianBlocs>(context)
            .add(OnMeridianCat());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMeridianBody2CatImageListView(imageCatListData, index) {
  final imageCatList = imageCatListData[index]["img"];
  int ind = imageCatList.indexOf(".");
  String catData = imageCatList.substring(0, ind);
  return Column(children: [
    Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 15.h),
        width: 320.w,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(5),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Container(
            child: Image.asset("assets/meridian_img/cat/" + catData + ".jpg",
                fit: BoxFit.fill))),
  ]);
}

Widget furkidMeridianBody2DogImageListView(imageDogListData, index) {
  final imageDogList = imageDogListData[index]["img"];
  int ind = imageDogList.indexOf(".");
  String dogData = imageDogList.substring(0, ind);
  return Column(
    children: [
      Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(bottom: 15.h),
          width: 320.w,
          decoration: BoxDecoration(
            color: Color(0xffffffff),
            borderRadius: BorderRadius.circular(5),
            boxShadow: const [
              BoxShadow(
                color: Color(0x3f000000),
                offset: Offset(0, 0),
                blurRadius: 5,
              ),
            ],
          ),
          child: Container(
              child: Image.asset("assets/meridian_img/dog/" + dogData + ".jpg",
                  fit: BoxFit.fill))),
    ],
  );
}

Widget furkidMeridianElement(
  dynamic unit1,
  dynamic unit2,
  dynamic unit3,
) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              Container(
                width: 70.w,
                child: Text(
                  "經路代碼",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 14,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.7,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                width: 60.w,
                child: Text("$unit1",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.6.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    )),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                width: 70.w,
                child: Text(
                  "編號",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 14,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.7,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                width: 60.w,
                child: Text("$unit2",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.6.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    )),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                width: 70.w,
                child: Text(
                  "經絡名稱",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 14,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.7,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                width: 90.w,
                child: Text("$unit3",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.6.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    )),
              ),
            ],
          ),
        ],
      ),
      Container(
        margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 7.h, bottom: 5.h),
        height: 1.h,
        color: Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget furkidMeridianElement2(
  dynamic unit1,
  dynamic unit2,
  dynamic unit3,
  dynamic unit4,
) {
  return Column(
    children: [
      Container(
        padding: EdgeInsets.only(left: 18.w, right: 27.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                SizedBox(
                  width: 70.w,
                  child: Text(
                    "五行",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                SizedBox(
                  width: 60.w,
                  child: Text(unit1 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 70.w,
                  child: Text(
                    "節氣",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                SizedBox(
                  width: 60.w,
                  child: Text(unit2 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 70.w,
                  child: Text(
                    "季節",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                SizedBox(
                  width: 85.w,
                  child: Text(unit3 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 70.w,
                  child: Text(
                    "時辰",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                SizedBox(
                  width: 60.w,
                  child: Text(unit4 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 7.h, bottom: 5.h),
        height: 1.h,
        color: Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget furkidMeridianElement3(
  dynamic unit1,
  dynamic unit2,
) {
  return Column(
    children: [
      Container(
        margin: EdgeInsets.only(left: 20.w, right: 30.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Container(
                  width: 80.w,
                  child: Text(
                    "背俞穴",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 60.w,
                  child: Text(unit1 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            // SizedBox(width: 40.w,),
            Column(
              children: [
                Container(
                  width: 75.w,
                  child: Text(
                    "募穴",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 60.w,
                  child: Text(unit2 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.2125.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 7.h, bottom: 5.h),
        height: 1.h,
        color: Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget furkidMeridianElement4(
  dynamic name1,
  dynamic unit1,
) {
  return Container(
    margin: EdgeInsets.only(left: 20.w, right: 20.w),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10.w),
          child: Text(
            name1,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14,
              fontWeight: FontWeight.w200,
              height: 1.2125.h,
              letterSpacing: 0.7,
              color: Color(0xff000000),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.w, top: 5.h),
          child: Text(unit1 ?? "無",
              textAlign: TextAlign.start,
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14,
                fontWeight: FontWeight.w200,
                height: 1.2125.h,
                letterSpacing: 0.7,
                color: Color.fromARGB(255, 52, 48, 48),
              )),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h, bottom: 5.h),
          // width: 200.w,
          height: 1.h,
          color: Color.fromARGB(157, 207, 206, 206),
        )
      ],
    ),
  );
}
//----------------------body2-------------------------

//------------------穴位之頁面---------------------
Widget furkidPointButtonOn(
    Map<String, bool> buttonStates,
    Function(String) toggleImage,
    Function(Map) togglebody2,
    bool gotobody2,
    name,
    BuildContext context,
    refreshState) {
  return gotobody2
      ? furkidPointBody2(name, context)
      : furkidPointBody1(
          buttonStates, toggleImage, togglebody2, context, refreshState);
}

// class MySlideTransition extends AnimatedWidget {
//   const MySlideTransition({
//     Key? key,
//     required Animation<Offset> position,
//     this.transformHitTests = true,
//     required this.child,
//   }) : super(key: key, listenable: position);

//   final bool transformHitTests;

//   final Widget child;

//   @override
//   Widget build(BuildContext context) {
//     final position = listenable as Animation<Offset>;
//     Offset offset = position.value;
//     if (position.status == AnimationStatus.reverse) {
//       offset = Offset(-offset.dx, offset.dy);
//     }
//     return FractionalTranslation(
//       translation: offset,
//       transformHitTests: transformHitTests,
//       child: child,
//     );
//   }
// }

//----------------------body1-------------------------
Widget furkidPointBody1(
    Map<String, bool> buttonStates,
    Function(String) toggleImage,
    Function(Map) togglebody2,
    BuildContext context,
    refreshState) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Center(
      child: Container(
        margin: EdgeInsets.only(top: 20.h, right: 230.w),
        child: Text(
          '經絡穴位查詢',
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125,
            letterSpacing: 0.8,
            color: Color(0xff000000),
          ),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField(refreshState),
    ),
    Column(children: [
      BlocBuilder<FurKidHeslthMeridainPointBlocs,
          FurKidHeslthMeridainPointStates>(builder: (context, state) {
        return SizedBox(
          width: 330.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: furkidMeridianButton(
                      context,
                      '經絡',
                      BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
                          .state
                          .onMeridainbuttonColor,
                      0xffffffff)),
              Container(
                  child: furkidPointButton(
                      context,
                      '穴位',
                      BlocProvider.of<FurKidHeslthMeridainPointBlocs>(context)
                          .state
                          .onPointbuttonColor,
                      0xffffffff)),
            ],
          ),
        );
      }),
      Container(
        margin: EdgeInsets.only(top: 10.h),
        height: 560.h,
        child: FutureBuilder(
            future: searchTextMeridianAcupointcontroller.text == ""
                ? SqliteMeridianAcuPoint().getAllMeridianQuery()
                : SqliteMeridianAcuPoint().getSearchAcupointMeridian(
                    searchTextMeridianAcupointcontroller.text),
            builder: (context, snap) {
              if (snap.hasData) {
                List meridianListData = snap.data as List;
                return ListView.builder(
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    itemCount: meridianListData.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          if (searchTextMeridianAcupointcontroller.text ==
                              "") ...[
                            furkidPointList(meridianListData[index], context,
                                () {
                              toggleImage(meridianListData[index]["name"]);
                            }, buttonStates[meridianListData[index]["name"]]),
                            (buttonStates[meridianListData[index]["name"]] ==
                                    false)
                                ? pressList(
                                    meridianListData[index], togglebody2, index)
                                : SizedBox()
                          ] else ...[
                            pressList(
                                meridianListData[index], togglebody2, index),
                          ]
                        ],
                      );
                    });
              }
              return Container();
            }),
      )
    ]),
  ]);
}

Widget furkidPoint() {
  return Container(
    width: 320.w,
    margin: EdgeInsets.only(top: 10.h, bottom: 5.h),
    decoration: const BoxDecoration(
      border: Border(bottom: BorderSide(color: Color(0xff929292))),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        furkidText("名稱"),
        SizedBox(),
        furkidText("別名"),
        SizedBox(),
        furkidText("對應穴位"),
        SizedBox(),
        furkidText("經絡代碼"),
        SizedBox(),
        furkidText("部位"),
      ],
    ),
  );
}

Widget furkidPointList(meridian, BuildContext context, Null Function() param2,
    bool? buttonStates) {
  return SizedBox(
    width: 330.w,
    child: TextButton(
      onPressed: () async {
        param2();
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        height: 42.h,
        decoration: BoxDecoration(
          color: Color(0xffececec),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          SizedBox(
            width: 8.w,
          ),
          SizedBox(
            width: 100.w,
            child: Text(
              meridian["name"],
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14.sp,
                fontWeight: FontWeight.w300,
                letterSpacing: 0.7.r,
                color: Color(0xff000000),
              ),
            ),
          ),
          Container(
            width: 20.w,
            margin: EdgeInsets.only(left: 190.h),
            child: Text(
              ">",
              style: TextStyle(fontSize: 18.sp, color: Colors.black),
            ),
          )
        ]),
      ),
    ),
  );
}

Widget pressList(meridian, Function togglebody2, allIndex) {
  return SizedBox(
    child: Column(children: [
      furkidPoint(),
      Container(
        width: 320.w,
        child: FutureBuilder(
            future: searchTextMeridianAcupointcontroller.text == ""
                ? SqliteMeridianAcuPoint().getAllAcupointQuery(meridian["id"])
                : SqliteMeridianAcuPoint().getSearchAcupoint(
                    searchTextMeridianAcupointcontroller.text),
            builder: (context, snap) {
              if (snap.hasData) {
                List acupointListData = snap.data as List;
                return ListView.builder(
                    shrinkWrap: true,
                    itemCount: acupointListData.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return noName(
                          acupointListData, index, meridian, togglebody2);
                    });
              }
              return Container();
            }),
      )
    ]),
  );
}

Widget noName(pointList, index, meridian, Function togglebody2) {
  
  return GestureDetector(
    onTap: () {
      togglebody2({
        "id": pointList[index]["id"],
        "meridian_name": meridian["name"],
        "meridian_code": meridian["code"],
        "acupoint_name": pointList[index]["name"],
        "acupoint_code": meridian["code"] + " ${index + 1}",
        "deepth": pointList[index]["deepth"],
        "sn": pointList[index]["sn"],
        "relative_acupoint": pointList[index]["relative_acupoint"],
        "category": pointList[index]["category"],
        "heavenly_stems": pointList[index]["heavenly_stems"],
        "five_elements": pointList[index]["five_elements"],
        "five_shu_points": pointList[index]["five_shu_points"],
        "other_name": pointList[index]["other_name"],
        "function": pointList[index]["function"],
        "sickness": pointList[index]["sickness"],
        "pathogenesis": pointList[index]["pathogenesis"],
        "diagnosis": pointList[index]["diagnosis"],
        "treatment": pointList[index]["treatment"],
        "memo": pointList[index]["memo"],
      });
    },
    child: Container(
      margin: EdgeInsets.only(bottom: 6.h),
      height: 34.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8.w)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1)
        ],
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            margin: EdgeInsets.only(left: 7.5.w),
            width: 52.w,
            child: Text(pointList[index]["name"] ?? "",
                style: TextStyle(
                  color: Color(0xFF595757),
                  fontSize: 12.sp,
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w300,
                  letterSpacing: 1,
                )),
          ),
          Container(
            margin: EdgeInsets.only(left: 1.w),
            width: 46.w,
            child: Text(pointList[index]["nick_name"] ?? "",
                style: TextStyle(
                  color: Color(0xFF595757),
                  fontSize: 12.sp,
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w300,
                  letterSpacing: 1,
                )),
          ),
          Container(
            margin: EdgeInsets.only(left: 11.w),
            width: 55.w,
            child: Center(
              child: Text(pointList[index]["relative_acupoint"] ?? "",
                  style: TextStyle(
                    color: Color(0xFF595757),
                    fontSize: 12.sp,
                    fontFamily: 'Inter',
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1,
                  )),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 28.w),
            width: 55.w,
            child: Center(
              child: Text(meridian["code"] + "${index + 1}",
                  style: TextStyle(
                    color: Color(0xFF595757),
                    fontSize: 12.sp,
                    fontFamily: 'Inter',
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1,
                  )),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 22.w),
            width: 40.w,
            child: Center(
              child: Text(pointList[index]["parts"] ?? "",
                  style: TextStyle(
                    color: Color(0xFF595757),
                    fontSize: 12.sp,
                    fontFamily: 'Inter',
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1,
                  )),
            ),
          ),
        ],
      ),
    ),
  );
}
//----------------------body1-------------------------

//----------------------body2-------------------------
Widget furkidPointBody2BasicInformationButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthPointBlocs>(context)
            .add(OnPointBasicInformation());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidPointBody2DogButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthPointBlocs>(context).add(OnPointDog());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidPointBody2CatButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthPointBlocs>(context).add(OnPointCat());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidPointBody2(Map element, BuildContext context) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            padding: EdgeInsets.only(
                left: 35.w, right: 35.w, top: 30.h, bottom: 15.h),
            child: BlocBuilder<FurKidHeslthPointBlocs, FurKidHeslthPointStates>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  furkidPointBody2BasicInformationButton(
                      '基本資料',
                      context,
                      BlocProvider.of<FurKidHeslthPointBlocs>(context)
                          .state
                          .onBasicInformationButtonColor),
                  furkidPointBody2DogButton(
                      '圖示(狗)',
                      context,
                      BlocProvider.of<FurKidHeslthPointBlocs>(context)
                          .state
                          .onDogButtonColor),
                  furkidPointBody2CatButton(
                      '圖示(貓)',
                      context,
                      BlocProvider.of<FurKidHeslthPointBlocs>(context)
                          .state
                          .onCatButtonColor),
                ],
              );
            })),
        BlocBuilder<FurKidHeslthPointBlocs, FurKidHeslthPointStates>(
            builder: (context, state) {
          if (BlocProvider.of<FurKidHeslthPointBlocs>(context).state.state ==
              1) {
            return furkidPointBasicInformationView(element);
          } else if (BlocProvider.of<FurKidHeslthPointBlocs>(context)
                  .state
                  .state ==
              2) {
            return Container(child: furkidPointDogView(element));
          } else {
            return furkidPointCatView(element);
          }
        })
      ],
    ),
  );
}

Widget furkidPointBasicInformationView(element) {
  return Container(
    child: Column(
      children: [
        furkidPointElement("穴位名稱", element['acupoint_name'], "穴位代碼",
            element['acupoint_code'], "穴位深度", element['deepth']),
        furkidPointElement("經絡名稱", element['meridian_name'], "經絡代碼",
            element['meridian_code'], "穴位編號", "${element['sn']}"),
        furkidPointElement("對應穴位", element['relative_acupoint'], "部位",
            element['parts'], "穴別", element['category']),
        furkidPointElement("天干", element['heavenly_stems'], "五行",
            element['five_elements'], "五俞", element['five_shu_points']),
        furkidPointElement2("別名/其他名稱", element['other_name']),
        furkidPointElement2("穴位功能", element['function']),
        furkidPointElement2("疾病", element['sickness']),
        furkidPointElement2("病機", element['pathogenesis']),
        furkidPointElement2("診斷", element['diagnosis']),
        furkidPointElement2("處置", element['treatment']),
        furkidPointElement2("備註", element['memo']),
        SizedBox(
          height: 20.h,
        ),
      ],
    ),
  );
}

Widget furkidPointDogView(element) {
  return FutureBuilder(
      future: SqliteMeridianAcuPoint().getAllAcupointImgDog(element["id"]),
      builder: (context, snap) {
        if (snap.hasData) {
          List imageDogListData = snap.data as List;
          return ListView.builder(
              shrinkWrap: true,
              itemCount: imageDogListData.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return furkidAcupointBody2DogImageListView(
                    imageDogListData, index);
              });
        }
        return Container();
      });
}

Widget furkidPointCatView(element) {
   return FutureBuilder(
      future: SqliteMeridianAcuPoint().getAllAcupointImgCat(element["id"]),
      builder: (context, snap) {
        if (snap.hasData) {
          List imageCatListData = snap.data as List;
          return ListView.builder(
              shrinkWrap: true,
              itemCount: imageCatListData.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return furkidAcupointBody2CatImageListView(
                    imageCatListData, index);
              });
        }
        return Container();
      });
}

Widget furkidAcupointBody2CatImageListView(imageCatListData, index) {
  final imageCatList = imageCatListData[index]["img"];
  int ind = imageCatList.indexOf(".");
  String catData = imageCatList.substring(0, ind);
  return Column(children: [
    Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 15.h),
        width: 320.w,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(5),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Container(
            child: Image.asset("assets/acupoint_img/" + catData + ".jpg",
                fit: BoxFit.fill))),
  ]);
}

Widget furkidAcupointBody2DogImageListView(imageDogListData, index) {
  final imageDogList = imageDogListData[index]["img"];
  int ind = imageDogList.indexOf(".");
  String dogData = imageDogList.substring(0, ind);
  return Column(
    children: [
      Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(bottom: 15.h),
          width: 320.w,
          decoration: BoxDecoration(
            color: Color(0xffffffff),
            borderRadius: BorderRadius.circular(5),
            boxShadow: const [
              BoxShadow(
                color: Color(0x3f000000),
                offset: Offset(0, 0),
                blurRadius: 5,
              ),
            ],
          ),
          child: Container(
              child: Image.asset("assets/acupoint_img/" + dogData + ".jpg",
                  fit: BoxFit.fill))),
    ],
  );
}

Widget furkidPointElement(
  name1,
  unit1,
  name2,
  unit2,
  name3,
  unit3,
) {
  return Column(
    children: [
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Container(
                  width: 70.w,
                  child: Text(
                    name1,
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 90.w,
                  child: Text(unit1 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  width: 70.w,
                  child: Text(
                    name2,
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 60.w,
                  child: Text(unit2 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  width: 70.w,
                  child: Text(
                    name3,
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 14,
                      fontWeight: FontWeight.w200,
                      height: 1.2125.h,
                      letterSpacing: 0.7,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
                Container(
                  width: 60.w,
                  child: Text(unit3 ?? "-",
                      textAlign: TextAlign.center,
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w200,
                        height: 1.6.h,
                        letterSpacing: 0.7,
                        color: Color(0xff000000),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 7.h, bottom: 5.h),
        height: 1.h,
        color: Color.fromARGB(157, 207, 206, 206),
      )
    ],
  );
}

Widget furkidPointElement2(
  name1,
  unit1,
) {
  return Container(
    margin: EdgeInsets.only(left: 32.w, right: 32.w),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name1,
          // textAlign: TextAlign.start,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 14,
            fontWeight: FontWeight.w200,
            height: 1.2125.h,
            letterSpacing: 0.7,
            color: Color(0xff000000),
          ),
        ),
        Text(unit1 ?? "無",
            textAlign: TextAlign.start,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14,
              fontWeight: FontWeight.w200,
              height: 1.6.h,
              letterSpacing: 0.7,
              color: Color(0xff000000),
            )),
        Container(
          margin: EdgeInsets.only(top: 7.h, bottom: 5.h),
          height: 1.h,
          color: Color.fromARGB(157, 207, 206, 206),
        )
      ],
    ),
  );
}

Widget furkidPointBody2Image() {
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    width: 320.w,
    height: 260.h,
    decoration: BoxDecoration(
      color: Color(0xffffffff),
      borderRadius: BorderRadius.circular(5),
      boxShadow: const [
        BoxShadow(
          color: Color(0x3f000000),
          offset: Offset(0, 0),
          blurRadius: 5,
        ),
      ],
    ),
  );
}
//----------------------body2-------------------------
