import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_point_matching_event.dart';
import 'my_point_matching_state.dart';

class FurKidHeslthMyPointMatchingBlocs extends Bloc<
    FurKidHeslthMyPointMatchingEvents, FurKidHeslthMyPointMatchingStates> {
  FurKidHeslthMyPointMatchingBlocs() : super(InitStates()) {
    on<OnNotFavorite>((event, emit) {
      emit(FurKidHeslthMyPointMatchingStates(
        state: true,
        onNotFavoriteButtonColor: 0xffe4007f,
        onFavoriteButtonColor: 0xff929292,
      ));
    });
    on<OnFavorite>((event, emit) {
      emit(FurKidHeslthMyPointMatchingStates(
        state: false,
        onNotFavoriteButtonColor: 0xff929292,
        onFavoriteButtonColor: 0xffe4007f,
      ));
    });
  }
}


class FurKidHeslthMyPointMatchinNotFavoriteBlocs
    extends Bloc<FurKidHeslthMyPointMatchinNotFavoriteEvents, FurKidHeslthMyPointMatchinNotFavoriteStates> {
  FurKidHeslthMyPointMatchinNotFavoriteBlocs() : super(MyPointMatchinNotFavoriteInitStates()) {
    
    on<OnNotFavoriteBasicInformation>((event, emit) {
      emit(FurKidHeslthMyPointMatchinNotFavoriteStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onAcupointButtonColor: 0xff929292,

      ));
    });
    on<OnNotFavoriteAcuPointData>((event, emit) {
      emit(FurKidHeslthMyPointMatchinNotFavoriteStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onAcupointButtonColor: 0xffe4007f,
      ));
    });
  }
}

class FurKidHeslthMyPointMatchinFavoriteBlocs
    extends Bloc<FurKidHeslthMyPointMatchinFavoriteEvents, FurKidHeslthMyPointMatchinFavoriteStates> {
  FurKidHeslthMyPointMatchinFavoriteBlocs() : super(MyPointMatchinFavoriteInitStates()) {
    
    on<OnFavoriteBasicInformation>((event, emit) {
      emit(FurKidHeslthMyPointMatchinFavoriteStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onAcupointButtonColor: 0xff929292,
        onMatchingAcupointRecordButtonColor:0xff929292,

      ));
    });
    on<OnFavoriteAcuPointData>((event, emit) {
      emit(FurKidHeslthMyPointMatchinFavoriteStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onAcupointButtonColor: 0xffe4007f,
        onMatchingAcupointRecordButtonColor:0xff929292,
      ));
    });
    on<OnFavoriteMatchingAcuPointRecordData>((event, emit) {
      emit(FurKidHeslthMyPointMatchinFavoriteStates(
        state: 3,
        onBasicInformationButtonColor: 0xff929292,
        onAcupointButtonColor: 0xff929292,
        onMatchingAcupointRecordButtonColor:0xffe4007f,
      ));
    });
  }
}