import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/my_point_matching/widgets/my_point_matching_widgets.dart';
import '../../../setup/setup.dart';
import 'bloc/my_point_matching_bloc.dart';
import 'bloc/my_point_matching_state.dart';

class FurkidMyPointMatching extends StatefulWidget {
  FurkidMyPointMatching({Key? key, required this.matchingAcupintNotFavorite,this.matchingAcupintFavorite}) : super(key: key);
  final matchingAcupintNotFavorite;
  final matchingAcupintFavorite;
  @override
  State<FurkidMyPointMatching> createState() => _FurkidMyPointMatchingState();
}

class _FurkidMyPointMatchingState extends State<FurkidMyPointMatching> {

  bool favoriteGotobody2 = false;
  dynamic favoriteBasicDataMap ;
  dynamic favoriteAcupointDataMap ;
  bool notFavoriteGotobody2 = false;
  dynamic notFavoriteBasicDataMap ;
  dynamic notFavoriteAcupointDataMap ;
  void toFavoritebody2(basicData,acupointDataMap) {
    setState(() {
      favoriteGotobody2 = !favoriteGotobody2;
      favoriteBasicDataMap=basicData;
      favoriteAcupointDataMap=acupointDataMap;
    });
  }

  void toFavoritebody1() {
    setState(() {
      favoriteGotobody2 = !favoriteGotobody2;
    });
  }

   void toNotFavoritebody2(basicData,acupointDataMap) {
    setState(() {
      notFavoriteGotobody2 = !notFavoriteGotobody2;
      notFavoriteBasicDataMap=basicData;
      notFavoriteAcupointDataMap=acupointDataMap;
    });
  }

  void toNotFavoritebody1() {
    setState(() {
      notFavoriteGotobody2 = !notFavoriteGotobody2;
    });
  }
  void refreshstate() {
    setState(() {});
    // change your state to refresh the screen
  }
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        drawerEdgeDragWidth: 220, //用拉的也能彈出設定
        endDrawer: Container(
          width: 75.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        appBar: furkidHeslthMyPointMatching(context, "我的配穴",notFavoriteGotobody2,favoriteGotobody2,toFavoritebody1,toNotFavoritebody1),
        body: BlocBuilder<FurKidHeslthMyPointMatchingBlocs,
            FurKidHeslthMyPointMatchingStates>(builder: (context, state) {
          return BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
                  .state
                  .state
              ? furkidMyPointMatchingNotFavoriteOn(
                  context,
                  toNotFavoritebody2,
                  notFavoriteGotobody2,
                  notFavoriteBasicDataMap,
                  notFavoriteAcupointDataMap,
                  refreshstate
                )
              : furkidMyPointMatchingFavoriteOn(
                context,
                toFavoritebody2,
                favoriteGotobody2,
                favoriteBasicDataMap,
                favoriteAcupointDataMap,
                refreshstate
              );
        }));
  }
}
