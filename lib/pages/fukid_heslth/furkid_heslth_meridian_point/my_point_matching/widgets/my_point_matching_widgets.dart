import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../../common/values/colors.dart';
import '../../../../../database/sqlite.dart';
import '../../../../../utils.dart';
import '../../edit_matching_acupoint/edit_matching_acupoint.dart';
import '../bloc/my_point_matching_bloc.dart';
import '../bloc/my_point_matching_event.dart';
import '../bloc/my_point_matching_state.dart';

class SqliteEditMatchingAcupoint {
  getAllMatchingPointData(matchingAcupointId) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    final List<Map<String, dynamic>> matchingPointResult = await db.rawQuery('''
      SELECT * FROM matching_acupoint
       WHERE matching_acupoint.id == $matchingAcupointId;
    ''');
    print(matchingPointResult);
    return matchingPointResult;
  }
}

class SqliteMyMatchingAcupoint {
  insertMatchingAcupointData(table, Map<String, dynamic> data) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianResult = await db.rawQuery('''
      select id from matching_acupoint order by id desc limit 1;
    ''');
    if (meridianResult.length != 0) {
      data['id'] = meridianResult[0]['id'] + 1;
    } else {
      data['id'] = 1;
    }
    await sqlite.insert(table, data);
    return data['id'];
  }

  insertPetMatchingAcupointRecordData(Map<String, dynamic> data) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> meridianResult = await db.rawQuery('''
      select id from pet_acupoint_record order by id desc limit 1;
    ''');
    if (meridianResult.length != 0) {
      data['id'] = meridianResult[0]['id'] + 1;
    } else {
      data['id'] = 1;
    }
    await sqlite.insert("pet_acupoint_record", data);
  }

  getSearchMatchingAcupointFavorite(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    final List<Map<String, dynamic>> matchingAcupoint = await db.rawQuery('''
      SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where name LIKE "%$searchWord%" AND matching_acupoint_collect.status==0 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');
    return matchingAcupoint;
  }

  getSearchMatchingAcupointNotFavorite(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();

    final List<Map<String, dynamic>> matchingAcupoint = await db.rawQuery('''
      SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where name LIKE "%$searchWord%" AND matching_acupoint_collect.status==1 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');
    return matchingAcupoint;
  }

  getAllMatchingAcupointNotFavoriteData() async {
    Database? db;
    final sqlite = Sqlite();

    db = await sqlite.connectDB();
    print(db);
    final List<Map<String, dynamic>> matchingAcupointNotCollect =
        await db.rawQuery('''
      SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where matching_acupoint_collect.status==1 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');

    return matchingAcupointNotCollect;
  }

  getAllMatchingAcupointFavoriteData() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> matchingAcupointNotCollect =
        await db.rawQuery('''
      SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where matching_acupoint_collect.status==0 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');
    return matchingAcupointNotCollect;
  }

  updateMatchingAcupointCollectStatus(status, id) async {
    Database? db;
    final sqlite = Sqlite();
    //print(status);
    print(id);
    db = await sqlite.connectDB();
    await db.rawUpdate('''
    UPDATE matching_acupoint_collect
    SET status=?
    WHERE matching_acupoint_id = ?
    ''', [status, id]);
  }

  getMatchingAcupointList(matching_acupoint_id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupoint = await db.rawQuery('''
      SELECT acupoint.id,acupoint.name,acupoint.sn,meridian.name as meridian_name,meridian.code
      FROM matching_acupoint_list
      LEFT JOIN acupoint,meridian
      WHERE matching_acupoint_list.matching_acupoint_id==$matching_acupoint_id AND acupoint.id=matching_acupoint_list.acupoint_id And acupoint.meridian_id=meridian.id;
    ''');
    return acupoint;
  }

  getPetMatchingAcupointRecordList(matching_acupoint_id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> petMatchingAcupointRecord =
        await db.rawQuery('''
      SELECT pet_acupoint_record.matching_acupoint_id,pet.name,pet_acupoint_record.time,pet_acupoint_record.date FROM pet_acupoint_record
      LEFT JOIN pet
      WHERE pet_acupoint_record.matching_acupoint_id==$matching_acupoint_id AND pet.id==pet_acupoint_record.pet_id;
    ''');
    return petMatchingAcupointRecord;
  }

  getPetList() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> petInfo = await db.rawQuery('''
      SELECT pet.id,pet.name FROM pet;
    ''');
    List<String> petDropdownList = [];

    for (var pet in petInfo) {
      petDropdownList.add(pet["name"]);
    }
    return petDropdownList;
  }

  deleteMatchingAcupointData(id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    await db.rawDelete('DELETE FROM matching_acupoint WHERE id = ?', [id]);
    await db.rawDelete(
        'DELETE FROM matching_acupoint_collect WHERE matching_acupoint_id = ?',
        [id]);
    await db.rawDelete(
        'DELETE FROM matching_acupoint_list WHERE matching_acupoint_id = ?',
        [id]);
  }
}

AppBar furkidHeslthMyPointMatching(
    BuildContext context,
    String names,
    bool notFavoriteGotobody2,
    bool favoriteGotobody2,
    Function toFavoritebody1,
    Function toNotFavoritebody1) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(notFavoriteGotobody2, favoriteGotobody2, toNotFavoritebody1,
          toFavoritebody1),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
        decoration: const BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            AppColors.primaryLogo,
            Colors.purple,
          ]),
    )),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget furkidText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? '-',
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color(0xff000000),
      ),
    ),
  );
}

Widget goBack(bool notFavoriteGotobody2, bool favoriteGotobody2,
    Function toNotFavoritebody1, Function toFavoritebody1) {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
            .state
            .state) {
          if (notFavoriteGotobody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            toNotFavoritebody1();
          }
        } else {
          if (favoriteGotobody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            toFavoritebody1();
          }
        }
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

final TextEditingController searchTextMeridianAcupointcontroller =
    new TextEditingController();
Widget buildTextField(String hintText, refreshstate) {
  return Container(
    height: 40.h,
    width: 330.w,
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Container(
      margin: EdgeInsets.only(left: 10.w),
      child: TextField(
        onChanged: (text) {
          refreshstate();
        },
        keyboardType: TextInputType.multiline,
        controller: searchTextMeridianAcupointcontroller,
        decoration: InputDecoration(
          hintText: hintText,
          isDense: true,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
        ),
        style: SafeGoogleFont(
          'Inter',
          fontSize: 16.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        autocorrect: false,
        obscureText: false,
      ),
    ),
  );
}

Future<void> _dialogDeleteBuilder(
    BuildContext context, matchingAcupointListData, index, refreshstate) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確認要刪除？',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                await SqliteMyMatchingAcupoint().deleteMatchingAcupointData(
                    matchingAcupointListData[index]['id']);
                refreshstate();
                toast("已刪除！");
                // ignore: use_build_context_synchronously
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Future<void> _dialogFavoriteBuilder(
    BuildContext context, matchingAcupointId, refreshstate) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '收藏您的配穴後，將無法再次編輯，是否確定要收藏?',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                refreshstate();
                await SqliteMyMatchingAcupoint()
                    .updateMatchingAcupointCollectStatus(0, matchingAcupointId);
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Future<void> _dialogNotFavoriteBuilder(
    BuildContext context, matchingAcupointId, refreshstate) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確定要解除收藏?',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                await SqliteMyMatchingAcupoint()
                    .updateMatchingAcupointCollectStatus(1, matchingAcupointId);
                refreshstate();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Future<void> _dialogCopyBuilder(
    BuildContext context, data, matchingAcupointData, refreshstate) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確認要複製此配穴？',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                final sqlite = Sqlite();
                Map<String, dynamic> copyMatchingAcupointData = {
                  'id': null,
                  'name': matchingAcupointData[0]['name'] + "的拷貝",
                  'sickness': matchingAcupointData[0]['sickness'],
                  'symptoms': matchingAcupointData[0]['symptoms'],
                  'diagnosis': matchingAcupointData[0]['diagnosis'],
                  'pathogenesis': matchingAcupointData[0]['pathogenesis'],
                  'times': matchingAcupointData[0]['times'],
                  'memo': matchingAcupointData[0]['memo'],
                  'mem_id': matchingAcupointData[0]['mem_id']
                };
                dynamic matchingAcupointId = await SqliteMyMatchingAcupoint()
                    .insertMatchingAcupointData(
                        "matching_acupoint", copyMatchingAcupointData);

                Map<String, dynamic> addMatchingAcupointCollect = {
                  'mem_id': copyMatchingAcupointData['mem_id'],
                  'matching_acupoint_id': matchingAcupointId,
                };
                await sqlite.insert(
                    "matching_acupoint_collect", addMatchingAcupointCollect);

                List<Map<String, dynamic>> matchingAcupointList = [];
                for (int i = 0; i < data.length; i++) {
                  Map<String, dynamic> item = {
                    "matching_acupoint_id": matchingAcupointId,
                    "acupoint_id": data[i]['id']
                  };
                  matchingAcupointList.add(item);
                }
                matchingAcupointList.forEach(
                    (e) async => sqlite.insert("matching_acupoint_list", e));
                refreshstate();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

Widget myMatchingAcupointFavoriteListView(context, matchingAcupointListData,
    index, Function toFavoritebody2, refreshstate) {
  return GestureDetector(
    onTap: () async {
      final acupointListData = await SqliteMyMatchingAcupoint()
          .getMatchingAcupointList(matchingAcupointListData[index]['id']);
      toFavoritebody2(matchingAcupointListData[index], acupointListData);
    },
    child: Container(
      constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
      margin: EdgeInsets.only(top: 10.h),
      width: 330.w,
      decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              color: Color.fromARGB(216, 191, 197, 197),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(-1, 1),
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.w)),
          border: Border.all(color: Colors.grey)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 200.w,
            //height: 150.h,
            margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  matchingAcupointListData[index]['name'],
                  softWrap: true,
                ),
                if (matchingAcupointListData[index]['symptoms'] == "")
                  Container()
                else
                  Text(
                    matchingAcupointListData[index]['symptoms'],
                    softWrap: true,
                    style: TextStyle(fontSize: 10.sp),
                  ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.w),
            child: Row(children: [
              GestureDetector(
                onTap: () {
                  _dialogNotFavoriteBuilder(context,
                      matchingAcupointListData[index]['id'], refreshstate);
                  print("favorite");
                },
                child: Icon(
                  Icons.favorite,
                  color: Colors.red,
                ),
              ),
              GestureDetector(
                onTap: () async {
                  final data = await SqliteMyMatchingAcupoint()
                      .getMatchingAcupointList(
                          matchingAcupointListData[index]['id']);
                  final matchingAcupointData =
                      await SqliteEditMatchingAcupoint()
                          .getAllMatchingPointData(
                              matchingAcupointListData[index]['id']);
                  _dialogCopyBuilder(
                      context, data, matchingAcupointData, refreshstate);
                  print("copy");
                },
                child: Icon(Icons.copy),
              ),
              GestureDetector(
                onTap: () {
                  _dialogDeleteBuilder(
                      context, matchingAcupointListData, index, refreshstate);
                  print("delete");
                },
                child: Icon(Icons.delete),
              ),
            ]),
          )
        ],
      ),
    ),
  );
}

Widget myMatchingAcupointNotFavoriteListView(context, matchingAcupointListData,
    index, toNotFavoritebody2, refreshstate) {
  return GestureDetector(
    onTap: () async {
      final acupointListData = await SqliteMyMatchingAcupoint()
          .getMatchingAcupointList(matchingAcupointListData[index]['id']);

      toNotFavoritebody2(matchingAcupointListData[index], acupointListData);
    },
    child: Container(
      constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
      margin: EdgeInsets.only(top: 10.h),
      width: 330.w,
      decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              color: Color.fromARGB(216, 191, 197, 197),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(-1, 1),
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.w)),
          border: Border.all(color: Colors.grey)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 200.w,
            //height: 150.h,
            margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  matchingAcupointListData[index]['name'],
                  softWrap: true,
                ),
                if (matchingAcupointListData[index]['symptoms'] == "" ||
                    matchingAcupointListData[index]['symptoms'] == null)
                  Container()
                else
                  Text(
                    matchingAcupointListData[index]['symptoms'],
                    softWrap: true,
                    style: TextStyle(fontSize: 10.sp),
                  ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.w),
            child: Row(children: [
              GestureDetector(
                onTap: () {
                  _dialogFavoriteBuilder(context,
                      matchingAcupointListData[index]['id'], refreshstate);
                  print("favorite");
                },
                child: const Icon(Icons.favorite_border),
              ),
              GestureDetector(
                onTap: () async {
                  print("edit");
                  final data = await SqliteMyMatchingAcupoint()
                      .getMatchingAcupointList(
                          matchingAcupointListData[index]['id']);
                  final matchingAcupointData =
                      await SqliteEditMatchingAcupoint()
                          .getAllMatchingPointData(
                              matchingAcupointListData[index]['id']);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FurkidEditPointMatching(
                                matchingAcupintEditdata: data,
                                matchingAcupointText: matchingAcupointData,
                              ))).then((value) => refreshstate());
                },
                child: Icon(Icons.edit),
              ),
              GestureDetector(
                onTap: () async {
                  final data = await SqliteMyMatchingAcupoint()
                      .getMatchingAcupointList(
                          matchingAcupointListData[index]['id']);
                  final matchingAcupointData =
                      await SqliteEditMatchingAcupoint()
                          .getAllMatchingPointData(
                              matchingAcupointListData[index]['id']);
                  _dialogCopyBuilder(
                      context, data, matchingAcupointData, refreshstate);
                  print("copy");
                },
                child: const Icon(Icons.copy),
              ),
              GestureDetector(
                onTap: () {
                  _dialogDeleteBuilder(
                      context, matchingAcupointListData, index, refreshstate);
                  //removePoint(index);
                  print("delete");
                },
                child: const Icon(Icons.delete),
              ),
            ]),
          )
        ],
      ),
    ),
  );
}

Widget furkidMyPointMatchingFavoriteOn(
    context,
    Function toFavoritebody2,
    bool favoriteGotobody2,
    favoriteBasicDataMap,
    favoriteAcupointDataMap,
    refreshstate) {
  return favoriteGotobody2
      ? furkidMyPointMatchingFavoriteBody2(
          favoriteBasicDataMap, favoriteAcupointDataMap, refreshstate)
      : furkidMyPointMatchingFavoriteBody1(toFavoritebody2, refreshstate);
}

Widget furkidMyPointMatchingFavoriteBody2(
    favoriteBasicDataMap, favoriteAcupointDataMap, refreshstate) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            padding: EdgeInsets.only(
                left: 10.w, right: 10.w, top: 30.h, bottom: 15.h),
            child: BlocBuilder<FurKidHeslthMyPointMatchinFavoriteBlocs,
                    FurKidHeslthMyPointMatchinFavoriteStates>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  furkidFavoriteBody2BasicInformationButton(
                      '基本資料',
                      context,
                      BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(
                              context)
                          .state
                          .onBasicInformationButtonColor),
                  furkidFavoriteAcupointBodyButton(
                      '穴位列表',
                      context,
                      BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(
                              context)
                          .state
                          .onAcupointButtonColor),
                  furkidFavoriteMatchingAcupointRecordBodyButton(
                      '配穴紀錄',
                      context,
                      BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(
                              context)
                          .state
                          .onMatchingAcupointRecordButtonColor),
                ],
              );
            })),
        BlocBuilder<FurKidHeslthMyPointMatchinFavoriteBlocs,
                FurKidHeslthMyPointMatchinFavoriteStates>(
            builder: (context, state) {
          if (BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(context)
                  .state
                  .state ==
              1) {
            return furkidFavoriteBasicInformationView(favoriteBasicDataMap);
          } else if (BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(
                      context)
                  .state
                  .state ==
              2) {
            return furkidFavoriteAcupointListView(favoriteAcupointDataMap);
          } else {
            return furkidFavoriteMatchingAcupointRecordListView(
                context, favoriteBasicDataMap, refreshstate);
          }
        })
      ],
    ),
  );
}

Widget furkidFavoriteBasicInformationView(favoriteBasicDataMap) {
  return Container(
    margin: EdgeInsets.only(top: 10.h),
    width: 360.w,
    child: Column(
      children: [
        furkidBasicDataElement2("配穴名稱", favoriteBasicDataMap["name"]),
        furkidBasicDataElement2("建議操作時間(分)", favoriteBasicDataMap["times"]),
        furkidBasicDataElement2("疾病名稱", favoriteBasicDataMap["sickness"]),
        furkidBasicDataElement2("症狀", favoriteBasicDataMap["symptoms"]),
        furkidBasicDataElement2("診斷", favoriteBasicDataMap["diagnosis"]),
        furkidBasicDataElement2("備註", favoriteBasicDataMap["memo"]),
        SizedBox(
          height: 20.h,
        ),
      ],
    ),
  );
}

Widget furkidFavoriteMatchingAcupointRecordView(
    petMatchingAcupointRecordListData, index) {
  return Container(
    height: 65.h,
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5.w)),
        border: Border.all(color: Colors.grey)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10.w),
          child: Text("${petMatchingAcupointRecordListData[index]["name"]}"),
        ),
        Container(
          margin: EdgeInsets.only(right: 10.w, top: 5.h, bottom: 5.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "${petMatchingAcupointRecordListData[index]["date"]}",
                style: TextStyle(color: Colors.grey),
              ),
              Container(
                width: 45.w,
                decoration: BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.all(Radius.circular(10.w)),
                    border: Border.all(color: Colors.grey)),
                child: Text(
                  "${petMatchingAcupointRecordListData[index]["time"]}" + " 分",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              )
            ],
          ),
        )
      ],
    ),
  );
}

final TextEditingController date = TextEditingController();
final TextEditingController petOperationTimeController =
    TextEditingController();

String petDropdownValue = "";
Future<void> _dialogMatchingAcupointRecordBuilder(
    BuildContext context, refreshstate, favoriteBasicDataMap, petDropdownList) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('輸入配穴操作記錄'),
        content: Container(
          height: 250.h,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: Color.fromARGB(255, 220, 210, 210))),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Text("選擇毛孩"),
              StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return AsyncDropdown(
                    dropdownList: petDropdownList,
                    selectedValue: petDropdownValue = petDropdownValue == ""
                        ? petDropdownList[0]
                        : petDropdownValue,
                    onChanged: (newValue) {
                      setState(() {
                        petDropdownValue = newValue;
                      });
                    },
                  );
                },
              ),
              SizedBox(
                height: 10.h,
              ),
              Text("日期"),
              DatePicker(
                controller: date,
              ),
              SizedBox(
                height: 10.h,
              ),
              Text("操作時間(分)"),
              Container(
                height: 40.h,
                width: 330.w,
                decoration: BoxDecoration(
                  color: const Color(0xffececec),
                  borderRadius: BorderRadius.all(Radius.circular(10.w)),
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 10.w),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    controller: petOperationTimeController,
                    decoration: InputDecoration(
                      //hintText: hintText,
                      isDense: true,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                    style: SafeGoogleFont(
                      'Inter',
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w200,
                      color: Colors.black,
                    ),
                    autocorrect: false,
                    obscureText: false,
                  ),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: Color.fromARGB(255, 220, 210, 210))),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 100.w,
              height: 40.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                print(petDropdownValue);
                int index = petDropdownList.indexOf(petDropdownValue) + 1;
                print(index);
                Map<String, dynamic> petMatchingAcupointRecordData = {
                  'id': null,
                  'pet_id': index,
                  'matching_acupoint_id': favoriteBasicDataMap['id'],
                  'time': petOperationTimeController.text,
                  'date': date.text,
                };
                await SqliteMyMatchingAcupoint()
                    .insertPetMatchingAcupointRecordData(
                        petMatchingAcupointRecordData);
                petOperationTimeController.clear();
                date.clear();
                // petDropdownValue
                refreshstate();
                Navigator.of(context).pop();
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 100.w,
                height: 40.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

class DatePicker extends StatelessWidget {
  const DatePicker({super.key, required this.controller});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.h),
      child: Container(
        height: 38.h,
        decoration: BoxDecoration(
          color: const Color(0xffececec),
          borderRadius: BorderRadius.all(Radius.circular(10.w)),
        ),
        child: Container(
          margin: EdgeInsets.only(left: 12.w),
          child: TextField(
            controller: controller,
            onTap: () async {
              var date = await showDatePickerForTheme(context);
              if (date?.year != null &&
                  date?.month != null &&
                  date?.day != null) {
                controller.text = "${date?.year}/${date?.month}/${date?.day}";
              }
              // print(date);
            },
            keyboardType: TextInputType.multiline,
            decoration: const InputDecoration(
              hintText: "請選擇時間",
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: Colors.black,
            ),
            autocorrect: false,
            obscureText: false,
          ),
        ),
      ),
    );
  }
}

Future<DateTime?> showDatePickerForTheme(BuildContext context) {
  return showDatePicker(
    context: context,
    locale: const Locale("zh", "TW"),
    initialDate: DateTime.now(), // 初始化选中日期
    firstDate: DateTime.now().subtract(const Duration(days: 180)), // 开始日期
    lastDate: DateTime.now().add(const Duration(days: 180)), // 结束日期
    currentDate: DateTime.now(), // 当前日期
    initialEntryMode: DatePickerEntryMode
        .input, // 日历弹框样式 calendar: 默认显示日历，可切换成输入模式，input:默认显示输入模式，可切换到日历，calendarOnly:只显示日历，inputOnly:只显示输入模式
    selectableDayPredicate: (dayTime) {
      // 自定义哪些日期可选
      // if (dayTime == DateTime(2022, 5, 6) || dayTime == DateTime(2022, 6, 8)) {
      //   return false;
      // }
      return true;
    },
    builder: (context, child) {
      return Theme(
        data: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        child: child!,
      );
    },
    helpText: "請選擇日期", // 左上角提示文字
    cancelText: "取消", // 取消按钮 文案
    confirmText: "確認", // 确认按钮 文案
    initialDatePickerMode: DatePickerMode.day, // 日期选择模式 默认为天
    useRootNavigator: false, // 是否使用根导航器
    textDirection: TextDirection.ltr, // 水平方向 显示方向 默认 ltr
  );
}

class AsyncDropdown extends StatefulWidget {
  final List<String> dropdownList;
  final String selectedValue;
  final dynamic onChanged;

  const AsyncDropdown({
    required this.dropdownList,
    required this.selectedValue,
    required this.onChanged,
    super.key,
  });

  @override
  State<AsyncDropdown> createState() => _AsyncDropdownState();
}

class _AsyncDropdownState extends State<AsyncDropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.h,
      margin: EdgeInsets.only(top: 5.h),
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: DropdownButton(
        padding: EdgeInsets.only(left: 15.w),
        isExpanded: true,
        value: widget.selectedValue,
        dropdownColor: Color.fromARGB(255, 232, 228, 228),
        onChanged: widget.onChanged,
        underline: Container(
          height: 0.h,
        ),
        iconSize: 40.w,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        items:
            widget.dropdownList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            alignment: Alignment.centerLeft,
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }
}

Widget furkidFavoriteMatchingAcupointRecordListView(
    context, favoriteBasicDataMap, refreshstate) {
  return Stack(
    children: [
      Container(
        padding: EdgeInsets.all(15),
        width: 360.w,
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                  color: Color.fromARGB(216, 191, 197, 197),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(-1, 1),
                )
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5.w)),
              border: Border.all(color: Colors.grey)),
          child: FutureBuilder(
              future: SqliteMyMatchingAcupoint()
                  .getPetMatchingAcupointRecordList(favoriteBasicDataMap['id']),
              builder: (context, snap) {
                if (snap.hasData) {
                  List petMatchingAcupointRecordListData = snap.data as List;
                  return ListView.builder(
                    shrinkWrap: true,
                    itemCount: petMatchingAcupointRecordListData.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return furkidFavoriteMatchingAcupointRecordView(
                          petMatchingAcupointRecordListData, index);
                    },
                  );
                }
                return SizedBox();
              }),
        ),
      ),
      Positioned(
        top: -10.h,
        left: 300.w,
        child: SizedBox(
          width: 35.w,
          child: FloatingActionButton(
            foregroundColor: Colors.black,
            backgroundColor: Colors.white,
            onPressed: () async {
              List petDropdownList =
                  await SqliteMyMatchingAcupoint().getPetList();
              _dialogMatchingAcupointRecordBuilder(
                  context, refreshstate, favoriteBasicDataMap, petDropdownList);
            },
            child: const Icon(Icons.add),
          ),
        ),
      ),
    ],
  );
}

Widget furkidFavoriteAcupointListView(favoriteAcupointDataMap) {
  return Column(
    children: [
      Container(
        width: 320.w,
        margin: EdgeInsets.only(bottom: 3.h),
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xff929292))),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 8.w),
              child: furkidText("序號"),
            ),
            Container(
              margin: EdgeInsets.only(left: 38.w),
              child: furkidText("穴位"),
            ),
            Container(
              margin: EdgeInsets.only(left: 68.w),
              child: furkidText("經絡"),
            ),
            Container(
              margin: EdgeInsets.only(left: 42.w),
              child: furkidText("經絡代碼"),
            ),
          ],
        ),
      ),
      ListView.builder(
          shrinkWrap: true,
          itemCount: favoriteAcupointDataMap.length,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return furkidAcupointList(favoriteAcupointDataMap[index], index);
          }),
    ],
  );
}

Widget furkidFavoriteBody2BasicInformationButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(context)
            .add(OnFavoriteBasicInformation());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidFavoriteAcupointBodyButton(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(context)
            .add(OnFavoriteAcuPointData());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidFavoriteMatchingAcupointRecordBodyButton(
    name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinFavoriteBlocs>(context)
            .add(OnFavoriteMatchingAcuPointRecordData());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMyPointMatchingFavoriteBody1(toFavoritebody2, refreshState) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Center(
      child: Container(
        margin: EdgeInsets.only(right: 260.w, top: 20.h),
        child: Text(
          '我的配穴',
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125,
            letterSpacing: 0.8,
            color: Color(0xff000000),
          ),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField("文字輸入", refreshState),
    ),
    Expanded(
        child: SingleChildScrollView(
            // controller: controller,
            child: Column(children: [
      BlocBuilder<FurKidHeslthMyPointMatchingBlocs,
          FurKidHeslthMyPointMatchingStates>(builder: (context, state) {
        return SizedBox(
          width: 330.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: furkidNotFavoriteButton(
                      context,
                      '未收藏',
                      BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
                          .state
                          .onNotFavoriteButtonColor,
                      0xffffffff)),
              Container(
                  child: furkidFavoriteButton(
                      context,
                      '已收藏',
                      BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
                          .state
                          .onFavoriteButtonColor,
                      0xffffffff)),
            ],
          ),
        );
      }),
      SizedBox(
          width: 330.w,
          child: FutureBuilder(
              future: searchTextMeridianAcupointcontroller.text == ""
                  ? SqliteMyMatchingAcupoint()
                      .getAllMatchingAcupointFavoriteData()
                  : SqliteMyMatchingAcupoint()
                      .getSearchMatchingAcupointFavorite(
                          searchTextMeridianAcupointcontroller.text),
              builder: (context, snap) {
                if (snap.hasData) {
                  List myMatchingAucpoint = snap.data as List;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: myMatchingAucpoint.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return myMatchingAcupointFavoriteListView(
                            context,
                            myMatchingAucpoint,
                            index,
                            toFavoritebody2,
                            refreshState);
                      });
                }
                return Container();
              })),
    ])))
  ]);
}

Widget furkidMyPointMatchingNotFavoriteOn(
    context,
    Function toNotFavoritebody2,
    bool notFavoriteGotobody2,
    notFavoriteBasicDataMap,
    notFavoriteAcupointDataMap,
    refreshstate) {
  return notFavoriteGotobody2
      ? furkidMyPointMatchingNotFavoriteBody2(
          notFavoriteBasicDataMap, notFavoriteAcupointDataMap)
      : furkidMyPointMatchingNotFavoriteBody1(toNotFavoritebody2, refreshstate);
}

Widget furkidMyPointMatchingNotFavoriteBody1(toNotFavoritebody2, refreshstate) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Center(
      child: Container(
        margin: EdgeInsets.only(right: 260.w, top: 20.h),
        child: Text(
          '我的配穴',
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125,
            letterSpacing: 0.8,
            color: const Color(0xff000000),
          ),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField("文字輸入", refreshstate),
    ),
    Expanded(
        child: SingleChildScrollView(
            // controller: controller,
            child: Column(children: [
      BlocBuilder<FurKidHeslthMyPointMatchingBlocs,
          FurKidHeslthMyPointMatchingStates>(builder: (context, state) {
        return SizedBox(
          width: 330.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: furkidNotFavoriteButton(
                      context,
                      '未收藏',
                      BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
                          .state
                          .onNotFavoriteButtonColor,
                      0xffffffff)),
              Container(
                  child: furkidFavoriteButton(
                      context,
                      '已收藏',
                      BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
                          .state
                          .onFavoriteButtonColor,
                      0xffffffff)),
            ],
          ),
        );
      }),
      SizedBox(
          width: 330.w,
          child: FutureBuilder(
              future: searchTextMeridianAcupointcontroller.text == ""
                  ? SqliteMyMatchingAcupoint()
                      .getAllMatchingAcupointNotFavoriteData()
                  : SqliteMyMatchingAcupoint()
                      .getSearchMatchingAcupointNotFavorite(
                          searchTextMeridianAcupointcontroller.text),
              builder: (context, snap) {
                if (snap.hasData) {
                  List myMatchingAucpoint = snap.data as List;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: myMatchingAucpoint.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return myMatchingAcupointNotFavoriteListView(
                            context,
                            myMatchingAucpoint,
                            index,
                            toNotFavoritebody2,
                            refreshstate);
                      });
                }
                return Container();
              })),
    ])))
  ]);
}

Widget furkidAcupointListText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? "",
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color.fromARGB(255, 42, 39, 39),
      ),
    ),
  );
}

Widget furkidMyPointMatchingNotFavoriteBody2(
    notFavoriteBasicDataMap, notFavoriteAcupointDataMap) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            padding: EdgeInsets.only(
                left: 10.w, right: 10.w, top: 30.h, bottom: 15.h),
            child: BlocBuilder<FurKidHeslthMyPointMatchinNotFavoriteBlocs,
                    FurKidHeslthMyPointMatchinNotFavoriteStates>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  furkidNotFavoriteBody2BasicInformationButton(
                      '基本資料',
                      context,
                      BlocProvider.of<
                                  FurKidHeslthMyPointMatchinNotFavoriteBlocs>(
                              context)
                          .state
                          .onBasicInformationButtonColor),
                  furkidNotFavoriteAcupointBody2Button(
                      '穴位列表',
                      context,
                      BlocProvider.of<
                                  FurKidHeslthMyPointMatchinNotFavoriteBlocs>(
                              context)
                          .state
                          .onAcupointButtonColor),
                ],
              );
            })),
        BlocBuilder<FurKidHeslthMyPointMatchinNotFavoriteBlocs,
                FurKidHeslthMyPointMatchinNotFavoriteStates>(
            builder: (context, state) {
          if (BlocProvider.of<FurKidHeslthMyPointMatchinNotFavoriteBlocs>(
                      context)
                  .state
                  .state ==
              1) {
            return furkidNotFavoriteBasicInformationView(
                notFavoriteBasicDataMap);
          } else {
            return furkidNotFavoriteAcupointListView(
                notFavoriteAcupointDataMap);
          }
        })
      ],
    ),
  );
}

Widget furkidNotFavoriteBasicInformationView(notFavoriteBasicDataMap) {
  return Container(
    margin: EdgeInsets.only(top: 10.h),
    width: 360.w,
    child: Column(
      children: [
        furkidBasicDataElement2("配穴名稱", notFavoriteBasicDataMap["name"]),
        furkidBasicDataElement2("建議操作時間(分)", notFavoriteBasicDataMap["times"]),
        furkidBasicDataElement2("疾病名稱", notFavoriteBasicDataMap["sickness"]),
        furkidBasicDataElement2("症狀", notFavoriteBasicDataMap["symptoms"]),
        furkidBasicDataElement2("診斷", notFavoriteBasicDataMap["diagnosis"]),
        furkidBasicDataElement2("備註", notFavoriteBasicDataMap["memo"]),
        SizedBox(
          height: 20.h,
        ),
      ],
    ),
  );
}

Widget furkidBasicDataElement2(
  dynamic name1,
  dynamic unit1,
) {
  return Container(
    margin: EdgeInsets.only(left: 20.w, right: 20.w),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10.w),
          child: Text(
            name1,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14,
              fontWeight: FontWeight.w200,
              height: 1.2125.h,
              letterSpacing: 0.7,
              color: Color(0xff000000),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.w, top: 5.h),
          child: Text("$unit1" ?? "無",
              textAlign: TextAlign.start,
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14,
                fontWeight: FontWeight.w200,
                height: 1.2125.h,
                letterSpacing: 0.7,
                color: Color.fromARGB(255, 52, 48, 48),
              )),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h, bottom: 5.h),
          // width: 200.w,
          height: 1.h,
          color: Color.fromARGB(157, 207, 206, 206),
        )
      ],
    ),
  );
}

Widget furkidNotFavoriteAcupointListView(notFavoriteAcupointDataMap) {
  return Column(
    children: [
      Container(
        width: 320.w,
        margin: EdgeInsets.only(bottom: 3.h),
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xff929292))),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 8.w),
              child: furkidText("序號"),
            ),
            Container(
              margin: EdgeInsets.only(left: 38.w),
              child: furkidText("穴位"),
            ),
            Container(
              margin: EdgeInsets.only(left: 68.w),
              child: furkidText("經絡"),
            ),
            Container(
              margin: EdgeInsets.only(left: 42.w),
              child: furkidText("經絡代碼"),
            ),
          ],
        ),
      ),
      ListView.builder(
          shrinkWrap: true,
          itemCount: notFavoriteAcupointDataMap.length,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return furkidAcupointList(notFavoriteAcupointDataMap[index], index);
          }),
    ],
  );
}

Widget furkidAcupointList(acupointListData, index) {
  return Column(children: [
    Container(
      width: 320.w,
      margin: EdgeInsets.only(bottom: 5.h, top: 5.h),
      decoration: const BoxDecoration(
        border: Border(
            bottom: BorderSide(color: Color.fromARGB(255, 220, 210, 210))),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 30.w,
            margin: EdgeInsets.only(left: 8.w),
            child: furkidAcupointListText("${acupointListData["sn"]}"),
          ),
          Container(
            width: 55.w,
            child: furkidAcupointListText(acupointListData["name"]),
          ),
          Container(
            width: 90.w,
            child: furkidAcupointListText(acupointListData["meridian_name"]),
          ),
          Container(
            width: 30.w,
            margin: EdgeInsets.only(right: 30.w),
            child: furkidAcupointListText(acupointListData["code"]),
          ),
        ],
      ),
    ),
  ]);
}

Widget furkidNotFavoriteBody2BasicInformationButton(
    name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinNotFavoriteBlocs>(context)
            .add(OnNotFavoriteBasicInformation());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidNotFavoriteAcupointBody2Button(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinNotFavoriteBlocs>(context)
            .add(OnNotFavoriteAcuPointData());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidNotFavoriteButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
              .add(OnNotFavorite());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget furkidFavoriteButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<FurKidHeslthMyPointMatchingBlocs>(context)
              .add(OnFavorite());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
