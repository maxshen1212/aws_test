




import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/point_matching_query/bloc/point_matching_query_event.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/point_matching_query/bloc/point_matching_query_state.dart';

class FurKidHeslthPointMatchingQueryBlocs extends Bloc<
    FurKidHeslthPointMatchingQueryEvents, FurKidHeslthPointMatchingQueryStates> {
  FurKidHeslthPointMatchingQueryBlocs() : super(InitStates()) {
    on<OnFavoriteAcupoints>((event, emit) {
      emit(FurKidHeslthPointMatchingQueryStates(
        state: true,
        onFavoriteAcupointsButtonColor: 0xffe4007f,
        onPublicAcupointsButtonColor: 0xff929292,
      ));
    });
    on<OnPublicAcupoints>((event, emit) {
      emit(FurKidHeslthPointMatchingQueryStates(
        state: false,
        onFavoriteAcupointsButtonColor: 0xff929292,
        onPublicAcupointsButtonColor: 0xffe4007f,
      ));
    });
  }
}


class FurKidHeslthMyPointMatchinMyFavoriteBlocs
    extends Bloc<FurKidHeslthMyPointMatchinMyFavoriteEvents, FurKidHeslthMyPointMatchinMyFavoriteStates> {
  FurKidHeslthMyPointMatchinMyFavoriteBlocs() : super(MyPointMatchinMyFavoriteInitStates()) {
    
    on<OnMyFavoriteBasicInformation>((event, emit) {
      emit(FurKidHeslthMyPointMatchinMyFavoriteStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onAcupointButtonColor: 0xff929292,

      ));
    });
    on<OnMyFavoriteAcuPointData>((event, emit) {
      emit(FurKidHeslthMyPointMatchinMyFavoriteStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onAcupointButtonColor: 0xffe4007f,
      ));
    });
  }
}

class FurKidHeslthMyPointMatchinPublicBlocs
    extends Bloc<FurKidHeslthMyPointMatchinPublicEvents, FurKidHeslthMyPointMatchinPublicStates> {
  FurKidHeslthMyPointMatchinPublicBlocs() : super(MyPointMatchinPublicInitStates()) {
    
    on<OnPublicBasicInformation>((event, emit) {
      emit(FurKidHeslthMyPointMatchinPublicStates(
        state: 1,
        onBasicInformationButtonColor: 0xffe4007f,
        onAcupointButtonColor: 0xff929292,

      ));
    });
    on<OnPublicAcuPointData>((event, emit) {
      emit(FurKidHeslthMyPointMatchinPublicStates(
        state: 2,
        onBasicInformationButtonColor: 0xff929292,
        onAcupointButtonColor: 0xffe4007f,
      ));
    });
  }
}