import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import 'bloc/point_matching_query_bloc.dart';
import 'bloc/point_matching_query_state.dart';
import 'widgets/point_matching_query_widgets.dart';

class FurkidPointMatchingQuery extends StatefulWidget {
  const FurkidPointMatchingQuery({super.key});
  
  @override
  State<FurkidPointMatchingQuery> createState() =>
      _FurkidPointMatchingQueryState();
  
  
}

class _FurkidPointMatchingQueryState extends State<FurkidPointMatchingQuery> {
  bool myFavoriteGotobody2 = false;
  dynamic myFavoriteBasicDataMap ;
  dynamic myFavoriteAcupointDataMap ;
  bool publicGotobody2 = false; 
  dynamic publicBasicDataMap ;
  dynamic publicAcupointDataMap ;
  void toMyFavoritebody2(basicData,acupointDataMap) {
    setState(() {
      myFavoriteGotobody2 = !myFavoriteGotobody2;
      myFavoriteBasicDataMap=basicData;
      myFavoriteAcupointDataMap=acupointDataMap;
    });
  }

  void toMyFavoritebody1() {
    setState(() {
      myFavoriteGotobody2 = !myFavoriteGotobody2;
    });
  }

  void toPublicbody2(basicData,acupointDataMap) {
    setState(() {
      publicGotobody2 = !publicGotobody2;
      publicBasicDataMap=basicData;
      publicAcupointDataMap=acupointDataMap;
    });
  }

  void toPublicbody1() {
    setState(() {
      publicGotobody2 = !publicGotobody2;
    });
  }

  void refreshState() {
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        drawerEdgeDragWidth: 220, //用拉的也能彈出設定
        endDrawer: Container(
          width: 75.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        appBar: furkidHeslthPointMatchingQuery(context, "配穴查詢",myFavoriteGotobody2,publicGotobody2,toPublicbody1,toMyFavoritebody1),
        body: BlocBuilder<FurKidHeslthPointMatchingQueryBlocs,
            FurKidHeslthPointMatchingQueryStates>(builder: (context, state) {
          return BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(context)
                  .state
                  .state
              ? furkidMyPointMatchingMyFavoriteOn(
                  context,
                  toMyFavoritebody2,
                  myFavoriteGotobody2,
                  myFavoriteBasicDataMap,
                  myFavoriteAcupointDataMap,
                  refreshState)
              : furkidMyPointMatchingPublicOn(refreshState);
        }));
  }
}
