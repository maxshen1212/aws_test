import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/pages/fukid_heslth/furkid_heslth_meridian_point/point_matching_query/bloc/point_matching_query_event.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../../common/values/colors.dart';
import '../../../../../utils.dart';
import '../bloc/point_matching_query_bloc.dart';
import '../bloc/point_matching_query_state.dart';

class SqliteMyMatchingAcupoint {
  getAllMatchingAcupointFavoriteData() async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> matchingAcupointCollect =
        await db.rawQuery('''
      SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where matching_acupoint_collect.status==0 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');
    print(matchingAcupointCollect);
    return matchingAcupointCollect;
  }

  getMatchingAcupointList(matching_acupoint_id) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> acupoint = await db.rawQuery('''
      SELECT acupoint.id,acupoint.name,acupoint.sn,meridian.name as meridian_name,meridian.code
      FROM matching_acupoint_list
      LEFT JOIN acupoint,meridian
      WHERE matching_acupoint_list.matching_acupoint_id==$matching_acupoint_id AND acupoint.id=matching_acupoint_list.acupoint_id And acupoint.meridian_id=meridian.id;
    ''');
    print(acupoint);
    return acupoint;
  }

  getSearchMyMatchingAcupoint(searchWord) async {
    Database? db;
    final sqlite = Sqlite();
    db = await sqlite.connectDB();
    final List<Map<String, dynamic>> matchingAcupoint = await db.rawQuery('''
        SELECT matching_acupoint.* FROM matching_acupoint
      LEFT JOIN matching_acupoint_collect
      Where name LIKE "%$searchWord%" AND matching_acupoint_collect.status==0 AND matching_acupoint_collect.mem_id==matching_acupoint.mem_id AND matching_acupoint_collect.matching_acupoint_id==matching_acupoint.id;
    ''');
    if (matchingAcupoint.length == 0) {
      return null;
    } else {
      return matchingAcupoint;
    }
  }
}

AppBar furkidHeslthPointMatchingQuery(
    BuildContext context,
    String names,
    bool myFavoriteGotobody2,
    bool publicGotobody2,
    Function toPublicGbody1,
    Function toMyFavoritebody1) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(myFavoriteGotobody2, publicGotobody2, toPublicGbody1,
          toMyFavoritebody1),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
        decoration: const BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            AppColors.primaryLogo,
            Colors.purple,
          ]),
    )),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget goBack(bool myFavoriteGotobody2, bool publicGotobody2,
    Function toPublicbody1, Function toMyFavoritebody1) {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(context)
            .state
            .state) {
          if (myFavoriteGotobody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            toMyFavoritebody1();
          }
        } else {
          if (publicGotobody2 == false) {
            searchTextMeridianAcupointcontroller.clear();
            Navigator.pop(context);
          } else {
            toPublicbody1();
          }
        }
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

final TextEditingController searchTextMeridianAcupointcontroller =
    new TextEditingController();
Widget buildTextField(String hintText, refreshState) {
  return Container(
    height: 40.h,
    width: 330.w,
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Container(
      margin: EdgeInsets.only(left: 10.w),
      child: TextField(
        onChanged: (text) {
            refreshState();
          },
        controller: searchTextMeridianAcupointcontroller,
        keyboardType: TextInputType.multiline,
        decoration: InputDecoration(
          hintText: hintText,
          isDense: true,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
        ),
        style: SafeGoogleFont(
          'Inter',
          fontSize: 16.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        autocorrect: false,
        obscureText: false,
      ),
    ),
  );
}

Widget furkidMyPointMatchingPublicOn(refreshState) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Center(
      child: Container(
        margin: EdgeInsets.only(top: 20.h, right: 260.w),
        child: Text(
          '配穴查詢',
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125,
            letterSpacing: 0.8,
            color: Color(0xff000000),
          ),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField("文字輸入", refreshState),
    ),
    Expanded(
        child: SingleChildScrollView(
            child: Column(children: [
      BlocBuilder<FurKidHeslthPointMatchingQueryBlocs,
          FurKidHeslthPointMatchingQueryStates>(builder: (context, state) {
        return Container(
          width: 330.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: furkidFavoriteAcupointsButton(
                      context,
                      '我的已收藏配穴',
                      BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(
                              context)
                          .state
                          .onFavoriteAcupointsButtonColor,
                      0xffffffff,
                      refreshState)),
              Container(
                  child: furkidPublicAcupointsButton(
                      context,
                      '公發配穴',
                      BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(
                              context)
                          .state
                          .onPublicAcupointsButtonColor,
                      0xffffffff,
                      refreshState)),
            ],
          ),
        );
      }),
    ])))
  ]);
}

Widget furkidMyPointMatchingMyFavoriteBody2(myFavoriteBasicDataMap, myFavoriteAcupointDataMap) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            padding: EdgeInsets.only(
                left: 10.w, right: 10.w, top: 30.h, bottom: 15.h),
            child: BlocBuilder<FurKidHeslthMyPointMatchinMyFavoriteBlocs,
                    FurKidHeslthMyPointMatchinMyFavoriteStates>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  furkidMyFavoriteBody2BasicInformationButton(
                      '基本資料',
                      context,
                      BlocProvider.of<
                                  FurKidHeslthMyPointMatchinMyFavoriteBlocs>(
                              context)
                          .state
                          .onBasicInformationButtonColor),
                  furkidMyFavoriteAcupointBody2Button(
                      '穴位列表',
                      context,
                      BlocProvider.of<
                                  FurKidHeslthMyPointMatchinMyFavoriteBlocs>(
                              context)
                          .state
                          .onAcupointButtonColor),
                ],
              );
            })),
        BlocBuilder<FurKidHeslthMyPointMatchinMyFavoriteBlocs,
                FurKidHeslthMyPointMatchinMyFavoriteStates>(
            builder: (context, state) {
          if (BlocProvider.of<FurKidHeslthMyPointMatchinMyFavoriteBlocs>(
                      context)
                  .state
                  .state ==
              1) {
            return furkidMyFavoriteBasicInformationView(
                myFavoriteBasicDataMap);
          } else {
            return furkidMyFavoriteAcupointListView(
                myFavoriteAcupointDataMap);
          }
        })
      ],
    ),
  );
}

Widget furkidText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? '-',
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color(0xff000000),
      ),
    ),
  );
}

Widget furkidMyFavoriteAcupointListView(notFavoriteAcupointDataMap) {
  return Column(
    children: [
      Container(
        width: 320.w,
        margin: EdgeInsets.only(bottom: 3.h),
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xff929292))),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 8.w),
              child: furkidText("序號"),
            ),
            Container(
              margin: EdgeInsets.only(left: 38.w),
              child: furkidText("穴位"),
            ),
            Container(
              margin: EdgeInsets.only(left: 68.w),
              child: furkidText("經絡"),
            ),
            Container(
              margin: EdgeInsets.only(left: 42.w),
              child: furkidText("經絡代碼"),
            ),
          ],
        ),
      ),
      ListView.builder(
          shrinkWrap: true,
          itemCount: notFavoriteAcupointDataMap.length,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return furkidAcupointList(notFavoriteAcupointDataMap[index], index);
          }),
    ],
  );
}

Widget furkidAcupointListText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text ?? "",
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color.fromARGB(255, 42, 39, 39),
      ),
    ),
  );
}

Widget furkidAcupointList(acupointListData, index) {
  return Column(children: [
    Container(
      width: 320.w,
      margin: EdgeInsets.only(bottom: 5.h, top: 5.h),
      decoration: const BoxDecoration(
        border: Border(
            bottom: BorderSide(color: Color.fromARGB(255, 220, 210, 210))),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 30.w,
            margin: EdgeInsets.only(left: 8.w),
            child: furkidAcupointListText("${acupointListData["sn"]}"),
          ),
          Container(
            width: 55.w,
            child: furkidAcupointListText(acupointListData["name"]),
          ),
          Container(
            width: 90.w,
            child: furkidAcupointListText(acupointListData["meridian_name"]),
          ),
          Container(
            width: 30.w,
            margin: EdgeInsets.only(right: 30.w),
            child: furkidAcupointListText(acupointListData["code"]),
          ),
        ],
      ),
    ),
  ]);
}

Widget furkidBasicDataElement2(
  dynamic name1,
  dynamic unit1,
) {
  return Container(
    margin: EdgeInsets.only(left: 20.w, right: 20.w),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10.w),
          child: Text(
            name1,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14,
              fontWeight: FontWeight.w200,
              height: 1.2125.h,
              letterSpacing: 0.7,
              color: Color(0xff000000),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10.w, top: 5.h),
          child: Text("$unit1" ?? "無",
              textAlign: TextAlign.start,
              style: SafeGoogleFont(
                'Inter',
                fontSize: 14,
                fontWeight: FontWeight.w200,
                height: 1.2125.h,
                letterSpacing: 0.7,
                color: Color.fromARGB(255, 52, 48, 48),
              )),
        ),
        Container(
          margin: EdgeInsets.only(top: 7.h, bottom: 5.h),
          // width: 200.w,
          height: 1.h,
          color: Color.fromARGB(157, 207, 206, 206),
        )
      ],
    ),
  );
}

Widget furkidMyFavoriteBasicInformationView(myFavoriteBasicDataMap) {
  return Container(
    margin: EdgeInsets.only(top: 10.h),
    width: 360.w,
    child: Column(
      children: [
        furkidBasicDataElement2("配穴名稱", myFavoriteBasicDataMap["name"]),
        furkidBasicDataElement2("建議操作時間(分)", myFavoriteBasicDataMap["times"]),
        furkidBasicDataElement2("疾病名稱", myFavoriteBasicDataMap["sickness"]),
        furkidBasicDataElement2("症狀", myFavoriteBasicDataMap["symptoms"]),
        furkidBasicDataElement2("診斷", myFavoriteBasicDataMap["diagnosis"]),
        furkidBasicDataElement2("備註", myFavoriteBasicDataMap["memo"]),
        SizedBox(
          height: 20.h,
        ),
      ],
    ),
  );
}


Widget furkidMyFavoriteBody2BasicInformationButton(
    name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinMyFavoriteBlocs>(context)
            .add(OnMyFavoriteBasicInformation());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}

Widget furkidMyFavoriteAcupointBody2Button(name, context, buttoncolor) {
  return Container(
    width: 80.w,
    height: 35.h,
    decoration: BoxDecoration(
      color: Color(buttoncolor),
      borderRadius: BorderRadius.circular(100),
    ),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<FurKidHeslthMyPointMatchinMyFavoriteBlocs>(context)
            .add(OnMyFavoriteAcuPointData());
      },
      child: Text(
        name,
        textAlign: TextAlign.center,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 14,
          fontWeight: FontWeight.w700,
          letterSpacing: 0.7,
          color: Color(0xffffffff),
        ),
      ),
    ),
  );
}



Widget furkidMyPointMatchingMyFavoriteBody1(toMyFavoritebody2, refreshState) {
  return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    Center(
      child: Container(
        margin: EdgeInsets.only(top: 20.h, right: 260.w),
        child: Text(
          '配穴查詢',
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125,
            letterSpacing: 0.8,
            color: Color(0xff000000),
          ),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h, top: 10.h),
      child: buildTextField("文字輸入", refreshState),
    ),
    Expanded(
        child: SingleChildScrollView(
            child: Column(children: [
      BlocBuilder<FurKidHeslthPointMatchingQueryBlocs,
          FurKidHeslthPointMatchingQueryStates>(builder: (context, state) {
        return Container(
          width: 330.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: furkidFavoriteAcupointsButton(
                      context,
                      '我的已收藏配穴',
                      BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(
                              context)
                          .state
                          .onFavoriteAcupointsButtonColor,
                      0xffffffff,
                      refreshState)),
              Container(
                  child: furkidPublicAcupointsButton(
                      context,
                      '公發配穴',
                      BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(
                              context)
                          .state
                          .onPublicAcupointsButtonColor,
                      0xffffffff,
                      refreshState)),
            ],
          ),
        );
      }),
      SizedBox(
          width: 330.w,
          child: FutureBuilder(
              future: searchTextMeridianAcupointcontroller.text == ""
                  ? SqliteMyMatchingAcupoint()
                      .getAllMatchingAcupointFavoriteData()
                  : SqliteMyMatchingAcupoint().getSearchMyMatchingAcupoint(
                      searchTextMeridianAcupointcontroller.text),
              builder: (context, snap) {
                if (snap.hasData) {
                  List myMatchingAucpoint = snap.data as List;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: myMatchingAucpoint.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return myMatchingAcupointFavoriteListView(
                            context,
                            myMatchingAucpoint,
                            index,
                            toMyFavoritebody2,
                            refreshState);
                      });
                } else {
                  return Container();
                  // return Center(
                  //   child: Container(
                  //     margin: EdgeInsets.only(top: 10.h),
                  //     child: Text(
                  //       "未查詢任何含有相關字" +
                  //           searchTextMeridianAcupointcontroller.text +
                  //           "之資料!!!",
                  //       style: TextStyle(
                  //           fontSize: 18.sp,
                  //           fontWeight: FontWeight.bold,
                  //           color: Colors.red),
                  //     ),
                  //   ),
                  // );
                }
              })),
    ])))
  ]);
}

Widget furkidMyPointMatchingMyFavoriteOn(
    context,
    Function toMyFavoritebody2,
    bool myFavoriteGotobody2,
    myFavoriteBasicDataMap,
    myFavoriteAcupointDataMap,
    refreshState) {
  return myFavoriteGotobody2
      ? furkidMyPointMatchingMyFavoriteBody2(myFavoriteBasicDataMap, myFavoriteAcupointDataMap)
      : furkidMyPointMatchingMyFavoriteBody1(toMyFavoritebody2, refreshState);
}

Widget myMatchingAcupointFavoriteListView(
    context, matchingAcupointListData, index, toMyFavoritebody2, refreshState) {
  return GestureDetector(
    onTap: () async {
      final acupointListData = await SqliteMyMatchingAcupoint()
          .getMatchingAcupointList(matchingAcupointListData[index]['id']);
      toMyFavoritebody2(matchingAcupointListData[index], acupointListData);
      print(matchingAcupointListData[index]);
    },
    child: Container(
      constraints: BoxConstraints(minHeight: 50.h, maxHeight: 150.h),
      margin: EdgeInsets.only(top: 10.h),
      width: 330.w,
      decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
              color: Color.fromARGB(216, 191, 197, 197),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(-1, 1),
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.w)),
          border: Border.all(color: Colors.grey)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 200.w,
            //height: 150.h,
            margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  matchingAcupointListData[index]['name'],
                  softWrap: true,
                ),
                if (matchingAcupointListData[index]['symptoms'] == "")
                  Container()
                else
                  Text(
                    matchingAcupointListData[index]['symptoms'],
                    softWrap: true,
                    style: TextStyle(fontSize: 10.sp),
                  ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget furkidFavoriteAcupointsButton(
    BuildContext context, String text, buttoncolor, textbutton, refreshState) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          refreshState();
          BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(context)
              .add(OnFavoriteAcupoints());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget furkidPublicAcupointsButton(
    BuildContext context, String text, buttoncolor, textbutton, refreshState) {
  return SizedBox(
    width: 140.w,
    height: 35.h,
    child: TextButton(
        onPressed: () {
          refreshState();
          BlocProvider.of<FurKidHeslthPointMatchingQueryBlocs>(context)
              .add(OnPublicAcupoints());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}
