import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/home/widgets/home_widgets.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final images = [
    'assets/appdesign/images/-83T.png',
    'assets/appdesign/images/-5gV.png',
    'assets/appdesign/images/-By3.png',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: 140.h,
              color: const Color(0xffaacd06),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 70.h),
                child: CarouselSlider.builder(
                  itemCount: images.length,
                  options: CarouselOptions(
                      enlargeCenterPage: true,
                      height: 200.h,
                      autoPlay: true,
                      viewportFraction: 1,
                      autoPlayInterval: const Duration(seconds: 3)),
                  itemBuilder: (context, index, realIndex) {
                    final image = images[index];
                    return buildImage(image, index);
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 320.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 10.h),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            petSafeButtonBox(context, '寵安快易通',
                                'assets/appdesign/images/logo-1.png'),
                            SizedBox(
                              width: 10.w,
                            ),
                            furryKidShopButtonBox(
                                'assets/appdesign/images/iclogofurkidshop.png'),
                          ])),
                  Container(
                      margin: EdgeInsets.only(top: 10.h),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            dAndkBoxButtonBox(
                                'assets/appdesign/images/logodkbox-1.png'),
                            SizedBox(
                              width: 10.w,
                            ),
                            multiplePartnerServicesButtonBox(context, '多元搭伙服務',
                                'assets/appdesign/images/iclogo-1.png')
                          ])),
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          customizedMealServiceButtonBox(context, '客製餐服務',
                              'assets/appdesign/images/logo-1.png'),
                          SizedBox(
                            width: 10.w,
                          ),
                          contentAndCommunityButtonBox(context, '內容與社團',
                              'assets/appdesign/images/logo-1.png'),
                        ]),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
