import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../utils.dart';

bool _visible = true;
Widget buildImage(String image, int index) {
  return Image.asset(
    image,
  );
}


Widget petSafeButtonBox(context, String text, String iconName) {
  return Container(
    child: TextButton(
      onPressed: () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("furkid_heslth_home", (route) => false);
      },
      style: TextButton.styleFrom(padding: EdgeInsets.zero),
      child: Container(
        width: 150.w,
        height: 140.h,
        padding: EdgeInsets.only(top: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 5.h),
                width: 85.w,
                height: 80.h,
                child: Image.asset(
                  iconName,
                ),
              ),
            ),
            Center(
              // pqP (94:156)
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w300,
                    height: 1.h,
                    letterSpacing: 2.8.r,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget multiplePartnerServicesButtonBox(context, String text, String iconName) {
  return Container(
    // margin: EdgeInsets.only(top: 12.h, right: 15.w),
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(padding: EdgeInsets.zero),
      child: Container(
        width: 150.w,
        height: 140.h,
        padding: EdgeInsets.only(top: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 5.h),
                width: 100.w,
                height: 80.h,
                child: Image.asset(
                  iconName,
                ),
              ),
            ),
            Center(
              // pqP (94:156)
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w300,
                    height: 1.h,
                    letterSpacing: 2.8.r,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget furryKidShopButtonBox(String iconName) {
  return Container(
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        // margin: EdgeInsets.only(left: 15.w),
        width: 150.w,
        height: 140.h,
        padding:
            EdgeInsets.only(top: 20.h, left: 15.w, right: 15.w, bottom: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                width: 120.w,
                height: 90.h,
                child: Image.asset(iconName, fit: BoxFit.contain),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget dAndkBoxButtonBox(String iconName) {
  return Container(
    // margin: EdgeInsets.only(top: 12.h, right: 15.w),
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        width: 150.w,
        height: 140.h,
        padding:
            EdgeInsets.only(top: 20.h, left: 15.w, right: 15.w, bottom: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 13.h),
                width: 120.w,
                height: 70.h,
                child: Image.asset(iconName, fit: BoxFit.contain),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget customizedMealServiceButtonBox(context, String text, String iconName) {
  return Container(
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(padding: EdgeInsets.zero),
      child: Container(
        width: 150.w,
        height: 140.h,
        padding: EdgeInsets.only(top: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 5.h),
                width: 100.w,
                height: 70.h,
              ),
            ),
            Center(
              // pqP (94:156)
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w300,
                    height: 1.h,
                    letterSpacing: 2.8.r,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget contentAndCommunityButtonBox(context, String text, String iconName) {
  return Container(
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(padding: EdgeInsets.zero),
      child: Container(
        width: 150.w,
        height: 140.h,
        padding: EdgeInsets.only(top: 15.h),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0, 0),
              blurRadius: 5,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 5.h),
                width: 100.w,
                height: 70.h,
                // child: Image.asset(
                //   iconName,
                // ),
              ),
            ),
            Center(
              // pqP (94:156)
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w300,
                    height: 1.h,
                    letterSpacing: 2.8.r,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
