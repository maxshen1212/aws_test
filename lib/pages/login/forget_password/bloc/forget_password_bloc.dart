import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/login/forget_password/bloc/forget_password_event.dart';
import 'package:pet_save_app/pages/login/forget_password/bloc/forget_password_state.dart';

class ForgetPasswordBlocs extends Bloc<ForgetPasswordEvents, ForgetPasswordStates> {
  ForgetPasswordBlocs() : super(InitStates()) {
    on<OnEmail>((event, emit) {
      emit(ForgetPasswordStates(
          forgetPasswordText: '請輸入您註冊時的帳號,我們將會寄\n新密碼至您的電子信箱。',
          forgetPasswordButtonText: '取得新密碼',
          textFieldEmailText: '請輸入您的電子郵件',
          state: true,
          onEmailbuttonColor: 0xffe4007f,
          onEmailTextColor: 0xffffffff,
          onPhoneNumnerbuttonColor: 0xff929292,
          onPhoneNumnerTextColor: 0xff000000));
    });
    on<OnPhoneNumber>((event, emit) {
      emit(ForgetPasswordStates(
          forgetPasswordText: '請輸入您的手機號碼,我們將會傳送\n更改密碼的驗證碼給您。',
          forgetPasswordButtonText: '取得驗證碼',
          textFieldEmailText: '請輸入您的手機',
          state: false,
          onEmailbuttonColor: 0xff929292,
          onEmailTextColor: 0xff000000,
          onPhoneNumnerbuttonColor: 0xffe4007f,
          onPhoneNumnerTextColor: 0xffffffff));
    });
  }
}
