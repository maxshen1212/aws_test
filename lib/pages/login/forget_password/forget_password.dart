import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'bloc/forget_password_bloc.dart';
import 'bloc/forget_password_state.dart';
import 'widgets/forget_password_widgets.dart';

Future<void> showAlertDialogForgetPassword(BuildContext context) {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        title: Center(
            child: Text(
          '忘記密碼',
          style: TextStyle(fontSize: 24.sp),
        )),
        content: SizedBox(
          height: 170.h,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 10.h),
                child: BlocBuilder<ForgetPasswordBlocs, ForgetPasswordStates>(
                    builder: (context, state) {
                  return Text(
                      BlocProvider.of<ForgetPasswordBlocs>(context)
                          .state
                          .forgetPasswordText,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 12.sp));
                }),
              ),
              Center(
                child: Container(
                  width: 180.w,
                  height: 20.h,
                  decoration: BoxDecoration(
                    color: Color(0xff929292),
                    borderRadius: BorderRadius.circular(10.r),
                  ),
                  child: BlocBuilder<ForgetPasswordBlocs, ForgetPasswordStates>(
                      builder: (context, state) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        forgetPasswordEmailButton(
                          context,
                          '電子郵件',
                          BlocProvider.of<ForgetPasswordBlocs>(context)
                              .state
                              .onEmailbuttonColor,
                        ),
                        //會員手機button
                        forgetPasswordPhoneNumberButton(
                          context,
                          "會員手機",
                          BlocProvider.of<ForgetPasswordBlocs>(context)
                              .state
                              .onPhoneNumnerbuttonColor,
                        ),
                      ],
                    );
                  }),
                ),
              ),
              BlocBuilder<ForgetPasswordBlocs, ForgetPasswordStates>(
                  builder: (context, state) {
                return Container(
                  child: BlocProvider.of<ForgetPasswordBlocs>(context)
                          .state
                          .state
                      ? forgetPasswordEmailTextField(
                          "${BlocProvider.of<ForgetPasswordBlocs>(context).state.textFieldEmailText}")
                      : forgetPasswordPhoneNumberTextField(
                          "${BlocProvider.of<ForgetPasswordBlocs>(context).state.textFieldEmailText}"),
                );
              }),
              Container(
                margin: EdgeInsets.only(top: 18.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 100.w,
                      height: 34.h,
                      child: BlocBuilder<ForgetPasswordBlocs,
                          ForgetPasswordStates>(builder: (context, state) {
                        return ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            bool state =
                                BlocProvider.of<ForgetPasswordBlocs>(context)
                                    .state
                                    .state;
                            if (state) {
                            } else {
                              showAlertDialogVerificationCode(context);
                              print(phoneNumberText.text);
                            }
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xff929292),
                              foregroundColor: const Color(0xffe4007f)),
                          child: BlocBuilder<ForgetPasswordBlocs,
                              ForgetPasswordStates>(builder: (context, state) {
                            return Text(
                              BlocProvider.of<ForgetPasswordBlocs>(context)
                                  .state
                                  .forgetPasswordButtonText,
                              style: TextStyle(
                                  fontFamily: "Avenir",
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12.sp),
                            );
                          }),
                        );
                      }),
                    ),
                    SizedBox(
                      width: 18.w,
                    ),
                    Container(
                      height: 34.h,
                      decoration: BoxDecoration(
                        border:
                            Border.all(width: 2.w, color: Color(0xffe4007f)),
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      child: TextButton(
                        child: Text(
                          '取消',
                          style: TextStyle(
                              fontFamily: "Avenir",
                              color: Color(0xffe4007f),
                              fontWeight: FontWeight.w600,
                              fontSize: 12.sp),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}