import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pinput/pinput.dart';
import '../../../../utils.dart';
import '../bloc/forget_password_bloc.dart';
import '../bloc/forget_password_event.dart';

final TextEditingController phoneNumberText = new TextEditingController();
String dropdownValue = "+886";

Widget forgetPasswordEmailButton(
    BuildContext context, String text, buttoncolor) {
  return SizedBox(
    width: 90.w,
    height: 20.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<ForgetPasswordBlocs>(context).add(OnEmail());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(10.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.8.r,
                  color: Color(0xffffffff),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget forgetPasswordPhoneNumberButton(
    BuildContext context, String text, buttoncolor) {
  return SizedBox(
    width: 90.w,
    height: 20.h,
    child: TextButton(
        onPressed: () {
          BlocProvider.of<ForgetPasswordBlocs>(context).add(OnPhoneNumber());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          width: 90.w,
          height: 20.h,
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(10.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.8.r,
                  color: Color(0xffffffff),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget forgetPasswordEmailTextField(String hintText) {
  return Container(
    margin: EdgeInsets.only(top: 10.h),
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Container(
      margin: EdgeInsets.only(left: 8.w, right: 8.w),
      child: TextField(
        keyboardType: TextInputType.multiline,
        inputFormatters: [
          FilteringTextInputFormatter.allow(
              RegExp('[a-z @ . A-Z 0-9]')), //限制只能输入中文和英文
        ],
        decoration: const InputDecoration(
          hintText: "請輸入您的會員帳號",
          isDense: true,
          enabledBorder: InputBorder.none,
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
            ),
          ),
          focusedBorder: InputBorder.none,
        ),
        style: SafeGoogleFont(
          'Inter',
          fontSize: 12.sp,
          fontWeight: FontWeight.w200,
          color: Colors.black,
        ),
        autocorrect: false,
        obscureText: false,
      ),
    ),
  );
}

Widget forgetPasswordPhoneNumberTextField(String hintText) {
  return Container(
    margin: EdgeInsets.only(top: 10.h),
    child: Row(
      children: [
        Container(
          width: 65.w,
          margin: EdgeInsets.only(left: 3.w, right: 5.w),
          child: CountryCode(),
        ),
        Container(
          width: 140.w,
          decoration: BoxDecoration(
            color: const Color(0xffececec),
            borderRadius: BorderRadius.all(Radius.circular(10.w)),
          ),
          child: Container(
            margin: EdgeInsets.only(left: 8.w, right: 8.w),
            child: TextField(
              // controller: TextEditingController()..text="",
              controller: phoneNumberText,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly //限制只能输入中文和英文
              ],
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintText: hintText,
                isDense: true,
                enabledBorder: InputBorder.none,
                disabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
                focusedBorder: InputBorder.none,
              ),
              //
              style: TextStyle(
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.normal,
                  fontSize: 12.sp),
              autocorrect: false,
              autofocus: false,
            ),
          ),
        ),
      ],
    ),
  );
}

class CountryCode extends StatefulWidget {
  const CountryCode({super.key});
  @override
  State<CountryCode> createState() => _CountryCodeState();
}

class _CountryCodeState extends State<CountryCode> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        padding: EdgeInsets.only(left: 5.w),
        decoration: BoxDecoration(
          border: Border.all(color: const Color(0xff000000)),
          borderRadius: BorderRadius.circular(5),
        ),
        child: DropdownButton(
          borderRadius: BorderRadius.circular(8.r),
          value: dropdownValue,
          hint: const Text(
            '國碼',
          ),
          iconSize: 18,
          isDense: true,
          onChanged: (newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
          items: <String>['+886', '+852', '+60']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              alignment: Alignment.centerLeft,
              value: value,
              child: Text(
                value,
                style: TextStyle(fontSize: 12.sp),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

Future<void> showAlertDialogVerificationCode(BuildContext context) {
  return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          title: Center(
            child: Text(
              '輸入驗證碼',
              style: TextStyle(fontSize: 24.sp),
            ),
          ),
          content: SizedBox(
            height: 170.h,
            child: Column(children: [
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Text('發送至'),
                  SizedBox(
                    width: 10.w,
                  ),
                  Text('$dropdownValue ${phoneNumberText.text}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: const Color(0xffe4007f), fontSize: 12.sp))
                ]),
              ),
              Container(
                margin: EdgeInsets.only(left: 160.w),
                child: verifyButton(),
              ),

              // margin: EdgeInsets.only(left: 110.w),
              buildPinPut(),
              SizedBox(
                height: 20.h,
              ),
              verifySendButton(context),
            ]),
          ),
        );
      });
}

Widget verifyButton() {
  return TextButton(
    onPressed: () {},
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Container(
      width: 70.w,
      height: 20.h,
      decoration: BoxDecoration(
        color: const Color(0xffe4007f),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: Text(
          "重發驗證碼",
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 10.sp,
            fontWeight: FontWeight.w700,
            letterSpacing: 2,
            color: const Color(0xffffffff),
          ),
        ),
      ),
    ),
  );
}

Widget buildPinPut() {
  return Container(
    width: 290,
    height: 50.h,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      shadows: const [
        BoxShadow(
          color: Color(0x3F000000),
          blurRadius: 10,
          offset: Offset(0, 0),
          spreadRadius: 0,
        )
      ],
    ),
    child: Container(
      margin: EdgeInsets.only(bottom: 5.h),
      child: Pinput(
        defaultPinTheme: defaultPinTheme,
        focusedPinTheme: focusedPinTheme,
        showCursor: true,
        onCompleted: (pin) => print(pin),
      ),
    ),
  );
}

final defaultPinTheme = PinTheme(
  width: 40.w,
  height: 56.h,
  textStyle: const TextStyle(
      fontSize: 20,
      color: Color.fromRGBO(30, 60, 87, 1),
      fontWeight: FontWeight.w600),
  decoration: const BoxDecoration(
    border: Border(bottom: BorderSide(width: 3, color: Color(0xff929292))),
  ),
);

final focusedPinTheme = defaultPinTheme.copyDecorationWith(
  border: const Border(bottom: BorderSide(width: 3, color: Colors.blue)),
);

Widget verifySendButton(context) {
  return Container(
    width: 70.w,
    height: 28.w,
    child: ElevatedButton(
      onPressed: () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("update_password", (route) => false);
      },
      style: ElevatedButton.styleFrom(
          backgroundColor: const Color(0xff929292),
          foregroundColor: const Color(0xffe4007f)),
      child: Text(
        '送出',
        style: TextStyle(
            fontFamily: "Avenir",
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 12.sp),
      ),
    ),
  );
}
