import 'package:flutter/material.dart';
import 'package:pet_save_app/pages/login/main_login/widgets/mainlogin_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 70.h,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          petSafeButtonBox(context, '寵安快易通',
                              'assets/appdesign/images/logo-1.png'),
                          SizedBox(
                            width: 10.w,
                          ),
                          furryKidShopButtonBox(
                              'assets/appdesign/images/iclogofurkidshop.png'),
                        ]),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          dAndkBoxButtonBox(
                              'assets/appdesign/images/logodkbox-1.png'),
                          SizedBox(
                            width: 10.w,
                          ),
                          multiplePartnerServicesButtonBox(context, '多元搭伙服務',
                              'assets/appdesign/images/iclogo-1.png')
                        ]),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          customizedMealServiceButtonBox(context, '客製餐服務',
                              'assets/appdesign/images/logo-1.png'),
                          SizedBox(
                            width: 10.w,
                          ),
                          contentAndCommunityButtonBox(context, '內容與社團',
                              'assets/appdesign/images/logo-1.png'),
                        ]),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30.h),
                    child: logInButton(context, '會員登入'),
                  ),
                  Container(
                    width: 250.w,
                    margin: EdgeInsets.only(top: 30.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        //會員註冊button
                        registerTextButton('會員註冊'),
                        //忘記密碼button
                        forgetPasswordTextButton('使用者條款'),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
