import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_event.dart';
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_state.dart';

class PetSafeLoginBlocs extends Bloc<PetSafeLoginEvents, PetSafeLoginStates> {
  PetSafeLoginBlocs() : super(InitStates()) {
    on<OnEmail>((event, emit) {
      emit(PetSafeLoginStates(
          textFieldEmailText: '請輸入您的電子郵件',
          loginstate: true,
          onEmailbuttonColor: 0xffe4007f,
          onEmailTextColor: 0xffffffff,
          onPhoneNumnerbuttonColor: 0xffd9d9d9,
          onPhoneNumnerTextColor: 0xff000000));
    });
    on<OnPhoneNumber>((event, emit) {
      emit(PetSafeLoginStates(
          textFieldEmailText: '請輸入您的手機',
          loginstate: false,
          onEmailbuttonColor: 0xffd9d9d9,
          onEmailTextColor: 0xff000000,
          onPhoneNumnerbuttonColor: 0xffe4007f,
          onPhoneNumnerTextColor: 0xffffffff));
    });
  }
}

class PetSafePasswordBlocs extends Bloc<PetSafePasswordEvents, PetSafePasswordStates> {
  PetSafePasswordBlocs() : super(PetSafePasswordInitStates()) {
    int buttonid = 0;
    on<OnPassword>((event, emit) {
      if (buttonid == 0) {
        buttonid = buttonid + 1;
        emit(PetSafePasswordStates(passwordState: buttonid));
      } else {
        buttonid = buttonid - 1;
        emit(PetSafePasswordStates(passwordState: buttonid));
      }
    });
  }
}
