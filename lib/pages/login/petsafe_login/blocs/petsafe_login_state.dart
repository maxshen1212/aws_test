class PetSafeLoginStates {
  String? textFieldEmailText;
  bool loginstate;
  var onEmailbuttonColor; //=0xffe4007f;
  var onEmailTextColor; //0xffffffff;
  var onPhoneNumnerbuttonColor; //0xffd9d9d9;
  var onPhoneNumnerTextColor; //0xff000000;
  PetSafeLoginStates(
      {this.textFieldEmailText,
      required this.loginstate,
      this.onEmailbuttonColor,
      this.onEmailTextColor,
      this.onPhoneNumnerbuttonColor,
      this.onPhoneNumnerTextColor});
}

class InitStates extends PetSafeLoginStates {
  InitStates()
      : super(
            textFieldEmailText: '請輸入您的電子郵件',
            loginstate: true,
            onEmailbuttonColor: 0xffe4007f,
            onEmailTextColor: 0xffffffff,
            onPhoneNumnerbuttonColor: 0xffd9d9d9,
            onPhoneNumnerTextColor: 0xff000000);
}

class PetSafePasswordStates {
  int passwordState;
  PetSafePasswordStates({required this.passwordState});
}

class PetSafePasswordInitStates extends PetSafePasswordStates {
  PetSafePasswordInitStates() : super(passwordState: 0);
}
