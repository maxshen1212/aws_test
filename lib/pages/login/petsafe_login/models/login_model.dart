import 'dart:convert';

import 'package:dio/dio.dart';

class EmailPasswordVerifyBody{
  String? email;
  String? password;
  String? source;
  EmailPasswordVerifyBody({required this.email,required this.password,required this.source});
  EmailPasswordVerifyBody.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    source = json['source'];
  }
}

class MobilePasswordVerifyModel {
  String? country_code;
  String? mobile;
  String? password;
  String? source;

  MobilePasswordVerifyModel({this.country_code,this.mobile, this.password, this.source});
  MobilePasswordVerifyModel.fromJson(Map<String, dynamic> json) {
    country_code = json['country_code'];
    mobile = json['mobile'];
    password = json['password'];
    source = json['source'];
  }
}