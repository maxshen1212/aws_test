import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:pet_save_app/pages/login/petsafe_login/widgets/petsafe_login_widget.dart';

class PetSafeLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ProgressHUD(
        child: Builder(
          builder: (context) => const Center(
            child: SafeArea(
              child: Scaffold(
                backgroundColor: Colors.white,
                body: SingleChildScrollView(
                  child: PetsafeLoginForm()
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
