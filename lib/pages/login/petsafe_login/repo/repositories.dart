import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';

dynamic responseData;
Future<dynamic> emailPasswordPost(context,emailText,passwordText) async {
  var emaildata = {
    'email': emailText.text,
    'password': passwordText.text,
    'source': 'app',
  };
  try {
    final progress = ProgressHUD.of(context);
    progress?.showWithText('');
    String url = callApiUrl("email_password_verify");
    Response response = await Dio().post(url,
        data: emaildata,
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        }));

    if (response.statusCode == 201) {
      progress?.dismiss();
      responseData = jsonDecode(response.toString());
      print("API data: $responseData");
    }
  } catch (e) {
    print(e);
  }
  return Future<dynamic>.value(responseData);
}

Future<dynamic> mobilePasswordPost(
    context, dropdownValue, phoneNumberText, passwordText) async {
  String countrycode = "";
  countrycode = dropdownValue!.substring(1);
  var data = {
    "country_code": countrycode,
    "mobile": phoneNumberText.text,
    "password": passwordText.text,
    "source": "app"
  };
  try {
    final progress = ProgressHUD.of(context);
    progress?.showWithText('');
    String url = callApiUrl("mobile_password_verify");
    Response response = await Dio().post(url,
        data: data,
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        }));

    if (response.statusCode == 201) {
      progress?.dismiss();
      responseData = jsonDecode(response.toString());
      print(responseData);
    }
  } catch (e) {
    print(e);
  }
  return Future<dynamic>.value(responseData);
}

String callApiUrl(String url) {
  url = 'http://test.yous.tw:9487/normal/user/' + url;
  return url;
}
