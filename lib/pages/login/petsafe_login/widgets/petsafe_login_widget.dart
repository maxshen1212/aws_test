import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sqflite/sqflite.dart';
import '../../../../database/sqlite.dart';
import '../../../../utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_event.dart';
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_state.dart';
import 'package:pet_save_app/pages/login/petsafe_login/blocs/petsafe_login_bloc.dart';
import '../../forget_password/forget_password.dart';
import '../repo/repositories.dart';

final TextEditingController emailText = new TextEditingController();
final TextEditingController passwordText = new TextEditingController();
final TextEditingController countryCodeText = new TextEditingController();
final TextEditingController phoneNumberText = new TextEditingController();
String? dropdownValue = "+886";
dynamic responseData;

class PetsafeLoginForm extends StatefulWidget {
  const PetsafeLoginForm({super.key});

  @override
  State<PetsafeLoginForm> createState() => _PetsafeLoginFormState();
}

final _formKey = GlobalKey<FormState>();

class _PetsafeLoginFormState extends State<PetsafeLoginForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        petSafeLogo('iclogo-1'),
        petSafeLogoText('會員登入'),
        Container(
          margin: EdgeInsets.only(top: 5.h),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  height: 50.h,
                  padding: EdgeInsets.only(right: 60.w),
                  child: BlocBuilder<PetSafeLoginBlocs, PetSafeLoginStates>(
                    builder: (context, state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          //icon
                          petIconUser('icon-user'),
                          //電子郵件button
                          petEmailButton(
                              context,
                              '電子郵件',
                              BlocProvider.of<PetSafeLoginBlocs>(context)
                                  .state
                                  .onEmailbuttonColor,
                              BlocProvider.of<PetSafeLoginBlocs>(context)
                                  .state
                                  .onEmailTextColor),
                          //會員手機button
                          petPhoneNumberButton(
                              context,
                              "會員手機",
                              BlocProvider.of<PetSafeLoginBlocs>(context)
                                  .state
                                  .onPhoneNumnerbuttonColor,
                              BlocProvider.of<PetSafeLoginBlocs>(context)
                                  .state
                                  .onPhoneNumnerTextColor),
                        ],
                      );
                    },
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 60.w),
                    child: BlocBuilder<PetSafeLoginBlocs, PetSafeLoginStates>(
                        builder: (context, state) {
                      return Column(
                        children: [
                          BlocProvider.of<PetSafeLoginBlocs>(context)
                                  .state
                                  .loginstate
                              ? textFieldEmail(
                                  "${BlocProvider.of<PetSafeLoginBlocs>(context).state.textFieldEmailText}")
                              : textFieldPhoneNumber(
                                  "${BlocProvider.of<PetSafeLoginBlocs>(context).state.textFieldEmailText}"),
                          //密碼text
                          Container(
                            margin: EdgeInsets.only(top: 20.h),
                            child: reusableText("icon-lock", "密碼"),
                          ),

                          //密碼textField
                          BlocBuilder<PetSafePasswordBlocs,
                              PetSafePasswordStates>(builder: (context, state) {
                            return textFieldPassword(
                                context,
                                "請輸入您的密碼",
                                BlocProvider.of<PetSafePasswordBlocs>(context)
                                    .state
                                    .passwordState);
                          })
                        ],
                      );
                    })),
                Container(
                  width: 250.w,
                  margin: EdgeInsets.only(top: 18.h),
                  //登入button
                  child: logInButton(context, '登入'),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //會員註冊button
                      SizedBox(
                        width: 100.w,
                        child: registerTextButton(context, '會員註冊'),
                      ),

                      Container(
                        width: 100.w,
                        //忘記密碼button
                        child: forgetPasswordTextButton('忘記密碼', context),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

Widget reusableText(String iconName, String text) {
  return Container(
    // margin: EdgeInsets.only(bottom: 5.h),
    child: Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 15.w),
          width: 20.w,
          height: 20.h,
          child: Image.asset("assets/appdesign/images/$iconName.png"),
        ),
        Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.normal,
            fontSize: 17.sp,
          ),
        ),
      ],
    ),
  );
}

Widget textFieldPassword(BuildContext context, String hintText, state) {
  return Container(
      width: 340.h,
      margin: EdgeInsets.only(bottom: 5.h),
      child: Row(
        children: [
          SizedBox(
            width: 260.w,
            height: 38.h,
            child: Container(
              margin: EdgeInsets.only(top: 10.h),
              child: TextField(
                inputFormatters: [
                  FilteringTextInputFormatter.allow(
                      RegExp('[a-z A-Z 0-9]')), //限制只能输入中文和英文
                ],
                controller: passwordText,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  hintText: hintText,
                  isDense: true,
                  suffixIcon: Container(
                    child: IconButton(
                      onPressed: () {
                        BlocProvider.of<PetSafePasswordBlocs>(context)
                            .add(OnPassword());
                      },
                      icon: Image.asset(
                        'assets/appdesign/images/btnicpweye-3kd.png',
                        width: 30.w,
                      ),
                    ),
                  ),
                ),
                style: TextStyle(
                    fontFamily: "Avenir",
                    fontWeight: FontWeight.normal,
                    fontSize: 15.sp),
                autocorrect: false,
                autofocus: false,
                obscureText: state == 0 ? true : false,
              ),
            ),
          ),
        ],
      ));
}

Widget textFieldEmail(String hintText) {
  return Container(
      width: 340.h,
      margin: EdgeInsets.only(bottom: 10.h),
      child: Row(
        children: [
          SizedBox(
            width: 260.w,
            height: 30.h,
            child: TextField(
              controller: emailText,
              inputFormatters: [
                FilteringTextInputFormatter.allow(
                    RegExp('[a-z @ . A-Z 0-9]')), //限制只能输入中文和英文
              ],
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: hintText,
                isDense: true,
                // errorText: "sss"
              ),
              style: TextStyle(
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.normal,
                  fontSize: 15.sp),
              autocorrect: false,
              autofocus: false,
            ),
          ),
        ],
      ));
}

Widget textFieldPhoneNumber(String hintText) {
  return Container(
    width: 340.h,
    // height: 40.h,
    margin: EdgeInsets.only(bottom: 10.h),
    child: Row(
      children: [
        SizedBox(
          width: 260.w,
          height: 30.h,
          child: TextField(
            controller: phoneNumberText,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly, //限制只能输入中文和英文
            ],
            keyboardType: TextInputType.multiline,

            decoration: InputDecoration(
              hintText: hintText,
              isDense: true,
              prefixIcon: Container(
                  width: 70.w,
                  margin: EdgeInsets.only(right: 5.w),
                  child: const Row(
                    children: [CountryCode()],
                  )),
            ),
            //prefixIconConstraints: BoxConstraints(minWidth: 0, minHeight: 0),
            style: TextStyle(
                fontFamily: "Avenir",
                fontWeight: FontWeight.normal,
                fontSize: 15.sp),
            autocorrect: false,
            autofocus: false,
          ),
        ),
      ],
    ),
  );
}

class CountryCode extends StatefulWidget {
  const CountryCode({super.key});
  @override
  State<CountryCode> createState() => _CountryCodeState();
}

class _CountryCodeState extends State<CountryCode> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70.w,
      child: Container(
        padding: EdgeInsets.only(left: 3.w),
        decoration: BoxDecoration(
          border: Border.all(color: const Color(0xff000000)),
          borderRadius: BorderRadius.circular(5),
        ),
        child: DropdownButton(
          borderRadius: BorderRadius.circular(8.r),
          value: dropdownValue,
          hint: const Text(
            '國碼',
          ),
          iconSize: 20,
          isDense: true,
          onChanged: (newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
          items: <String>['+886', '+852', '+60']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              alignment: Alignment.centerLeft,
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}

Widget countryCode() {
  return Container(
    width: 60.w,
    height: 20.h,
    child: DropdownButton(
      hint: const Text(
        '國碼',
      ),
      // iconSize: 1,
      isDense: true,
      icon: Image.asset(
          'assets/appdesign/images/btncountrycodesresearch-G5P.png'),
      onChanged: (value) {},
      items: <String>['+886', '+852', '+60']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    ),
  );
}

Widget countryCodeButton() {
  return Container(
    width: 20.w,
    child: TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        child: Image.asset(
          'assets/appdesign/images/btncountrycodesresearch-G5P.png',
          width: 13.w,
          height: 20.h,
        ),
      ),
    ),
  );
}

Widget petSafeLogo(String iconName) {
  return Container(
    margin: EdgeInsets.only(top: 130.h),
    width: 200.w,
    height: 150.h,
    child: Image.asset(
      'assets/appdesign/images/$iconName.png',
    ),
  );
}

Widget petSafeLogoText(String logoname) {
  return Center(
    child: Container(
      margin: EdgeInsets.only(top: 20.h),
      child: Text(
        logoname,
        style: SafeGoogleFont('Inter',
            fontSize: 21.sp,
            fontWeight: FontWeight.w500,
            height: 1.h,
            letterSpacing: 10.r,
            color: const Color(0xff000000),
            decoration: TextDecoration.none),
      ),
    ),
  );
}

Widget petIconUser(String logoname) {
  return Container(
    width: 40.w,
    height: 20.h,
    child: Image.asset(
      'assets/appdesign/images/$logoname.png',
    ),
  );
}

Widget petEmailButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return Container(
    width: 75.w,
    height: 25.h,
    margin: EdgeInsets.only(left: 20.w),
    child: TextButton(
        onPressed: () {
          BlocProvider.of<PetSafeLoginBlocs>(context).add(OnEmail());
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Color(buttoncolor),
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.8.r,
                  color: Color(textbutton),
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Widget petPhoneNumberButton(
    BuildContext context, String text, buttoncolor, textbutton) {
  return Container(
    width: 75.w,
    height: 25.h,
    margin: EdgeInsets.only(left: 20.w),
    child: TextButton(
      onPressed: () {
        BlocProvider.of<PetSafeLoginBlocs>(context).add(OnPhoneNumber());
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Container(
        // margin: EdgeInsets.only(left: 14.w),
        decoration: BoxDecoration(
          color: Color(buttoncolor),
          borderRadius: BorderRadius.circular(6.r),
        ),
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: SafeGoogleFont('Inter',
                fontSize: 15.sp,
                fontWeight: FontWeight.w500,
                letterSpacing: 0.8.r,
                color: Color(textbutton),
                decoration: TextDecoration.none),
          ),
        ),
      ),
    ),
  );
}

Widget logInButton(BuildContext context, String text) {
  return TextButton(
    onPressed: () async {
      bool loginstate =
          BlocProvider.of<PetSafeLoginBlocs>(context).state.loginstate;

      // 下面兩行帳號密碼為測試用
      emailText.text = "test@gmail.com";
      passwordText.text = "1234";

      if (loginstate) {
        //emailPasswordPost api
        if ((emailText.text != '' && passwordText.text != '')) {
          Map<String, dynamic> emailData =
              await emailPasswordPost(context, emailText, passwordText);
          bool emailSuccess = emailData['success'];
          dynamic emailResult = emailData['result'];
          dynamic emailresultVerify = emailResult['verify'];
          if (emailSuccess) {
            if (emailresultVerify) {
              // readJsonToInsertSqlite("acupoint", "acupoint");
              // readJsonToInsertSqlite("meridian", "meridian");
              WidgetsFlutterBinding.ensureInitialized();
              // print(emailData.runtimeType);

              final sqlite = Sqlite();
              var test = await sqlite.query("member");
              // print("test1: $test");

              emailData["result"].remove("verify");
              await sqlite.insert("member", emailData["result"]);
              test = await sqlite.query("member");
              // print("test2: $test");

              Navigator.of(context)
                  .pushNamedAndRemoveUntil("home", (route) => false);
              toast("登入成功");
            } else {
              toast("密碼錯誤 請重新輸入");
            }
          } else {
            toast("此帳號尚未註冊");
          }
        } else {
          toast("輸入匡請勿空白");
        }
      } else {
        if ((phoneNumberText.text != '' && passwordText.text != '')) {
          //mobilePasswordPost api
          dynamic mobileData = await mobilePasswordPost(
              context, dropdownValue, phoneNumberText, passwordText);
          bool mobileSuccess = mobileData['success'];
          dynamic mobileResult = mobileData['result'];
          dynamic mobileresultVerify = mobileResult['verify'];
          if (mobileSuccess) {
            if (mobileresultVerify) {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil("home", (route) => false);
              toast("登錄成功");
            } else {
              toast("密碼錯誤 請重新輸入");
            }
          } else {
            toast("此帳號尚未註冊");
          }
        } else {
          toast("輸入匡請勿空白");
        }
      }
    },
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Container(
      height: 30.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: const LinearGradient(
          begin: Alignment(1, -1),
          end: Alignment(-1, -1),
          colors: <Color>[Color(0xff601986), Color(0xffe4007f)],
          stops: <double>[0, 1],
        ),
      ),
      child: Center(
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: SafeGoogleFont(
              'Inter',
              fontSize: 16.sp,
              fontWeight: FontWeight.w700,
              letterSpacing: 8.r,
              color: const Color(0xffffffff),
            ),
          ),
        ),
      ),
    ),
  );
}

Widget registerTextButton(BuildContext context, String text) {
  return TextButton(
    onPressed: () {
      Navigator.of(context).pushNamed("register_mail_and_phone");
    },
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Text(
      text,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.8.r,
        decoration: TextDecoration.underline,
        color: const Color(0xff000000),
        decorationColor: const Color(0xff000000),
      ),
    ),
  );
}

Widget forgetPasswordTextButton(String text, BuildContext context) {
  return TextButton(
    onPressed: () {
      showAlertDialogForgetPassword(context);
    },
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Text(
      text,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.8.r,
        decoration: TextDecoration.underline,
        color: const Color(0xff000000),
        decorationColor: const Color(0xff000000),
      ),
    ),
  );
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
