import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/login/update_password/widgets/update_password_widget.dart';

import '../../../utils.dart';

class UpdatePassword extends StatefulWidget {
  const UpdatePassword({super.key});

  @override
  State<UpdatePassword> createState() => _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
          body: SingleChildScrollView(
              child: Center(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30.h),
                  child: Text(
                    '修改密碼',
                    style: SafeGoogleFont('Inter',
                        fontSize: 21.sp,
                        fontWeight: FontWeight.w500,
                        height: 1.h,
                        letterSpacing: 2.r,
                        color: const Color(0xff000000),
                        decoration: TextDecoration.none),
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 40.h,
                      ),
                      UpdatePasswordForm(),
                      updatePasswordSendButton(context),
                    ],
                  ),
                ),
              ],
            ),
          )),
        ),
      ),
    );
  }
}
