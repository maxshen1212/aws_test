import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  String textStr = "";
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    controller.addListener(() {
      setState(() {
        textStr = controller.text;
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
        children: [TextField(controller: controller), Text(textStr)],
        );
  }
}

Widget reusableText(String text) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Row(
      children: [
        // Image.asset("assets/icons/signin/$iconName.png"),
        Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.normal,
            fontSize: 16.sp,
          ),
        ),
      ],
    ),
  );
}

String password = "";
String confirmPassword = "";

class UpdatePasswordForm extends StatefulWidget {
  const UpdatePasswordForm({super.key});

  @override
  State<UpdatePasswordForm> createState() => _UpdatePasswordFormState();
}

final _formKey = GlobalKey<FormState>();

class _UpdatePasswordFormState extends State<UpdatePasswordForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 325.h,
      // height: 50.h,
      margin: EdgeInsets.only(left: 30.w, bottom: 15.h),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            reusableText("輸入新密碼"),
            SizedBox(
              width: 300.w,
              child: TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                        RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
                  ],
                  keyboardType: TextInputType.multiline,
                  validator: (passwordValue) {
                    if (passwordValue == null || passwordValue.isEmpty) {
                      password = passwordValue!;
                      return '請勿為空';
                    } else {
                      password = passwordValue;

                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                    hintText: '輸入新密碼',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blueGrey),
                    ),
                  ),
                  style: TextStyle(
                      fontFamily: "Avenir",
                      fontWeight: FontWeight.normal,
                      fontSize: 14.sp),
                  autocorrect: false,
                  obscureText: true),
            ),
            SizedBox(
              height: 10.h,
            ),
            reusableText("再次輸入新密碼"),
            SizedBox(
              width: 300.w,
              child: TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                        RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
                  ],
                  keyboardType: TextInputType.multiline,
                  validator: (confirmPasswordValue) {
                    if (confirmPasswordValue == null ||
                        confirmPasswordValue.isEmpty) {
                      confirmPassword = confirmPasswordValue!;
                      return '請勿為空';
                    } else {
                      confirmPassword = confirmPasswordValue;
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                    hintText: '輸入新密碼',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blueGrey),
                    ),
                  ),
                  style: TextStyle(
                      fontFamily: "Avenir",
                      fontWeight: FontWeight.normal,
                      fontSize: 14.sp),
                  autocorrect: false,
                  obscureText: true),
            ),
          ],
        ),
      ),
    );
  }
}

Widget updatePasswordSendButton(context) {
  return Center(
    child: SizedBox(
      width: 100.w,
      height: 40.w,
      child: ElevatedButton(
        onPressed: () async {
          print(password);
          print(confirmPassword);
          if (_formKey.currentState!.validate()) {
            if (password == confirmPassword) {
              showAlertDialogUpdatePassword(context);
              _showTimer(context);
            } else {
              toast("密碼不一致，請重新輸入");
            }
          }
        },
        style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xff929292),
            foregroundColor: const Color(0xffe4007f)),
        child: Text(
          '確認',
          style: TextStyle(
              fontFamily: "Avenir",
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontSize: 14.sp),
        ),
      ),
    ),
  );
}

Future<void> showAlertDialogUpdatePassword(BuildContext context) {
  return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: SizedBox(
            height: 200.h,
            child: Column(children: [
              Container(
                margin: EdgeInsets.only(top: 30.h),
                width: 110.w,
                child: Stack(
                  children: [
                    Image.asset('assets/appdesign/images/忘記密碼圓.png'),
                    Center(
                      child: Container(
                        width: 58.w,
                        margin: EdgeInsets.only(top: 35.h),
                        child: Image.asset('assets/appdesign/images/忘記密碼勾.png'),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 18.h),
                child: Text('新密碼設置完畢\n請以新密碼登入帳號',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Inter',
                        fontSize: 18.sp,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1)),
              ),

              // margin: EdgeInsets.only(left: 110.w),
            ]),
          ),
        );
      });
}

_showTimer(context) {
  Timer.periodic(const Duration(milliseconds: 1000), (timer) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil("petsafe_login", (route) => false);
    timer.cancel(); //取消定時器
  });
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
