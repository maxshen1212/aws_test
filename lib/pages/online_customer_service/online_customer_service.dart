import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'widgets/online_customer_service_widgets.dart';

final TextEditingController messageText = new TextEditingController();
ScrollController _msgController = new ScrollController();

class OnlineCustomerService extends StatefulWidget {
  const OnlineCustomerService({super.key});
  @override
  State<OnlineCustomerService> createState() => _OnlineCustomerServiceState();
}

class _OnlineCustomerServiceState extends State<OnlineCustomerService> {
  File? _selectedImage;

  @override
  Future _pickImageFromGallery() async {
    final returnImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (returnImage == null) return;
    setState(() {
      _selectedImage = File(returnImage!.path);
    });
  }

  Future _pickImageFromCamera() async {
    final returnImage =
        await ImagePicker().pickImage(source: ImageSource.camera);
    if (returnImage == null) return;
    setState(() {
      _selectedImage = File(returnImage.path);
    });
  }

  bool sendState = true;
  bool onImage = true;
  void updateForm() {
    setState(() {
      sendState = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: onlineCustomerService(context, "線上客服"),
      body: Column(children: [
        Expanded(
          flex: 1,
          child: Container(
              alignment: Alignment.topCenter,
              child: sendState == true ? renderList() : SizedBox()),
        ),
        Container(
          constraints: BoxConstraints(minHeight: 60.h, maxHeight: 150.h),
          decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(216, 191, 197, 197),
                spreadRadius: 2,
                blurRadius: 5,
              ),
            ],
            color: Colors.white,
          ),
          child: Container(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              pictureBottom(_pickImageFromGallery),
              cameraBottom(_pickImageFromCamera),
              buildTextField("請輸入您的文字", messageText),
              sendBottom(messageText, updateForm, messageText),
            ],
          )),
        )
      ]),
    );
  }

  renderList() {
    return GestureDetector(
      child: ListView.builder(
          controller: _msgController,
          padding: EdgeInsets.only(top: 10.h, bottom: 20.h),
          //shrinkWrap: true,
          itemCount: messages.length,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              child: messages[index].isMe
                  ? renderRowSendByMe(context, index, messages, _selectedImage)
                  : renderRowSendByOthers(context, index, messages),
              onTap: () {},
            );
          }),
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
    );
  }
}

void scrollMsgBottom() {
  final timer = Timer(Duration(milliseconds: 100),
      () => _msgController.jumpTo(_msgController.position.maxScrollExtent));
}
