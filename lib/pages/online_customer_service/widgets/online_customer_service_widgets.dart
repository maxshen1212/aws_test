import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';
import '../../../common/values/colors.dart';
import '../../../utils.dart';
import '../online_customer_service.dart';

class Message {
  Message(this.message, this.isMe);
  final String message;
  final bool isMe;
}

dynamic messages = getMessages();
var list = [];
List getMessages() {
  return list;
}

AppBar onlineCustomerService(
  BuildContext context,
  String names,
) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      //child: goBack(),
    ),
    //actions: [setUp(context)],
    flexibleSpace: Container(
      color: Color(0xffaacd06),
    ),
    title: Text(
      names,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 23.sp,
        fontWeight: FontWeight.bold,
        letterSpacing: 10.w,
        color: Color.fromARGB(255, 255, 255, 255),
      ),
    ),
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget pictureBottom(Function pickImageFromGallery) {
  return Container(
      margin: EdgeInsets.only(left: 5.w),
      width: 45.w,
      child: TextButton(
        onPressed: () {
          pickImageFromGallery();
        },
        child: Image.asset("assets/appdesign/images/btnimage.png"),
      ));
}

Widget cameraBottom(Function pickImageFromCamera) {
  return Container(
      margin: EdgeInsets.only(left: 5.w),
      width: 45.w,
      child: TextButton(
        onPressed: () {
          pickImageFromCamera();
        },
        child: Image.asset("assets/appdesign/images/btncamera.png"),
      ));
}

ScrollController _msgController = new ScrollController();
Widget sendBottom(messageText, state, message) {
  return StatefulBuilder(builder: (context, setState) {
    return Container(
        width: 45.w,
        child: TextButton(
          onPressed: () {
            if (messageText.text == "") {
            } else {
              list.add(Message(messageText.text, true));
              list.add(Message("已收到", false));
              message.clear();
              state();
              scrollMsgBottom();
            }
          },
          child: Image.asset("assets/appdesign/images/btnenter.png"),
        ));
  });
}

Widget renderRowSendByOthers(BuildContext context, index, messages) {
  return Column(
    children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10.h),
                    width: 40.w,
                    height: 30.h,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage("assets/images/cat.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 1000.0),
                    child: Container(
                      margin: EdgeInsets.only(top: 8.h, right: 10.w),
                      decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0.0, 2.0),
                              color: Colors.grey,
                              blurRadius: 2,
                            )
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      padding: EdgeInsets.all(10),
                      child: Text(
                        messages[index].message,
                        softWrap: true,
                        style: TextStyle(color: Colors.black, fontSize: 15.sp),
                      ),
                    ),
                  )
                ],
              )
            ],
          )
        ],
      )
    ],
  );
}

Widget renderRowSendByMe(BuildContext context, index, messages, selectedImage) {
  return Column(
    children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 200.0.w),
                    child: Container(
                        margin: EdgeInsets.only(top: 8.h, right: 10.w),
                        decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0.0, 2.0),
                                color: Colors.grey,
                                blurRadius: 1,
                              )
                            ],
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        padding: EdgeInsets.all(10),
                        child: Container(
                          child: Text(
                            messages[index].message,
                            softWrap: true,
                            style:TextStyle(color: Colors.black, fontSize: 15.sp),
                          ),
                        )),
                  ),
                ],
              )
            ],
          )
        ],
      )
    ],
  );
}

Widget buildTextField(String hintText, messageText) {
  return Container(
      constraints: BoxConstraints(minHeight: 30.h, maxHeight: 150.h),
      margin: EdgeInsets.only(left: 10.w, top: 10.h, bottom: 10.h),
      //height: 30.h,
      width: 200.w,
      decoration: BoxDecoration(
        color: const Color(0xffececec),
        borderRadius: BorderRadius.all(Radius.circular(5.w)),
      ),
      child: Container(
        margin: EdgeInsets.only(left: 10.w),
        child: TextField(
          maxLines: null,
          controller: messageText,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: hintText,
            isDense: true,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
          ),
          style: SafeGoogleFont(
            'Inter',
            fontSize: 12.sp,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
          autocorrect: false,
          obscureText: false,
        ),
      ));
}

// class FullImageWidget extends StatelessWidget {
//   final String imageUrl;
//   const FullImageWidget({Key? key, required this.imageUrl}) : super(key: key);

//   //保存照片
//   // _saveImage() async {
//   //   var response = await Dio().get(
//   //       imageUrl,
//   //       options: Options(responseType: ResponseType.bytes));
//   //   final result = await ImageGallerySaver.saveImage(
//   //       Uint8List.fromList(response.data),
//   //       name: "hello");
//   //   if(result['isSuccess']){
//   //     //Toast.showToast("照片保存成功", ToastGravity.CENTER);
//   //   }
//   // }

//   //长摁保存照片组件
//   _showSaveVideoWidget(BuildContext context) async {
//     showModalBottomSheet(
//         context: context,
//         isDismissible: true,
//         isScrollControlled: false,
//         shape: const RoundedRectangleBorder(
//             borderRadius: BorderRadius.only(
//                 topLeft: Radius.circular(15), topRight: Radius.circular(15))),
//         builder: (BuildContext context) {
//           return Container(
//             decoration: const BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(15),
//                     topRight: Radius.circular(15))),
//             //height: ScreenAdapter.height(400),
//             child: Column(
//               children: [
//                 //Container(padding: EdgeInsets.all(ScreenAdapter.height(40)),child:  Text(textAlign:TextAlign.center,"保存照片到相册",maxLines:2,style: TextStyle(color: const Color.fromRGBO(102, 102, 102, 1),fontSize: ScreenAdapter.size(25)),),),
//                 //Divider(height: ScreenAdapter.height(0.5)),
//                 InkWell(
//                   //child:Padding(padding: EdgeInsets.all(ScreenAdapter.height(18)),child: Center(child: Text("保存照片",style: TextStyle(color: Colors.red,fontSize: ScreenAdapter.size(30)),),),),
//                   onTap: () async {
//                     //_saveImage();
//                     Navigator.pop(context);
//                   },
//                 ),
//                 //Container(color: const Color.fromRGBO(245, 245, 245,1),height: ScreenAdapter.height(15)),
//                 // InkWell(
//                 //   onTap: (){
//                 //     Navigator.pop(context);
//                 //   },
//                 //   //child:Padding(padding:  EdgeInsets.fromLTRB(ScreenAdapter.width(0),ScreenAdapter.height(10),ScreenAdapter.width(0),ScreenAdapter.height(15)),child:  Text("取消",style: TextStyle(color: const Color.fromRGBO(51, 51, 51, 1),fontSize: ScreenAdapter.size(30)),)),),
//               ],
//             ),
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.black87,
//       body: GestureDetector(
//         child: Center(
//             child: PhotoView(
//           imageProvider: NetworkImage(imageUrl),
//         )),
//         onTap: () {
//           Navigator.pop(context);
//         },
//         //长摁弹出保存照片界面
//         onLongPress: () {
//           _showSaveVideoWidget(context);
//         },
//       ),
//     );
//   }
// }
