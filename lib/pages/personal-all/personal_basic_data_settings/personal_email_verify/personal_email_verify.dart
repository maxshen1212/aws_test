import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import 'widgets/personal_email_verify_widgets.dart';

class PersonalEmailVerify extends StatefulWidget {
  const PersonalEmailVerify({super.key});

  @override
  State<PersonalEmailVerify> createState() => _PersonalEmailVerifyState();
}

class _PersonalEmailVerifyState extends State<PersonalEmailVerify> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: personalEmailVerifyAppBar(context, "E-mail認證"),
      body: personalEmailVerifyBody(context),
    );
  }
}

