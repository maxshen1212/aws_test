import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../../common/values/colors.dart';
import '../../../../../utils.dart';
import '../../../../login/update_password/widgets/update_password_widget.dart';

AppBar personalEmailVerifyAppBar(
  BuildContext context,
  String names,
) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
      color: Color(0xffaacd06),
    ),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget personalEmailVerifyBody(context) {
  final TextEditingController emailController = TextEditingController();

  return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
    Container(
      margin: EdgeInsets.only(left: 30.w, top: 20.h, bottom: 10.h),
      child: Text(
        '電子郵件驗證',
        style: SafeGoogleFont(
          'Inter',
          fontSize: 16.sp,
          fontWeight: FontWeight.w200,
          height: 1.2125,
          letterSpacing: 0.8,
          color: Color(0xff000000),
        ),
      ),
    ),
    Container(
      margin: EdgeInsets.only(bottom: 10.h),
      child: Center(child: buildTextField("文字輸入", emailController)),
    ),
    Center(child: sendVerifyCodeButton("發送驗證碼", context, emailController))
  ]);
}

String password = "";
Widget buildTextField(String hintText, TextEditingController emailController) {
  return Form(
      key: _formKey,
      child: Container(
        width: 300.w,
        child: TextFormField(
            controller: emailController,
            inputFormatters: [
              FilteringTextInputFormatter.allow(
                  RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
            ],
            keyboardType: TextInputType.multiline,
            validator: (passwordValue) {
              if (passwordValue == null || passwordValue.isEmpty) {
                password = passwordValue!;
                return '請勿為空';
              } else {
                password = passwordValue;
                return null;
              }
            },
            decoration: const InputDecoration(
              hintText: '文字輸入',
              isDense: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.transparent),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
            ),
            style: TextStyle(
                fontFamily: "Avenir",
                fontWeight: FontWeight.normal,
                fontSize: 14.sp),
            autocorrect: false,
            obscureText: false),
      ));
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

final _formKey = GlobalKey<FormState>();
Widget sendVerifyCodeButton(
    String text, context, TextEditingController emailController) {
  return Container(
    width: 95.w,
    height: 35.h,
    margin: EdgeInsets.only(top: 10.w),
    child: TextButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            print(emailController.text);
            showAlertDialogUpdatePassword(context).then((_) {
              Navigator.pop(context);
            });
          } else {}
        },
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Container(
          // margin: EdgeInsets.only(left: 14.w),
          decoration: BoxDecoration(
            color: Color(0xffaacd06),
            borderRadius: BorderRadius.circular(8.r),
          ),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: SafeGoogleFont('Inter',
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8.r,
                  color: Colors.white,
                  decoration: TextDecoration.none),
            ),
          ),
        )),
  );
}

Future<void> showAlertDialogUpdatePassword(BuildContext context) {
  return showDialog<void>(
      context: context,
      //barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: SizedBox(
            height: 220.h,
            child: Column(children: [
              Container(
                margin: EdgeInsets.only(top: 30.h),
                width: 110.w,
                child: Stack(
                  children: [
                    Image.asset('assets/appdesign/images/忘記密碼圓.png'),
                    Center(
                      child: Container(
                        width: 58.w,
                        margin: EdgeInsets.only(top: 35.h),
                        child: Image.asset('assets/appdesign/images/忘記密碼勾.png'),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 18.h),
                child: Text('信箱發送成功',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xffaacd06),
                        fontFamily: 'Inter',
                        fontSize: 24.sp,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1)),
              ),
              // Container(
              //   margin: EdgeInsets.only(top: 18.h),
              //   child: Text('請至信箱收取認證郵件',
              //       textAlign: TextAlign.center,
              //       style: TextStyle(
              //           color: Colors.black,
              //           fontFamily: 'Inter',
              //           fontSize: 18.sp,
              //           letterSpacing: 0,
              //           fontWeight: FontWeight.normal,
              //           height: 1)),
              // ),

              // margin: EdgeInsets.only(left: 110.w),
            ]),
          ),
        );
      });
}

_showTimer(context) {
  Timer.periodic(const Duration(milliseconds: 1000), (timer) {
    timer.cancel(); //取消定時器
  });
}
