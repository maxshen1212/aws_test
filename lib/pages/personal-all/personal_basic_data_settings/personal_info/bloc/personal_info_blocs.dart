import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_events.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_states.dart';

class PersonalinfoBloc extends Bloc<PersonalInfoEvent, PersonalinfoState> {
  PersonalinfoBloc() : super(PersonalinfoState()) {
    on<IsNullstate>(_Nullornot);
    on<IsnotNullstate>(_IsnotNullstate);
  }
  void _Nullornot(IsNullstate event, Emitter<PersonalinfoState> emit) {
    emit(state.copyWith(isNull: true));
  }

  void _IsnotNullstate(IsnotNullstate event, Emitter<PersonalinfoState> emit) {
    emit(state.copyWith(isNull: false));
  }
}
