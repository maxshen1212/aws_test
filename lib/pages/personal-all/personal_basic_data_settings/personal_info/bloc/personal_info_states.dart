class PersonalinfoState {
  bool isNull = false;
  PersonalinfoState copyWith({required bool isNull}) {
    return PersonalinfoState()..isNull = isNull;
  }
}
