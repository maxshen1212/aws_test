import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/widgets/personal_info_wedgits.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';

class PersonalInfo extends StatefulWidget {
  const PersonalInfo({super.key});
  @override
  // ignore: no_logic_in_create_state
  State<PersonalInfo> createState() => _PersonalInfoState();
}

class _PersonalInfoState extends State<PersonalInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: personalInfoAppBar("個人資料", context),
        drawerEdgeDragWidth: 200, //用拉的也能彈出設定
        endDrawer: SizedBox(
          width: 90.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        body: bodyy());

    //bottomNavigationBar: bottomNavigation(),
  }
}
