import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_blocs.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_events.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_info/bloc/personal_info_states.dart';
import 'package:pet_save_app/utils.dart';

//appbar

PreferredSize personalInfoAppBar(String names, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      leading: goBack(),
      leadingWidth: 100.w,
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      automaticallyImplyLeading: false,
      backgroundColor: Color(0xffaacd06),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: Padding(
        padding: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Image.asset(
              'assets/appdesign/images/goback.png',
              width: 18.w,
              height: 16.h,
            ),
            const Text(
              "返回",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  });
}

class bodyy extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 20.h),
        padding: EdgeInsets.only(left: 30.w, right: 30.w),
        child: Form(
          key: _formKey,
          child: BlocBuilder<PersonalinfoBloc, PersonalinfoState>(
              builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                reusableText("個人代碼"),
                buildTextField('顯示會員個人代碼', '個人代碼'),
                reusableText("經銷商代碼"),
                buildTextField('請輸入介紹人代碼,共五碼,例:AA001', "經銷商代碼"),
                redText("一旦經銷商完成設定,即不可更改,請確認代碼正確後輸入。"),
                reusableText("介紹人代碼"),
                buildTextField('請輸入介紹人代碼,共五碼,例:AA001', "介紹人代碼"),
                reusableText("姓名"),
                buildTextField('文字輸入', "姓名"),
                redText("必填，請務必輸入真實姓名，以免影響您的會員寶貴權益，例如紅利積點以及未來累積健康里程的諸多優惠與福利。"),
                reusableText("性別"),
                choices(),
                reusableText("暱稱"),
                buildTextField('文字輸入', "暱稱"),
                reusableText("生日"),
                buildTextField('YYYY/MM/DD', "生日"),
                reusableText("住家電話"),
                buildTextField('住家電話', "住家電話"),
                reusableText("行動電話"),
                buildTextField('+886975768767', "行動電話"),
                reusableText("地址"),
                address(),
                address2(),
                state.isNull == true
                    ? Container(
                        margin: EdgeInsets.only(left: 20.w, bottom: 10.h),
                        child: const Text(
                          '請勿為空',
                          style: TextStyle(
                            color: Colors.red, // 错误消息文本的颜色
                            fontSize: 12.0, // 错误消息文本的大小
                          ),
                        ),
                      )
                    : SizedBox(),
                reusableText("是否意願收到系統信息"),
                choices2(),
                Center(
                  child: BlocBuilder<PersonalinfoBloc, PersonalinfoState>(
                      builder: (context, state) {
                    return Container(
                      margin: EdgeInsets.fromLTRB(110, 20, 110, 0),
                      child: TextButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate() &&
                              addressController.text != '') {
                            print('個人代碼${personCodeController.text}');
                            print('經銷商代碼${dealerCodeController.text}');
                            print('介紹人代碼${introduceController.text}');
                            print('姓名${nameController.text}');
                            if (sex == 1) {
                              print('男生');
                            } else
                              print('女生');
                            print('暱稱${snameController.text}');
                            print('生日${birthController.text}');
                            print('住家電話${homeNumberController.text}');
                            print('行動電話${personNumberController.text}');
                            print(selectedCity);
                            print(selectedDistrict);
                            print(addressController.text);
                            if (message == 1) {
                              print('同意');
                            } else {
                              print('不同意');
                            }
                          }
                          if (addressController.text == '') {
                            BlocProvider.of<PersonalinfoBloc>(context)
                                .add(IsNullstate());
                          }
                          if (addressController.text != '') {
                            BlocProvider.of<PersonalinfoBloc>(context)
                                .add(IsnotNullstate());
                          }
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.zero,
                        ),
                        child: Container(
                          width: double.infinity,
                          height: 40.h,
                          decoration: BoxDecoration(
                            color: Color(0xffaacd06),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Center(
                              child: Text(
                                '送出',
                                textAlign: TextAlign.center,
                                style: SafeGoogleFont(
                                  'Inter',
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w700,
                                  height: 1.2125.h,
                                  letterSpacing: 0.8,
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}

Widget reusableText(String text) {
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    child: Text(
      text,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125.h,
        letterSpacing: 0.8,
        color: const Color(0xff000000),
      ),
    ),
  );
}

Widget redText(String text) {
  return Container(
    margin: EdgeInsets.only(bottom: 10.h, left: 10.w),
    child: Text(
      text,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 11.sp,
        fontWeight: FontWeight.w200,
        height: 1.2125.h,
        letterSpacing: 0.55,
        color: const Color(0xffc30d23),
      ),
    ),
  );
}

String password = "";
final TextEditingController personCodeController = TextEditingController();
final TextEditingController dealerCodeController = TextEditingController();
final TextEditingController introduceController = TextEditingController();
final TextEditingController nameController = TextEditingController();
final TextEditingController snameController = TextEditingController();
final TextEditingController birthController = TextEditingController();
final TextEditingController homeNumberController = TextEditingController();
final TextEditingController personNumberController = TextEditingController();
TextEditingController selectedController = TextEditingController();

Widget buildTextField(String text, String which) {
  if (which == '個人代碼') {
    selectedController = personCodeController;
  } else if (which == '經銷商代碼') {
    selectedController = dealerCodeController;
  } else if (which == '介紹人代碼') {
    selectedController = introduceController;
  } else if (which == '姓名') {
    selectedController = nameController;
  } else if (which == '暱稱') {
    selectedController = snameController;
  } else if (which == '生日') {
    selectedController = birthController;
  } else if (which == '住家電話') {
    selectedController = homeNumberController;
  } else if (which == '行動電話') {
    selectedController = personNumberController;
  }
  return Container(
    margin: EdgeInsets.only(bottom: 15.h),
    width: 350.w,
    child: TextFormField(
        controller: selectedController,
        inputFormatters: [
          FilteringTextInputFormatter.allow(
              RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
        ],
        keyboardType: TextInputType.multiline,
        validator: (passwordValue) {
          if (passwordValue == null || passwordValue.isEmpty) {
            return '請勿為空';
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          hintText: text,
          isDense: true,
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(color: Colors.black),
          ),
        ),
        style: TextStyle(
            fontFamily: "Avenir",
            fontWeight: FontWeight.normal,
            fontSize: 14.sp),
        autocorrect: false,
        obscureText: false),
  );
}

int sex = 1;
Widget choices() {
  return StatefulBuilder(
    builder: (BuildContext context, setState) {
      return Row(
        children: <Widget>[
          Radio(
            // 按钮的值
            value: 1,
            // 改变事件
            onChanged: (value) {
              setState(() {
                sex = value!;
              });
            },
            // 按钮组的值
            groupValue: sex,
          ),
          Text("男"),
          SizedBox(
            width: 20,
          ),
          Radio(
            value: 2,
            onChanged: (value) {
              setState(() {
                sex = value!;
              });
            },
            groupValue: sex,
          ),
          Text("女"),
        ],
      );
    },
  );
}

int message = 1;
Widget choices2() {
  return StatefulBuilder(
    builder: (BuildContext context, setState) {
      return Row(
        children: <Widget>[
          Radio(
            // 按钮的值
            value: 1,
            // 改变事件
            onChanged: (value) {
              setState(() {
                message = value!;
              });
            },
            // 按钮组的值
            groupValue: message,
          ),
          const Text("願意"),
          const SizedBox(
            width: 20,
          ),
          Radio(
            value: 2,
            onChanged: (value) {
              setState(() {
                message = value!;
              });
            },
            groupValue: message,
          ),
          const Text("不願意"),
        ],
      );
    },
  );
}

//底部導覽列
Widget bottomNavigation() {
  return Container(
    margin: EdgeInsets.only(top: 600.h),
    child: Row(
      children: [
        Container(
          color: Colors.black,
        ),
        Container(
          color: Colors.blue,
        ),
      ],
    ),
  );
}

Widget address() {
  return StatefulBuilder(builder: (BuildContext context, setState) {
    return Column(
      children: [
        Container(
          width: 350.w, // 宽度
          height: 57.h, // 高度
          margin: EdgeInsets.only(top: 5.h),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 223, 223, 222),
            border: Border.all(color: Color.fromARGB(255, 146, 147, 147)),
            borderRadius: BorderRadius.circular(10), // 圆角半径
          ),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20.h, left: 17.w),
                child: Text(
                  '縣市',
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.8,
                    color: const Color(0xff000000),
                  ),
                ),
              ),
              Positioned.fill(
                  left: 70.w,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                    ),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      underline: Container(
                        height: 0.h,
                      ),
                      padding: EdgeInsets.only(left: 15.w),
                      value: selectedCity,
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedCity = newValue!;
                          selectedDistrict = city[selectedCity]!.first;
                          print(selectedCity);
                          print(selectedDistrict);
                        });
                      },
                      items: city.keys.map((String citychoice) {
                        return DropdownMenuItem<String>(
                          value: citychoice,
                          child: Text(citychoice),
                        );
                      }).toList(),
                    ),
                  ))
            ],
          ),
        ),
        Container(
          width: 350.w, // 宽度
          height: 57.h, // 高度
          margin: EdgeInsets.only(top: 5.h),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 223, 223, 222),
            border: Border.all(color: Color.fromARGB(255, 146, 147, 147)),
            borderRadius: BorderRadius.circular(10), // 圆角半径
          ),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20.h, left: 17.w),
                child: Text(
                  '鄉鎮市區',
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w200,
                    height: 1.2125.h,
                    letterSpacing: 0.8,
                    color: const Color(0xff000000),
                  ),
                ),
              ),
              Positioned.fill(
                left: 100.w,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                  child: DropdownButton<String>(
                      key: UniqueKey(),
                      isExpanded: true,
                      underline: Container(
                        height: 0.h,
                      ),
                      padding: EdgeInsets.only(left: 15.w),
                      value: selectedDistrict,
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedDistrict = newValue!;
                        });
                      },
                      items: city[selectedCity]!.map((String citychoice) {
                        return DropdownMenuItem<String>(
                          value: citychoice,
                          child: Text(citychoice),
                        );
                      }).toList()),
                ),
              )
            ],
          ),
        ),
      ],
    );
  });
}

final TextEditingController addressController = TextEditingController();
Widget address2() {
  return BlocBuilder<PersonalinfoBloc, PersonalinfoState>(
      builder: (context, state) {
    return Container(
      width: 350.w, // 宽度
      height: 57.h, // 高度
      margin: EdgeInsets.only(top: 5.h, bottom: 5.h),
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 223, 223, 222),
        border: state.isNull == false
            ? Border.all(color: Color.fromARGB(255, 146, 147, 147))
            : Border.all(color: Color.fromARGB(255, 230, 32, 45)),
        borderRadius: BorderRadius.circular(10), // 圆角半径
      ),
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 20.h, left: 17.w),
            child: Text(
              '地址',
              style: SafeGoogleFont(
                'Inter',
                fontSize: 16.sp,
                fontWeight: FontWeight.w200,
                height: 1.2125.h,
                letterSpacing: 0.8,
                color: const Color(0xff000000),
              ),
            ),
          ),
          Positioned.fill(
            left: 70.w,
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: Form(
                child: TextFormField(
                    controller: addressController,
                    keyboardType: TextInputType.multiline,
                    validator: (passwordValue) {
                      if (passwordValue == null || passwordValue.isEmpty) {
                        return '請勿為空';
                      } else {
                        return null;
                      }
                    },
                    decoration: const InputDecoration(
                      hintText: '文字輸入',
                      contentPadding: EdgeInsets.only(left: 10, top: 10),
                      isDense: true,
                      border: InputBorder.none,
                    ),
                    style: TextStyle(
                        fontFamily: "Avenir",
                        fontWeight: FontWeight.normal,
                        fontSize: 14.sp),
                    autocorrect: false,
                    obscureText: false),
              ),
            ),
          )
        ],
      ),
    );
  });
}

Map<String, List<String>> city = {
  '台北市': [
    '北投區',
    '士林區',
    '大同區',
    '中山區',
    '松山區',
    '內湖區',
    '萬華區',
    '中正區',
    '大安區',
    '信義區',
    '南港區',
    '文山區'
  ],
  '新北市': [
    '板橋區',
    '三重區',
    '中和區',
    '永和區',
    '新莊區',
    '新店區',
    '土城區',
    '蘆洲區',
    '汐止區',
    '樹林區',
    '鶯歌區',
    '三峽區',
    '淡水區',
    '瑞芳區',
    '五股區',
    '泰山區',
    '林口區',
    '八里區',
    '深坑區',
    '石碇區',
    '坪林區',
    '三芝區',
    '石門區',
    '金山區',
    '萬里區',
    '平溪區',
    '雙溪區',
    '貢寮區',
    '烏來區',
  ],
  '基隆市': [
    '仁愛區',
    '中正區',
    '信義區',
    '中山區',
    '安樂區',
    '七堵區',
    '暖暖區',
  ],
  '桃園市': [
    '桃園區',
    '中壢區',
    '平鎮區',
    '八德區',
    '大溪區',
    '楊梅區',
    '龜山區',
    '蘆竹區',
    '大園區',
    '觀音區',
    '新屋區',
    '龍潭區',
    '復興區',
  ],
  '台中市': [
    '中區',
    '東區',
    '西區',
    '南區',
    '北區',
    '西屯區',
    '南屯區',
    '北屯區',
    '豐原區',
    '大里區',
    '太平區',
    '清水區',
    '沙鹿區',
    '大甲區',
    '東勢區',
    '梧棲區',
    '烏日區',
    '神岡區',
    '大肚區',
    '大雅區',
    '后里區',
    '霧峰區',
    '潭子區',
    '龍井區',
    '外埔區',
    '和平區',
    '石岡區',
    '大安區',
    '新社區'
  ],
  '台南市': [
    '東區',
    '南區',
    '北區',
    '新營區',
    '永康區',
    '鹽水區',
    '白河區',
    '佳里區',
    '學甲區',
    '麻豆區',
    '下營區',
    '新化區',
    '善化區',
    '柳營區',
    '後壁區',
    '東山區',
    '六甲區',
    '官田區',
    '大內區',
    '西港區',
    '七股區',
    '將軍區',
    '北門區',
    '新市區',
    '安定區',
    '山上區',
    '玉井區',
    '楠西區',
    '南化區',
    '左鎮區',
    '仁德區',
    '歸仁區',
    '關廟區',
    '龍崎區',
    '中西區',
    '安南區',
    '安平區'
  ],
  '高雄市': [
    '鳳山區',
    '美濃區',
    '旗山區',
    '岡山區',
    '茄萣區',
    '永安區',
    '彌陀區',
    '梓官區',
    '橋頭區',
    '燕巢區',
    '大社區',
    '仁武區',
    '鳥松區',
    '大寮區',
    '林園區',
    '湖內區',
    '路竹區',
    '阿蓮區',
    '田寮區',
    '內門區',
    '杉林區',
    '甲仙區',
    '三民區',
    '桃源區',
    '茂林區',
    '六龜區',
    '大樹區',
    '楠梓區',
    '左營區',
    '鼓山區',
    '那瑪夏區',
    '鹽埕區',
    '前金區',
    '新興區',
    '苓雅區',
    '旗津區',
    '前鎮區',
    '小港區',
  ],
  '新竹市': [
    '東區',
    '北區',
    '香山區',
  ],
  '新竹縣': [
    '竹北市',
    '竹東鎮',
    '新埔鎮',
    '關西鎮',
    '新豐鄉',
    '峨眉鄉',
    '寶山鄉',
    '五峰鄉',
    '橫山鄉',
    '北埔鄉',
    '尖石鄉',
    '芎林鄉',
    '湖口鄉'
  ],
  '苗栗縣': [
    '苗栗市',
    '頭份市',
    '竹南鎮',
    '後龍鎮',
    '通霄鎮',
    '苑裡鎮',
    '卓蘭鎮',
    '造橋鄉',
    '西湖鄉',
    '頭屋鄉',
    '公館鄉',
    '銅鑼鄉',
    '三義鄉',
    '三灣鄉',
    '南庄鄉',
    '獅潭鄉',
    '大湖鄉',
    '山地鄉',
    '泰安鄉'
  ],
  '彰化縣': [
    '彰化市',
    '員林巿',
    '鹿港鎮',
    '和美鎮',
    '北斗鎮',
    '溪湖鎮',
    '田中鎮',
    '二林鎮',
    '線西鄉',
    '伸港鄉',
    '福興鄉',
    '秀水鄉',
    '花壇鄉',
    '芬園鄉',
    '大村鄉',
    '埔鹽鄉',
    '埔心鄉',
    '永靖鄉',
    '社頭鄉',
    '二水鄉',
    '田尾鄉',
    '埤頭鄉',
    '芳苑鄉',
    '大城鄉',
    '竹塘鄉',
    '溪州鄉'
  ],
  '南投縣': [
    '南投市',
    '埔里鎮',
    '草屯鎮',
    '竹山鎮',
    '集集鎮',
    '名間鄉',
    '中寮鄉',
    '鹿谷鄉',
    '水里鄉',
    '魚池鄉',
    '國姓鄉',
    '信義鄉',
    '仁愛鄉'
  ],
  '雲林縣': [
    '麥寮鄉',
    '崙背鄉',
    '二崙鄉',
    '西螺鎮',
    '莿桐鄉',
    '林內鄉',
    '臺西鄉',
    '東勢鄉',
    '褒忠鄉',
    '元長鄉',
    '土庫鎮',
    '大埤鄉',
    '虎尾鎮',
    '斗六市',
    '斗南鎮',
    '古坑鄉',
    '四湖鄉',
    '口湖鄉',
    '水林鄉',
    '北港鎮',
  ],
  '嘉義市': [
    '東區',
    '西區',
  ],
  '嘉義縣': [
    '太保市',
    '朴子市',
    '布袋鎮',
    '大林鎮',
    '民雄鄉',
    '溪口鄉',
    '六腳鄉',
    '東石鄉',
    '義竹鄉',
    '鹿草鄉',
    '水上鄉',
    '中埔鄉',
    '竹崎鄉',
    '梅山鄉',
    '番路鄉',
    '大埔鄉',
    '新港鄉',
    '阿里山鄉'
  ],
  '屏東縣': [
    '屏東市',
    '潮州鎮',
    '東港鎮',
    '恆春鎮',
    '萬丹鄉',
    '長治鄉',
    '麟洛鄉',
    '九如鄉',
    '里港鄉',
    '鹽埔鄉',
    '高樹鄉',
    '萬巒鄉',
    '內埔鄉',
    '竹田鄉',
    '新埤鄉',
    '枋寮鄉',
    '新園鄉',
    '崁頂鄉',
    '林邊鄉',
    '南州鄉',
    '佳冬鄉',
    '琉球鄉',
    '車城鄉',
    '滿州鄉',
    '枋山鄉 ',
    '霧臺鄉',
    '瑪家鄉',
    '泰武鄉',
    '來義鄉',
    '春日鄉',
    '獅子鄉',
    '牡丹鄉',
    '三地門鄉'
  ],
  '宜蘭縣': [
    '宜蘭市',
    '羅東鎮',
    '蘇澳鎮',
    '頭城鎮',
    '礁溪鄉',
    '員山鄉',
    '壯圍鄉',
    '五結鄉',
    '冬山鄉',
    '三星鄉',
    '大同鄉',
    '南澳鄉'
  ],
  '花蓮縣': [
    '花蓮市',
    '鳳林鎮',
    '玉里鎮',
    '新城鄉',
    '吉安鄉',
    '壽豐鄉',
    '光復鄉',
    '豐濱鄉',
    '瑞穗鄉',
    '富里鄉',
    '秀林鄉',
    '萬榮鄉',
    '卓溪鄉'
  ],
  '台東市': [
    '臺東市',
    '成功鎮',
    '關山鎮',
    '卑南鄉',
    '大武鄉',
    '太麻里鄉',
    '東河鄉',
    '長濱鄉',
    '鹿野鄉',
    '池上鄉',
    '綠島鄉',
    '延平鄉',
    '海端鄉',
    '達仁鄉',
    '金峰鄉',
    '蘭嶼鄉'
  ],
  '澎湖縣': [
    '馬公市',
    '湖西鄉',
    '白沙鄉',
    '西嶼鄉',
    '望安鄉',
    '七美鄉',
  ],
  '金門縣': [
    '金城鎮',
    '金湖鎮',
    '金沙鎮',
    '金寧鄉',
    '烈嶼鄉',
    '烏坵鄉',
  ],
  '連江縣': [
    '南竿鄉',
    '北竿鄉',
    '東引鄉',
    '莒光鄉',
  ],
};
String selectedCity = '台北市';
String selectedDistrict = '北投區';
