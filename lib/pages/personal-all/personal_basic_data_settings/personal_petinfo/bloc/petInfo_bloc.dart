import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_event.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_state.dart';

class PersonalPetInfoBloc extends Bloc<PetInfoEvent, PetinfoState> {
  PersonalPetInfoBloc() : super(PetinfoState()) {
    on<bodychange>(_bodychange);
  }
  void _bodychange(bodychange event, Emitter<PetinfoState> emit) {
    emit(state.copyWith(bodystate1: !state.bodystate1));
  }
}

class PetinfoSelectBloc extends Bloc<PetinfoSelectEvent, PetinfoSelectState> {
  PetinfoSelectBloc() : super(PetinfoSelectState()) {}
}
