class PetinfoState {
  bool bodystate1 = true;

  PetinfoState copyWith({required bool bodystate1}) {
    return PetinfoState()..bodystate1 = bodystate1;
  }
}

class PetinfoSelectState {
  String selectSex = 'male';
  String selectdogcat = 'dog';
}
