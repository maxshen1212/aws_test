import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_bloc.dart';

import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_event.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_state.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'package:pet_save_app/utils.dart';

import 'personal_petinfo_widget/personal_petinfo_widget.dart';

class PersonalPetInfo extends StatelessWidget {
  const PersonalPetInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffaacd06),
      child: SafeArea(
        child: Scaffold(
          appBar: personalPetInfoAppBar("毛孩資料", context),
          drawerEdgeDragWidth: 200, //用拉的也能彈出設定
          endDrawer: SizedBox(
            width: 90.w,
            child: const Drawer(
              child: SetUp(),
            ),
          ),
          body: BlocBuilder<PersonalPetInfoBloc, PetinfoState>(
            builder: (context, state) {
              return state.bodystate1 == true ? body1() : body2();
            },
          ),
        ),
      ),
    );
  }
}
