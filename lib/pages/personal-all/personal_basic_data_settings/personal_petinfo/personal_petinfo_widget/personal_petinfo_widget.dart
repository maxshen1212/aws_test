import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_bloc.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_event.dart';
import 'package:pet_save_app/pages/personal-all/personal_basic_data_settings/personal_petinfo/bloc/petInfo_state.dart';
import 'package:pet_save_app/utils.dart';

PreferredSize personalPetInfoAppBar(String names, BuildContext context) {
  return PreferredSize(
    preferredSize: Size.fromHeight(55.h),
    child: AppBar(
      leading: goBack(),
      leadingWidth: 100.w,
      title: Text(
        names,
        style: TextStyle(
            color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
      ),
      automaticallyImplyLeading: false,
      backgroundColor: Color(0xffaacd06),
      actions: [newSetUp()],
      centerTitle: true,
    ),
  );
}

Widget newSetUp() {
  return Builder(
    builder: (BuildContext context) {
      return Padding(
        padding: EdgeInsets.only(right: 30.w),
        child: IconButton(
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
          icon: Image.asset(
            'assets/appdesign/images/icsetting-1UV.png',
          ),
        ),
      );
    },
  );
}

Widget goBack() {
  return BlocBuilder<PersonalPetInfoBloc, PetinfoState>(
    builder: (context, state) {
      return GestureDetector(
        onTap: () {
          if (state.bodystate1 == false) {
            BlocProvider.of<PersonalPetInfoBloc>(context).add(bodychange());
          } else {
            Navigator.pop(context);
          }
        },
        child: Padding(
          padding: EdgeInsets.only(left: 30.w),
          child: Row(
            children: [
              Image.asset(
                'assets/appdesign/images/goback.png',
                width: 18.w,
                height: 16.h,
              ),
              const Text(
                "返回",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              )
            ],
          ),
        ),
      );
    },
  );
}

//-------------------------------------body1------------------------------------
class body1 extends StatelessWidget {
  const body1({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.h),
      height: 50.h,
      child: Row(
        //crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const button(buttonText: '新增', green: true),
          SizedBox(
            width: 30.w,
          ),
          const button(buttonText: '刪除', green: false)
        ],
      ),
    );
  }
}

class button extends StatelessWidget {
  final String buttonText;
  final bool green;
  const button({super.key, required this.buttonText, required this.green});

  @override
  Widget build(BuildContext context) {
    return Container(
      // btnUQy (I578:1333;578:1304)
      width: 102.w,
      height: double.infinity,
      decoration: green == false
          ? BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black))
          : BoxDecoration(
              color: Color(0xffaacd06),
              borderRadius: BorderRadius.circular(10),
            ),
      child: TextButton(
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.all(Colors.transparent), // 禁用点击效果
        ),
        onPressed: () {
          if (green == true) {
            BlocProvider.of<PersonalPetInfoBloc>(context).add(bodychange());
          }
        },
        child: Text(
          buttonText,
          textAlign: TextAlign.center,
          style: SafeGoogleFont('Inter',
              fontSize: 16.sp,
              fontWeight: FontWeight.w700,
              height: 1.2125.h,
              letterSpacing: 0.8,
              color: green == false ? Colors.black : Colors.white),
        ),
      ),
    );
  }
}
//-------------------------------------body1------------------------------------

//-------------------------------------body2------------------------------------

class body2 extends StatelessWidget {
  body2({super.key});
  final TextEditingController nameCodeController = TextEditingController();
  final TextEditingController birthCodeController = TextEditingController();
  final TextEditingController kindCodeController = TextEditingController();
  final TextEditingController weightCodeController = TextEditingController();
  final TextEditingController colorCodeController = TextEditingController();
  final TextEditingController hairCodeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 25.h),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const reusableText(name: '毛孩名字'),
              formA(codeController: nameCodeController),
              const reusableText(name: '性別'),
              RadioButtonsel(kind: '性別'),
              const reusableText(name: '種類'),
              RadioButtonsel(kind: '種類'),
              const reusableText(name: '生日'),
              formA(
                codeController: birthCodeController,
              ),
              const reusableText(name: '品種'),
              formA(
                codeController: kindCodeController,
              ),
              const reusableText(name: '體重'),
              formA(
                codeController: weightCodeController,
              ),
              const reusableText(name: '毛色'),
              formA(
                codeController: colorCodeController,
              ),
              const reusableText(name: '毛長'),
              formA(
                codeController: hairCodeController,
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(bottom: 20.h),
                  width: 102.w,
                  height: 50.h,
                  decoration: BoxDecoration(
                    color: Color(0xffaacd06),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: BlocBuilder<PetinfoSelectBloc, PetinfoSelectState>(
                    builder: (context, state) {
                      return TextButton(
                        style: ButtonStyle(
                          overlayColor: MaterialStateProperty.all(
                              Colors.transparent), // 禁用点击效果
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            BlocProvider.of<PersonalPetInfoBloc>(context)
                                .add(bodychange());
                            Map data = {
                              "name": nameCodeController.text,
                              "sex": state.selectSex,
                              "dogcat": state.selectdogcat,
                              "birth": birthCodeController.text,
                              "kind": kindCodeController.text,
                              "weight": weightCodeController.text,
                              "color": colorCodeController.text,
                              "hair": hairCodeController.text,
                            };
                            print(data);
                          }
                        },
                        child: Text(
                          '送出',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont('Inter',
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w700,
                              height: 1.2125.h,
                              letterSpacing: 0.8,
                              color: Colors.white),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class formA extends StatelessWidget {
  TextEditingController codeController = TextEditingController();
  formA({super.key, required this.codeController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.h),
      child: TextFormField(
          controller: codeController,
          //autovalidateMode: AutovalidateMode.onUserInteraction,
          inputFormatters: [
            FilteringTextInputFormatter.allow(
                RegExp('[a-z A-Z 0-9]')), //限制只能输入數字和英文
          ],
          keyboardType: TextInputType.multiline,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return '請勿為空';
            }

            return null;
          },
          decoration: const InputDecoration(
            hintText: '文字輸入',
            isDense: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.transparent),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          style: TextStyle(
              fontFamily: "Avenir",
              fontWeight: FontWeight.normal,
              fontSize: 14.sp),
          autocorrect: false,
          obscureText: false),
    );
  }
}

class reusableText extends StatelessWidget {
  final String name;

  const reusableText({super.key, required this.name});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.h, bottom: 10.h),
      child: Text(
        name,
        style: SafeGoogleFont(
          'Inter',
          fontSize: 16.sp,
          fontWeight: FontWeight.w200,
          height: 1.2125.h,
          letterSpacing: 0.8,
          color: const Color(0xff000000),
        ),
      ),
    );
  }
}

class RadioButtonsel extends StatefulWidget {
  String kind;
  RadioButtonsel({required this.kind});
  @override
  State<RadioButtonsel> createState() => _RadioButton();
}

class _RadioButton extends State<RadioButtonsel> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PetinfoSelectBloc, PetinfoSelectState>(
      builder: (context, state) {
        return widget.kind == '性別'
            ? Row(children: [
                Expanded(
                  child: RadioListTile(
                    title: const Text('男生'),
                    value: 'male',
                    groupValue: state.selectSex,
                    onChanged: (value) {
                      setState(() {
                        state.selectSex = value!;
                      });
                    },
                  ),
                ),
                Expanded(
                  child: RadioListTile(
                    title: const Text('女生'),
                    value: 'female',
                    groupValue: state.selectSex,
                    onChanged: (value) {
                      setState(() {
                        state.selectSex = value!;
                      });
                    },
                  ),
                ),
              ])
            : Row(
                children: [
                  Expanded(
                    child: RadioListTile(
                      title: const Text('狗'),
                      value: 'dog',
                      groupValue: state.selectdogcat,
                      onChanged: (value) {
                        setState(() {
                          state.selectdogcat = value!;
                        });
                      },
                    ),
                  ),
                  Expanded(
                    child: RadioListTile(
                      title: const Text('貓'),
                      value: 'cat',
                      groupValue: state.selectdogcat,
                      onChanged: (value) {
                        setState(
                          () {
                            state.selectdogcat = value!;
                          },
                        );
                      },
                    ),
                  ),
                ],
              );
      },
    );
  }
}
//-------------------------------------body2------------------------------------
