import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../setup/setup.dart';
import 'widgets/personal_update_password_widgets.dart';

class PersonalUpdatePassword extends StatefulWidget {
  const PersonalUpdatePassword({super.key});

  @override
  State<PersonalUpdatePassword> createState() => _PersonalUpdatePasswordState();
}

class _PersonalUpdatePasswordState extends State<PersonalUpdatePassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: personalUpdatePasswordAppBar(context, "修改密碼"),
      body: personalUpdatePasswordBody(),
    );
  }
}
