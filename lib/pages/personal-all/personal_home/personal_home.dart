import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/personal-all/personal_home/widget/personal_home_widget.dart';
import 'package:pet_save_app/pages/setup/setup.dart';
import 'dart:ui';
import 'package:pet_save_app/utils.dart';

class Personal extends StatefulWidget {
  const Personal({super.key});

  @override
  State<Personal> createState() => _Personal();
}

class _Personal extends State<Personal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffaacd06),
      child: Scaffold(
        appBar: buildAppbar(context,"彭子羿", "pp203yy12@gmail.com", "會員等級", 4),
        drawerEdgeDragWidth: 200, //用拉的也能彈出設定
        endDrawer: Container(
          width: 75.w,
          child: const Drawer(
            child: SetUp(),
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                buildPersonalInfoSetContainer("基本資料設定", 310),
                buildMemberContainer("會員折扣權益", 310, 120),
                moreServiceContainer("更多服務", 310, 120),
                SizedBox(
                  height: 30.h,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}