import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/values/colors.dart';

PreferredSize buildAppbar(
  BuildContext context,
  String username,
  String useremail,
  String membershiplevel,
  int level,
) {
  return PreferredSize(
    preferredSize: Size.fromHeight(100.h),
    child: AppBar(
      automaticallyImplyLeading: false,
      actions: [setUp(context)],
      flexibleSpace: Container(
        color: Color(0xffaacd06),
        child: Container(
          margin: EdgeInsets.only(top: 30.h, left: 25.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 70.w,
                height: 80.h,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage("assets/images/cat.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 18.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      username,
                      style: TextStyle(
                          fontSize: 18.sp,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Text(
                      useremail,
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: Colors.white,
                          fontWeight: FontWeight.normal),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 5.w, right: 5.w),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(15.w)),
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          membershiplevel + " : Lv${level}",
                          style: TextStyle(
                              fontSize: 12.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

//點選按鈕後轉換頁面程式碼暫時寫在這
Widget reusableLoginIcon(String iconName) {
  return Builder(
    builder: (context) {
      return GestureDetector(
        onTap: () {
          if (iconName == "icsetting-1UV") {
            Scaffold.of(context).openEndDrawer();
          }
          if (iconName == "vector-Enm") {
            Navigator.of(context).pushNamed("personalinfo");
          }
          if (iconName == "vector-Eru") {
            Navigator.of(context).pushNamed("personalpetinfo");
          }
          if (iconName == "vector-bow") {
            Navigator.of(context).pushNamed("personal_email_verify");
          }
          if (iconName == "vector-VLm") {
            Navigator.of(context).pushNamed("personal_update_password");
          }
          if (iconName == "vector-BSH") {
            Navigator.of(context).pushNamed("personal_my_wallet");
          }
          if (iconName == "vector-cMf") {
            Navigator.of(context).pushNamed("personal_bonus_points");
          }
        },
        child: SizedBox(
          width: 35.w,
          height: 28.h,
          child: Image.asset("assets/appdesign/images/$iconName.png"),
        ),
      );
    },
  );
}

Widget buildPersonalInfoSetContainer(
  String containername,
  int widths,
) {
  return Container(
    margin: EdgeInsets.only(top: 15.h),
    width: widths.w, //300
    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h, left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              personalInfoSetIcon("個人資料", "vector-Enm"),
              personalInfoSetIcon("毛孩資料", "vector-Eru"),
              personalInfoSetIcon("E-mail認證", "vector-bow"),
              personalInfoSetIcon("修改密碼", "vector-VLm"),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget buildMemberContainer(
  String containername,
  int width,
  int height,
) {
  return Container(
    margin: EdgeInsets.only(top: 15.h),
    width: width.w, //300
    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp, color: Colors.black),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h, left: 5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              memberIcon("我的錢包", "vector-BSH"),
              memberIcon("紅利積點", "vector-cMf"),
              memberIcon("優惠折扣", "vector-tfj"),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget moreServiceContainer(
  String containername,
  int width,
  int height,
) {
  return Container(
    margin: EdgeInsets.only(top: 15.h),
    width: width.w, //300
    // height: height.h, //190
    decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(216, 191, 197, 197),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(-1, 1),
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.w)),
        border: Border.all(color: Colors.grey)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 15.w, top: 10.w),
          child: Text(
            containername,
            style: TextStyle(fontSize: 16.sp, color: Colors.black),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 8.h, bottom: 7.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              moreServiceIcon("常見問題", "vector-pzu"),
              moreServiceIcon("門市查詢", "vector-iU9"),
              moreServiceIcon("會員條款", "auto-group-akus"),
              moreServiceIcon("隱私權問題", "group-20"),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget personalInfoSetIcon(String name, iconName) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 65.h,
        child: Column(
          children: [
            reusableLoginIcon(iconName),
            SizedBox(
              height: 3.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget memberIcon(String name, iconName) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 70.h,
        child: Column(
          children: [
            reusableLoginIcon(iconName),
            SizedBox(
              height: 5.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              // style: TextStyle(
              //   fontSize: 1.sp
              // ),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget moreServiceIcon(String name, iconName) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 70.h,
        child: Column(
          children: [
            reusableLoginIcon(iconName),
            SizedBox(
              height: 5.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              // style: TextStyle(
              //   fontSize: 1.sp
              // ),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget cbcIcon(String icon_name, String name) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 65.h,
        child: Column(
          children: [
            Container(
              width: 32.w,
              height: 28.h,
              decoration: BoxDecoration(
                border:
                    Border.all(color: const Color.fromARGB(255, 230, 9, 126)),
              ),
              child: Center(
                child: Text(
                  icon_name,
                  style: TextStyle(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.bold,
                      color: const Color.fromARGB(233, 227, 14, 128)),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      )
    ],
  );
}

Widget calorieRecommendations(String icon_name, String name) {
  return Row(
    children: [
      SizedBox(
        width: 72.w,
        height: 65.h,
        child: Column(
          children: [
            Container(
              width: 32.w,
              height: 28.h,
              decoration: BoxDecoration(
                border:
                    Border.all(color: const Color.fromARGB(255, 87, 11, 101)),
              ),
              child: Center(
                child: Text(
                  icon_name,
                  style: TextStyle(
                      fontSize: 11.sp,
                      fontWeight: FontWeight.bold,
                      color: const Color.fromARGB(255, 87, 11, 101)),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Text(
              name,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        ),
      )
    ],
  );
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 30.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}
