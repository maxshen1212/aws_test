import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/pages/personal-all/personal_member_discount/bonus_points/widgets/bonus_points_widgets.dart';

import '../../../setup/setup.dart';

class PersonalBonusPoint extends StatefulWidget {
  const PersonalBonusPoint({super.key});

  @override
  State<PersonalBonusPoint> createState() => _PersonalBonusPointState();
}

class _PersonalBonusPointState extends State<PersonalBonusPoint> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: personalBonusPointApp(context, "紅利積點"),
      body: personalBonusPointBody(),
    );
  }
}