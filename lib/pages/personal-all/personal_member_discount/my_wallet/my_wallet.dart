import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../setup/setup.dart';
import 'widgets/my_wallet_widgets.dart';

class PersonalMyWallet extends StatefulWidget {
  const PersonalMyWallet({super.key});

  @override
  State<PersonalMyWallet> createState() => _PersonalMyWalletState();
}

class _PersonalMyWalletState extends State<PersonalMyWallet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 220, //用拉的也能彈出設定
      endDrawer: Container(
        width: 75.w,
        child: const Drawer(
          child: SetUp(),
        ),
      ),
      appBar: personalMyWallet(context, "我的錢包"),
      body: personalMyWalletBody(),
    );
  }
}