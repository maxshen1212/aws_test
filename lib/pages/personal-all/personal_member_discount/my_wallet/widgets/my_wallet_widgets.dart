import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../utils.dart';

AppBar personalMyWallet(
  BuildContext context,
  String names,
) {
  return AppBar(
    automaticallyImplyLeading: false,
    leadingWidth: 100.w,
    leading: Container(
      padding: EdgeInsets.only(left: 20.w),
      child: goBack(),
    ),
    actions: [setUp(context)],
    flexibleSpace: Container(
      color: Color(0xffaacd06),
    ),
    title: Text(
      names,
      style: TextStyle(
          color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold),
    ),
  );
}

Widget goBack() {
  return Builder(builder: (BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Row(
        children: [
          Image.asset(
            'assets/appdesign/images/goback.png',
            width: 18.w,
            height: 16.h,
          ),
          const Text(
            "返回",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  });
}

Widget setUp(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: 40.w),
    child: Builder(
      builder: (context) => IconButton(
        icon: Image.asset('assets/appdesign/images/icsetting-X7F.png'),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
  );
}

Widget personalMyWalletBody() {
  return Column(
    children: [
      Center(
        child: Container(
          margin: EdgeInsets.only(top: 20.h),
          width: 300.w,
          height: 90.h,
          decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                  color: Color.fromARGB(216, 191, 197, 197),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(-1, 1),
                )
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(15.w)),
              border: Border.all(color: Colors.grey)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "\$0,000",
                style: TextStyle(fontSize: 32.sp, color: Color(0xffff792e)),
              ),
              SizedBox(
                height: 4.h,
              ),
              Text(
                "目前金額",
                style: TextStyle(color: Color(0xff000000)),
              )
            ],
          ),
        ),
      ),
      topTextLine(),
      textLine()
    ],
    
  );
}

Widget topTextLine() {
  return Container(
    margin: EdgeInsets.only(bottom: 3.h,top: 25.h),
    width: 300.w,
    decoration: const BoxDecoration(
      border: Border(bottom: BorderSide(width: 2,color: Color(0xff929292))),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(left: 8.w),
          child: furkidText("時間"),
        ),
        Container(
          child: furkidText("金額"),
        ),
        Container(
          margin: EdgeInsets.only(right: 8.w),
          child: furkidText("訂單"),
        ),
      ],
    ),
  );
}

Widget textLine() {
  return Container(
    margin: EdgeInsets.only(bottom: 3.h,top:3.h),
    width: 300.w,
    decoration: const BoxDecoration(
      border: Border(bottom: BorderSide(color: Color.fromARGB(255, 227, 222, 222))),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(left: 8.w),
          child: furkidText("2023/00/00"),
        ),
        Container(
          child: furkidText("\$0,000"),
        ),
        Container(
          margin: EdgeInsets.only(right: 8.w),
          child: furkidText("訂單號碼"),
        ),
      ],
    ),
  );
}

Widget furkidText(
  text,
) {
  return Container(
    margin: EdgeInsets.only(bottom: 5.h),
    child: Text(
      text,
      textAlign: TextAlign.center,
      style: SafeGoogleFont(
        'Inter',
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        letterSpacing: 0.7.r,
        color: Color(0xff000000),
      ),
    ),
  );
}
