import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pet_save_app/pages/register/widgets/register_widgets.dart';
import 'bloc/register_bloc.dart';
import 'bloc/register_state.dart';
// import 'package:pet_save_app/pages/register/repo/repositories.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';

//主畫面
class Register extends StatelessWidget {
  const Register({super.key});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ProgressHUD(
        child: Builder(
          builder: (context) {
            return SafeArea(
              child: Scaffold(
                appBar: AppBar(
                  bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(1.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 20.h),
                          margin: EdgeInsets.only(left: 15.w).w,
                        ),
                      ],
                    ),
                  ),
                ),
                backgroundColor: Colors.white,
                body: BlocBuilder<RegisterBloc, RegisterState>(
                  builder: (
                    context,
                    state,
                  ) {
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [registerBasicInfoForm(context)],
                      ),
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
