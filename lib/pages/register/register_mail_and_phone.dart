import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/utils.dart';

import 'package:pet_save_app/pages/register/widgets/register_widgets.dart';

class RegisterMailAndPhone extends StatelessWidget {
  const RegisterMailAndPhone({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Builder(builder: (context) {
        return SafeArea(
          child: Scaffold(
            appBar: buildAppBar(context),
            backgroundColor: Colors.white,
            body: Container(
              padding: EdgeInsets.only(left: 25.w, right: 25.w),
              child: Column(
                children: [
                  // title
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 36.h, bottom: 21.h),
                      child: Text(
                        '註冊帳號',
                        textAlign: TextAlign.center,
                        style: SafeGoogleFont(
                          'Inter',
                          fontSize: 26.sp,
                          color: const Color(0xff000000),
                        ),
                      ),
                    ),
                  ),
                  // email和電話表單
                  const MyCustomForm(),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
