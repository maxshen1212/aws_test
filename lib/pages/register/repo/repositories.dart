import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';

String callApiUrl(String url) {
  url = 'http://test.yous.tw:9487/normal/user/' + url;
  return url;
}

Future<dynamic> registerPost(context, reqdata) async {
  dynamic responseData;

  // print(reqdata);
  var data = reqdata;
  try {
    final progress = ProgressHUD.of(context);
    progress?.showWithText('');
    String url = callApiUrl("add");
    Response response = await Dio().post(url,
        data: data,
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        }));

    if (response.statusCode == 201) {
      progress?.dismiss();
      responseData = jsonDecode(response.toString());
    }
  } catch (e) {
    print(e);
  }
  return Future<dynamic>.value(responseData);
}

Future<dynamic> checkEmailExist(context, email) async {
  dynamic responseData;
  var reqdata = {"email": "$email", "source": "app"};
  print(reqdata);
  try {
    String url = callApiUrl("email_exist");
    Response response = await Dio().post(url,
        data: reqdata,
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        }));

    if (response.statusCode == 201) {
      responseData = jsonDecode(response.toString());
      print("email response data: ${responseData["result"]["exist"]}");
    }
  } catch (e) {
    print(e);
  }
  // try {
  //   await Future.delayed(const Duration(seconds: 3), () {
  //     responseData = true;
  //   });
  // } catch (e) {
  //   print(e);
  // }
  return Future<dynamic>.value(responseData["result"]["exist"]);
}

Future<dynamic> checkMobileExist(context, phone) async {
  dynamic responseData;
  var reqdata = {"country_code": "886", "mobile": "$phone", "source": "app"};
  print(reqdata);
  try {
    String url = callApiUrl("mobile_exist");
    Response response = await Dio().post(url,
        data: reqdata,
        options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'YOIS-ChannelSecret': 'yois53918206furrypaws26083748yois'
        }));

    if (response.statusCode == 201) {
      responseData = jsonDecode(response.toString());
      print("mobile response data: ${responseData["result"]["exist"]}");
    }
  } catch (e) {
    print(e);
  }
  // try {
  //   await Future.delayed(const Duration(seconds: 3), () {
  //     responseData = true;
  //   });
  // } catch (e) {
  //   print(e);
  // }
  return Future<dynamic>.value(responseData["result"]["exist"]);
}
