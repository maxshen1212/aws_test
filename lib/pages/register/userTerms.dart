import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:pet_save_app/pages/register/widgets/register_widgets.dart';

class UserTerms extends StatelessWidget {
  const UserTerms({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Builder(builder: (context) {
        return SafeArea(
          child: Scaffold(
            appBar: buildAppBar(context),
            backgroundColor: Colors.white,
            body: Container(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 15.h),
              child: SingleChildScrollView(
                child: Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text:
                            '歡迎您使用OOOAPP（以下簡稱本服務），為了讓您能夠安心的使用本服務的各項服務與資訊，特此向您說明本服務的隱私權保護政策，請確認您同意所有規範後，再開始使用本服務。\n\n',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.sp,
                          fontFamily: 'Inter',
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.70,
                        ),
                      ),
                      TextSpan(
                        text:
                            '請您詳閱下列內容：\n一、隱私權保護政策的適用範圍\n隱私權保護政策內容，包括本服務如何處理在您使用APP服務時收集到的個人識別資料。隱私權保護政策不適用於本服務以外的相關連結，也不適用於非本服務所委託或參與管理的人員。\n\n二、個人資料的蒐集、處理及利用方式\n• 當您造訪本服務或使用本服務所提供之功能時，我們將視該服務功能性質，請您提供必要的個人資料，並在該特定目的範圍內處理及利用您的個人資料；非經您書面同意，本服務不會將個人資料用於其他用途。\n• 本服務在您使用服務信箱、問卷調查等互動性功能時，會保留您所提供的姓名、電子郵件地址、聯絡方式及使用時間等。\n• 於一般瀏覽時，伺服器會自行記錄相關行徑，包括您使用連線設備的 IP 位址、使用時間、使用的瀏覽器、瀏覽及點選資料記錄等，做為我們增進服務的參考依據，此記錄為內部應用，決不對外公佈。\n• 為提供精確的服務，我們會將收集的問卷調查內容進行統計與分析，分析結果之統計數據或說明文字呈現，除供內部研究外，我們會視需要公佈統計數據及說明文字，但不涉及特定個人之資料。\n• 您可以隨時向我們提出請求，以更正或刪除本服務所蒐集您錯誤或不完整的個人資料。\n\n三、資料之保護\n• 本服務主機均設有防火牆、防毒系統等相關的各項資訊安全設備及必要的安全防護措施，加以保護APP及您的個人資料採用嚴格的保護措施，只由經過授權的人員才能接觸您的個人資料，相關處理人員皆簽有保密合約，如有違反保密義務者，將會受到相關的法律處分。\n• 如因業務需要有必要委託其他單位提供服務時，本服務亦會嚴格要求其遵守保密義務，並且採取必要檢查程序以確定其將確實遵守。\n\n四、APP對外的相關連結\n本服務提供其他網站的連結，您也可經由本服務所提供的連結，點選進入其他網站。但該連結網站不適用本服務的隱私權保護政策，您必須參考該連結中的隱私權保護政策。\n\n五、與第三人共用個人資料之政策\n本服務絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。\n前項但書之情形包括不限於：\n• 經由您書面同意。\n• 法律明文規定。\n• 為免除您生命、身體、自由或財產上之危險。\n• 與公務機關或學術研究機構合作，基於公共利益為統計或學術研究而有必要，且資料經過提供者處理或蒐集者依其揭露方式無從識別特定之當事人。\n• 當您在APP的行為，違反服務條款或可能損害或妨礙與其他使用者權益或導致任何人遭受損害時，經APP管理單位研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。\n• 有利於您的權益。\n• 本服務委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。\n\n六、隱私權保護政策之修正\n本服務隱私權保護政策將因應需求隨時進行修正，修正後的條款將刊登於APP上。',
                        style: TextStyle(
                          color: Color(0xFF595757),
                          fontSize: 14.sp,
                          fontFamily: 'Inter',
                          fontWeight: FontWeight.w300,
                          letterSpacing: 0.70,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
