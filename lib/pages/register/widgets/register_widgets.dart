import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pet_save_app/utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pinput/pinput.dart';
import 'package:pet_save_app/pages/register/repo/repositories.dart';

import 'dart:async';

final _formKey = GlobalKey<FormState>();

final TextEditingController emailText = TextEditingController();
final TextEditingController nameText = TextEditingController();
final TextEditingController mobileText = TextEditingController();
final TextEditingController verifyText = TextEditingController();
final TextEditingController birthText = TextEditingController();
final TextEditingController passwordText = TextEditingController();
final TextEditingController passwordCheckText = TextEditingController();
// final TextEditingController pinCode = TextEditingController();

int gender = 1;
String dropdownValue = "+886";
bool confirmRule = false;

// 性別選擇鈕
class GenderRadioButton extends StatefulWidget {
  const GenderRadioButton({super.key});

  @override
  State<GenderRadioButton> createState() => _GenderRadioButtonState();
}

class _GenderRadioButtonState extends State<GenderRadioButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 80.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Radio<int>(
            value: 1,
            groupValue: gender,
            onChanged: (value) {
              setState(() {
                gender = value!;
              });
            },
          ),
          const Text("先生"),
          SizedBox(
            width: 25.w,
          ),
          Radio<int>(
            value: 0,
            groupValue: gender,
            onChanged: (value) {
              setState(() {
                gender = value!;
              });
            },
          ),
          const Text("小姐"),
        ],
      ),
    );
  }
}

// 條款確認按鈕
class CheckboxExample extends StatefulWidget {
  const CheckboxExample({super.key});

  @override
  State<CheckboxExample> createState() => _CheckboxExampleState();
}

class _CheckboxExampleState extends State<CheckboxExample> {
  @override
  Widget build(BuildContext context) {
    return Checkbox(
      checkColor: Colors.white,
      value: confirmRule,
      onChanged: (bool? value) {
        setState(() {
          confirmRule = value!;
          // print(confirmRule);
        });
      },
    );
  }
}

// 註冊驗證表單
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  final _emailFormFieldKey = GlobalKey<FormFieldState>();
  final _mobileFormFieldKey = GlobalKey<FormFieldState>();
  bool mobileExist = true;
  bool emailExist = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: Colors.white),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // email
            Container(
              width: 230.w,
              margin: EdgeInsets.only(
                bottom: 15.h,
              ),
              child: SizedBox(
                width: 230.w,
                child: TextFormField(
                  key: _emailFormFieldKey,
                  controller: emailText,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                        RegExp('[a-z A-Z 0-9 @.]')), //限制只能输入數字和英文
                  ],
                  autofocus: true,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value!.isEmpty || !value.contains("@")) {
                      return '電子郵件格式錯誤';
                    } else if (emailExist) {
                      return "電子郵件已註冊";
                    }
                    return null; // 驗證通過
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: '電子郵件',
                    hintText: "Email(帳號/訂單資訊/優惠通知)",
                    isDense: true,
                    hintStyle: TextStyle(
                      color: Color.fromARGB(255, 44, 44, 44),
                    ),
                  ),
                  style: SafeGoogleFont(
                    'Inter',
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w200,
                    color: const Color(0xff595757),
                  ),
                  autocorrect: false,
                  obscureText: false,
                ),
              ),
            ),
            // phone
            Container(
              margin: EdgeInsets.only(
                bottom: 15.h,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(right: 12.w),
                      child: const CountryCode()),
                  SizedBox(
                    width: 150.w,
                    child: TextFormField(
                      key: _mobileFormFieldKey,
                      controller: mobileText,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                            RegExp('[0-9]')), //限制只能输入數字
                      ],
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return '手機號碼格式錯誤';
                        } else if (mobileExist) {
                          return "手機號碼已註冊";
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: '手機',
                        hintText: "請輸入手機號碼",
                        isDense: true,
                        hintStyle: TextStyle(
                          color: Color.fromARGB(255, 44, 44, 44),
                        ),
                      ),
                      style: SafeGoogleFont(
                        'Inter',
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w200,
                        color: const Color(0xff595757),
                      ),
                      autocorrect: false,
                      obscureText: false,
                    ),
                  ),
                ],
              ),
            ),
            // submit btn
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 5.h),
                width: 110.w,
                height: 33.h,
                clipBehavior: Clip.antiAlias,
                decoration: ShapeDecoration(
                  color: const Color(0xFFE4007F),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  shadows: const [
                    BoxShadow(
                      color: Color(0x26000000),
                      blurRadius: 3,
                      offset: Offset(0, 1),
                      spreadRadius: 1,
                    ),
                    BoxShadow(
                      color: Color(0x4C000000),
                      blurRadius: 2,
                      offset: Offset(0, 1),
                      spreadRadius: 0,
                    )
                  ],
                ),
                child: TextButton(
                  onPressed: () async {
                    try {
                      // 通過進行 API 調用以驗證是否註冊過
                      emailExist =
                          await checkEmailExist(context, emailText.text);
                      print("emailExist:$emailExist");
                      mobileExist =
                          await checkMobileExist(context, mobileText.text);
                      print("mobileExist:$mobileExist");
                    } catch (error) {
                      print(error);
                    }
                    if (_formKey.currentState!.validate()) {
                      showAlertDialogVerificationCode(context);
                    }
                  },
                  child: const Text(
                    '傳送驗證碼',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w600,
                      height: 1.43,
                      letterSpacing: 0.10,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// 手機國碼
class CountryCode extends StatefulWidget {
  const CountryCode({super.key});
  @override
  State<CountryCode> createState() => _CountryCodeState();
}

class _CountryCodeState extends State<CountryCode> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 70.w,
      child: Container(
        padding: EdgeInsets.only(left: 5.w),
        decoration: BoxDecoration(
          border: Border.all(color: const Color(0xff000000)),
          borderRadius: BorderRadius.circular(5),
        ),
        child: DropdownButton(
          borderRadius: BorderRadius.circular(8.r),
          value: dropdownValue,
          hint: const Text(
            '國碼',
          ),
          iconSize: 20,
          isDense: true,
          onChanged: (newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
          items: <String>['+886', '+852', '+60']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              alignment: Alignment.centerLeft,
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}

// 註冊基本資料表單用
AppBar buildAppBar(BuildContext context) {
  return AppBar(
    bottom: PreferredSize(
      preferredSize: const Size.fromHeight(1.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.only(bottom: 20.h),
              margin: EdgeInsets.only(left: 15.w).w,
              child: goBack(context)),
        ],
      ),
    ),
  );
}

Widget goBack(BuildContext context) {
  return GestureDetector(
    onTap: () {
      Navigator.pop(context);
    },
    child: const Row(
      children: [
        Icon(Icons.arrow_back_ios),
        Text(
          "返回",
          style: TextStyle(
              color: Colors.black, fontSize: 16, fontWeight: FontWeight.normal),
        ),
      ],
    ),
  );
}

Widget formText(String text) {
  return Container(
    margin: EdgeInsets.only(
      left: 55.w,
      bottom: 5.h,
    ),
    width: double.infinity,
    child: Text(
      "*$text",
      style: SafeGoogleFont(
        'Inter',
        fontSize: 16.sp,
        fontWeight: FontWeight.w200,
        letterSpacing: 0.9.w,
        color: const Color(0xff000000),
      ),
    ),
  );
}

Widget buildTextField(String hintText, TextEditingController ctrler) {
  return Container(
    width: 230.w,
    margin: EdgeInsets.only(
      bottom: 15.h,
      left: 55.w,
    ),
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Row(
      children: [
        SizedBox(
          width: 230.w,
          child: TextField(
            controller: ctrler,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
              hintText: hintText,
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromARGB(0, 249, 208, 208),
                ),
              ),
              disabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                ),
              ),
              hintStyle: const TextStyle(
                color: Colors.black,
              ),
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: const Color(0xff595757),
            ),
            autocorrect: false,
            obscureText: false,
          ),
        )
      ],
    ),
  );
}

Widget passwordTextField(String hintText, TextEditingController ctrler) {
  return Container(
    width: 230.w,
    margin: EdgeInsets.only(
      bottom: 15.h,
      left: 55.w,
    ),
    decoration: BoxDecoration(
      color: const Color(0xffececec),
      borderRadius: BorderRadius.all(Radius.circular(10.w)),
    ),
    child: Row(
      children: [
        SizedBox(
          width: 230.w,
          child: TextField(
            controller: ctrler,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
              hintText: hintText,
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromARGB(0, 249, 208, 208),
                ),
              ),
              disabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                ),
              ),
              hintStyle: const TextStyle(
                color: Colors.black,
              ),
            ),
            style: SafeGoogleFont(
              'Inter',
              fontSize: 14.sp,
              fontWeight: FontWeight.w200,
              color: const Color(0xff595757),
            ),
            autocorrect: false,
            autofocus: false,
          ),
        )
      ],
    ),
  );
}

Widget agreeTerms(BuildContext context) {
  return // 條款
      Container(
    margin: EdgeInsets.only(bottom: 10.h),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const CheckboxExample(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "同意遵守",
            style: TextStyle(fontSize: 16.sp),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, 'user_terms');
            },
            child: Text(
              "使用者條款",
              style: TextStyle(
                fontSize: 16.sp,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget registerButton(BuildContext context) {
  return TextButton(
    onPressed: () async {
      // print(passwordText.text);
      // print(passwordCheckText.text);
      if (_formKey.currentState!.validate()) {
        if (passwordText.text == passwordCheckText.text) {
          if (confirmRule) {
            Map data = {
              "email": emailText.text,
              "password": passwordText.text,
              "name": nameText.text,
              "gender": gender.toString(),
              "country_code": dropdownValue.substring(1),
              "mobile": mobileText.text,
              "birthday": birthText.text,
              "source": "app",
            };
            print(data);
            print("註冊會員成功");
            dynamic registerPostData = await registerPost(context, data);
            print(registerPostData);
            showAlertDialogRegister(context);
            showTimer(context);
          } else {
            toast("請確認條款");
          }
        } else {
          toast("密碼不一致，請重新輸入");
        }
      }
    },
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Container(
      width: 110.w,
      height: 35.h,
      decoration: BoxDecoration(
        color: const Color(0xFF929292),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: Text(
          "送出",
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 16.sp,
            fontWeight: FontWeight.w700,
            letterSpacing: 8,
            color: const Color(0xffffffff),
          ),
        ),
      ),
    ),
  );
}

//總表單
Widget registerBasicInfoForm(BuildContext context) {
  return Form(
    key: _formKey,
    child: Container(
      padding: EdgeInsets.only(left: 25.w, right: 25.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // 測試輸入匡觸發驗證
          // MyWidget(),
          // title
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 21.h),
              child: Text(
                '會員註冊基本資料',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Inter',
                  fontSize: 26.sp,
                  color: const Color(0xff000000),
                ),
              ),
            ),
          ),
          formText("姓名"),
          buildTextField("請輸入您的姓名", nameText),
          formText("性別"),
          const GenderRadioButton(),
          formText("生日"),
          buildTextField("1900/00/00", birthText),
          formText("密碼"),
          passwordTextField("請填寫6-12個字元", passwordText),
          formText("再次確認密碼"),
          passwordTextField("再次確認密碼", passwordCheckText),
          // 條款
          agreeTerms(context),
          // 送出
          Center(
            child: registerButton(context),
          ),
        ],
      ),
    ),
  );
}

// 驗證碼提示匡 Dialog
Future<void> showAlertDialogVerificationCode(BuildContext context) {
  return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          title: Center(
            child: Text(
              '輸入驗證碼',
              style: TextStyle(fontSize: 24.sp),
            ),
          ),
          content: SizedBox(
            height: 170.h,
            child: Column(children: [
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Text('發送至'),
                  SizedBox(
                    width: 10.w,
                  ),
                  Text(dropdownValue + ' ' + mobileText.text,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: const Color(0xffe4007f), fontSize: 12.sp))
                ]),
              ),
              Container(
                margin: EdgeInsets.only(left: 160.w),
                child: verifyButton(),
              ),

              // margin: EdgeInsets.only(left: 110.w),
              buildPinPut(),
              SizedBox(
                height: 20.h,
              ),
              verifySendButton(context),
            ]),
          ),
        );
      });
}

Widget verifyButton() {
  return TextButton(
    onPressed: () {
      print("重發驗證碼");
    },
    style: TextButton.styleFrom(
      padding: EdgeInsets.zero,
    ),
    child: Image.asset("assets/appdesign/images/a0996-rabxy.png"),
  );
}

Widget buildPinPut() {
  return Container(
    width: 290,
    height: 50.h,
    decoration: ShapeDecoration(
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      shadows: const [
        BoxShadow(
          color: Color(0x3F000000),
          blurRadius: 10,
          offset: Offset(0, 0),
          spreadRadius: 0,
        )
      ],
    ),
    child: Container(
      margin: EdgeInsets.only(bottom: 9.h),
      child: Pinput(
        autofocus: true,
        defaultPinTheme: defaultPinTheme,
        focusedPinTheme: focusedPinTheme,
        showCursor: true,
        // controller: pinCode,
        onCompleted: (pin) => print(pin),
      ),
    ),
  );
}

final defaultPinTheme = PinTheme(
  width: 40.w,
  height: 58.h,
  textStyle: TextStyle(
    color: Colors.black,
    fontSize: 32.sp,
    fontFamily: 'Inter',
    fontWeight: FontWeight.w500,
    letterSpacing: 1.60,
  ),
  decoration: const BoxDecoration(
    border: Border(bottom: BorderSide(width: 3, color: Color(0xff929292))),
  ),
);

final focusedPinTheme = defaultPinTheme.copyDecorationWith(
  border: const Border(bottom: BorderSide(width: 3, color: Colors.blue)),
);

Widget verifySendButton(BuildContext context) {
  return SizedBox(
    width: 80.w,
    height: 28.h,
    child: ElevatedButton(
      onPressed: () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("register", (route) => false);
      },
      style: ElevatedButton.styleFrom(
        backgroundColor: const Color(0xff929292),
        // foregroundColor: const Color(0xffe4007f),
      ),
      child: Text(
        '送出',
        style: TextStyle(
          color: Colors.white,
          fontSize: 14.sp,
          fontFamily: 'Inter',
          fontWeight: FontWeight.w600,
          letterSpacing: 0.80,
        ),
      ),
    ),
  );
}

// 註冊成功圖示
Future<void> showAlertDialogRegister(BuildContext context) {
  return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: SizedBox(
            height: 200.h,
            child: Column(children: [
              Container(
                margin: EdgeInsets.only(top: 30.h),
                width: 110.w,
                child: Stack(
                  children: [
                    Image.asset('assets/appdesign/images/忘記密碼圓.png'),
                    Center(
                      child: Container(
                        width: 58.w,
                        margin: EdgeInsets.only(top: 35.h),
                        child: Image.asset('assets/appdesign/images/忘記密碼勾.png'),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 18.h),
                child: Text('註冊會員成功',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Inter',
                        fontSize: 18.sp,
                        letterSpacing: 0,
                        fontWeight: FontWeight.normal,
                        height: 1)),
              ),

              // margin: EdgeInsets.only(left: 110.w),
            ]),
          ),
        );
      });
}

showTimer(BuildContext context) {
  Timer.periodic(const Duration(milliseconds: 1000), (timer) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil("petsafe_login", (route) => false);
    timer.cancel(); //取消定時器
  });
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
