import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_save_app/database/sqlite.dart';
import 'package:pet_save_app/utils.dart';
import 'package:sqflite/sqflite.dart';

import '../online_customer_service/online_customer_service.dart';

class SetUp extends StatelessWidget {
  const SetUp({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60.h,
          ),
          setUpbutton(context, "首頁", "home", "home"),
          SizedBox(
            height: 5.h,
          ),
          setUpbutton(context, "通知", "bell", ""),
          SizedBox(
            height: 5.h,
          ),
          setUpbutton(context, "個人檔案", "user", "personal"),
          SizedBox(
            height: 5.h,
          ),
          setUpbutton(context, "線上客服", "help", "online_customer_service"),
          SizedBox(
            height: 5.h,
          ),
          setUpbutton(context, "登出", "signout", "petsafe_login"),
        ],
      ),
    );
  }
}

Future<void> _dialogBuilder(BuildContext context, arrive) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('寵安快譯通'),
        content: const Text(
          '是否確定要登出?',
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: Container(
              width: 70.w,
              height: 30.h,
              // margin: EdgeInsets.only(left: 14.w),
              decoration: BoxDecoration(
                color: Color(0xff929292),
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Center(
                child: Text(
                  "取消",
                  textAlign: TextAlign.center,
                  style: SafeGoogleFont('Inter',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.8.r,
                      color: Color(0xffffffff),
                      decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
          TextButton(
              onPressed: () async {
                Database? db;
                    final sqlite = Sqlite();
                    db = await sqlite.connectDB();
                    await db
                        .rawDelete('DELETE FROM member WHERE base_id = ?', [0]);
                    await db.rawDelete('DELETE FROM pet WHERE mem_id = ?', [0]);
                    toast("登出成功");
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("$arrive", (route) => false);
              },
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: Container(
                width: 70.w,
                height: 30.h,
                // margin: EdgeInsets.only(left: 14.w),
                decoration: BoxDecoration(
                  color: Color(0xffe4007f),
                  borderRadius: BorderRadius.circular(6.r),
                ),
                child: Center(
                  child: Text(
                    "確認",
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont('Inter',
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.8.r,
                        color: Color(0xffffffff),
                        decoration: TextDecoration.none),
                  ),
                ),
              )),
        ],
      );
    },
  );
}

//按鈕文字 按鈕圖片 按鈕到達位置
Widget setUpbutton(
    BuildContext context, String setuptext, String icons, String arrive) {
  return TextButton(
    onPressed: () async {
      print(arrive);
      if (arrive == "") {
                  Navigator.pop(context);
                } else {
                  if (arrive == "online_customer_service") {
                    Navigator.of(context).pushNamed("$arrive");
                    scrollMsgBottom();
                  } else if (arrive == "petsafe_login") {
                    // 登出並刪除資料庫使用者資料
                    _dialogBuilder(context,arrive);
                    
                  } else {
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("$arrive", (route) => false);
                  } // 關閉右側抽屜
                }
     
    },
    child: Column(
      children: [
        SizedBox(
          width: 25.w,
          height: 25.w,
          child: Image.asset('assets/appdesign/images/$icons.png'),
        ),
        Text(
          setuptext,
          textAlign: TextAlign.center,
          style: SafeGoogleFont(
            'Inter',
            fontSize: 12.sp,
            fontWeight: FontWeight.w200,
            height: 1.2125.h,
            letterSpacing: 0.5.r,
            color: Color(0xff565555),
          ),
        ),
      ],
    ),
  );
}

void toast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 18.0);
}
